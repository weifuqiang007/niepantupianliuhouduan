-- 20231009 添加字段
CREATE TABLE `pull_data` (
  `image_id` varchar(255) NOT NULL COMMENT '主键',
  `sn_code` varchar(255) DEFAULT NULL COMMENT 'snCode',
  `dws_code` varchar(255) DEFAULT NULL COMMENT 'dws_code',
  `forbid_id` varchar(255) DEFAULT NULL COMMENT '安检机图片ID',
  `is_forbid` varchar(10) DEFAULT NULL COMMENT '违禁品标记,只有发现违禁品才会上报，默认为true',
  `forbid_code` int(11) DEFAULT NULL COMMENT '违禁品类型编号',
  `forbid_type` varchar(255) DEFAULT NULL COMMENT '违禁品类型名称',
  `scan_time` datetime DEFAULT NULL COMMENT '扫描时间',
  `extended_field` varchar(1000) DEFAULT NULL COMMENT '扩展字段，',
  `pull_state` int(11) DEFAULT NULL COMMENT '推送总部状态，0：未推送，1：已推送，-1：推送失败',
  `pull_time` timestamp NULL DEFAULT NULL COMMENT '推送时间',
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='推送到zto数据保存本地';

-- 20231027 添加版本号表
CREATE TABLE `sys_version` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `version` varchar(100) DEFAULT NULL COMMENT '版本号',
  `version_time` varchar(64) DEFAULT NULL COMMENT '发版日期',
  `update_content` varchar(1000) DEFAULT NULL COMMENT '更新内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO niepan.sys_version
(id, version, version_time, update_content)
VALUES('1', 'V1.0.20231107', '2023-11-07', '1、tcp升级稳定版本');

-- 20231108  添加违禁品阈值
CREATE TABLE `sys_danger_value` (
            `id` varchar(64) NOT NULL,
            `danger_code` int DEFAULT NULL COMMENT '违禁品编号',
            `danger_name` varchar(255) DEFAULT NULL COMMENT '违禁品名称',
            `danger_value` int DEFAULT NULL COMMENT '阈值',
            `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
            `create_name` varchar(10) DEFAULT NULL COMMENT '创建人',
            `update_time` timestamp NULL DEFAULT NULL COMMENT '最后一次编辑时间',
            `update_name` varchar(10) DEFAULT NULL COMMENT '最后一次编辑人',
            `del_flag` char(1) DEFAULT NULL COMMENT '是否删除,1是;0否',
            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='违禁品阈值表';
-- 添加违禁品阈值启动配置
INSERT INTO sys_config
(id, param_key, param_value, status, remark)
VALUES(3, 'danger_value', '0', 1, '违禁品阈值启动配置(1:启动，0：禁用)');
