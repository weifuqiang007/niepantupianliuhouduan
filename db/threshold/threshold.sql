/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : niepan

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 09/03/2023 11:45:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for threshold
-- ----------------------------
DROP TABLE IF EXISTS `threshold`;
CREATE TABLE `threshold`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `em_obj_type` int(11) NULL DEFAULT NULL COMMENT '物品的类型',
  `item_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物品的名称',
  `threshole` int(11) NULL DEFAULT NULL COMMENT '阈值',
  `person_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作用户',
  `create_time` datetime(6) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of threshold
-- ----------------------------
INSERT INTO `threshold` VALUES (1, 7, '充电宝', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (2, 6, '笔记本', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (3, 4, '雨伞', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (4, 3, '枪支', 30, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (5, 13, '烟花爆竹', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (6, 12, '喷雾喷灌', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (7, 1, '刀具', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (8, 23, '斧头', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (9, 33, '剪刀', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (10, 30, '打火机油', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (11, 31, '指甲油', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (12, 2, '瓶装液体', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (13, 28, '玻璃杯', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (14, 27, '保温杯', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (15, 29, '塑料瓶', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (16, 17, '手铐', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (17, 15, '警棍', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (18, 16, '指虎', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (19, 14, '打火机', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (20, 32, '工具', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (21, 34, '电子产品', 80, NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold` VALUES (22, 5, '手机', 80, NULL, '2023-03-09 10:15:25.000000');

SET FOREIGN_KEY_CHECKS = 1;
