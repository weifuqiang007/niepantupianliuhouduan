package com.niepan.common.websocket.server;

import javax.websocket.Session;

/**
 * 已连接的客户端
 *
 * @author:liuchenyu
 * @data:2023/03/07
 */
public class WebSocketEntity {
    private Session session;

    public WebSocketEntity(Session session) {
        this.session = session;
    }

    public void sendText(String message) {
        session.getAsyncRemote().sendText(message);
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
