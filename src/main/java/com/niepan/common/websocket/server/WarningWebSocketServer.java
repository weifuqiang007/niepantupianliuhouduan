package com.niepan.common.websocket.server;

import com.niepan.common.utils.logs.LogUtils;
import org.springframework.stereotype.Component;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 * WebSocket 服务端
 *
 * @author:liuchenyu
 * @data:2023/03/07
 */
@ServerEndpoint("/MessageCenter")
@Component
public class WarningWebSocketServer extends BaseWebsocketServer {

    /**
     * 连接建立后触发的方法
     *
     * @param session
     */
    @OnOpen
    public void onOpen(Session session) {
        LogUtils.info("已连接 WebSocket");
        CONNECTIONS.add(new WebSocketEntity(session));
    }

    /**
     * 接收到客户端消息时触发的方法
     */
    @OnMessage
    public void onMessage(String message) {
//        sendMessage("asdasd");
        LogUtils.info("WebSocket.onMessage： " + message);
    }
}
