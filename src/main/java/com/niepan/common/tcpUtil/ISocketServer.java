package com.niepan.common.tcpUtil;

/**
 * 启用socket服务
 * @author: kongyz
 * @date: 2023/10/25 on 9:42
 */
public interface ISocketServer {

    /**
     * @description: 启动服务
     */
    public void startServer();

    /**
     * @description: 停止服务
     */
    public void stopServer();

    /**
     * @description: 判断是否启动
     */
    public boolean isStart();
}
