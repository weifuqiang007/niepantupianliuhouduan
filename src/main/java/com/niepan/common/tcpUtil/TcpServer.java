package com.niepan.common.tcpUtil;

/**
 * @author: liuchenyu
 * @date: 2023/6/25
 */
import cn.hutool.core.util.HexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpServer {
    private final static Logger log= LoggerFactory.getLogger(TcpServer.class);

    private static String host = "192.168.2.100";
//    private static String host = "192.168.0.49";

    private static Integer port = 3000;

    private static volatile TcpServer instance = null;

    private static PrintWriter output = null;

    private static OutputStream outputStream = null;

    public static byte[] left = new byte[]{(byte) 0x88,0x11,0x00,0x00,0x16};

    public static byte[] right = new byte[]{(byte) 0x88,0x21,0x00,0x00,0x16};

    public static byte[] back = new byte[]{(byte) 0x88,0x31,0x00,0x00,0x16};

    public static byte[] start = new byte[]{(byte) 0x88,0x51,0x00,0x00,0x16};

    public static byte[] end = new byte[]{(byte) 0x88,0x00,0x00,0x00,0x16};

    public static byte[] reset = new byte[]{(byte) 0x88,0x41,0x00,0x00,0x16};

    public static void close(){
        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static TcpServer getInstance() {
        if (instance == null) {
            synchronized (TcpServer.class) {
                if (instance == null) {
                    instance = new TcpServer();
                }
            }
        }
        return instance;
    }

    public TcpServer() {
        init();
    }

    /**
     * 初始化
     */
    private static void init() {
        try {
            // 创建 ServerSocket 对象并监听指定端口
            ServerSocket serverSocket = new ServerSocket(port,5, InetAddress.getByName(host));
            log.info("服务器已启动，正在监听端口{},{} " ,host, port);

            // 接受客户端连接并处理请求
            Socket clientSocket = serverSocket.accept();
            log.info("客户端连接成功: " + clientSocket.getInetAddress().getHostAddress());


            outputStream = clientSocket.getOutputStream();

            // 创建 PrintWriter 用于向客户端发送数据
//            output = new PrintWriter(clientSocket.getOutputStream(), true);

            //连接成功立马给摆轮发送一个启动指令
//            output.println(TcpServer.start);
            outputStream.write(TcpServer.start);
            outputStream.write(TcpServer.reset);
        } catch (IOException e) {
            log.error("tcp服务初始化链接失败"+host+":"+port,e);
        }
    }

    /**
     * 自动回复消息
     *
     * @param code
     */
    public static void returnMsg(String code) {
        output.println(code);
    }

    public static void returnMsg(byte[]  code) {
        try {
            outputStream.write(code);
        } catch (Exception e) {
            instance = null;
            output = null;
            outputStream = null;
            log.error("tcp发送消息失败",e);
        }
    }


    public static void main(String[] args) throws InterruptedException {
//        String messageToSend = "Hello, 下位机!"; // 指定要发送的信息
//        try {
//            // 创建 ServerSocket 对象并监听指定端口
//            ServerSocket serverSocket = new ServerSocket(port,5, InetAddress.getByName(host));
//            System.out.println("服务器已启动，正在监听端口 " + port);
//
//            // 接受客户端连接并处理请求
//            Socket clientSocket = serverSocket.accept();
//            System.out.println("客户端连接成功: " + clientSocket.getInetAddress().getHostAddress());
//
//            // 创建 PrintWriter 用于向客户端发送数据
//            PrintWriter output = new PrintWriter(clientSocket.getOutputStream(), true);
//
//            // 向客户端发送指定的信息
//            output.println(messageToSend);
//            System.out.println("已向客户端发送消息: " + messageToSend);
//
//            // 关闭连接
//            clientSocket.close();
//            serverSocket.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


//        boolean isStr1Hex = isHexadecimal(right);
//        System.out.println("isStr1Hex = " + isStr1Hex);

//        boolean hexNumber = HexUtil.isHexNumber(TcpServer.start);
//        System.out.println("hexNumber = " + hexNumber);
        TcpServer.getInstance();
        while (true){
            Thread.sleep(5000);
            TcpServer.returnMsg(TcpServer.right);
        }
    }

    public static boolean isHexadecimal(String input) {
        String hexPattern = "^[0-9A-Fa-f]+$";
        return input.matches(hexPattern);
    }
}