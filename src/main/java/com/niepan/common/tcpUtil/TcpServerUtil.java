package com.niepan.common.tcpUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: kongyz
 * @date: 2023/10/26 on 9:57
 */
public class TcpServerUtil {

    private final static Logger log= LoggerFactory.getLogger(TcpServer.class);

    private static String host = "192.168.2.100";
//    private static String host = "192.168.0.49";

    private static Integer port = 3000;

    private static volatile TcpServerUtil instance = null;

    public static byte[] left = new byte[]{(byte) 0x88,0x11,0x00,0x00,0x16};

    public static byte[] right = new byte[]{(byte) 0x88,0x21,0x00,0x00,0x16};

    public static byte[] back = new byte[]{(byte) 0x88,0x31,0x00,0x00,0x16};

    public static byte[] start = new byte[]{(byte) 0x88,0x51,0x00,0x00,0x16};

    public static byte[] end = new byte[]{(byte) 0x88,0x00,0x00,0x00,0x16};

    public static byte[] reset = new byte[]{(byte) 0x88,0x41,0x00,0x00,0x16};

    public static SocketServer server=null;

    public static TcpServerUtil getInstance() {
        if (instance == null) {
            synchronized (TcpServerUtil.class) {
                if (instance == null) {
                    instance = new TcpServerUtil();
                }
            }
        }
        return instance;
    }

    public TcpServerUtil() {
        init();
    }

    private static void init() {
        server = new SocketServer("192.168.0.108",3000);
        server.setMaxConnectSize(1); //设置最大连接数量
        List<byte[]> commandS=new ArrayList<>();
        commandS.add(start);
        commandS.add(reset);
        server.setInitializeCommand(commandS);
        server.startServer();
    }

    public static void returnMsg(byte[] bytes){
        server.sendToALl(bytes);
    }
}
