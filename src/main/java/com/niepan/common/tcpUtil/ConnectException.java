package com.niepan.common.tcpUtil;

/**
 * @author: kongyz
 * @date: 2023/10/25 on 9:46
 */
public class ConnectException extends Exception {
    public ConnectException(int maxSize) {
        super("连接数量超出最大限制，连接失败！ 当前最大连接数："+maxSize);
    }
}
