package com.niepan.common.tcpUtil;


import com.niepan.common.tcpUtil.client.ICommunication;
import com.niepan.common.tcpUtil.client.SocketCommunication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: kongyz
 * @date: 2023/10/25 on 9:43
 */
public class AbstractSocketServer implements ISocketServer {
    private static final Logger log= LoggerFactory.getLogger(AbstractSocketServer.class);

    //服务器端口
    protected int port;
    //默认ip地址
    protected String ipAddress = "192.168.2.100";
    //默认最大连接数
    protected int maxConnectSize = 1;
    //当前连接状态
    protected boolean isConn = false;
    //java ServerSocket 对象
    private ServerSocket server;
    protected List<byte[]> initializeCommand =new ArrayList<>();

    //保存所有连接通讯类
    protected ConcurrentHashMap<String, ICommunication> communications = new ConcurrentHashMap<>();

    public AbstractSocketServer(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
    }

    public AbstractSocketServer(int port) {
        this.port = port;
    }
    /**
     * @description: 启动服务
     */
    @Override
    public void startServer() {
        new Thread(() -> {
            try {
                if (isConn) {
                    stopServer();
                }
                server = new ServerSocket();
                //绑定数据连接地址端口号
                server.bind(new InetSocketAddress(ipAddress,port));
                //绑定成功设置当前服务器状态为true
                isConn = true;
                //循环等待客户端连接
                while(true){
                    //阻塞 等待socket client 连接
                    Socket socket = server.accept();
                    for (byte[] item:initializeCommand){
                        socket.getOutputStream().write(item);
                    }
                    //生成连接通讯对象
//                    SocketCommunication socketCommunication = new SocketCommunication(socket,this);
//                    new Thread(() -> {
//                        try {
//                            addCommunication(socketCommunication);
//                            socketCommunication.handle();
//                        } catch (Exception e) {
//                            log.error("客户端建立连接通讯失败",e);
//                        }
//                        removeCommunication(socket.getInetAddress().getHostAddress());
//                    }).start();
                }
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }).start();
    }

    /**
     * @description: 停止服务
     */
    @Override
    public void stopServer(){
        if (server != null) {
            try {
                server.close();
            } catch (IOException ioException) {
                log.error("停止服务失败",ioException);
            }
            isConn = false;
            removeAllCommunication();
        }
    }


    /**
     * @description: 给所有连接发送
     */
    public void sendToALl(byte[] bytes){
        for (String ipAddress : communications.keySet()) {
            send(ipAddress,bytes);
        }
    }

    /**
     * @description: 给某个ip发送
     */
    public boolean send(String ip,byte[] bytes) {
        if (communications.get(ip) == null)
            return false;
        try {
            communications.get(ip).send(bytes);
            return true;
        } catch (Exception e) {
            log.error("给"+ip+"发送",e);
            return false;
        }
    }

    /**
     * @description: 当前是否启动 socket 服务端
     */
    @Override
    public boolean isStart() {
        return isConn;
    }

    /**
     * @description: 设置最大连接数量
     */
    public void setMaxConnectSize(int maxConnectSize) {
        this.maxConnectSize = maxConnectSize;
    }

    /**
     * 设置当有链接进来时发送的初始化发送命令
     * @param initializeCommand
     */
    public void setInitializeCommand(List<byte[]> initializeCommand){
        this.initializeCommand=initializeCommand;
    }

    /**
     * @description: 获取当前连接数据
     */
    public int getConnectSize(){
        return communications.size();
    }

    /**
     * @description: 获取当前所有连接的Ip地址
     * @param:
     */
    public Set<String> getConnectIpAddress(){
        return communications.keySet();
    }

    /**
     * @description: 添加连接对象
     */
    protected void addCommunication(ICommunication communication) throws Exception {
        Socket socket = communication.getSocket();
        //判断是否超出最大连接数量，超出后断开连接
        if (maxConnectSize > communications.size()) {
            ICommunication iCommunication = communications.get(socket.getInetAddress().getHostAddress());
            if (iCommunication != null) {
                try {
                    iCommunication.disconnect();
                } catch (Exception e) {
                    log.error("添加连接对象："+socket.getInetAddress().getHostAddress(),e);
                }
                communications.remove(socket.getInetAddress().getHostAddress());
            }
            communications.put(socket.getInetAddress()
                    .getHostAddress(),communication);
        } else {
            socket.close();
            throw new ConnectException(maxConnectSize);
        }
    }

    /**
     * @description: 移除连接对象
     */
    public void removeCommunication(String ip){
        ICommunication iCommunication = communications.get(ip);
        if (iCommunication != null) {
            try {
                iCommunication.disconnect();
            } catch (Exception e) {
                log.error("移除"+ ip +"连接对象",e);
            }
            communications.remove(ip);
        }
    }

    /**
     * @description: 移除所有连接对象
     */
    public void removeAllCommunication(){
        for (String ipAddress : communications.keySet()) {
            removeCommunication(ipAddress);
        }
    }

}
