package com.niepan.common.tcpUtil;

/**
 * socket 服务启动类
 * @author: kongyz
 * @date: 2023/10/25 on 9:44
 */
public class SocketServer  extends AbstractSocketServer {

    public SocketServer(String ipAddress, int port) {
        super(ipAddress, port);
    }

    public SocketServer(int port) {
        super(port);
    }
}
