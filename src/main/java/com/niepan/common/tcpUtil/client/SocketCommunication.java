package com.niepan.common.tcpUtil.client;

import com.niepan.common.tcpUtil.SocketServerUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * socket 连接对象
 * 主要用于对已连接的客户端收发消息
 * @author: kongyz
 * @date: 2023/10/25 on 9:45
 */
public class SocketCommunication implements ICommunication {
    private static final Logger log= LoggerFactory.getLogger(SocketCommunication.class);

    private static Socket socket; //已经连接的客户端对象

    private SocketServerUtil socketServer; //来自哪个服务器，创建的服务器对象

    public SocketCommunication( Socket socket, SocketServerUtil socketServer) {
        this.socket = socket;
        this.socketServer = socketServer;
    }

    @Override
    public Socket getSocket() {
        return socket;
    }

    @Override
    public void handle() throws Exception {
        try {
            log.info(socket.getInetAddress().getHostAddress()+" 已连接");
            InputStream in = socket.getInputStream();
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int len;
            while ( (len = in.read(b)) != -1) {
                output.write(b, 0, len);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e1) {
                    log.error("客户端连接后等待异常",e1);
                }
                if(in.available() == 0) {
                    this.receive(socket,output.toByteArray());
                    output.reset();
                }
                b = new byte[1024];
                len = 0;
            }
        } catch (IOException ioException) {
            throw ioException;
        }
    }

    /**
     * @description: 接收数据，对客户端接收的数据进行统一处理
     * 可以编写相应的处理逻辑，我这里是服务器端收到消息后。回复当前连接数量、
     */
    @Override
    public void receive(Socket socket, byte[] data) throws Exception {
        //服务器接收客户端消息
        log.info(socket.getInetAddress().getHostAddress()+" 发送："+new String(data,"gbk"));
        //服务器回复客户端消息
        String msg = "当前连接数量："+socketServer.getConnectSize();
        socketServer.send(socket.getInetAddress().getHostAddress(),msg.getBytes("gbk"));
    }

    /**
     * @description: 发送数据
     */
    @Override
    public void send(byte[] data) throws Exception {
        socket.getOutputStream().write(data);
        String out= StringUtils.EMPTY;
        for (byte b:data){
            out+=" "+ Integer.toHexString(b & 0xFF);
        }
        log.info("往"+socket.getInetAddress().getHostAddress()+" 发送："+out);
    }
    /**
     * @description: 断开连接
     */
    @Override
    public void disconnect() throws Exception {
        log.info(socket.getInetAddress().getHostAddress()+" 已断开");
        socket.close();
    }
}
