package com.niepan.common.tcpUtil.client;

import java.net.Socket;

/**
 * @author: kongyz
 * @date: 2023/10/25 on 9:45
 */
public interface ICommunication {
    /**
     * @description: 获取当前连接的socket对象
     */
    public Socket getSocket();

    /**
     * @description: socket创建成功后读取数据
     */
    public void handle() throws Exception;

    /**
     * @description: 将读取到的数据统一处理
     */
    public void receive(Socket socket, byte[] data) throws Exception;

    /**
     * @description: 发送数据
     */
    public void send(byte[] data) throws Exception;

    /**
     * @description: 断开连接
     */
    public void disconnect() throws Exception;
}
