package com.niepan.common.utils.R;
import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author weifuqiang
 * @date 2023/3/9
 */

@Component
@Data
public class Result {
    private static final ApiResponse apiResponse  = new ApiResponse();

    public static ApiResponse success(){
        apiResponse.setCode(ResultStatusEnum.SUCCESS_STATUS.getStatus());
        apiResponse.setMsg(ResultStatusEnum.SUCCESS_STATUS.getMessage());
        apiResponse.setData("");
        return apiResponse;
    }

    //重载可传递数据的成功统一返回
    public static <T>  ApiResponse<T> success(T data){
        apiResponse.setCode(ResultStatusEnum.SUCCESS_STATUS.getStatus());
        apiResponse.setMsg(ResultStatusEnum.SUCCESS_STATUS.getMessage());
        apiResponse.setData(data);
        return apiResponse;
    }

    //重载可传递数据的失败统一返回
    public static <T> ApiResponse<T> error(){
        // todo 这里需要加上对code的判断 需要重写
        apiResponse.setMsg(ResultStatusEnum.UNKNOW_ERROR_STATUS.getMessage());
        return apiResponse;
    }

    //重载可传递数据的失败统一返回
    public static <T> ApiResponse<T> error(T data){
        // todo 这里需要加上对code的判断 需要重写
        apiResponse.setCode(ResultStatusEnum.USER_PWR_STATUS.getStatus());
        apiResponse.setMsg(ResultStatusEnum.USER_PWR_STATUS.getMessage());
        apiResponse.setData(data);
        return apiResponse;
    }


    public static <T> ApiResponse<T> fieldsError(String data){
        // todo 这里需要加上对code的判断 需要重写
        apiResponse.setCode(ResultStatusEnum.FIELDS_ERROR_STATUS.getStatus());
        apiResponse.setMsg(ResultStatusEnum.FIELDS_ERROR_STATUS.getMessage());
        apiResponse.setData(data);
        return apiResponse;
    }
    public static <T> ApiResponse<T> error(String msg,T data){
        // todo 这里需要加上对code的判断 需要重写
        apiResponse.setCode(ResultStatusEnum.INSERT_ERROR_STATUS.getStatus());
        apiResponse.setMsg(msg);
        apiResponse.setData(data);
        return apiResponse;
    }

    //重载可传递数据的失败统一返回
    public static <T> ApiResponse<T> error(Integer code,T data){
        // todo 这里需要加上对code的判断 需要重写
        apiResponse.setCode(code);
        apiResponse.setMsg(ResultStatusEnum.UNKNOW_ERROR_STATUS.getMessage());
        apiResponse.setData(data);
        return apiResponse;
    }

    public static <T> ApiResponse<T> error(Integer code, String msg,T data){
        // todo 这里需要加上对code的判断 需要重写
//        if(ResultStatusEnum.)
        apiResponse.setCode(code);
        apiResponse.setMsg(msg);
        apiResponse.setData(data);
        return apiResponse;
    }

    public static <T> ApiResponse<T> error(Integer code, String msg){
        // todo 这里需要加上对code的判断 需要重写
//        if(ResultStatusEnum.)
        apiResponse.setCode(code);
        apiResponse.setMsg(msg);
        return apiResponse;
    }
}
