/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.niepan.common.utils;

/**
 * 系统参数相关Key
 *
 * @author Mark sunlightcs@gmail.com
 */
public class ConfigConstant {
    /**
     * 云存储配置KEY
     */
    public final static String CLOUD_STORAGE_CONFIG_KEY = "CLOUD_STORAGE_CONFIG_KEY";
    /**
     * 摆轮协议
     */
    public final static String BALANCE_PROTOCOL="balance_protocol";
    /**
     * 是否启用ftp
     */
    public final static String FTP_ENABLE="ftp_enable";

    /**
     * 是否开启违禁品阈值筛选剔除功能（1：开启，0：关闭）
     */
    public final static String DANGER_VALUE="danger_value";
}
