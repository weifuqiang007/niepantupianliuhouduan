package com.niepan.common.utils.json;

import sun.misc.BASE64Decoder;

import java.io.*;

/**
 * @author weifuqiang
 * @create_time 2022/11/26
 */
public class JsonToFile {
        /**
         * JSON文件
         *
         * @param jsonString JSON字符串
         * @param filePath   文件路径
         * @param fileName   文件名
         * @return JSON文件
         */
        public static boolean createJsonFile(String jsonString, String filePath, String fileName) {
            // 文件生成否
            boolean flag = true;
            // 拼接文件完整路径
            String fullPath = filePath + File.separator;
            // 生JSON文件
            try {
                // 创新文件
                File file = new File(fullPath, fileName);
                if (!file.getParentFile().exists()) {
                    // 父目录不存则创父目录
                    file.getParentFile().mkdirs();
                }
                if (file.exists()) {
                    // 已存删旧文件
                    file.delete();
                }
                file.createNewFile();
                // 格式化JSON字符串
                jsonString = JsonFormat.formatJson(jsonString);
                // 格式化后字符串写入文件
                Writer write = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
                write.write(jsonString);
                write.flush();
                write.close();
            } catch (Exception e) {
                flag = false;
                e.printStackTrace();
            }
            // 返成否
            return flag;
        }



    /**
     * 将字符串写入指定文件(当指定的父路径中文件夹不存在时，会最大限度去创建，以保证保存成功！)
     *
     * @param res      原字符串
     * @param filePath 文件路径
     * @return 成功标记
     */
    public static boolean stringToJpg(String res, String filePath) {
        boolean flag = true;
        BufferedReader bufferedReader = null;
        BufferedWriter bufferedWriter = null;
        OutputStream fileOutputStream = null;

        try {
            File distFile = new File(filePath);
            if (!distFile.getParentFile().exists()) distFile.getParentFile().mkdirs();
//            BASE64Encoder base64Encoder = new BASE64Encoder();

            // 把图片转换成成字符串
            //base64Encoder.encode(res.getBytes());

            // 吧字符串转换成图片
            BASE64Decoder base64Decoder = new BASE64Decoder();
            byte[] bytes = base64Decoder.decodeBuffer(res);
            for (int i=0 ; i < bytes.length; i++){
                if (bytes[i] < 0){
                    bytes[i] += 256;
                }
            }

            fileOutputStream = new FileOutputStream(distFile);
            fileOutputStream.write(bytes);
            fileOutputStream.flush();
            fileOutputStream.close();

//            bufferedReader = new BufferedReader(new StringReader(res));
//            bufferedWriter = new BufferedWriter(new FileWriter(distFile));
//            String len;
//            while ((len = bufferedReader.readLine()) != null) {
//                // 这里因为 单引号 ' 有些问题, 做了下面特殊处理
//                len = len.replace("&apos;", "'");
//                bufferedWriter.write(len);
//                bufferedWriter.newLine();
//            }
//            bufferedWriter.flush();
//            bufferedReader.close();
//            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            flag = false;
            return flag;
        } finally {
//            if (bufferedReader != null) {
//                try {
//                    bufferedReader.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
        if(fileOutputStream != null){
                try{
                    fileOutputStream.close();

                }catch (IOException e){
                    e.printStackTrace();
                }
            }

        }
        return flag;
    }

}
