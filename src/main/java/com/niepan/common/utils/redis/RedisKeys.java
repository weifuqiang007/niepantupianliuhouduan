/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.niepan.common.utils.redis;

/**
 * Redis所有Keys
 * todo 这个有什么用呢，涉及到权限的问题嘛
 *
 * @author Mark sunlightcs@gmail.com
 */
public class RedisKeys {

    public static String getSysConfigKey(String key){
        return "sys:config:" + key;
    }

    /**
     * 违禁品阈值key
     * @param code
     * @return
     */
    public static String getDangerCodeKey(String code) {
        return "sys:dangerCode:" + code;
    }
}
