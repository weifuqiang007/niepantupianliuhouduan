package com.niepan.common.utils.minIO;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import io.minio.MinioClient;
import io.minio.PutObjectOptions;
import io.minio.errors.MinioException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * MinIO 连接池类
 */
public class MinIOConnectionPool {

    private static final int MIN_INITIAL_POOL_SIZE = 5;

    private static final int MAX_INITIAL_POOL_SIZE = 10;

    private static int ALIVE_NUM = 0;

    @Value("${minio.address}")
    private String ENDPOINT;
    @Value("${minio.accessKey}")
    private String ACCESS_KEY;
    @Value("${minio.secretKey}")
    private String SECRET_KEY;
    @Value("${minio.bucketName}")
    private String bucketName;

    private List<MinioClient> pool; // 存放 MinIO 客户端连接的列表

    private static MinIOConnectionPool instance; // 单例实例

    private MinIOConnectionPool() {
        pool = new ArrayList<>();
        for (int i = 0; i < MIN_INITIAL_POOL_SIZE; i++) {
            pool.add(createNewClient()); //初始化连接池，创建一定数量的 MinIO 客户端连接
        }
    }

    /**
     * 获取 MinIO 连接池的单例实例
     * @return MinIOConnectionPool 单例实例
     */
    public static MinIOConnectionPool getInstance() {
        if (instance == null) {
            synchronized (MinIOConnectionPool.class) {
                if (instance == null) {
                    instance = new MinIOConnectionPool();
                }
            }
        }
        return instance;
    }

    private MinioClient createNewClient() {
        try {
            // 创建新的 MinIO 客户端连接对象
            return new MinioClient(ENDPOINT, ACCESS_KEY, SECRET_KEY);
        } catch (MinioException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to create a new MinIO client.");
        }
    }

    /**
     * 获取 MinIO 客户端连接
     * @return MinioClient MinIO 客户端连接
     */
    public synchronized MinioClient getClient() {
        while (ALIVE_NUM > MIN_INITIAL_POOL_SIZE && ALIVE_NUM > MAX_INITIAL_POOL_SIZE) {
            try {
                wait(); // 等待，直到有可用连接
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        ALIVE_NUM++;
        return pool.remove(0); // 从连接池中获取一个连接并移除
    }

    /**
     * 释放 MinIO 客户端连接
     * @param client 要释放的 MinIO 客户端连接
     */
    public synchronized void releaseClient(MinioClient client) {
        if (client != null) {
            ALIVE_NUM--;
            pool.add(client); // 将连接放回连接池中
            notify();
        }
    }

    /**
     * 检查存储桶是否存在
     *
     * @param bucketName 存储桶名称
     * @return
     */
    public boolean bucketExists(String bucketName) {
        MinioClient minioClient = this.getClient();
        boolean flag = false;
        try {
            flag = minioClient.bucketExists(bucketName);
            if (flag) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    /**
     * 创建存储桶
     *
     * @param bucketName 存储桶名称
     */
    public boolean makeBucket(String bucketName) {
        MinioClient minioClient = this.getClient();
        try {
            boolean flag = bucketExists(bucketName);
            //存储桶不存在则创建存储桶
            if (!flag) {
                minioClient.makeBucket(bucketName);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }



    /**
     * 上传文件
     *
     * @param file 上传文件
     * @return 成功则返回文件名，失败返回空
     */
    public String uploadFile(MultipartFile file) {
        MinioClient minioClient = this.getClient();
        //创建存储桶
        boolean createFlag = makeBucket(bucketName);
        //创建存储桶失败
        if (createFlag == false) {
            return "";
        }
        try {
            PutObjectOptions putObjectOptions = new PutObjectOptions(file.getSize(), PutObjectOptions.MIN_MULTIPART_SIZE);
            putObjectOptions.setContentType(file.getContentType());
            String originalFilename = file.getOriginalFilename();
            int pointIndex = originalFilename.lastIndexOf(".");
            //得到文件流
            InputStream inputStream = file.getInputStream();
            //保证文件不重名(并且没有特殊字符)
            String fileName = bucketName+ DateUtil.format(new Date(), "_yyyyMMddHHmmss") + (pointIndex > -1 ? originalFilename.substring(pointIndex) : "");
            minioClient.putObject(bucketName, fileName, inputStream, putObjectOptions);
            return fileName;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 下载文件
     *
     * @param originalName 文件路径
     */
    public InputStream downloadFile(String originalName, HttpServletResponse response) {
        MinioClient minioClient = this.getClient();
        try {
            InputStream file = minioClient.getObject(bucketName, originalName);
            String filename = new String(originalName.getBytes("ISO8859-1"), StandardCharsets.UTF_8);
            if (StrUtil.isNotBlank(originalName)) {
                filename = originalName;
            }
            response.setHeader("Content-Disposition", "attachment;filename=" + filename);
            ServletOutputStream servletOutputStream = response.getOutputStream();
            int len;
            byte[] buffer = new byte[1024];
            while ((len = file.read(buffer)) > 0) {
                servletOutputStream.write(buffer, 0, len);
            }
            servletOutputStream.flush();
            file.close();
            servletOutputStream.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 删除文件
     *
     * @param fileName 文件路径
     * @return
     */
    public boolean deleteFile(String fileName) {
        MinioClient minioClient = this.getClient();
        try {
            minioClient.removeObject(bucketName, fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 得到指定文件的InputStream
     *
     * @param originalName 文件路径
     * @return
     */
    public InputStream getObject(String originalName) {
        MinioClient minioClient = this.getClient();
        try {
            return minioClient.getObject(bucketName, originalName);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 根据文件路径得到预览文件绝对地址
     * 返回的地址前端能够直接展示出来
     *
     * @param fileName    文件路径
     * @return
     */
    public String getPreviewFileUrl(String fileName) {
        MinioClient minioClient = this.getClient();
        try {
            return minioClient.presignedGetObject(bucketName,fileName);
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }
}
