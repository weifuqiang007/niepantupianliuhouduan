/**
 * 版权所有，侵权必究！
 */

package com.niepan;

import com.niepan.common.BasePackages;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = "com")
@MapperScan(basePackages = {BasePackages.all})
@EnableScheduling
public class BackstageApplication {
	public static void main(String[] args) {
		SpringApplication.run(BackstageApplication.class, args);
	}
}