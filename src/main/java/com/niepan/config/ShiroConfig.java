/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.niepan.config;

import com.niepan.modules.sys.oauth2.OAuth2Filter;
import com.niepan.modules.sys.oauth2.OAuth2Realm;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Shiro配置
 *
 * @author Mark sunlightcs@gmail.com
 */
@Configuration
public class ShiroConfig {

    @Bean("securityManager")
    public SecurityManager securityManager(OAuth2Realm oAuth2Realm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(oAuth2Realm);
        securityManager.setRememberMeManager(null);
        return securityManager;
    }

    @Bean("shiroFilter")
    public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
        shiroFilter.setSecurityManager(securityManager);

        //oauth过滤
        Map<String, Filter> filters = new HashMap<>();
        filters.put("oauth2", new OAuth2Filter());
        shiroFilter.setFilters(filters);

        Map<String, String> filterMap = new LinkedHashMap<>();
//        filterMap.put("/webjars/**", "anon");
//        filterMap.put("/druid/**", "anon");//德鲁伊连接池
//        filterMap.put("/app/**", "anon");
        filterMap.put("/sys/login", "anon");//登录接口
//        filterMap.put("/swagger/**", "anon");
//        filterMap.put("/v2/api-docs", "anon");
//        filterMap.put("/swagger-ui.html", "anon");
//        filterMap.put("/swagger-resources/**", "anon");
//        filterMap.put("/captcha", "anon");
//        filterMap.put("/api/file/hello", "anon");
//        filterMap.put("/aaa.txt", "anon");
//        filterMap.put("/largeScreen/**", "anon");//大屏界面的所有接口
//        filterMap.put("/ContrabandList/**", "anon");//行包管理的接口
//        filterMap.put("/atx/api/securitydevice/**", "anon");//安天下的接口
//        filterMap.put("/api/open/**", "anon");//尤为泰科的接口
//        filterMap.put("/picture/**", "anon");//推送图片测试的接口
//        filterMap.put("/bjhy/api/securitydevice/**", "anon");//中集的接口
        filterMap.put("/largeScreen/receiveData", "anon");//接收中通数据的接口
        filterMap.put("/largeScreen/receiveHeartData", "anon");//接收中通心跳数据的接口
        filterMap.put("/ReceiveInfo", "anon");//接收数据的接口
        filterMap.put("/ReceiveInfoDanger", "anon");//接收违禁品信息的接口
        // filterMap.put("/**", "anon");
       filterMap.put("/**", "oauth2");//其他全部拦截
        shiroFilter.setFilterChainDefinitionMap(filterMap);

        return shiroFilter;
    }

    @Bean("lifecycleBeanPostProcessor")
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }

}
