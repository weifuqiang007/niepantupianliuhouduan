package com.niepan.modules.sys.service.impl;

import com.niepan.modules.sys.dao.CentreDao;
import com.niepan.modules.sys.entity.CentreEntity;
import com.niepan.modules.sys.service.CentreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class CentreServiceImpl implements CentreService {

    @Autowired
    private CentreDao centreDao;

    @Override
    public List<Map<String, Object>> list(String uniqueCode) {
        List<Map<String, Object>> list = centreDao.list(uniqueCode);
        return list;
    }

    @Override
    public Integer addCentre(CentreEntity centreEntity) {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        centreEntity.setId(uuid);
        Integer res = centreDao.addCentre(centreEntity);
        return res;
    }

    @Override
    public List<Map<String, Object>> centreDetail(String id) {
        List<Map<String,Object>> list = centreDao.centreDetail(id);
        return list;
    }

    @Override
    public Integer updateCentre(CentreEntity centreEntity) {
        Integer res = centreDao.updateCentre(centreEntity);
        return res;
    }

    @Override
    public Integer deleteCentre(String id) {
        Integer res = centreDao.deleteCentre(id);
        return res;
    }
}
