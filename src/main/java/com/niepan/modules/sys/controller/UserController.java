package com.niepan.modules.sys.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.modules.sys.entity.UserEntity;
import com.niepan.modules.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 展示所有的用户
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping("/list")
    public PageInfo list(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                         @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum){
        PageHelper.startPage(pageNum, pageSize);
        List<Map<String,Object>> res = userService.list();
        return new PageInfo<>(res);
    }

    /**
     * 添加用户
     * @param userEntity
     * @return
     */
    @PostMapping("/addUser")
    public ApiResponse addUser(@RequestBody UserEntity userEntity){
        Integer res = userService.addUser(userEntity);
        return Result.success(res);
    }

    /**
     * 回显用户的信息
     * @param userId
     * @return
     */
    @GetMapping("/userDetail")
    public ApiResponse userDetail(@RequestParam("user_id") String userId){
        List<Map<String,Object>> res = userService.userDetail(userId);
        return Result.success(res);
    }

    /**
     * 更新用户信息
     * @param userEntity
     * @return
     */
    @PostMapping("/updateUser")
    public ApiResponse updateUser(@RequestBody UserEntity userEntity){
        Integer res = userService.updateUser(userEntity);
        return Result.success(res);
    }

    /**
     * 删除用户
     * @param userId
     * @return
     */
    @GetMapping("/deleteUser")
    public ApiResponse deleteUser(@RequestParam("user_id") String userId){
        Integer res = userService.deleteUser(userId);
        return Result.success(res);
    }
}
