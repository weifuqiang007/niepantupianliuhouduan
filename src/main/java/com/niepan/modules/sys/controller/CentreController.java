package com.niepan.modules.sys.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.modules.sys.entity.CentreEntity;
import com.niepan.modules.sys.service.CentreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/centre")
public class CentreController {

    @Autowired
    private CentreService centreService;

    @GetMapping("/list")
    public PageInfo list(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                         @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                         @RequestParam(value = "uniqueCode",required = false) String uniqueCode){
        PageHelper.startPage(pageNum, pageSize);
        List<Map<String,Object>> res = centreService.list(uniqueCode);
        return new PageInfo<>(res);
    }

    @PostMapping("/addCentre")
    public ApiResponse addCentre(@RequestBody CentreEntity centreEntity){
        Integer res = centreService.addCentre(centreEntity);
        return Result.success(res);
    }

    @GetMapping("/centreDetail")
    public ApiResponse centreDetail(@RequestParam("id") String id){
        List<Map<String,Object>> res = centreService.centreDetail(id);
        return Result.success(res);
    }

    @PostMapping("/updateCentre")
    public ApiResponse updateCentre(@RequestBody CentreEntity centreEntity){
        Integer res = centreService.updateCentre(centreEntity);
        return Result.success(res);
    }

    @GetMapping("/deleteCentre")
    public ApiResponse deleteCentre(@RequestParam("id") String id){
        Integer res = centreService.deleteCentre(id);
        return Result.success(res);
    }

}
