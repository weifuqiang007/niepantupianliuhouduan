package com.niepan.modules.sys.dao;

import com.niepan.modules.sys.entity.RoleEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RoleDao {
    List<Map<String, Object>> list();

    Integer addRole(RoleEntity roleEntity);

    void addRoleMenu(@Param("uuid") String uuid, @Param("menu_id") String menu_id);

    List<Map<String, Object>> roleDetail(@Param("roleId") String roleId);

    Integer updateRole(RoleEntity roleEntity);

    void updateRoleMenu(@Param("menu_id") String menu_id, @Param("role_id") String role_id);

    Integer deleteRole(@Param("role_id") String role_id);

    void deleteRoleMenu(@Param("role_id") String role_id);

    List<Map<String, Object>> getRole();

    List<Integer> getRoleMenu(@Param("roleId") String roleId);

    void insertRoleMenu(@Param("munuid") String munuid, @Param("role_id") String role_id);
}
