package com.niepan.modules.sys.dao;

import com.niepan.modules.sys.entity.CentreEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CentreDao {
    List<Map<String, Object>> list(@Param("uniqueCode") String uniqueCode);

    Integer addCentre(CentreEntity centreEntity);

    List<Map<String, Object>> centreDetail(@Param("id") String id);

    Integer updateCentre(CentreEntity centreEntity);

    Integer deleteCentre(@Param("id") String id);
}
