package com.niepan.modules.sys.dao;

import com.niepan.modules.sys.entity.UserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserDao {
    /**
     * 展示所有的用户
     * @return
     */
    List<Map<String, Object>> list();

    /**
     * 向user表中插入用户
     * @param userEntity
     * @return
     */
    Integer addUser(UserEntity userEntity);


    /**
     * 根据用户名查询用户信息
     * @param username
     * @return
     */
    UserEntity getUserByUserName(@Param("username") String username);

    /**
     * 向用户角色表中插入信息（即改用户属于哪个角色）
     * @param user_id
     * @param role_id
     */
    void addUserRole(@Param("user_id") String user_id, @Param("role_id") String role_id);

    /**
     * 获取用户的详细信息，用来回显
     * @param userId
     * @return
     */
    List<Map<String,Object>> userDetail(@Param("userId") String userId);

    /**
     * 更新用户信息
     * @param userEntity
     * @return
     */
    Integer updateUser(UserEntity userEntity);

    /**
     * 更新用户角色表中的信息
     * @param user_id
     * @param role_id
     */
    void updateUserRole(@Param("user_id") String user_id, @Param("role_id") String role_id);

    /**
     * 删除用户
     * @param user_id
     * @return
     */
    Integer deleteUser(@Param("user_id") String user_id);

    /**
     * 删除用户角色表中改用户的数据
     * @param user_id
     */
    void deleteUserRole(@Param("user_id") String user_id);
}
