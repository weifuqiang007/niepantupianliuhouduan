package com.niepan.modules.sys.entity;

import lombok.Data;

@Data
public class RoleEntity {
    String role_id;
    String role_name;
    String remark;
    String create_user_id;
    String create_time;
    String sort;
    String[] menuList;
    String status;
}
