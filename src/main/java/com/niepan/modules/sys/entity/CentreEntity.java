package com.niepan.modules.sys.entity;

import lombok.Data;

@Data
public class CentreEntity {
    private String id;
    private String uniqueCode;
    private String level;
    private String name;
    private String phone;
    private String server_code;
    private String remark;
    private String state;
}
