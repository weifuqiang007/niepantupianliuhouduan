/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.niepan.modules.sys.redis;


import cn.hutool.core.collection.CollectionUtil;
import com.niepan.common.utils.redis.RedisKeys;
import com.niepan.common.utils.redis.RedisUtils;
import com.niepan.modules.zhinenganjianback.entity.SysDangerValue;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统配置Redis
 *
 * @author Mark sunlightcs@gmail.com
 */
@Component
public class SysDangerValueRedis {
    @Autowired
    private RedisUtils redisUtils;

    /**
     * 保存缓存
     * @param valueVoList
     */
    public void saveOrUpdate(List<SysDangerValue> valueVoList) {
        if(CollectionUtil.isEmpty(valueVoList)){
            return ;
        }
        for (SysDangerValue item:valueVoList){
            String key = RedisKeys.getDangerCodeKey(item.getDangerCode().toString());
            redisUtils.delete(key);
            redisUtils.set(key, item.getDangerValue());
        }
    }

    /**
     * 删除缓存
     * @param valueVoList
     */
    public void delete(List<SysDangerValue> valueVoList){
        if(CollectionUtil.isEmpty(valueVoList)){
            return ;
        }
        for (SysDangerValue item:valueVoList){
            String key = RedisKeys.getDangerCodeKey(item.getDangerCode().toString());
            redisUtils.delete(key);
        }
    }

    /**
     * 获取阈值
     * @param dangeValueCode
     * @return
     */
    public Integer get(Integer dangeValueCode){
        String key = RedisKeys.getDangerCodeKey(dangeValueCode.toString());
        return redisUtils.get(key, Integer.class);
    }

}
