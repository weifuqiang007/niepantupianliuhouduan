package com.niepan.modules.zhinenganjianback.enums;

/**
 * @author weifuqiang
 * @date 2023/2/14
 */


public enum ResultStatusEnum {
    SUCCESS_STATUS(1001,"SUCCESS"),
    USER_PWR_STATUS(1002,"用户密码有误"),
    USER_ROLE_STATUS(1003,"用户权限不足"),
    ORDER_ERROR_STATUS(1004,"订单有误"),
    INSERT_ERROR_STATUS(1005,"插入数据库异常");

    ResultStatusEnum(Integer status,String message) {
        this.status = status;

        this.message = message;
    }

    private Integer status;

    private String message;

    public Integer getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
