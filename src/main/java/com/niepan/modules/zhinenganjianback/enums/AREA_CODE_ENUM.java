package com.niepan.modules.zhinenganjianback.enums;

/**
 * @author: liuchenyu
 * @date: 2023/4/3
 */
public enum AREA_CODE_ENUM {

    AREA_CODE_ENUM_HB_SJZ(130100,"石家庄市"),
    AREA_CODE_ENUM_HB_TS(130200,"唐山市"),
    AREA_CODE_ENUM_HB_QHD(130300,"秦皇岛市"),
    AREA_CODE_ENUM_HB_HD(130400,"邯郸市"),
    AREA_CODE_ENUM_HB_XT(130500,"邢台市"),
    AREA_CODE_ENUM_HB_BD(130600,"保定市"),
    AREA_CODE_ENUM_HB_ZJK(130700,"张家口市"),
    AREA_CODE_ENUM_HB_CD(130800,"承德市"),
    AREA_CODE_ENUM_HB_CZ(130900,"沧州市"),
    AREA_CODE_ENUM_HB_LF(131000,"廊坊市"),
    AREA_CODE_ENUM_HB_HS(131100,"衡水市"),
    AREA_CODE_ENUM_SX_TY(140100,"太原市"),
    AREA_CODE_ENUM_SX_DT(140200,"大同市"),
    AREA_CODE_ENUM_SX_YQ(140300,"阳泉市"),
    AREA_CODE_ENUM_SX_CZ(140400,"长治市"),
    AREA_CODE_ENUM_SX_JC(140500,"晋城市"),
    AREA_CODE_ENUM_SX_SZ(140600,"朔州市"),
    AREA_CODE_ENUM_SX_JZ(140700,"晋中市"),
    AREA_CODE_ENUM_SX_YC(140800,"运城市"),
    AREA_CODE_ENUM_SX_XZ(140900,"忻州市"),
    AREA_CODE_ENUM_SX_LF(141000,"临汾市"),
    AREA_CODE_ENUM_SX_LL(141100,"吕梁市"),
    AREA_CODE_ENUM_NMG_HHHT(150100,"呼和浩特市"),
    AREA_CODE_ENUM_NMG_BT(150200,"包头市"),
    AREA_CODE_ENUM_NMG_WH(150300,"乌海市"),
    AREA_CODE_ENUM_NMG_CF(150400,"赤峰市"),
    AREA_CODE_ENUM_NMG_TL(150500,"通辽市"),
    AREA_CODE_ENUM_NMG_EEDS(150600,"鄂尔多斯市"),
    AREA_CODE_ENUM_NMG_HLBE(150700,"呼伦贝尔市"),
    AREA_CODE_ENUM_NMG_BYNE(150800,"巴彦淖尔市"),
    AREA_CODE_ENUM_NMG_WLCB(150900,"乌兰察布市"),
    AREA_CODE_ENUM_NMG_XA(1501000,"兴安市"),
    AREA_CODE_ENUM_NMG_XLGL(151100,"锡林郭勒市"),
    AREA_CODE_ENUM_NMG_ALS(151200,"阿拉善市"),
    AREA_CODE_ENUM_LN_SY(210100,"沈阳市"),
    AREA_CODE_ENUM_LN_DL(210200,"大连市"),
    AREA_CODE_ENUM_LN_AS(210300,"鞍山市"),
    AREA_CODE_ENUM_LN_FS(210400,"抚顺市"),
    AREA_CODE_ENUM_LN_BX(210500,"本溪市"),
    AREA_CODE_ENUM_LN_DD(210600,"丹东市"),
    AREA_CODE_ENUM_LN_JZ(210700,"锦州市"),
    AREA_CODE_ENUM_LN_YK(210800,"营口市"),
    AREA_CODE_ENUM_LN_FX(210900,"阜新市"),
    AREA_CODE_ENUM_LN_LY(211000,"辽阳市"),
    AREA_CODE_ENUM_LN_PJ(211100,"盘锦市"),
    AREA_CODE_ENUM_LN_TL(211200,"铁岭市"),
    AREA_CODE_ENUM_LN_CY(211300,"朝阳市"),
    AREA_CODE_ENUM_LN_HLD(211400,"葫芦岛市"),
    AREA_CODE_ENUM_JL_CC(220100,"长春市"),
    AREA_CODE_ENUM_JL_JL(220200,"吉林市"),
    AREA_CODE_ENUM_JL_SP(220300,"四平市"),
    AREA_CODE_ENUM_JL_LY(220400,"辽源市"),
    AREA_CODE_ENUM_JL_TH(220500,"通化市"),
    AREA_CODE_ENUM_JL_BS(220600,"白山市"),
    AREA_CODE_ENUM_JL_SY(220700,"松原市"),
    AREA_CODE_ENUM_JL_BC(220800,"白城市"),
    AREA_CODE_ENUM_JL_YB(222400,"延边市"),
    AREA_CODE_ENUM_HLJ_HEB(230100,"哈尔滨市"),
    AREA_CODE_ENUM_HLJ_QQHE(230200,"齐齐哈尔市"),
    AREA_CODE_ENUM_HLJ_JX(230300,"鸡西市"),
    AREA_CODE_ENUM_HLJ_HG(230400,"鹤岗市"),
    AREA_CODE_ENUM_HLJ_SYS(230500,"双鸭山市"),
    AREA_CODE_ENUM_HLJ_DQ(230600,"大庆市"),
    AREA_CODE_ENUM_HLJ_YC(230700,"伊春市"),
    AREA_CODE_ENUM_HLJ_JMS(230800,"佳木斯市"),
    AREA_CODE_ENUM_HLJ_QTH(230900,"七台河市"),
    AREA_CODE_ENUM_HLJ_MDJ(231000,"牡丹江市"),
    AREA_CODE_ENUM_HLJ_HH(231100,"黑河市"),
    AREA_CODE_ENUM_HLJ_SH(231200,"绥化市"),
    AREA_CODE_ENUM_HLJ_DXAL(232700,"大兴安岭市"),
    AREA_CODE_ENUM_SD_JN(370100,"济南市"),
    AREA_CODE_ENUM_SD_QD(370200,"青岛市"),
    AREA_CODE_ENUM_SD_ZB(370300,"淄博市"),
    AREA_CODE_ENUM_SD_ZZ(370400,"枣庄市"),
    AREA_CODE_ENUM_SD_DY(370500,"东营市"),
    AREA_CODE_ENUM_SD_YT(370600,"烟台市"),
    AREA_CODE_ENUM_SD_WF(370700,"潍坊市"),
    AREA_CODE_ENUM_SD_JINING(370800,"济宁市"),
    AREA_CODE_ENUM_SD_TA(370900,"泰安市"),
    AREA_CODE_ENUM_SD_WH(371000,"威海市"),
    AREA_CODE_ENUM_SD_RZ(371100,"日照市"),
    AREA_CODE_ENUM_SD_LY(371300,"临沂市"),
    AREA_CODE_ENUM_SD_DZ(371400,"德州市"),
    AREA_CODE_ENUM_SD_LC(371500,"聊城市"),
    AREA_CODE_ENUM_SD_BZ(371600,"滨州市"),
    AREA_CODE_ENUM_SD_HZ(371700,"菏泽市"),
    AREA_CODE_ENUM_HN_ZZ(410100,"郑州市"),
    AREA_CODE_ENUM_HN_KF(410100,"开封市"),
    AREA_CODE_ENUM_HN_LY(410100,"洛阳市"),
    AREA_CODE_ENUM_HN_PDS(410100,"平顶山市"),
    AREA_CODE_ENUM_HN_AY(410100,"安阳市"),
    AREA_CODE_ENUM_HN_HB(410100,"鹤壁市"),
    AREA_CODE_ENUM_HN_XX(410100,"新乡市"),
    AREA_CODE_ENUM_HN_JZ(410100,"焦作市"),
    AREA_CODE_ENUM_HN_PY(410100,"濮阳市"),
    AREA_CODE_ENUM_HN_XC(410100,"许昌市"),
    AREA_CODE_ENUM_HN_LH(410100,"漯河市"),
    AREA_CODE_ENUM_HN_SMX(410100,"三门峡市"),
    AREA_CODE_ENUM_HN_NY(410100,"南阳市"),
    AREA_CODE_ENUM_HN_SQ(410100,"商丘市"),
    AREA_CODE_ENUM_HN_XY(410100,"信阳市"),
    AREA_CODE_ENUM_HN_ZK(410100,"周口市"),
    AREA_CODE_ENUM_HN_ZMD(410100,"驻马店市"),
    AREA_CODE_ENUM_HN_HNX(410100,"河南辖市"),
    AREA_CODE_ENUM_SX_XA(610100,"西安市");





    private int value;

    private String note;

    public String getNote() {
        return note;
    }

    public int getValue() {
        return value;
    }

    AREA_CODE_ENUM(int givenValue, String note) {
        this.value = givenValue;
        this.note = note;
    }

    public static String getNoteByValue(int givenValue) {
        for (AREA_CODE_ENUM enumType : AREA_CODE_ENUM.values()) {
            if (givenValue == enumType.getValue()) {
                return enumType.getNote();
            }
        }
        return null;
    }

    public static int getValueByNote(String givenNote) {
        for (AREA_CODE_ENUM enumType : AREA_CODE_ENUM.values()) {
            if (givenNote.equals(enumType.getNote())) {
                return enumType.getValue();
            }
        }
        return -1;
    }

    public static AREA_CODE_ENUM getEnum(int value) {
        for (AREA_CODE_ENUM e : AREA_CODE_ENUM.values()) {
            if (e.getValue() == value)
                return e;
        }
        return AREA_CODE_ENUM.AREA_CODE_ENUM_HB_BD;
    }

}
