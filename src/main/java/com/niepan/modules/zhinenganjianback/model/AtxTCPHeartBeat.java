package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;

/**
 * @author: liuchenyu
 * @date: 2023/5/23
 */
@Data
public class AtxTCPHeartBeat {

    private String DeviceId;

    /**
     * 时间
     * 2022-06-02 11:34:30 000
     * yyyy-MM-dd HH:mm:ss SSS
     */
    private String DataTime;

    /**
     * 软件版本（当前集成服务软件版本）
     */
    private String ProtocolVersion;
}
