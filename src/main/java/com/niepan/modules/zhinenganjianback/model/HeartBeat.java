package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;

import java.util.Date;

/**
 * @author weifuqiang
 * @create_time 2022/12/19
 */

@Data
public class HeartBeat {
    /**
     * 安检机设备序列号
     */
    private String device_sn;

    /**
     * line_code
     * 生产线号(1-18)
     */
    private String line_code;

    /**
     * 故障状态码
     */
    private String  status_code;

    /**
     * 故障信息描述
     */
    private String status_desc;

    /**
     * 故障级别，0正常1通知2警告3故障…
     */
    private String fault_level;

    /**
     * 状态故障采集时间戳，ms
     */
    private Date msgtime;

    /**
     * 主键id
     */
    private Integer id;

}
