package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;

@Data
public class ImageUrlResult {
    private String message;

    private String result;

    private Boolean status;

    private String statusCode;
}
