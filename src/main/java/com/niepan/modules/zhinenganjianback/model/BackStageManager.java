package com.niepan.modules.zhinenganjianback.model;

import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: weifuqiang
 * @date: 2023/7/18
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BackStageManager {

    public Integer menu_id;

    // 父菜单ID，一级菜单为0
    public Integer parent_id;

    // 用户
    public String username;

    // 菜单名称
    public String name;

    // 菜单URL
    public String url;

    // 授权(多个用逗号分隔，如：user:list,user:create)
    public String perms;

    // 类型   0：目录   1：菜单   2：按钮
    public Integer type;

    // 菜单图标
    public String icon;

    // 排序
    public Integer order_num;

}
