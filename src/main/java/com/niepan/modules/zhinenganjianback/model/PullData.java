package com.niepan.modules.zhinenganjianback.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * 推送到zto数据保存本地
 * @TableName pull_data
 */
@TableName(value ="pull_data")
public class PullData implements Serializable {
    /**
     * 主键
     */
    private String imageId;

    /**
     * snCode
     */
    private String snCode;

    /**
     * dws_code
     */
    private String dwsCode;

    /**
     * 安检机图片ID
     */
    private String forbidId;

    /**
     * 违禁品标记,只有发现违禁品才会上报，默认为true
     */
    private String isForbid;

    /**
     * 违禁品类型编号
     */
    private Integer forbidCode;

    /**
     * 违禁品类型名称
     */
    private String forbidType;

    /**
     * 扫描时间
     */
    private Date scanTime;

    /**
     * 扩展字段，
     */
    private String extendedField;

    /**
     * 推送总部状态，0：未推送，1：已推送，2：推送失败
     */
    private Integer pullState;

    /**
     * 推送时间
     */
    private Date pullTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    public String getImageId() {
        return imageId;
    }

    /**
     * 主键
     */
    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    /**
     * snCode
     */
    public String getSnCode() {
        return snCode;
    }

    /**
     * snCode
     */
    public void setSnCode(String snCode) {
        this.snCode = snCode;
    }

    /**
     * dws_code
     */
    public String getDwsCode() {
        return dwsCode;
    }

    /**
     * dws_code
     */
    public void setDwsCode(String dwsCode) {
        this.dwsCode = dwsCode;
    }

    /**
     * 安检机图片ID
     */
    public String getForbidId() {
        return forbidId;
    }

    /**
     * 安检机图片ID
     */
    public void setForbidId(String forbidId) {
        this.forbidId = forbidId;
    }

    /**
     * 违禁品标记,只有发现违禁品才会上报，默认为true
     */
    public String getIsForbid() {
        return isForbid;
    }

    /**
     * 违禁品标记,只有发现违禁品才会上报，默认为true
     */
    public void setIsForbid(String isForbid) {
        this.isForbid = isForbid;
    }

    /**
     * 违禁品类型编号
     */
    public Integer getForbidCode() {
        return forbidCode;
    }

    /**
     * 违禁品类型编号
     */
    public void setForbidCode(Integer forbidCode) {
        this.forbidCode = forbidCode;
    }

    /**
     * 违禁品类型名称
     */
    public String getForbidType() {
        return forbidType;
    }

    /**
     * 违禁品类型名称
     */
    public void setForbidType(String forbidType) {
        this.forbidType = forbidType;
    }

    /**
     * 扫描时间
     */
    public Date getScanTime() {
        return scanTime;
    }

    /**
     * 扫描时间
     */
    public void setScanTime(Date scanTime) {
        this.scanTime = scanTime;
    }

    /**
     * 扩展字段，
     */
    public String getExtendedField() {
        return extendedField;
    }

    /**
     * 扩展字段，
     */
    public void setExtendedField(String extendedField) {
        this.extendedField = extendedField;
    }

    /**
     * 推送总部状态，0：未推送，1：已推送，2：推送失败
     */
    public Integer getPullState() {
        return pullState;
    }

    /**
     * 推送总部状态，0：未推送，1：已推送，2：推送失败
     */
    public void setPullState(Integer pullState) {
        this.pullState = pullState;
    }

    /**
     * 推送时间
     */
    public Date getPullTime() {
        return pullTime;
    }

    /**
     * 推送时间
     */
    public void setPullTime(Date pullTime) {
        this.pullTime = pullTime;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PullData other = (PullData) that;
        return (this.getImageId() == null ? other.getImageId() == null : this.getImageId().equals(other.getImageId()))
            && (this.getSnCode() == null ? other.getSnCode() == null : this.getSnCode().equals(other.getSnCode()))
            && (this.getDwsCode() == null ? other.getDwsCode() == null : this.getDwsCode().equals(other.getDwsCode()))
            && (this.getForbidId() == null ? other.getForbidId() == null : this.getForbidId().equals(other.getForbidId()))
            && (this.getIsForbid() == null ? other.getIsForbid() == null : this.getIsForbid().equals(other.getIsForbid()))
            && (this.getForbidCode() == null ? other.getForbidCode() == null : this.getForbidCode().equals(other.getForbidCode()))
            && (this.getForbidType() == null ? other.getForbidType() == null : this.getForbidType().equals(other.getForbidType()))
            && (this.getScanTime() == null ? other.getScanTime() == null : this.getScanTime().equals(other.getScanTime()))
            && (this.getExtendedField() == null ? other.getExtendedField() == null : this.getExtendedField().equals(other.getExtendedField()))
            && (this.getPullState() == null ? other.getPullState() == null : this.getPullState().equals(other.getPullState()))
            && (this.getPullTime() == null ? other.getPullTime() == null : this.getPullTime().equals(other.getPullTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getImageId() == null) ? 0 : getImageId().hashCode());
        result = prime * result + ((getSnCode() == null) ? 0 : getSnCode().hashCode());
        result = prime * result + ((getDwsCode() == null) ? 0 : getDwsCode().hashCode());
        result = prime * result + ((getForbidId() == null) ? 0 : getForbidId().hashCode());
        result = prime * result + ((getIsForbid() == null) ? 0 : getIsForbid().hashCode());
        result = prime * result + ((getForbidCode() == null) ? 0 : getForbidCode().hashCode());
        result = prime * result + ((getForbidType() == null) ? 0 : getForbidType().hashCode());
        result = prime * result + ((getScanTime() == null) ? 0 : getScanTime().hashCode());
        result = prime * result + ((getExtendedField() == null) ? 0 : getExtendedField().hashCode());
        result = prime * result + ((getPullState() == null) ? 0 : getPullState().hashCode());
        result = prime * result + ((getPullTime() == null) ? 0 : getPullTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", imageId=").append(imageId);
        sb.append(", snCode=").append(snCode);
        sb.append(", dwsCode=").append(dwsCode);
        sb.append(", forbidId=").append(forbidId);
        sb.append(", isForbid=").append(isForbid);
        sb.append(", forbidCode=").append(forbidCode);
        sb.append(", forbidType=").append(forbidType);
        sb.append(", scanTime=").append(scanTime);
        sb.append(", extendedField=").append(extendedField);
        sb.append(", pullState=").append(pullState);
        sb.append(", pullTime=").append(pullTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}