package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 磁盘信息
 * @author: kongyz
 * @date: 2023/10/27 on 17:02
 */
@Data
public class DiskHealth {

    /**
     * 盘符
     */
    private String diskName;
    /**
     * 可用空间
     */
    private String freeSpace;
    /**
     * 总空间
     */
    private String totalSpace;
    /**
     * 占比
     */
    private BigDecimal rate;
}
