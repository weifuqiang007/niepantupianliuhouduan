package com.niepan.modules.zhinenganjianback.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author weifuqiang
 * @date 2023/3/8
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("np_device_register")
public class NPDeviceRegister {

    /**
     * 主键
     */
    Integer id;

    /**
     * 省份
     */
    String province;

    /**
     * 市区
     */
    String city;

    /**
     * 中心
     */
    String center;

    /**
     * 设备ip
     */
    String ip;

    /**
     * 设备channle，第几号线
     */
    String channel;

    /**
     * dws码，动态称编码
     */
    String dwscode;

    /**
     * sn码，安检服务器编码
     */
    String sncode;

    /**
     * 安检机ip
     */
    String ajj_ip;

    /**
     * 状态（0 不可用 1 可用）
     */
    Integer status;

    /**
     * 创建时间
     */
    Date createtime;

    /**
     * 创建人
     */
    String createuser;

    /**
     * 更新时间
     */
    Date updatetime;

    /**
     * 更新人
     */
    String updateuser;

    /**
     * 二次安捡
     */
    Integer twice;

}
