package com.niepan.modules.zhinenganjianback.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


/**
 * 智能服务器图像识别返回记录中 ---包裹详细信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("package_item_info")
public class PackageItemInfo {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    /**
     * 视角：目前是分为 主视角/从视角
     */
    @TableField("view_type")
    private String viewType;

    /**
     *  物品危险等级
     */
    @TableField("em_danger_grade")
    private int	emDangerGrade;

    /**
     *  物品类型
     */
    @TableField("em_obj_type")
    private int	emObjType;

    /**
     *  相似度
     */
    @TableField("n_similarity")
    private int	nSimilarity;

    /**
     * 包裹只能识别记录id
     */
    @TableField("image_identify_records_id")
    private String imageIdentificationRecordsId;

    /**
     *  包围盒
     *
     *  //区域；各边距按整长8192的比例
     *     public static class NET_RECT extends SdkStructure
     *     {
     *         public int left;
     *         public int top;
     *         public int right;
     *         public int bottom;
     *
     *         public String toString() {
     *             return "[" + left + " " + top + " " + right + " " + bottom + "]";
     *         }
     *     }
     */
    @TableField("stu_bounding_box")
    private String stuBoundingBox;

    @TableField("create_time")
    private Date createTime;

    /**
     * 物品类型对应名称
     */
    @TableField("item_name")
    private String itemName;

    private String unique_code;


}