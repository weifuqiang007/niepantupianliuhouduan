package com.niepan.modules.zhinenganjianback.dto;

import lombok.Data;

/**
 * @author weifuqiang
 * @create_time 2022/12/19
 */

@Data
public class GetHeartInfoDto {

    /**
     * 安检机设备序列号
     */
    private String device_sn;

    /**
     * 生产线号(1-18)
     */
    private String line_code;

    /**
     * 安检机当前指令状态，0在线，1前进，2后退，3暂停，4其它
     */
    private String status_code;

}
