package com.niepan.modules.zhinenganjianback.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName area_count
 */
@TableName(value ="area_count")
@Data
public class AreaCount implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 省
     */
    private String province;

    private String county;

    /**
     * 市
     */
    private String city;

    /**
     * 中心
     */
    private String centre;

    /**
     * 地区唯一编码
     */
    private String areaCode;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 违禁品数量
     */
    private Integer dangerCount;

    /**
     * 非违禁品数量
     */
    private Integer noDangerCount;

    //排序
    private Integer sort;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}