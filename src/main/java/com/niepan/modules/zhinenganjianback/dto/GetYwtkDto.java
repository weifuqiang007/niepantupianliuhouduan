package com.niepan.modules.zhinenganjianback.dto;

import lombok.Data;

import java.io.File;

/**
 * @author: liuchenyu
 * @date: 2023/9/12
 */

@Data
public class GetYwtkDto {

    /**
     * 安检机设备序列号,需提供可配置入口
     *
     */
    String snCode;

    /**
     * 生产线号(1-18) ,需提供可配置入口
     */
    String dwsCode ;

    /**
     * 图片,文件大小<2M（
     */
    File img;

}
