package com.niepan.modules.zhinenganjianback.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName area_map
 */
@TableName(value ="area_map")
@Data
public class AreaMap implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 省份
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区，县
     */
    private String county;

    /**
     * 动态称编码
     */
    private String dwsCode;

    /**
     * 线号
     */
    private String channle;

    /**
     * 各地中心唯一编码
     */
    private String areaCode;

    /**
     * 转运中心
     */
    private String centre;

    //排序
    private Integer sort;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}