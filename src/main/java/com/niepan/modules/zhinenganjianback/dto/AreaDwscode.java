package com.niepan.modules.zhinenganjianback.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName area_dwscode
 */
@TableName(value ="area_dwscode")
@Data
public class AreaDwscode implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 动态秤编码
     */
    private String dwsCode;

    /**
     * 线号
     */
    private String channel;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    private  String county;

    /**
     * 转运中心
     */
    private String centre;

    /**
     * 地区唯一编码
     */
    private String areaCode;

    /**
     * 非违禁品表名
     */
    private String tableName;

    /**
     * 违禁品表名
     */
    private String dangerTableName;

    //排序
    private Integer sort;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}