package com.niepan.modules.zhinenganjianback.lib.structure;

import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import static com.niepan.modules.zhinenganjianback.lib.NetSDKLib.MAX_PATH;

/**
 * @author 47081
 * @version 1.0
 * @description
 * @date 2021/2/22
 */
public class ObjectUrl extends NetSDKLib.SdkStructure {
    public byte[] objectUrl=new byte[MAX_PATH];
}
