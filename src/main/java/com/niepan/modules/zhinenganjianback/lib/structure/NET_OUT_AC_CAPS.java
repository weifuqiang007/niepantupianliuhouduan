package com.niepan.modules.zhinenganjianback.lib.structure;


import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;

/** 
* @author 291189
* @description  门禁AC服务协议与设备能力获取出参 
* @origin autoTool
* @date 2023/02/10 14:34:21
*/
public class NET_OUT_AC_CAPS extends NetSDKLib.SdkStructure {
/** 
结构体大小
*/
public			int					dwSize;
/** 
ACCaps能力集
*/
public			NET_AC_CAPS					stuACCaps=new NET_AC_CAPS();
/** 
user操作能力集
*/
public			NET_ACCESS_USER_CAPS					stuUserCaps=new NET_ACCESS_USER_CAPS();
/** 
card操作能力集
*/
public			NET_ACCESS_CARD_CAPS					stuCardCaps=new NET_ACCESS_CARD_CAPS();
/** 
信息操作能力集
*/
public			NET_ACCESS_FINGERPRINT_CAPS					stuFingerprintCaps=new NET_ACCESS_FINGERPRINT_CAPS();
/** 
目标操作能力集
*/
public			NET_ACCESS_FACE_CAPS					stuFaceCaps=new NET_ACCESS_FACE_CAPS();
/** 
眼睛相关能力集
*/
public			NET_ACCESS_IRIS_CAPS					stuIrisCaps=new NET_ACCESS_IRIS_CAPS();

public NET_OUT_AC_CAPS(){
		this.dwSize=this.size();
}
}