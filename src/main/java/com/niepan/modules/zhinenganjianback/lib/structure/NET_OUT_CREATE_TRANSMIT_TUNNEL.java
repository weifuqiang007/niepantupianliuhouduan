package com.niepan.modules.zhinenganjianback.lib.structure;

import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;

/**
 * CLIENT_CreateTransmitTunnel 接口输出参数
 */
public class NET_OUT_CREATE_TRANSMIT_TUNNEL extends NetSDKLib.SdkStructure {
	/**
	 * /< 结构体大小
	 */
	public int dwSize;
	/**
	 * /< 对上侦听端口
	 */
	public int nPort;
	/**
	 * 私有web代理访问协议 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_WEB_TUNNEL_PROTOCOL}
	 */
	public int emWebProtocol;
	/**
	 * 用户名
	 */
	public byte[] szUserName = new byte[128];
	/**
	 * 令牌
	 */
	public byte[] szTempToken = new byte[128];

	public NET_OUT_CREATE_TRANSMIT_TUNNEL() {
		this.dwSize = this.size();
	}

}