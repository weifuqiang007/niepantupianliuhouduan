package com.niepan.modules.zhinenganjianback.lib.structure;

import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;

/**
 * @author 291189
 * @version 1.0
 * @description  二维数组中的一维大小
 * @date 2021/9/9 10:38
 */
public class OneDimensionalInformation extends NetSDKLib.SdkStructure {
  public   byte[]  oneDimensionalSize=new byte[32];
}
