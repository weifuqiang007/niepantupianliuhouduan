package com.niepan.modules.zhinenganjianback.lib.structure;

import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;

/**
 * @author 251823
 * @description
 * @date 2023/02/17 19:39:19
 */
public class NET_POINT_EX extends NetSDKLib.SdkStructure {
	public short nx;
	public short ny;
	public NET_POINT_EX() {
	}
}