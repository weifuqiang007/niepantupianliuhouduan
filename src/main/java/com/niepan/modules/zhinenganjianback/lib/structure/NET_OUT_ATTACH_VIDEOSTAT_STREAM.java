package com.niepan.modules.zhinenganjianback.lib.structure;

import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;

/**
 * @author 251823
 * @description 订阅统计通道数据出参
 * @date 2022/12/12 11:39:32
 */
public class NET_OUT_ATTACH_VIDEOSTAT_STREAM extends NetSDKLib.SdkStructure {
	/**
	 * 此结构体大小
	 */
	public int dwSize;

	public NET_OUT_ATTACH_VIDEOSTAT_STREAM() {
		this.dwSize = this.size();
	}
}