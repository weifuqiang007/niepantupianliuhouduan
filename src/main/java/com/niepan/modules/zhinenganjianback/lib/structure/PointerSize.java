package com.niepan.modules.zhinenganjianback.lib.structure;

import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.sun.jna.Pointer;

/**
 * @author 291189
 * @version 1.0
 * @description
 * @date 2022/3/8 14:05
 */
public class PointerSize extends NetSDKLib.SdkStructure {
    public Pointer pointer;

}
