package com.niepan.modules.zhinenganjianback.service;

import com.niepan.modules.zhinenganjianback.model.BackStageManager;

/**
 * @author: liuchenyu
 * @date: 2023/7/18
 */
public interface BackstageManagerService {

    void addSidebar(BackStageManager backStageManager);
}
