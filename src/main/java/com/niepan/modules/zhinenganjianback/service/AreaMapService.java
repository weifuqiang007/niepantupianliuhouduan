package com.niepan.modules.zhinenganjianback.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.niepan.modules.zhinenganjianback.dto.AreaMap;

/**
* @author zq4568
* @description 针对表【area_map】的数据库操作Service
* @createDate 2023-08-18 15:19:28
*/
public interface AreaMapService extends IService<AreaMap> {

}
