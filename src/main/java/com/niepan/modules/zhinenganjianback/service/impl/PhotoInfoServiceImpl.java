package com.niepan.modules.zhinenganjianback.service.impl;

import com.niepan.modules.zhinenganjianback.dao.PhotoInfoDao;
import com.niepan.modules.zhinenganjianback.model.PhotoInfo;
import com.niepan.modules.zhinenganjianback.service.PhotoInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhotoInfoServiceImpl implements PhotoInfoService {

    @Autowired
    public PhotoInfoDao photoInfoDao;

    public Boolean insertPhotoInfo(PhotoInfo photoInfo){
        Integer result = photoInfoDao.Add(photoInfo);
        if(result == 0){
            return false;
        }else {
            return true;
        }

    }

}
