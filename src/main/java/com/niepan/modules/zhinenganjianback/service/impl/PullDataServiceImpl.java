package com.niepan.modules.zhinenganjianback.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.niepan.modules.zhinenganjianback.model.PullData;
import com.niepan.modules.zhinenganjianback.service.PullDataService;
import com.niepan.modules.zhinenganjianback.dao.PullDataMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class PullDataServiceImpl extends ServiceImpl<PullDataMapper, PullData> implements PullDataService{

    /**
     * 添加
     * @param pullData
     * @return
     */
    @Override
    public int insert(PullData pullData){
        return baseMapper.insert(pullData);
    }

    /**
     * 查询未推送的列表
     * @return
     */
    @Override
    public List<PullData> listByPushState() {
        LambdaQueryWrapper<PullData> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(PullData::getPullState,0);
        wrapper.last("limit 1000");
        return baseMapper.selectList(wrapper);
    }

    /**
     * 修改
     * @param item
     * @return
     */
    @Override
    public int updatePullState(PullData item){
        LambdaQueryWrapper<PullData> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(PullData::getImageId,item.getImageId());
        PullData record=new PullData();
        record.setPullState(item.getPullState());
        record.setPullTime(DateUtil.date());
        return baseMapper.update(record,wrapper);
    }
}




