package com.niepan.modules.zhinenganjianback.service.impl;

import com.niepan.modules.zhinenganjianback.VO.YwtkajVO;
import com.niepan.modules.zhinenganjianback.controller.DeviceManagerController;
import com.niepan.modules.zhinenganjianback.service.YwtkajService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class YwtkajServiceImpl implements YwtkajService {

    @Autowired
    private DeviceManagerController deviceManagerController;

    @Override
    public void sendImg(YwtkajVO ywtkajVO) {
        try {
            MultipartFile img = ywtkajVO.getImg();
            deviceManagerController.pushPictures(img);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
