package com.niepan.modules.zhinenganjianback.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.niepan.modules.zhinenganjianback.dao.AreaDwscodeMapper;
import com.niepan.modules.zhinenganjianback.dto.AreaDwscode;
import com.niepan.modules.zhinenganjianback.service.AreaDwscodeService;
import org.springframework.stereotype.Service;

/**
* @author zq4568
* @description 针对表【area_dwscode】的数据库操作Service实现
* @createDate 2023-08-16 17:12:33
*/
@Service
public class AreaDwscodeServiceImpl extends ServiceImpl<AreaDwscodeMapper, AreaDwscode>
    implements AreaDwscodeService {

    @Override
    public String selectByDwsCode(String dwsCode) {
        String dwscode =  baseMapper.selectByDwsCode(dwsCode);
        return dwscode;
    }
}




