package com.niepan.modules.zhinenganjianback.service;

import com.niepan.common.utils.R.ApiResponse;
import com.niepan.modules.sys.entity.SysUserEntity;
import com.niepan.modules.zhinenganjianback.VO.DangerValueVo;
import com.niepan.modules.zhinenganjianback.entity.SysDangerValue;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
* @author Administrator
* @description 针对表【sys_danger_value】的数据库操作Service
* @createDate 2023-11-08 17:09:01
*/
public interface SysDangerValueService extends IService<SysDangerValue> {

    /**
     * 新增和修改
     * @param user
     * @param dangerValueList
     * @return
     */
    ApiResponse<Boolean> insertOrUpdate(SysUserEntity user, DangerValueVo dangerValueList);

}
