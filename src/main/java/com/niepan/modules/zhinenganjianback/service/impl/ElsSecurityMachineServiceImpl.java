package com.niepan.modules.zhinenganjianback.service.impl;

import com.niepan.modules.zhinenganjianback.VO.JsonObjectVO;
import com.niepan.modules.zhinenganjianback.demo.DeviceManage;
import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
import com.niepan.modules.zhinenganjianback.service.ElsSecurityMachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

/**
 * @author: liuchenyu
 * @date: 2023/6/19
 */
@Service
public class ElsSecurityMachineServiceImpl implements ElsSecurityMachineService {

    @Autowired
    private DeviceManage deviceManage;

    @Override
    public Boolean getImgInfo(SecurityMachinePicture securityMachinePicture, String base64Image) {
        JsonObjectVO jsonObjectVO = new JsonObjectVO();
        jsonObjectVO.setDevice_sn(securityMachinePicture.getDevice_sn());
        jsonObjectVO.setLine_code(securityMachinePicture.getLine_code());
        jsonObjectVO.setName(securityMachinePicture.getName());
        //传来的时间是yyyy-MM-dd HH:mm:ss SSS 将其转化为时间戳
        String substring = securityMachinePicture.getTime().substring(0, securityMachinePicture.getTime().lastIndexOf("."));
        Timestamp timestamp = Timestamp.valueOf(substring);
        long time = timestamp.getTime();
        jsonObjectVO.setTime(time);
        jsonObjectVO.setCurrentTime(securityMachinePicture.getCurrentTime());
        jsonObjectVO.setImg_base64_side(base64Image);
        jsonObjectVO.setAjjTime(securityMachinePicture.getAjjTime());
        Boolean aBoolean = deviceManage.newPushPicture1(jsonObjectVO);
        return aBoolean;
    }
}
