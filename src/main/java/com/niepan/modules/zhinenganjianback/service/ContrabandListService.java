package com.niepan.modules.zhinenganjianback.service;

import java.util.List;
import java.util.Map;

/**
 * @author:liuchenyu
 * @data:2023/02/27
 */
public interface ContrabandListService {
    /**
     * 获取所有的地区
     * @return
     */
    List<Map<String, Object>> getArea();

    /**
     * 根据条件查询违禁品
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> getItem(String uniqueCode, String machineGrade, String machineBrand, String startTime, String endTime);

    /**
     * 根据条件查询违禁品的数量统计
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> getCount(String uniqueCode, String machineGrade, String machineBrand, String startTime, String endTime);

    /**
     * 根据唯一标识查询图片的详情
     * @param uuid
     * @return
     */
    List<Map<String, Object>> getDetail(String uuid);

    /**
     * 根据省市获取集散中心列表
     * @param uniqueCode
     * @return
     */
    List<Map<String, Object>> getCentre(String uniqueCode);

    /**
     * 获取总数以及违禁品的总数
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> getTotal(String uniqueCode, String machineGrade, String machineBrand, String startTime, String endTime);

    /**
     * 获取某个违禁品类下的所有违禁品信息
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @param typeId
     * @return
     */
    List<Map<String, Object>> getCountDetail(String uniqueCode, String machineGrade, String machineBrand, String startTime, String endTime, String typeId);


    /**
     * 获取违禁品的总数
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> getDangerTotal(String uniqueCode, String machineGrade, String machineBrand, String startTime, String endTime);

    /**
     * 根据条件查询每天的过包数
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> getCountByDay(String uniqueCode, String machineGrade, String machineBrand, String startTime, String endTime);

    /**
     * 根据条件获取信息的统计数（统计曲线图使用）
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> getCountStatistics(String uniqueCode, String machineGrade, String machineBrand, String startTime, String endTime);

    /**
     * 查询所有的图片信息
     * @return
     */
    List getPicture(String uniqueCode,String startTime,String endTime,String lineCode,Boolean isDanger);

    /**
     * 按照给定的时间查询此时间段内按时间段分割的数据
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> getCountByHour(String uniqueCode, String machineGrade, String machineBrand, String startTime, String endTime);

    /**
     * 获取卸货口
     * @param uniqueCode
     * @return
     */
    List<String>  getChannel(String uniqueCode);

    /**
     * 获取IP，sn配置表中的所有信息
     * @return
     */
    List<Map<String, Object>> getLineCode();

    String getReportData(String imageId);

    List getPictureTwice(String uniqueCode, String startTime, String endTime, String lineCode, Boolean isDanger);

    List<Map<String, Object>> getTotalTwice(String uniqueCode, String lineCode, String machineBrand, String startTime, String endTime);

    List<Map<String, Object>> getDangerTotalTwice(String uniqueCode, String lineCode, String machineBrand, String startTime, String endTime);
}
