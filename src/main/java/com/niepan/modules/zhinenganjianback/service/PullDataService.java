package com.niepan.modules.zhinenganjianback.service;

import com.niepan.modules.zhinenganjianback.model.PullData;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *
 */
public interface PullDataService extends IService<PullData> {

    /**
     * 添加
     * @param pullData
     * @return
     */
    int insert(PullData pullData);

    /**
     * 查询待推送的数据
     */
    List<PullData> listByPushState();

    /**
     * 修改
     * @param item
     * @return
     */
    int updatePullState(PullData item);
}
