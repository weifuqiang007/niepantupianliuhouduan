package com.niepan.modules.zhinenganjianback.service;

import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;

/**
 * @author: liuchenyu
 * @date: 2023/6/19
 */
public interface ElsSecurityMachineService {
    Boolean getImgInfo(SecurityMachinePicture securityMachinePicture, String base64Image);
}
