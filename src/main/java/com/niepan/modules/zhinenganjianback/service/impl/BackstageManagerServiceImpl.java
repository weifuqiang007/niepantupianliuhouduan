package com.niepan.modules.zhinenganjianback.service.impl;

import com.niepan.modules.zhinenganjianback.dao.BackstageManagerDao;
import com.niepan.modules.zhinenganjianback.model.BackStageManager;
import com.niepan.modules.zhinenganjianback.service.BackstageManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: liuchenyu
 * @date: 2023/7/18
 */

@Service
public class BackstageManagerServiceImpl implements BackstageManagerService {

    @Autowired
    BackstageManagerDao backstageManagerDao;

    @Override
    public void addSidebar(BackStageManager backStageManager) {

        backstageManagerDao.addSidebar();
    }
}
