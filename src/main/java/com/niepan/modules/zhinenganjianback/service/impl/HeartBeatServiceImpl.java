package com.niepan.modules.zhinenganjianback.service.impl;

import com.niepan.modules.zhinenganjianback.dao.HeartBeatDao;
import com.niepan.modules.zhinenganjianback.model.HeartBeat;
import com.niepan.modules.zhinenganjianback.service.HeartBeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HeartBeatServiceImpl implements HeartBeatService {

    @Autowired
    public HeartBeatDao heartBeatDao;

    public Boolean insertHeartBeat(HeartBeat heartBeat){
        Integer result = heartBeatDao.Add(heartBeat);
        if(result == 0){
            return false;
        }else {
            return true;
        }
    }
}
