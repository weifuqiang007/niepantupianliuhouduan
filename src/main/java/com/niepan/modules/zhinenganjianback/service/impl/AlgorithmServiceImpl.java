package com.niepan.modules.zhinenganjianback.service.impl;

import com.niepan.modules.zhinenganjianback.dao.AlgorithmDao;
import com.niepan.modules.zhinenganjianback.VO.AlgorithmVo;
import com.niepan.modules.zhinenganjianback.service.AlgorithmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AlgorithmServiceImpl implements AlgorithmService {

    @Autowired
    private AlgorithmDao algorithmDao;

    @Override
    public List<Map<String, Object>> list() {
        List<Map<String, Object>> list = algorithmDao.list();
        return list;
    }

    @Override
    public Integer addAlgorithm(AlgorithmVo algorithmVo) {
        Integer res = algorithmDao.addAlgorithm(algorithmVo);
        return res;
    }

    @Override
    public List<Map<String,Object>> deviceDetail() {
        List<Map<String, Object>> list = algorithmDao.deviceDetail();
        return list;
    }
}
