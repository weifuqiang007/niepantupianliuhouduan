package com.niepan.modules.zhinenganjianback.service;

import com.niepan.modules.zhinenganjianback.VO.JsonObjectVO;
import com.niepan.modules.zhinenganjianback.VO.ThresholdVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface DeviceManagerService {
    void InitialInterface();

    /**
     * 大华服务器初始化
     */
    void init();

    /**
     * 查看大华设备状态
     */
    void viewDeviceStatus();

    /**
     * 查询大华设备的阈值
     */
    List<Map<String,Object>> queryThreshold();

    /**
     * 设置大华设备物品的阈值（全部穿传）
     * @param thresholdVOList
     */
    void setThreshold(List<ThresholdVO> thresholdVOList);

    /**
     * 推送图片 将图片推送到大华的服务器上
     * @param file
     */
    void pushPictures(MultipartFile file);

    /**
     * 关闭大华所有的安检机
     */
    void exit();

    /**
     * 推送图片 将图片推送到大华的服务器上
     * @param jsonObjectVO
     */
    void pushPicture(JsonObjectVO jsonObjectVO);

    /**
     * 安检机扫描 x 光图片信息上报
     * @param file
     */
    void savePicture(MultipartFile file);

    /**
     * 安检机扫描 x 光图片信息上报
     */
    Integer savePicture(JsonObjectVO jsonObjectVO);
}
