package com.niepan.modules.zhinenganjianback.service.impl;


import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.niepan.common.utils.R.ResultStatusEnum;
import com.niepan.common.utils.logs.LogUtils;
import com.niepan.common.utils.redis.RedisUtils;
import com.niepan.modules.zhinenganjianback.VO.JsonObjectVO;
import com.niepan.modules.zhinenganjianback.dao.HeartBeatDao;
import com.niepan.modules.zhinenganjianback.dao.SecurityMachineDao;
import com.niepan.modules.zhinenganjianback.demo.DeviceManage;
import com.niepan.modules.zhinenganjianback.dto.GetHeartInfoDto;
import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
import com.niepan.modules.zhinenganjianback.model.SecurityMachineStatus;
import com.niepan.modules.zhinenganjianback.service.SecurityMachineService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class SecurityMachineServiceImpl implements SecurityMachineService {

    @Autowired
    private SecurityMachineDao securityMachineDao;

    @Autowired
    DeviceManage deviceManage;

    @Autowired
    private RedisUtils redisUtils;

    public static List<String> ips = new ArrayList<>();

    @Value("${external.imageCheckServer.requestSecretKey}")
    private String requestSecretKey;

    @Value("${external.imageCheckServer.msgHeart}")
    private String msgHeart;

    @Value("${external.imageCheckServer.requestCompanyId}")
    private String companyId;

    @Autowired
    private HeartBeatDao heartBeatDao;

    private List<Map<String,Object>> list = new ArrayList<>();

    private Map<String,Object> snAnddwsCode = new HashMap<>();

    @Override
    public Boolean getImgInfo(SecurityMachinePicture securityMachinePicture,String imgBase64Side) {
        JsonObjectVO jsonObjectVO = new JsonObjectVO();
        jsonObjectVO.setDevice_sn(securityMachinePicture.getDevice_sn());
        jsonObjectVO.setLine_code(securityMachinePicture.getLine_code());
        jsonObjectVO.setName(securityMachinePicture.getName());
        jsonObjectVO.setTime(Long.valueOf(securityMachinePicture.getTime()));
        jsonObjectVO.setCurrentTime(securityMachinePicture.getCurrentTime());
        jsonObjectVO.setImg_base64_side(imgBase64Side);
        Boolean aBoolean = deviceManage.newPushPicture(jsonObjectVO);

        if (null == aBoolean || false == aBoolean){
//            securityMachineDao.updateStatus(jsonObjectVO.getName());
        }
        return aBoolean;
    }

    /**
     * 安检机状态和故障信息上报
     * @param securityMachineStatus 安检机状态对象
     * @return
     */
    @Override
    public String SecurityMachineStatus(SecurityMachineStatus securityMachineStatus) {

        log.info(securityMachineStatus.getDevice_sn());
        log.info(securityMachineStatus.getLine_code());
        log.info(securityMachineStatus.getStatus_code());
        log.info(securityMachineStatus.getFault_level());

        log.info(securityMachineStatus.getStatus_desc());

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTimeFormat = formatter.format(new Date());

        securityMachineStatus.setMsgtime(currentTimeFormat);
        Integer res = securityMachineDao.securityMachineStatus(securityMachineStatus);

        if(res == 1 &&  "0".equals(securityMachineStatus.getFault_level())){
            LogUtils.info("安检机状态正常");
            LogUtils.info(formatter.format(new Date()) +"@#"+ securityMachineStatus.getDevice_sn() +"@#"+
                    securityMachineStatus.getFault_level() +"@#" + securityMachineStatus.getStatus_desc());
            return securityMachineStatus.getFault_level();
        }else if (res == 0){
            LogUtils.error("数据库插入异常");
            LogUtils.error(formatter.format(new Date()) +"@#"+ securityMachineStatus.getDevice_sn() +"@#"+
                    securityMachineStatus.getFault_level() +"@#" + securityMachineStatus.getStatus_desc());
            return  ResultStatusEnum.INSERT_ERROR_STATUS.getStatus().toString();
        } else {
            LogUtils.error("安检机状态异常");
            LogUtils.error(formatter.format(new Date()) +"@#"+ securityMachineStatus.getDevice_sn() +"@#"+
                    securityMachineStatus.getFault_level() +"@#" + securityMachineStatus.getStatus_desc());
        }
        return securityMachineStatus.getFault_level() ;
    }



    @Override
    public void getHeart(GetHeartInfoDto getHeartInfoDto) {
        if (list.isEmpty()){
            list = heartBeatDao.getInfo();
            for (Map<String, Object> stringObjectMap : list) {
                snAnddwsCode.put(stringObjectMap.get("sncode").toString(),stringObjectMap.get("dwscode").toString());
            }
        }
//        Jedis jedis = new Jedis("localhost", 6379);
//        jedis.set("heart","true");
        redisUtils.set("heart","true",-1);

        JSONObject data = new JSONObject();
        data.put("snCode", getHeartInfoDto.getDevice_sn());
        //DWS设备SN码
        data.put("dwsCode", snAnddwsCode.get(getHeartInfoDto.getDevice_sn()));
        //安检机图片ID, 目前无法采集到
        data.put("heartbeatTime", new Date());
        String json = data.toJSONString();
        String data_digest = DigestUtil.md5Hex(json+requestSecretKey);
        Map map = new HashMap<>();
        map.put("data",json);
        map.put("data_digest",data_digest);
        map.put("msg_type",msgHeart);
        map.put("company_id",companyId);
        log.info("map = " + map);

        String result2 = HttpRequest.post(DeviceManage.url)
                .header(Header.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=utf-8")//头信息，多个头信息多次调用此方法即可
                .form(map)//表单内容
                .timeout(500)//超时，毫秒
                .execute().body();
        LogUtils.info("向总部上传心跳 接口返回信息 : " + result2);
    }

    @Override
    public void getHeartELS(GetHeartInfoDto getHeartInfoDto) {
        String dwscode = heartBeatDao.getDwsCodeByAJJIP(getHeartInfoDto.getDevice_sn());
        redisUtils.set("heart","true",-1);

        JSONObject data = new JSONObject();
        data.put("snCode", getHeartInfoDto.getDevice_sn());
        //DWS设备SN码
        data.put("dwsCode", dwscode);
        //安检机图片ID, 目前无法采集到
        data.put("heartbeatTime", new Date());
        String json = data.toJSONString();
        String data_digest = DigestUtil.md5Hex(json+requestSecretKey);
        Map map = new HashMap<>();
        map.put("data",json);
        map.put("data_digest",data_digest);
        map.put("msg_type",msgHeart);
        map.put("company_id",companyId);
        log.info("map = " + map);
        String result2 = HttpRequest.post(DeviceManage.url)
                .header(Header.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=utf-8")//头信息，多个头信息多次调用此方法即可
                .form(map)//表单内容
                .timeout(500)//超时，毫秒
                .execute().body();
        LogUtils.info("向总部上传心跳 接口返回信息 : " + result2);
    }
}