package com.niepan.modules.zhinenganjianback.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.niepan.common.utils.ConfigConstant;
import com.niepan.common.utils.Constant;
import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.common.utils.R.ResultStatusEnum;
import com.niepan.modules.sys.entity.SysUserEntity;
import com.niepan.modules.sys.redis.SysDangerValueRedis;
import com.niepan.modules.sys.service.SysConfigService;
import com.niepan.modules.zhinenganjianback.VO.DangerValueVo;
import com.niepan.modules.zhinenganjianback.dao.SysDangerValueMapper;
import com.niepan.modules.zhinenganjianback.entity.SysDangerValue;
import com.niepan.modules.zhinenganjianback.service.SysDangerValueService;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
* @author Administrator
* @description 针对表【sys_danger_value】的数据库操作Service实现
* @createDate 2023-11-08 17:09:01
*/
@Service
public class SysDangerValueServiceImpl extends ServiceImpl<SysDangerValueMapper, SysDangerValue>
    implements SysDangerValueService{

    @Autowired
    private SysConfigService sysConfigService;
    @Autowired
    private SysDangerValueRedis dangerValueRedis;

    /**
     * 新增和修改
     * @param user
     * @param vo
     * @return
     */
    @Override
    @Transactional
    public ApiResponse<Boolean> insertOrUpdate(SysUserEntity user, DangerValueVo vo){
        if(StringUtils.isBlank(vo.getEnable())){
            return Result.error(ResultStatusEnum.FIELDS_ERROR_STATUS.getStatus(), "开启状态不能为空");
        }
        sysConfigService.updateValueByKey(ConfigConstant.DANGER_VALUE,vo.getEnable());
        if(vo.getEnable().equals("0")){
            return Result.success();
        }
        List<SysDangerValue> dangerValueList=vo.getList();
        if(CollectionUtil.isEmpty(dangerValueList)){
            return Result.error("保存数据不能为空",null);
        }
        for (SysDangerValue value:dangerValueList){
            if(StringUtils.isBlank(value.getId())){
                value.setCreateTime(DateUtil.date());
                value.setCreateName(user.getUsername());
                value.setDelFlag(Constant.DEL_FLAG_NO);
                baseMapper.insert(value);
            }else{
                SysDangerValue old= baseMapper.selectById(value.getId());
                if(Objects.isNull(old)){
                    value.setCreateTime(DateUtil.date());
                    value.setCreateName(user.getUsername());
                    value.setDelFlag(Constant.DEL_FLAG_NO);
                    baseMapper.insert(value);
                }
                old.setDangerValue(value.getDangerValue());
                old.setUpdateTime(DateUtil.date());
                old.setUpdateName(user.getUsername());
                baseMapper.updateById(old);
            }
        }
        dangerValueRedis.saveOrUpdate(dangerValueList);
        return Result.success();
    }


    /**
     * 获取违禁品阈值
     * @param dangerCode
     * @return
     */
    public Integer getDangerValue(Integer dangerCode){
        Integer dangerValue= dangerValueRedis.get(dangerCode);
        if(Objects.isNull(dangerValue)){
            List<SysDangerValue> list= baseMapper.selectList(null);
            if(CollectionUtil.isEmpty(list)){
                return 100;
            }
            dangerValueRedis.saveOrUpdate(list);
            SysDangerValue item=list.stream().filter(x->x.getDangerCode().equals(dangerCode)).findFirst().orElseGet(null);
            if(Objects.isNull(item)){
                return 100;
            }
            dangerValue=item.getDangerValue();
        }
        return dangerValue;
    }
}




