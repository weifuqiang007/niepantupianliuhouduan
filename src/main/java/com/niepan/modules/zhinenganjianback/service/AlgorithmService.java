package com.niepan.modules.zhinenganjianback.service;

import com.niepan.modules.zhinenganjianback.VO.AlgorithmVo;

import java.util.List;
import java.util.Map;

public interface AlgorithmService {
    List<Map<String, Object>> list();

    Integer addAlgorithm(AlgorithmVo algorithmVo);

    List<Map<String,Object>> deviceDetail();
}
