package com.niepan.modules.zhinenganjianback.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.niepan.modules.zhinenganjianback.dto.AreaCount;

/**
* @author zq4568
* @description 针对表【area_count】的数据库操作Service
* @createDate 2023-08-16 17:12:33
*/
public interface AreaCountService extends IService<AreaCount> {

    int insertData(AreaCount areaCount);

    String dayAndAreaCode(String areaCode);

    int updateDangerCount(Integer integer);

    int updateNoDangerCount(Integer integer);
}
