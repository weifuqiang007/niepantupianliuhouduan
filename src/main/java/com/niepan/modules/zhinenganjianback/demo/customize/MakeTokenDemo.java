package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.*;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

import static com.niepan.modules.zhinenganjianback.lib.enumeration.NET_EM_CFG_OPERATE_TYPE.NET_EM_CFG_NAS_DIRECTORY;

/**
 * @author 291189
 * @version 1.0
 * @description GIP220926008 GIP220919028 
 * @date 2022/10/12 11:02
 */
public class MakeTokenDemo extends Initialization {

    //获取token
    public void makeToken(){

        NET_IN_MAKE_TOKEN input=new NET_IN_MAKE_TOKEN();

        String szUUID= UUID.randomUUID().toString();
        StringToByteArr(szUUID,input.szUUID,encode);
        System.out.println("szUUID:"+szUUID);
        input.nKeepLiveTime=1000;
        Pointer pointerInput= new Memory(input.size());
        pointerInput.clear(input.size());

        ToolKits.SetStructDataToPointer(input,pointerInput,0);

        NET_OUT_MAKE_TOKEN output=new NET_OUT_MAKE_TOKEN();
        Pointer pointerOutput= new Memory(output.size());
        pointerOutput.clear(output.size());

        ToolKits.SetStructDataToPointer(output,pointerOutput,0);

        boolean b
                = netSdk.CLIENT_MakeToken(loginHandle,pointerInput,pointerOutput,3000 );
        if (!b) {
            System.out.println("CLIENT_MakeToken  Config Failed!" + ToolKits.getErrorCode());
            return;
        } else {
            System.out.println("CLIENT_MakeToken  Config Succeed!" );
        }

        ToolKits.GetPointerData(pointerOutput,output);
        int nToken = output.nToken;
        System.out.println("nToken:"+nToken);
    }

//获取共享文件夹工作目录信息
    public void getNASDirectoryInfo(){

        NET_IN_NAS_DIRECTORY_GET_INFO input=new NET_IN_NAS_DIRECTORY_GET_INFO();

        String szName="dir1";

        StringToByteArr(szName,input.szName,encode);

        Pointer pointerInput= new Memory(input.size());
        pointerInput.clear(input.size());

        ToolKits.SetStructDataToPointer(input,pointerInput,0);


        NET_OUT_NAS_DIRECTORY_GET_INFO outPut=new NET_OUT_NAS_DIRECTORY_GET_INFO();
        Pointer pointerOutPut= new Memory(outPut.size());
        pointerOutPut.clear(outPut.size());

        ToolKits.SetStructDataToPointer(outPut,pointerOutPut,0);


        boolean b
                = netSdk.CLIENT_GetNASDirectoryInfo(loginHandle, pointerInput, pointerOutPut, 3000);


        if (!b) {
            System.out.println("CLIENT_GetNASDirectoryInfo  Config Failed!" + ToolKits.getErrorCode());
            return;
        } else {
            System.out.println("CLIENT_GetNASDirectoryInfo  Config Succeed!" );
        }

        ToolKits.GetPointerData(pointerOutPut,outPut);
        /**
         NAS状态 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_NAS_STATE_TYPE}
         */
        int emState = outPut.emState;

        System.out.println("emState:"+emState );

            /**
             剩余空间, 单位MB
             */
        int nFreeSpace = outPut.nFreeSpace;

        System.out.println("nFreeSpace:"+nFreeSpace );

        /**
         总空间, 单位MB
         */
        int nTotalSpace = outPut.nTotalSpace;
        System.out.println("nTotalSpace:"+nTotalSpace );

    }


//根据文件路径获取外部导入文件信息
    public void GetFileManagerExFileInfoByPath(){
        NET_IN_GET_FILE_INFO_BY_PATH_INFO input =new NET_IN_GET_FILE_INFO_BY_PATH_INFO();

        String szPath="dir1/aa/d.mp4";

        StringToByteArr(szPath,input.szPath,encode);

        Pointer pointerInput= new Memory(input.size());
        pointerInput.clear(input.size());

        ToolKits.SetStructDataToPointer(input,pointerInput,0);

        NET_OUT_GET_FILE_INFO_BY_PATH_INFO outPut=new NET_OUT_GET_FILE_INFO_BY_PATH_INFO();

        Pointer pointerOutPut= new Memory(outPut.size());
        pointerOutPut.clear(outPut.size());

        ToolKits.SetStructDataToPointer(outPut,pointerOutPut,0);

        boolean b
                = netSdk.CLIENT_GetFileManagerExFileInfoByPath(loginHandle, pointerInput, pointerOutPut, 3000);

        if (!b) {
            System.out.println("CLIENT_GetFileManagerExFileInfoByPath  Config Failed!" + ToolKits.getErrorCode());
            return;
        } else {
            System.out.println("CLIENT_GetFileManagerExFileInfoByPath  Config Succeed!" );
        }

        ToolKits.GetPointerData(pointerOutPut,outPut);
/**
 文件上传状态 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_FILE_UPDATE_STATE}
 */
        int emState = outPut.emState;
        System.out.println("emState:"+emState );
            /**
             文件开始时间
             */
        NET_TIME_EX stuStartTime = outPut.stuStartTime;
        System.out.println("stuStartTime:"+stuStartTime );

        NET_TIME_EX stuEndTime = outPut.stuEndTime;
        System.out.println("stuEndTime:"+stuEndTime );
        /**
         文件上传进度
         */
        int nProgress = outPut.nProgress;
        System.out.println("nProgress:"+nProgress );

        /**
         当前分片(切片上传模式所需数据)
         */
        int nCurrentSlice = outPut.nCurrentSlice;
        System.out.println("nCurrentSlice:"+nCurrentSlice );
        /**
         文件大小
         */
        long nFileLength = outPut.nFileLength;
        System.out.println("nFileLength:"+nFileLength );

        int nReason = outPut.nReason;
        System.out.println("nReason:"+nReason );

    }

    //共享文件夹信息配置
    public void nasDirectory(){
        NET_CFG_NAS_DIRECTORY  cfgNasDirectory=new NET_CFG_NAS_DIRECTORY();
        /**
         需要获取到的NAS配置数量
         */
        cfgNasDirectory.nNASCfgGetNum=10;

        NAS_DIRECTORY_CFG_INFO[]  infos=new NAS_DIRECTORY_CFG_INFO[cfgNasDirectory.nNASCfgGetNum];

        //初始化
            for(int i=0;i<infos.length;i++){
                infos[i]=new NAS_DIRECTORY_CFG_INFO();
            }

        cfgNasDirectory.pNASDirectoryInfo  =new Memory(infos[0].size()* cfgNasDirectory.nNASCfgGetNum);
        cfgNasDirectory.pNASDirectoryInfo.clear(infos[0].size()* cfgNasDirectory.nNASCfgGetNum);

        ToolKits.SetStructArrToPointerData(infos,cfgNasDirectory.pNASDirectoryInfo);


        Pointer  info= new Memory(cfgNasDirectory.size());

        info.clear(cfgNasDirectory.size());

        ToolKits.SetStructDataToPointer(cfgNasDirectory,info,0);

        boolean b=netSdk.CLIENT_GetConfig(loginHandle, NET_EM_CFG_NAS_DIRECTORY, 0, info,cfgNasDirectory.size() , 3000, null);

        if (!b) {
            System.out.println("CLIENT_GetConfig  Config Failed!" + ToolKits.getErrorCode());
            return;
        } else {
            System.out.println("CLIENT_GetConfig  Config Succeed!" );
        }

        ToolKits.GetPointerData(info,cfgNasDirectory);

        int nNASCfgRealNum = cfgNasDirectory.nNASCfgRealNum;

        System.out.println("nNASCfgRealNum:"+nNASCfgRealNum);


        ToolKits.GetPointerDataToStructArr(cfgNasDirectory.pNASDirectoryInfo,infos);

        for(int i=0;i<nNASCfgRealNum;i++){

            NAS_DIRECTORY_CFG_INFO info1 = infos[i];

            try {
                System.out.println("szname:"+new String(info1.szName,encode));

                System.out.println("szGroupName:"+new String(info1.szGroupName,encode));

                System.out.println("szMemo:"+new String(info1.szMemo,encode));



                int nUserNameRealNum = info1.nUserNameRealNum;

                System.out.println("nUserNameRealNum:"+nUserNameRealNum);

                BYTE_32[] szUserName = info1.szUserName;
                for(int j=0;j<nUserNameRealNum;j++){
                    System.out.println("szUserName["+i+"]["+j+"]:"+new String(szUserName[j].SN_32,encode));

                }
                /**
                 共享用户的读写权限实际有效数量
                 */
                int nUserAuthRealNum = info1.nUserAuthRealNum;
                System.out.println("nUserAuthRealNum:"+nUserAuthRealNum);
                /**
                 共享用户的读写权限, 是一个数组, 下标与用户名对应:1 可读, 2 可新建, 4 可删除
                 */
                int[] nUserAuth = info1.nUserAuth;
                for(int j=0;j<nUserAuthRealNum;j++){
                    System.out.println("nUserAuth["+i+"]["+j+"]:"+nUserAuth[j]);

                }
                /**
                 有效IP
                 */
                byte[] szValidIP = info1.szValidIP;
                System.out.println("szValidIP:"+new String(szValidIP,encode));
                /**
                 总容量(单位M), 总容量, 创建卷时要用到. 只在新增时可以添加, 后续不允许修改
                 */
                int nTotalSpace = info1.nTotalSpace;
                System.out.println("nTotalSpace:"+nTotalSpace);
                /**
                 设置虚拟磁盘最小的块大小, 仅限于IPSAN使用, 单位字节.默认4096字节,必须是512的整数倍
                 */
                int nBlockSize = info1.nBlockSize;
                System.out.println("nBlockSize:"+nBlockSize);

                /**
                 共享类型 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_CFG_NAS_DIRECTORY_PROTOCAL}
                 */
                int emProtocal = info1.emProtocal;
                System.out.println("emProtocal:"+emProtocal);

                /**
                 缓存类型 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_CFG_NAS_DIRECTORY_CACHE_TYPE}
                 */
                int emCacheType = info1.emCacheType;
                System.out.println("emCacheType:"+emCacheType);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

    }

    public static void StringToByteArr(String src, byte[] dst,String encode) {
        try {
            byte[] GBKBytes = src.getBytes(encode);
            for (int i = 0; i < GBKBytes.length; i++) {
                dst[i] = (byte) GBKBytes[i];
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "获取token" , "makeToken")));
        menu.addItem((new CaseMenu.Item(this , "根据文件路径获取外部导入文件信息" , "GetFileManagerExFileInfoByPath")));
        menu.addItem((new CaseMenu.Item(this , "获取共享文件夹工作目录信息" , "getNASDirectoryInfo")));
        menu.addItem((new CaseMenu.Item(this , "共享文件夹信息配置" , "nasDirectory")));

        menu.run();
    }

    public static void main(String[] args) {
        MakeTokenDemo makeTokenDemo=new MakeTokenDemo();
        InitTest("172.12.10.47",37777,"admin","admin123");
        makeTokenDemo.RunTest();
        LoginOut();

    }
}
