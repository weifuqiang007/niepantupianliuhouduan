package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_CFG_THERMOMETRY_SHIELD_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.THERMOMETRY_SHIELD_INFO;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import java.io.UnsupportedEncodingException;

import static com.niepan.modules.zhinenganjianback.lib.enumeration.NET_EM_CFG_OPERATE_TYPE.NET_EM_CFG_THERMOMETRY_SHIELD;


/**
 * @author 291189
 * @version 1.0
 * @description ERR221024049
 * @date 2022/11/7 10:57
 */
public class ThermometryShieldDemo extends Initialization {

    int nChannelID=0;
    NET_CFG_THERMOMETRY_SHIELD_INFO msg=new NET_CFG_THERMOMETRY_SHIELD_INFO();
    public void getThermometry() {

        Pointer pstuConfigInfo=new Memory(msg.size());
        pstuConfigInfo.clear(msg.size());
        ToolKits.SetStructDataToPointer(msg, pstuConfigInfo, 0);

        boolean gRet = netSdk.CLIENT_GetConfig(loginHandle, NET_EM_CFG_THERMOMETRY_SHIELD, nChannelID, pstuConfigInfo, msg.size(), 3000, null);

        if(gRet){
            System.out.println("CLIENT_GetConfig success");
            ToolKits.GetPointerDataToStruct(pstuConfigInfo,0,msg);
            Native.free(Pointer.nativeValue(pstuConfigInfo)); //清理内存
            Pointer.nativeValue(pstuConfigInfo, 0);

        }else {
            System.err.printf("CLIENT_GetConfig  failed, ErrCode=%x\n", netSdk.CLIENT_GetLastError());
                    return;
        }
        int nRealInfoNumOfEachChennel
                = msg.nRealInfoNumOfEachChennel;
        System.out.println("nRealInfoNumOfEachChennel:"+nRealInfoNumOfEachChennel);
        THERMOMETRY_SHIELD_INFO[] stuThermometryShieldInfo
                = msg.stuThermometryShieldInfo;

        for(int i=0;i<nRealInfoNumOfEachChennel;i++){
            THERMOMETRY_SHIELD_INFO info = stuThermometryShieldInfo[i];
            System.out.println("["+i+"]:");
            /**
             屏蔽使能, 1开启, 0关闭
             */
            System.out.println("bEnable:"+info.bEnable);
            /**
             屏蔽区编号
             */
            System.out.println("nShieldId:"+info.nShieldId);
            try {
                /**
                 自定义名称
                 */
                System.out.println("szName:"+new String(info.szName,encode) );
                /**
                 坐标点
                 */
                System.out.println("stuRect:"+info.stuRect.toString() );

                /**
                 预置点编号
                 */
                System.out.println("nPresetId:"+info.nPresetId);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }

    }


    public void setThermometry() {

        msg.nRealInfoNumOfEachChennel=1;

        THERMOMETRY_SHIELD_INFO info
                = msg.stuThermometryShieldInfo[0];
            info.bEnable=1;

            String name="test0001";
         byte[] szName
                = info.szName;
         StringToByteArr(name,szName,encode);

        Pointer pstuConfigInfo=new Memory(msg.size());
        pstuConfigInfo.clear(msg.size());
        ToolKits.SetStructDataToPointer(msg, pstuConfigInfo, 0);
        // 设置，在获取的基础上设置
        IntByReference restart = new IntByReference(0);
        boolean gSet
                = netSdk.CLIENT_SetConfig(loginHandle, NET_EM_CFG_THERMOMETRY_SHIELD, nChannelID, pstuConfigInfo, msg.size(), 3000, restart, null);

        if(gSet){
            System.out.println("CLIENT_SetConfig success");
            ToolKits.GetPointerDataToStruct(pstuConfigInfo,0,msg);
            Native.free(Pointer.nativeValue(pstuConfigInfo)); //清理内存
            Pointer.nativeValue(pstuConfigInfo, 0);

        }else {
            System.err.printf("CLIENT_SetConfig  failed, ErrCode=%x\n", netSdk.CLIENT_GetLastError());
            return;
        }


    }

    public static void StringToByteArr(String src, byte[] dst,String encode) {
        try {
            byte[] GBKBytes = src.getBytes(encode);
            for (int i = 0; i < GBKBytes.length; i++) {
                dst[i] = (byte) GBKBytes[i];
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();
        menu.addItem(new CaseMenu.Item(this , "获取测温屏蔽区配置" , "getThermometry"));

        menu.addItem(new CaseMenu.Item(this , "设置测温屏蔽区配置" , "setThermometry"));
        menu.run();
    }

    public static void main(String[]args)
    {

        ThermometryShieldDemo demo = new ThermometryShieldDemo();
        InitTest("10.35.247.130",37777,"admin","admin123");
        demo.RunTest();
        LoginOut();
    }
}
