package com.niepan.modules.zhinenganjianback.demo.util;

public interface Testable {
	void initTest();
	
	void runTest() throws InterruptedException;
	
	void endTest();
}
