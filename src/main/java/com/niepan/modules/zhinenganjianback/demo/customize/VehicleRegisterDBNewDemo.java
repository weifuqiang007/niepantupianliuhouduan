package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.*;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;

import java.io.UnsupportedEncodingException;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Scanner;


/**
 * @author 291189
 * @version 1.0
 * @description ERR221009143 
 * @date 2022/10/24 9:45
 */
public class VehicleRegisterDBNewDemo extends Initialization {

    //创建车辆组
    public void CLIENT_CreateGroupForVehicleRegisterDB(){
        //入参
        NET_IN_CREATE_GROUP_FOR_VEHICLE_REG_DB inparam=new NET_IN_CREATE_GROUP_FOR_VEHICLE_REG_DB();

        String groupName="one";
        String GroupDetail="第一个分组";


        try {
            System.arraycopy(groupName.getBytes(encode),0,inparam.szGroupName,0,groupName.getBytes(encode).length);

            System.arraycopy(GroupDetail.getBytes(encode),0,inparam.szGroupDetail,0,GroupDetail.getBytes(encode).length);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        inparam.nGroupType=0;

        int pInParamSize=inparam.size();
        Pointer pInParam =new Memory(pInParamSize);
        ToolKits.SetStructDataToPointer(inparam, pInParam, 0);

        //出参
        NET_OUT_CREATE_GROUP_FOR_VEHICLE_REG_DB outParam=new NET_OUT_CREATE_GROUP_FOR_VEHICLE_REG_DB();
        int poutParamSize=outParam.size();
        Pointer poutParam =new Memory(poutParamSize);
        ToolKits.SetStructDataToPointer(outParam, poutParam, 0);


        boolean isCreate
                = netSdk.CLIENT_CreateGroupForVehicleRegisterDB(loginHandle, pInParam, poutParam, 3000);
        String id="";
        if(isCreate){

            ToolKits.GetPointerDataToStruct(poutParam,0,outParam);

            byte[] szGroupID = outParam.szGroupID;
            id= new String(szGroupID);
            System.out.println("CLIENT_CreateGroupForVehicleRegisterDB success ");
            System.out.println("szGroupID:"+id);


        }else {
            System.err.println(" CLIENT_CreateGroupForVehicleRegisterDB fail :"+ToolKits.getErrorCode());
        }
    }
    // 删除车辆组
    public void CLIENT_DeleteGroupFromVehicleRegisterDB(){
        Scanner scanner=new Scanner(System.in);
        //入参
        NET_IN_DELETE_GROUP_FROM_VEHICLE_REG_DB inparam=new NET_IN_DELETE_GROUP_FROM_VEHICLE_REG_DB();
        System.out.println("输入要删除车辆组的标识 为空时表示删除全部车辆组");
        String next = scanner.nextLine();
        String groupID=next.trim();

        if(!"".equals(groupID)){
            try {
                System.arraycopy(groupID.getBytes(encode),0,inparam.szGroupID,0,groupID.getBytes(encode).length);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }



        int pInParamSize=inparam.size();

        Pointer pInParam =new Memory(pInParamSize);

        ToolKits.SetStructDataToPointer(inparam, pInParam, 0);

        //出参
        NET_OUT_DELETE_GROUP_FROM_VEHICLE_REG_DB outParam=new NET_OUT_DELETE_GROUP_FROM_VEHICLE_REG_DB();

        int pOutParamSize=outParam.size();

        Pointer pOutParam =new Memory(pOutParamSize);

        ToolKits.SetStructDataToPointer(outParam, pOutParam, 0);

        boolean isDelete = netSdk.CLIENT_DeleteGroupFromVehicleRegisterDB(loginHandle, pInParam, pOutParam, 3000);

        if(isDelete){
            System.out.println(" delete is success");
        }else {
            System.err.println(" delete is fail:"+ToolKits.getErrorCode());
        }
    }

    /**
     * 修改车辆组
     */
 public void CLIENT_ModifyGroupForVehicleRegisterDB(){
     Scanner scanner=new Scanner(System.in);
     NET_IN_MODIFY_GROUP_FOR_VEHICLE_REG_DB inPut=new NET_IN_MODIFY_GROUP_FOR_VEHICLE_REG_DB();


     NET_VEHICLE_GROUP_INFO stuGroupInfo = inPut.stuGroupInfo;


     System.out.println("输入要修改车辆组的Id");
     String next = scanner.next();
     // 车辆组标识
     String szGroupID=next.trim();

     System.out.println("输入要修改车辆组类型,0:未知 1:禁止列表 2：允许列表");
     stuGroupInfo.nGroupType=scanner.nextInt();
     System.out.println("输入要修改车辆组名称");

     String  szGroupName=scanner.next();

     System.out.println("输入要修改车辆组备注信息");

     String szGroupDetail=scanner.next();

     try {
         System.arraycopy(szGroupID.getBytes(encode),0,  stuGroupInfo.szGroupID,0,szGroupID.getBytes(encode).length);

         System.arraycopy(szGroupName.getBytes(encode),0,  stuGroupInfo.szGroupName,0,szGroupName.getBytes(encode).length);

         System.arraycopy(szGroupDetail.getBytes(encode),0,  stuGroupInfo.szGroupDetail,0,szGroupDetail.getBytes(encode).length);

     } catch (UnsupportedEncodingException e) {
         e.printStackTrace();
     }

     Pointer inputPointer = new Memory(inPut.size());
     inputPointer.clear(inPut.size());
     ToolKits.SetStructDataToPointer(inPut,inputPointer,0);


     NET_OUT_MODIFY_GROUP_FOR_VEHICLE_REG_DB outPut=new NET_OUT_MODIFY_GROUP_FOR_VEHICLE_REG_DB();

     Pointer outPutPointer = new Memory(outPut.size());
     outPutPointer.clear(outPut.size());
     ToolKits.SetStructDataToPointer(outPut,outPutPointer,0);

     boolean isModify = netSdk.CLIENT_ModifyGroupForVehicleRegisterDB(loginHandle, inputPointer, outPutPointer, 3000);
     if(isModify){
         System.out.println(" CLIENT_ModifyGroupForVehicleRegisterDB is success");
     }else {
         System.err.println(" CLIENT_ModifyGroupForVehicleRegisterDB is fail:"+ToolKits.getErrorCode());
     }
 }

    /**
     * 查找车辆组信息
     */
     public void CLIENT_FindGroupFormVehicleRegisterDB(){

         Scanner scanner=new Scanner(System.in);
         NET_IN_FIND_GROUP_FROM_VEHICLE_REG_DB inPut=new NET_IN_FIND_GROUP_FROM_VEHICLE_REG_DB();

        // 组ID，为空表示查找全部车辆组信息

         System.out.println("输入要查询车辆组的Id,为空表示查找全部车辆组信息");
         String next = scanner.nextLine();
         String groupID=next.trim();

         if(!"".equals(groupID)){
             try {
                 System.arraycopy(groupID.getBytes(encode),0,inPut.szGroupID,0,groupID.getBytes(encode).length);
             } catch (UnsupportedEncodingException e) {
                 e.printStackTrace();
             }
         }


         Pointer inputPointer = new Memory(inPut.size());
         inputPointer.clear(inPut.size());
         ToolKits.SetStructDataToPointer(inPut,inputPointer,0);


         NET_OUT_FIND_GROUP_FROM_VEHICLE_REG_DB outPut=new NET_OUT_FIND_GROUP_FROM_VEHICLE_REG_DB();
         Pointer outPutPointer = new Memory(outPut.size());
         outPutPointer.clear(outPut.size());
         ToolKits.SetStructDataToPointer(outPut,outPutPointer,0);


         boolean b = netSdk.CLIENT_FindGroupFormVehicleRegisterDB(loginHandle, inputPointer, outPutPointer, 3000);
         if(b){
             System.out.println(" CLIENT_FindGroupFormVehicleRegisterDB is success");
         }else {
             System.err.println(" CLIENT_FindGroupFormVehicleRegisterDB is fail:"+ToolKits.getErrorCode());
         }

         ToolKits.GetPointerData(outPutPointer,outPut);

         int nGroupListNum = outPut.nGroupListNum;
         NET_VEHICLE_GROUP_FOUND_INFO[] stuGroupList = outPut.stuGroupList;

         for(int i=0;i<nGroupListNum;i++){
             NET_VEHICLE_GROUP_FOUND_INFO info = stuGroupList[i];

             byte[] szGroupID1 = info.szGroupID;
             byte[] szGroupName = info.szGroupName;
             byte[] szGroupDetail = info.szGroupDetail;
           //  车辆组类型，0:未知 1:禁止列表 2：允许列表
             int nGroupType = info.nGroupType;
             System.out.println("nGroupType:"+nGroupType);

             try {
                 System.out.println("szGroupID1:"+new String(szGroupID1,encode));
                 System.out.println("szGroupName:"+new String(szGroupName,encode));
                 System.out.println("szGroupDetail:"+new String(szGroupDetail,encode));
             } catch (UnsupportedEncodingException e) {
                 e.printStackTrace();
             }


             int nGroupSize = info.nGroupSize;
             System.out.println("nGroupSize:"+nGroupSize);

             int nChannelsNum = info.nChannelsNum;
             System.out.println("nChannelsNum:"+nChannelsNum);

             int[] nChannels = info.nChannels;
             for(int j=0;j<nChannelsNum;j++){
                 System.out.println("nChannels["+j+"]:"+nChannels[j]);
             }
         }
     }

    /**
     *向车牌库添加车辆信息
     */
    public void CLIENT_MultiAppendToVehicleRegisterDB() throws UnsupportedEncodingException {
        Scanner scanner=new Scanner(System.in);
        //入参
        NET_IN_MULTI_APPEND_TO_VEHICLE_REG_DB inparam=new NET_IN_MULTI_APPEND_TO_VEHICLE_REG_DB();

        inparam.nVehicleNum=10; // 车辆个数
        // Memory memory = new Memory(info.size() * inparam.nVehicleNum);

        //设置车辆数组对象。
        NetSDKLib.NET_VEHICLE_INFO[] infos = new  NetSDKLib.NET_VEHICLE_INFO[inparam.nVehicleNum];

        LinkedHashSet<Integer> randomSet
                = getRandomSet(infos.length, 1000000000);
        Integer[]  ranArr=new Integer[randomSet.size()];
        Integer[] integers
                = randomSet.toArray(ranArr);
        System.out.println("输入车辆所属组ID（注意该groupID需在设备已存在） ");
        String  groupID
                = scanner.next().trim();
        for(int i=0;i<inparam.nVehicleNum;i++){
            NetSDKLib.NET_VEHICLE_INFO info=new NetSDKLib.NET_VEHICLE_INFO();
            info.nUID= integers[i];
        //    String groupID="6";//车辆所属组ID（注意该groupID需在设备已存在）

            System.arraycopy(groupID.getBytes(encode),0,info.szGroupID,0,groupID.getBytes(encode).length);
            String   groupName="one";// 车辆所属组名
            System.arraycopy(groupName.getBytes(encode),0,info.szGroupName,0,groupName.getBytes(encode).length);
            int pla=12345+i;
            String  plateNumber="浙A"+pla;//车牌号码（号码牌不重复）
            System.arraycopy(plateNumber.getBytes(encode),0,info.szPlateNumber,0,plateNumber.getBytes(encode).length);
            String  plateCountry="CN";
            System.arraycopy(plateCountry.getBytes(encode),0,info.szPlateCountry,0,plateCountry.getBytes(encode).length);
            info.nPlateType=1; // 车牌类型
            info.nVehicleType=6;  // 车型(轿车、卡车等)
            info.nBrand=10; //车辆车标,需要通过映射表得到真正的车标.同卡口事件的CarLogoIndex
            info.nCarSeries=1005; //车辆子品牌，需要通过映射表得到真正的子品牌,同卡口事件的SubBrand
            info.nCarSeriesModelYearIndex=12; // 车辆品牌年款，需要通过映射表得到真正的年款，同卡口事件的BrandYear 车头年款序号范围1~999；车尾年款序号范围1001~1999；0表示未知；1000预留。

            // 车色 第一个元素表示红色分量值； 第二个元素表示绿色分量值； 第三个元素表示蓝色分量值； 第四个元素表示透明度分量(无意义)
            info.stuVehicleColor.nRed = 128;
            info.stuVehicleColor.nGreen = 128;
            info.stuVehicleColor.nBlue = 128;
            info.stuVehicleColor.nAlpha = 255;

            // 车牌颜色,规则同车色
            info.stuPlateColor.nRed = 128;
            info.stuPlateColor.nGreen = 128;
            info.stuPlateColor.nBlue = 128;
            info.stuPlateColor.nAlpha = 255;

            String  ownerName="TOM";//车主名称
            System.arraycopy(ownerName.getBytes(encode),0,info.szOwnerName,0,ownerName.getBytes(encode).length);

            info.nSex=1;

            info.nCertificateType = 0;

            long id=123456001+i;

            String personID=id+"";// 人员证件号码,工号,或其他编号

            System.arraycopy(personID.getBytes(encode),0,info.szPersonID,0,personID.getBytes(encode).length);

            String ownerCountry="CN";// 车主国籍,2字节,符合ISO3166规范

            System.arraycopy(ownerCountry.getBytes(encode),0,info.szOwnerCountry,0,ownerCountry.getBytes(encode).length);

            String    province="ZHEJIANG";//省份

            System.arraycopy(province.getBytes(encode),0,info.szProvince,0,province.getBytes(encode).length);

            String city="HANGZHOU";

            System.arraycopy(city.getBytes(encode),0,info.szCity,0,city.getBytes(encode).length);

            String  homeAddress="BINANLU1188";

            System.arraycopy(homeAddress.getBytes(encode),0,info.szHomeAddress,0,homeAddress.getBytes(encode).length);

            long e= 123456782l+i;
            String email= e+"@qq.com";

            System.arraycopy(email.getBytes(encode),0,info.szEmail,0,email.getBytes(encode).length);

            long p=12345678911l+i;
            String  phoneNo=""+p; // 注册车主电话号码
            System.arraycopy(phoneNo.getBytes(encode),0,info.szPhoneNo,0,phoneNo.getBytes(encode).length);
            System.out.println("info:"+new String(info.szPlateNumber,encode)+info.toString());
            infos[i]=info;
        }
        //将车辆数组对象赋值。
        inparam.stuVehicleInfo = new Memory(infos[0].size() * inparam.nVehicleNum);
        inparam.stuVehicleInfo.clear(infos[0].size() * inparam.nVehicleNum);
        ToolKits.SetStructArrToPointerData(infos, inparam.stuVehicleInfo);


        inparam.bReplace=0;


        int pInParamSize=inparam.dwSize;
        Pointer pInParam =new Memory(pInParamSize);
        pInParam.clear(pInParamSize);
        ToolKits.SetStructDataToPointer(inparam, pInParam, 0);

        //出参
        NET_OUT_MULTI_APPEND_TO_VEHICLE_REG_DB outParam=new NET_OUT_MULTI_APPEND_TO_VEHICLE_REG_DB();

        outParam.nErrCodeNum=10;
        int pOutParamSize=outParam.size();

        Pointer pOutParam =new Memory(pOutParamSize);
        pOutParam.clear(pOutParamSize);
        ToolKits.SetStructDataToPointer(outParam, pOutParam, 0);

        try {
            boolean isAdd = netSdk.CLIENT_MultiAppendToVehicleRegisterDB(loginHandle, pInParam, pOutParam, 3000);

            if(isAdd){
                System.out.println(" add is success");

                ToolKits.GetPointerData(pOutParam,outParam);

                int nErrCodeNum = outParam.nErrCodeNum;
                System.out.println("nErrCodeNum:"+nErrCodeNum);

                int nRetErrCodeNum = outParam.nRetErrCodeNum;
                System.out.println("nRetErrCodeNum:"+nRetErrCodeNum);
                Pointer ems = outParam.emErrCode;

                int[] emErrCode=new int[1000];

                ems.read(0,emErrCode,0,emErrCode.length);

                for(int i=0;i<nErrCodeNum;i++){
                    System.out.println("emErrCode["+i+"]:"+emErrCode[i]);
                }
            }else {
                System.err.println(" add is fail:"+ToolKits.getErrorCode());
            }
        }catch (Exception e){
            System.out.println("err:"+ToolKits.getErrorCode());
        }
    }

    /**
     *生成随机数组
     *@param size 目标数组大小
     *@param max 目标数最大值
     */
    public LinkedHashSet<Integer> getRandomSet(int size,int max){
        Random random= new Random();
        LinkedHashSet<Integer> result= new LinkedHashSet<>();
        while (result.size() < size)
        {
            Integer next = random.nextInt(max) + 1;
            result.add(next);
        }
        return result;
    }

    /**
     * 修改车辆信息
     */
    public void CLIENT_ModifyVehicleForVehicleRegisterDB(){
        Scanner scanner=new Scanner(System.in);
        NET_IN_MODIFY_VEHICLE_FOR_VEHICLE_REG_DB inPut=new NET_IN_MODIFY_VEHICLE_FOR_VEHICLE_REG_DB();

        NetSDKLib.NET_VEHICLE_INFO stuVehicleInfo = inPut.stuVehicleInfo;

        System.out.println("输入车辆唯一标识符");
        // 车辆唯一标识符,由服务端生成用于程序中表示惟一
        String uid
                = scanner.nextLine().trim();
        stuVehicleInfo.nUID=  Integer.parseInt(uid);

        System.out.println("输入车辆组ID");
        String szGroupID= scanner.nextLine().trim();

        System.out.println("输入车主名称");
        // 车主名称
        String szOwnerName=scanner.nextLine().trim();

        try {
            System.arraycopy(szGroupID.getBytes(encode),0,stuVehicleInfo.szGroupID,0,szGroupID.getBytes(encode).length);

            System.arraycopy(szOwnerName.getBytes(encode),0,stuVehicleInfo.szOwnerName,0,szOwnerName.getBytes(encode).length);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Pointer inputPointer = new Memory(inPut.size());
        inputPointer.clear(inPut.size());
        ToolKits.SetStructDataToPointer(inPut,inputPointer,0);

        NET_OUT_MODIFY_VEHICLE_FOR_VEHICLE_REG_DB outPut=new NET_OUT_MODIFY_VEHICLE_FOR_VEHICLE_REG_DB();

        Pointer outPutPointer = new Memory(outPut.size());
        outPutPointer.clear(outPut.size());
        ToolKits.SetStructDataToPointer(outPut,outPutPointer,0);


        boolean b = netSdk.CLIENT_ModifyVehicleForVehicleRegisterDB(loginHandle, inputPointer, outPutPointer, 3000);

        if(b){
            System.out.println(" CLIENT_ModifyVehicleForVehicleRegisterDB is success");
        }else {
            System.err.println(" CLIENT_ModifyVehicleForVehicleRegisterDB is fail:"+ToolKits.getErrorCode());
        }
    }

    /**
     * 删除车辆信息
     */
    public void   CLIENT_DeleteVehicleFromVehicleRegisterDB(){
        Scanner scanner=new Scanner(System.in);
        NET_IN_DELETE_VEHICLE_FROM_VEHICLE_REG_DB inPut=new NET_IN_DELETE_VEHICLE_FROM_VEHICLE_REG_DB();

        System.out.println("输入车辆唯一标识符");
        // 车辆唯一标识符,由服务端生成用于程序中表示惟一
        String s = scanner.nextLine();
        inPut.nUID=  Integer.parseInt(s.trim());

        /* System.out.println("输入车辆组ID");
        // 输入车辆组ID
        String szGroupID = scanner.nextLine().trim();


        System.out.println("输入车牌");
        // 输入车辆组ID
        String szPlateNumber = scanner.nextLine().trim();

        try {
            System.arraycopy(szGroupID.getBytes(encode),0,inPut.szGroupID,0,szGroupID.getBytes(encode).length);

            System.arraycopy(szPlateNumber.getBytes(encode),0,inPut.szPlateNumber,0,szPlateNumber.getBytes(encode).length);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/


        Pointer inputPointer = new Memory(inPut.size());
        inputPointer.clear(inPut.size());

        ToolKits.SetStructDataToPointer(inPut,inputPointer,0);

        NET_OUT_DELETE_VEHICLE_FROM_VEHICLE_REG_DB outPut=new NET_OUT_DELETE_VEHICLE_FROM_VEHICLE_REG_DB();

        Pointer outPutPointer = new Memory(outPut.size());
        outPutPointer.clear(outPut.size());
        ToolKits.SetStructDataToPointer(outPut,outPutPointer,0);

        boolean b = netSdk.CLIENT_DeleteVehicleFromVehicleRegisterDB(loginHandle, inputPointer, outPutPointer, 3000);
        if(b){
            System.out.println(" CLIENT_DeleteVehicleFromVehicleRegisterDB is success");
        }else {
            System.err.println(" CLIENT_DeleteVehicleFromVehicleRegisterDB is fail:"+ToolKits.getErrorCode());
        }

    }

    /**
     * 向指定注册库查询车辆
     */
    public void FindVehicleRegisterDB(){
        Scanner scanner=new Scanner(System.in);
        /**
         * 向指定注册库查询车辆
         */
        NET_IN_START_FIND_VEHICLE_REG_DB inPut=new NET_IN_START_FIND_VEHICLE_REG_DB();

        NetSDKLib.NET_VEHICLE_INFO stuVehicleInfo = inPut.stuVehicleInfo;

        System.out.println("输入要查询车辆组的Id,为空表示查找全部车辆组信息");
        String next = scanner.nextLine();
        // 车辆所属组ID
        String szGroupID=next.trim();


        // 车色 第一个元素表示红色分量值； 第二个元素表示绿色分量值； 第三个元素表示蓝色分量值； 第四个元素表示透明度分量(无意义)
        //此参数必填，不填则查询不到数据
        stuVehicleInfo.stuVehicleColor.nAlpha = 255;

        try {
            System.arraycopy(szGroupID.getBytes(encode),0,stuVehicleInfo.szGroupID,0,szGroupID.getBytes(encode).length);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Pointer inputPointer = new Memory(inPut.size());
        inputPointer.clear(inPut.size());

        ToolKits.SetStructDataToPointer(inPut,inputPointer,0);

        NET_OUT_START_FIND_VEHICLE_REG_DB outPut=new NET_OUT_START_FIND_VEHICLE_REG_DB();
        Pointer outPutPointer = new Memory(outPut.size());
        outPutPointer.clear(outPut.size());
        ToolKits.SetStructDataToPointer(outPut,outPutPointer,0);


        boolean b = netSdk.CLIENT_StartFindVehicleRegisterDB(loginHandle, inputPointer, outPutPointer, 3000);
        if(b){
            System.out.println(" CLIENT_StartFindVehicleRegisterDB is success");
        }else {
            System.err.println(" CLIENT_StartFindVehicleRegisterDB is fail:"+ToolKits.getErrorCode());
            return;
        }

        ToolKits.GetPointerData(outPutPointer,outPut);
        // 取到的查询令牌
        int nToken = outPut.nToken;
        // 符合此次查询条件的结果总条数
        int nTotalCount = outPut.nTotalCount;

        System.out.println(" nToken:"+nToken);
        System.out.println(" nTotalCount:"+nTotalCount);

        /**
         * 获取车辆查询结果信息
         */
        NET_IN_DO_FIND_VEHICLE_REG_DB inPutFind=new NET_IN_DO_FIND_VEHICLE_REG_DB();

        NET_FIND_VEHICLE_CONDITION stuCondition = inPutFind.stuCondition;
        // 查询令牌
        stuCondition.nToken=nToken;

        stuCondition. nBeginNumber=0;

        stuCondition.nCount=100;

   // System.out.println("stuCondition:"+stuCondition.toString());

        Pointer inPutFindPointer = new Memory(inPutFind.size());

        inPutFindPointer.clear(inPutFind.size());

        ToolKits.SetStructDataToPointer(inPutFind,inPutFindPointer,0);

        NET_OUT_DO_FIND_VEHICLE_REG_DB outPutFind=new NET_OUT_DO_FIND_VEHICLE_REG_DB();

        Pointer outPutFindPointer = new Memory(outPutFind.size());
        outPutFindPointer.clear(outPutFind.size());
        ToolKits.SetStructDataToPointer(outPutFind,outPutFindPointer,0);

        b = netSdk.CLIENT_DoFindVehicleRegisterDB(loginHandle, inPutFindPointer, outPutFindPointer, 3000);

        if(b){
            System.out.println(" CLIENT_DoFindVehicleRegisterDB is success");
        }else {
            System.err.println(" CLIENT_DoFindVehicleRegisterDB is fail:"+ToolKits.getErrorCode());
            return;
        }
        ToolKits.GetPointerData(outPutFindPointer,outPutFind);
        /**
         *  候选车辆数量
         */
        int nCarCandidateNum = outPutFind.nCarCandidateNum;

        System.out.println("nCarCandidateNum："+nCarCandidateNum);
        /**
         * 查询到的条数
         */
        int nFound = outPutFind.nFound;

        System.out.println("nFound："+nFound);

        NetSDKLib.NET_CAR_CANDIDATE_INFO[] stuCarCandidate = outPutFind.stuCarCandidate;

        for(int i=0;i<nCarCandidateNum;i++){

            NetSDKLib.NET_CAR_CANDIDATE_INFO info = stuCarCandidate[i];

            NetSDKLib.NET_VEHICLE_INFO stuVehicleInfo1 = info.stuVehicleInfo;
            // 车辆唯一标识符,由服务端生成用于程序中表示惟一
            System.out.println("nUID："+stuVehicleInfo1.nUID);

            try {

                System.out.println("szOwnerName："+ new String(stuVehicleInfo1.szOwnerName, encode) );

                System.out.println("szGroupID："+ new String(stuVehicleInfo1.szGroupID, encode) );

                System.out.println("szGroupName："+ new String(stuVehicleInfo1.szGroupName, encode) );

                System.out.println("szPlateNumber："+ new String(stuVehicleInfo1.szPlateNumber, encode) );

                System.out.println("szPlateCountry："+ new String(stuVehicleInfo1.szPlateCountry, encode) );
/**
 // 01  大型汽车号牌 黄底黑字
 // 02  小型汽车号牌 蓝底白字
 // 03  使馆汽车号牌 黑底白字、红“使”字
 // 04  领馆汽车号牌 黑底白字、红“领”字
 // 05  境外汽车号牌 黑底白、红字
 // 06  外籍汽车号牌 黑底白字
 // 13  农用运输车号牌 黄底黑字黑框线
 // 15  挂车号牌 黄底黑字黑框线
 // 16  教练汽车号牌 黄底黑字黑框线
 // 18  试验汽车号牌
 // 20  临时入境汽车号牌 白底红字黑“临时入境”
 // 22  临时行驶车号牌 白底黑字黑线框
 // 23  公安警用汽车号牌
 */
                System.out.println("nPlateType："+ stuVehicleInfo1.nPlateType );
/**
 // 001  巡逻车
 // 002  交警车辆
 // 003  消防车
 // 004  单兵
 // 005  其他警车
 // 006  其他设备
 // 020  政府车辆
 // 031  校车
 // 032  运钞车
 // 033  客运车辆
 // 034  公交车
 // 035  出租车
 // 036  危险品车辆
 */
                System.out.println("nVehicleType："+ stuVehicleInfo1.nVehicleType );



            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            // 和数据库不相符的属性数目
            int nDifferentAttributresNum = info.nDifferentAttributresNum;
            System.out.println("nDifferentAttributresNum："+nDifferentAttributresNum);

            int[] nDifferentAttributres = info.nDifferentAttributres;
         //   和数据库不相符的属性集合,元素值取值意义:0-未知 1-车牌属地 2-车标 3-车型 4-车色 5-车牌颜色
            for(int j=0;j<nDifferentAttributresNum;j++){
                System.out.println("["+j+"]:"+nDifferentAttributres[j]);
            }


        }


        /**
         * 结束车辆查询
         *param[in]   lLoginID 登录句柄
         *param[in]   pstInParam 接口输入参数{@link  NET_IN_STOP_FIND_VEHICLE_REG_DB}
         *param[out]  pstOutParam 接口输出参数{@link  NET_OUT_STOP_FIND_VEHICLE_REG_DB}
         *param[in]   nWaitTime 接口超时时间, 单位毫秒
         *return TRUE表示成功 FALSE表示失败
         */
        NET_IN_STOP_FIND_VEHICLE_REG_DB stopInput=new NET_IN_STOP_FIND_VEHICLE_REG_DB();
        stopInput.nToken=nToken;

        Pointer stopInputPointer = new Memory(stopInput.size());

        stopInputPointer.clear(stopInput.size());

        ToolKits.SetStructDataToPointer(stopInput,stopInputPointer,0);

        NET_OUT_STOP_FIND_VEHICLE_REG_DB stopOutPut=new NET_OUT_STOP_FIND_VEHICLE_REG_DB();

        Pointer stopOutPutPointer = new Memory(stopOutPut.size());
        stopOutPutPointer.clear(stopOutPut.size());
        ToolKits.SetStructDataToPointer(stopOutPut,stopOutPutPointer,0);

         b = netSdk.CLIENT_StopFindVehicleRegisterDB(loginHandle, stopInputPointer, stopOutPutPointer, 3000);

        if(b){
            System.out.println(" CLIENT_StopFindVehicleRegisterDB is success");
        }else {
            System.err.println(" CLIENT_StopFindVehicleRegisterDB is fail:"+ToolKits.getErrorCode());
            return;
        }
    }

    public  void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();

        menu.addItem(new CaseMenu.Item(this , "创建车辆组" , "CLIENT_CreateGroupForVehicleRegisterDB"));

        menu.addItem(new CaseMenu.Item(this , "删除车辆组" , "CLIENT_DeleteGroupFromVehicleRegisterDB"));

        menu.addItem(new CaseMenu.Item(this , "修改车辆组" , "CLIENT_ModifyGroupForVehicleRegisterDB"));

        menu.addItem(new CaseMenu.Item(this , "查询车辆组" , "CLIENT_FindGroupFormVehicleRegisterDB"));

        menu.addItem(new CaseMenu.Item(this , "向车牌库添加车辆信息" , "CLIENT_MultiAppendToVehicleRegisterDB"));

        menu.addItem(new CaseMenu.Item(this , "修改车辆信息" , "CLIENT_ModifyVehicleForVehicleRegisterDB"));

        menu.addItem(new CaseMenu.Item(this , "删除车辆信息" , "CLIENT_DeleteVehicleFromVehicleRegisterDB"));

        menu.addItem(new CaseMenu.Item(this , "查询车辆" , "FindVehicleRegisterDB"));
        menu.run();
    }


    public static void main(String[] args) {

        VehicleRegisterDBNewDemo vc=new VehicleRegisterDBNewDemo();
        Initialization.InitTest("172.12.5.122", 37777, "admin", "admin123");
        vc.RunTest();

        Initialization.LoginOut();
    }

}
