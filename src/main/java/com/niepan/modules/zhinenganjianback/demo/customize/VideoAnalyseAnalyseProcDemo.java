package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.*;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Pointer;

import java.util.Scanner;

public class VideoAnalyseAnalyseProcDemo extends Initialization {

	int channel = 0;

	public void setChannel(int channel) {
		this.channel = channel;
	}

	NetSDKLib.LLong attachHandle = new NetSDKLib.LLong(0);

	/**
	 * 订阅智能任务
	 */

	public NetSDKLib.LLong AttachVideoAnalyseAnalyseProc() {
		// 先退订，设备不会对重复订阅作校验，重复订阅后会有重复的事件返回
		if (attachHandle.longValue() != 0) {
			this.DetachVideoAnalyseAnalyseProc();
		}

		NET_IN_ATTACH_VIDEO_ANALYSE_ANALYSE_PROC stIn = new NET_IN_ATTACH_VIDEO_ANALYSE_ANALYSE_PROC();
		stIn.nChannelId = channel;
		NET_OUT_ATTACH_VIDEO_ANALYSE_ANALYSE_PROC stOut = new NET_OUT_ATTACH_VIDEO_ANALYSE_ANALYSE_PROC();
		stIn.cbVideoAnalyseAnalyseProc = new VideoAnalyseAnalyseCB();

		stIn.write();
		stOut.write();

		System.out.println("stIn.dwSize = " + stIn.dwSize);
		System.out.println("stOut.dwSize = " + stOut.dwSize);

		attachHandle = netSdk.CLIENT_AttachVideoAnalyseAnalyseProc(loginHandle, stIn.getPointer(), stOut.getPointer(),
				5000);
		if (attachHandle.longValue() != 0) {
			System.out.printf("Chn[%d] CLIENT_AttachVideoAnalyseAnalyseProc Success\n", channel);
		} else {
			System.out.printf("Chn[%d] CLIENT_AttachVideoAnalyseAnalyseProc Failed!LastError = %s\n", channel,
					ToolKits.getErrorCode());
		}

		return attachHandle;
	}

	/**
	 * 报警事件（智能）回调
	 */
	private static class VideoAnalyseAnalyseCB implements NetSDKLib.fVideoAnalyseAnalyseProc {
		private static VideoAnalyseAnalyseProcDemo.VideoAnalyseAnalyseCB instance;

		private VideoAnalyseAnalyseCB() {
		}

		public static VideoAnalyseAnalyseProcDemo.VideoAnalyseAnalyseCB getInstance() {
			if (instance == null) {
				synchronized (VideoAnalyseAnalyseProcDemo.VideoAnalyseAnalyseCB.class) {
					if (instance == null) {
						instance = new VideoAnalyseAnalyseProcDemo.VideoAnalyseAnalyseCB();
					}
				}
			}
			return instance;
		}

		@Override
		public void invoke(NetSDKLib.LLong lAttachHandle, Pointer pstuVideoAnalyseTrackProc, Pointer dwUser) {
			System.out.println("========================Enter invoke========================");
			NET_VIDEO_ANALYSE_ANALYSE_PROC msg = new NET_VIDEO_ANALYSE_ANALYSE_PROC();
			ToolKits.GetPointerData(pstuVideoAnalyseTrackProc, msg);
			System.out.println("msg.nChannelId = " + msg.nChannelId);
			System.out.println("msg.stuUTC = " + msg.stuUTC.toStringTime());

			System.out.println("msg.nObjectListNum = " + msg.nObjectListNum);
			NET_VEHICLE_FUSED_OBJECT[] tmpObjectList = new NET_VEHICLE_FUSED_OBJECT[msg.nObjectListNum];
			for (int i = 0; i < msg.nObjectListNum; i++) {
				tmpObjectList[i] = new NET_VEHICLE_FUSED_OBJECT();
			}
//            msg.pstuObjectList = new Memory(tmpObjectList[0].size() * msg.nObjectListNum);
			ToolKits.GetPointerDataToStructArr(msg.pstuObjectList, tmpObjectList);
			for (int i = 0; i < msg.nObjectListNum; i++) {
				System.out.println("ObjectList[" + i + "].nObjectID = " + tmpObjectList[i].nObjectID);
				System.out.println("ObjectList[" + i + "].emObjectType = " + tmpObjectList[i].emObjectType);
				System.out.println("ObjectList[" + i + "].fSpeed = " + tmpObjectList[i].fSpeed);
				System.out.println("ObjectList[" + i + "].fAcceleration = " + tmpObjectList[i].fAcceleration);
				System.out.println("ObjectList[" + i + "].nLaneID = " + tmpObjectList[i].nLaneID);
			}

			System.out.println("msg.nEventListCount = " + msg.nEventListCount);
			int[] dwEventCode = msg.dwEventCode;
			for (int i = 0; i < msg.nEventListCount; i++) {
				if (dwEventCode[i] != NetSDKLib.EVENT_IVS_COLLISION_CONFLICT) {
					return;
				}
				;
				NET_DEV_EVENT_COLLISION_CONFLICT_INFO tmpEvent = new NET_DEV_EVENT_COLLISION_CONFLICT_INFO();
				ToolKits.GetPointerDataToStruct(msg.pstuEventList[i], 0, tmpEvent);

				System.out.println("EventList[" + i + "].nChannelID = " + tmpEvent.nChannelID);
				System.out.println("EventList[" + i + "].nAction = " + tmpEvent.nAction);
				System.out.println("EventList[" + i + "].szName = " + new String(tmpEvent.szName).trim());
				System.out.println("EventList[" + i + "].nEventID = " + tmpEvent.nEventID);
				System.out.println("EventList[" + i + "].nType = " + tmpEvent.nType);
				System.out.println("EventList[" + i + "].nLeftTime = " + tmpEvent.nLeftTime);
				System.out.println("EventList[" + i + "].fConflictLevel = " + tmpEvent.fConflictLevel);
				System.out.println("EventList[" + i + "].fConfidence = " + tmpEvent.fConfidence);
				System.out.println("EventList[" + i + "].nObjectsCount = " + tmpEvent.nObjectsCount);
				for (int j = 0; j < tmpEvent.nObjectsCount; j++) {
					System.out.println("EventList[" + i + "].stuObjects[" + j + "].nObjectID = "+ tmpEvent.stuObjects[j].nObjectID);
					System.out.println("EventList[" + i + "].stuObjects[" + j + "].emObjectType = "+ tmpEvent.stuObjects[j].emObjectType);
					System.out.println("EventList[" + i + "].stuObjects[" + j + "].fSpeed = " + tmpEvent.stuObjects[j].fSpeed);
					System.out.println("EventList[" + i + "].stuObjects[" + j + "].fAcceleration = "+ tmpEvent.stuObjects[j].fAcceleration);
					System.out.println("EventList[" + i + "].stuObjects[" + j + "].nLaneID = " + tmpEvent.stuObjects[j].nLaneID);
				}
			}

		}
	}

	/**
	 * 停止侦听智能事件
	 */
	public void DetachVideoAnalyseAnalyseProc() {
		if (this.attachHandle.longValue() != 0) {
			netSdk.CLIENT_DetachVideoAnalyseAnalyseProc(this.attachHandle);
		}
	}

	public void RunTest() {
		System.out.println("Run Test");
		CaseMenu menu = new CaseMenu();
		;
		menu.addItem((new CaseMenu.Item(this, "AttachVideoAnalyseAnalyseProc", "AttachVideoAnalyseAnalyseProc")));
		menu.addItem((new CaseMenu.Item(this, "DetachVideoAnalyseAnalyseProc", "DetachVideoAnalyseAnalyseProc")));

		menu.run();
	}

	public static void main(String[] args) {
		VideoAnalyseAnalyseProcDemo RealLoadPicExDemo = new VideoAnalyseAnalyseProcDemo();
		Scanner sc = new Scanner(System.in);
//        System.out.print("ip:");
//        String ip = sc.nextLine();
//        System.out.print("port:");
//        String tmp = sc.nextLine();
//        int port = Integer.parseInt(tmp);
//        System.out.print("username:");
//        String username = sc.nextLine();
//        System.out.print("password:");
//        String pwd = sc.nextLine();
		String ip = "10.33.121.89";
		int port = 32200;
		String username = "admin";
		String pwd = "admin123";
		InitTest(ip, port, username, pwd);

		System.out.print("channel:");
//        String tmp2 = sc.nextLine();
//        int channel = Integer.parseInt(tmp2);
		int channel = 0;
		RealLoadPicExDemo.setChannel(channel);

		RealLoadPicExDemo.RunTest();
		LoginOut();

	}
}
