package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.enumeration.ENUMERROR;
import com.niepan.modules.zhinenganjianback.lib.structure.*;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Date;

/**
 * @author 291189
 * @version 1.0
 * @description ERR221129117
 * @date 2022/11/30 20:33
 */
public class XRayMultiLevelDemo extends Initialization {

	NET_XRAY_MULTILEVEL_DETECT_CFG_INFO stuInfo = new NET_XRAY_MULTILEVEL_DETECT_CFG_INFO();

	public void getXRayMultiLevelDetectCFG() {
		NET_IN_GET_XRAY_MULTILEVEL_DETECT_INFO input = new NET_IN_GET_XRAY_MULTILEVEL_DETECT_INFO();
		Pointer pointerInput = new Memory(input.size());
		pointerInput.clear(input.size());
		ToolKits.SetStructDataToPointer(input, pointerInput, 0);

		NET_OUT_GET_XRAY_MULTILEVEL_DETECT_INFO outPut = new NET_OUT_GET_XRAY_MULTILEVEL_DETECT_INFO();

		outPut.stuInfo = stuInfo;

		stuInfo.nMultiLevelConfigMax = 10;

		NET_MULTI_LEVEL_INFO info = new NET_MULTI_LEVEL_INFO();
		/**
		 * 等级配置 此部分内存需要用户申请,申请大小为(nMultiLevelConfigMax*sizeof(NET_MULTI_LEVEL_INFO))。
		 */
		outPut.stuInfo.pstuMultiLevelConfig = new Memory(info.size() * outPut.stuInfo.nMultiLevelConfigMax);

		outPut.stuInfo.pstuMultiLevelConfig.clear(info.size() * outPut.stuInfo.nMultiLevelConfigMax);

		Pointer pointerOutPut = new Memory(outPut.size());
		pointerOutPut.clear(outPut.size());
		ToolKits.SetStructDataToPointer(outPut, pointerOutPut, 0);

		NetSDKLib.LLong lLong = netSdk.CLIENT_GetXRayMultiLevelDetectCFG(loginHandle, pointerInput, pointerOutPut,
				3000);
		if (lLong.longValue() != 0) {
			ToolKits.GetPointerData(pointerOutPut, outPut);

			Native.free(Pointer.nativeValue(pointerInput)); // 清理内存
			Pointer.nativeValue(pointerInput, 0); // 防止gc重复回收

			Native.free(Pointer.nativeValue(pointerOutPut));
			Pointer.nativeValue(pointerOutPut, 0);
			System.out.println("CLIENT_GetFinancialCabinInfo success");

			stuInfo = outPut.stuInfo;

			/**
			 * 当前配置等级 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_XRAY_OBJECT_DETECT_CONFIG_LEVEL}
			 */
			System.out.println("emCurrentLevel:" + stuInfo.emCurrentLevel);

			System.out.println("nMultiLevelConfigMax:" + stuInfo.nMultiLevelConfigMax);

			int nMultiLevelConfigCnt = stuInfo.nMultiLevelConfigCnt;

			System.out.println("nMultiLevelConfigCnt:" + nMultiLevelConfigCnt);

			NET_MULTI_LEVEL_INFO[] infos = new NET_MULTI_LEVEL_INFO[nMultiLevelConfigCnt];
			for (int i = 0; i < infos.length; i++) {
				infos[i] = new NET_MULTI_LEVEL_INFO();
			}

			ToolKits.GetPointerDataToStructArr(stuInfo.pstuMultiLevelConfig, infos);

			for (int i = 0; i < infos.length; i++) {
				NET_MULTI_LEVEL_INFO info1 = infos[i];
				/**
				 * 配置等级 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_XRAY_OBJECT_DETECT_CONFIG_LEVEL}
				 */
				System.out.println("emLevel:" + info1.emLevel);

				/**
				 * 默认参考等级，该等级的默认配置与参考等级的默认配置一样
				 * {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_XRAY_OBJECT_DETECT_CONFIG_LEVEL}
				 */
				System.out.println("emDefaultReferenceLevel:" + info1.emDefaultReferenceLevel);
				/**
				 * 等级名字
				 */
				try {
					System.out.println("szName:" + new String(info1.szName, encode));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				/**
				 * 物品分组数组个数
				 */
				int nObjectGroupsNum = info1.nObjectGroupsNum;
				NET_XRAY_OBJECT_GROUP[] stuObjectGroups = info1.stuObjectGroups;

				for (int j = 0; j < nObjectGroupsNum; j++) {
					NET_XRAY_OBJECT_GROUP stuObjectGroup = stuObjectGroups[j];
					/**
					 * 分组检测使能
					 */
					System.out.println("bGroupEnable:" + stuObjectGroup.bGroupEnable);
					/**
					 * 分组类型 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_INSIDE_OBJECT_TYPE}
					 */
					System.out.println("emGroupType:" + stuObjectGroup.emGroupType);
					/**
					 * 分组安全等级 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_GROUP_GRADE_TYPE}
					 */
					System.out.println("emGroupGrade:" + stuObjectGroup.emGroupGrade);
					/**
					 * 分组物品颜色 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_GROUP_COLOR_TYPE}
					 */
					System.out.println("emGroupColor:" + stuObjectGroup.emGroupColor);

				}

			}

		} else {
			System.out.printf("CLIENT_GetFinancialCabinInfo Failed!LastError = %s\n", ToolKits.getErrorCode());
		}

	}

	public void setXRayMultiLevelDetectCFG() {

		NET_IN_SET_XRAY_MULTILEVEL_DETECT_INFO input = new NET_IN_SET_XRAY_MULTILEVEL_DETECT_INFO();

		input.stuInfo = stuInfo;
		/**
		 * 当前配置等级 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_XRAY_OBJECT_DETECT_CONFIG_LEVEL}
		 */
		input.stuInfo.emCurrentLevel = 1;

		Pointer pointerInput = new Memory(input.size());

		pointerInput.clear(input.size());

		ToolKits.SetStructDataToPointer(input, pointerInput, 0);

		NET_OUT_SET_XRAY_MULTILEVEL_DETECT_INFO outPut = new NET_OUT_SET_XRAY_MULTILEVEL_DETECT_INFO();

		Pointer pointerOutput = new Memory(outPut.size());

		pointerOutput.clear(outPut.size());

		ToolKits.SetStructDataToPointer(outPut, pointerOutput, 0);

		NetSDKLib.LLong lLong = netSdk.CLIENT_SetXRayMultiLevelDetectCFG(loginHandle, pointerInput, pointerOutput,
				3000);

		if (lLong.longValue() != 0) {
			System.out.println("CLIENT_GetFinancialCabinInfo success");
			Native.free(Pointer.nativeValue(pointerInput)); // 清理内存
			Pointer.nativeValue(pointerInput, 0); // 防止gc重复回收

			Native.free(Pointer.nativeValue(pointerOutput));
			Pointer.nativeValue(pointerOutput, 0);

		} else {
			Native.free(Pointer.nativeValue(pointerInput)); // 清理内存
			Pointer.nativeValue(pointerInput, 0); // 防止gc重复回收

			Native.free(Pointer.nativeValue(pointerOutput));
			Pointer.nativeValue(pointerOutput, 0);
			System.out.printf("CLIENT_GetFinancialCabinInfo Failed!LastError = %s\n", ToolKits.getErrorCode());
		}

	}

	/**
	 * 智能分析结果 句柄
	 */
	private NetSDKLib.LLong AttachHandle = new NetSDKLib.LLong(0);

	public void AttachAnalyseTaskResult() {
		// 入参
		NetSDKLib.NET_IN_ATTACH_ANALYSE_RESULT pInParam = new NetSDKLib.NET_IN_ATTACH_ANALYSE_RESULT();
		pInParam.cbAnalyseTaskResult = CbfAnalyseTaskResultCallBack.getInstance();

		pInParam.nTaskIdNum = 1;
		pInParam.nTaskIDs[0] = myTaskID;

		pInParam.stuFilter.nEventNum = 1; // 只获取 EVENT_IVS_FEATURE_ABSTRACT 特征提取事件
		pInParam.stuFilter.dwAlarmTypes[0] = NetSDKLib.EVENI_IVS_XRAY_DETECTION;

		AttachHandle = netSdk.CLIENT_AttachAnalyseTaskResult(loginHandle, pInParam, 5000);
		if (AttachHandle.longValue() != 0) {
			System.out.println("AttachAnalyseTaskResult Succeed!" + AttachHandle.longValue());
		} else {
			System.err.printf("AttachAnalyseTaskResult Failed!Last Error[0x%x]\n", netSdk.CLIENT_GetLastError());
			return;
		}
	}

	/**
	 * 智能分析退订
	 *
	 * @return 是否成功退订
	 */
	public void detachAnalyseTaskResult() {
		System.out.println("detachAnalyseTaskResult:" + AttachHandle.longValue());

		boolean result = netSdk.CLIENT_DetachAnalyseTaskResult(AttachHandle);
		if (!result) {
			System.out.println("detach analyseTask result failed. error is " + ENUMERROR.getErrorMessage());
		} else {
			System.out.println("detachAnalyseTaskResult Succeed");

		}

	}

	int myTaskID = 0;

	/**
	 * 查询智能分析任务
	 */
	public void findAnalyseTask() {
		// 入参
		NetSDKLib.NET_IN_FIND_ANALYSE_TASK stuInParam = new NetSDKLib.NET_IN_FIND_ANALYSE_TASK();
		// 出参
		NetSDKLib.NET_OUT_FIND_ANALYSE_TASK stuOutParam = new NetSDKLib.NET_OUT_FIND_ANALYSE_TASK();

		if (netSdk.CLIENT_FindAnalyseTask(loginHandle, stuInParam, stuOutParam, 5000)) {
			System.out.println("FindAnalyseTask Succeed!" + "智能分析任务个数" + stuOutParam.nTaskNum);
			// ID和状态 可以从 stuTaskInfos 中获取
			for (int i = 0; i < stuOutParam.nTaskNum; i++) { // 状态值参考 EM_ANALYSE_STATE

				myTaskID = stuOutParam.stuTaskInfos[i].nTaskID;
				System.out.printf("任务%d: %d, 状态：%d\n", (i + 1), stuOutParam.stuTaskInfos[i].nTaskID,
						stuOutParam.stuTaskInfos[i].emAnalyseState);
			}
		} else {
			System.err.printf("FindAnalyseTask Failed!Last Error:%s\n", ToolKits.getErrorCode());
		}
	}

	// 测试用的图片
	private final String testPicPath1 = "D:/A1.jpg";
	private final String testPicPath2 = "D:/A2.jpg";

	/**
	 * 主动推送图片
	 */
	public void PushAnalysePictureFile() {
		// 入参
		NetSDKLib.NET_IN_PUSH_ANALYSE_PICTURE_FILE stuInParam = new NetSDKLib.NET_IN_PUSH_ANALYSE_PICTURE_FILE();
		stuInParam.nTaskID = myTaskID;
		stuInParam.nPicNum = 1; // 以两张图为例

		// ftp://用户名:密码@hostname:端口号/文件路径
		// byte[] fileUrl = "ftp://username:password@hostname:port/filepath".getBytes();
		// System.arraycopy(fileUrl, 0, stuInParam.stuPushPicInfos[0].szUrl, 0,
		// fileUrl.length);

		// 加载本地图片到缓冲区, 这里以两张图为例
		int totalLen = 0; // 这是总的图片缓冲区长度

		byte[] testFileBuffer1 = ToolKits.readPictureToByteArray(testPicPath1);
		byte[] testFileBuffer2 = ToolKits.readPictureToByteArray(testPicPath2);

		// 文件id
		byte[] fileId1 = "file001".getBytes(); // 这个ID是图片的标识符，设备分析完毕返回的数据也以这个ID区分
		System.arraycopy(fileId1, 0, stuInParam.stuPushPicInfos[0].szFileID, 0, fileId1.length);

		if (testFileBuffer1 != null) {
			stuInParam.stuPushPicInfos[0].nLength = testFileBuffer1.length;
			stuInParam.stuPushPicInfos[0].nOffset = totalLen; // 偏移量，有多张图时这里要特别注意
			totalLen = totalLen + stuInParam.stuPushPicInfos[0].nLength; // 总的长度是各个图的累加
		}

		byte[] fileId2 = "file002".getBytes(); // 这个ID是图片的标识符，设备分析完毕返回的数据也以这个ID区分
		System.arraycopy(fileId2, 0, stuInParam.stuPushPicInfos[1].szFileID, 0, fileId2.length);
		if (testFileBuffer2 != null) {
			stuInParam.stuPushPicInfos[1].nLength = testFileBuffer2.length;
			stuInParam.stuPushPicInfos[1].nOffset = totalLen; // 偏移量，有多张图时这里要特别注意
			totalLen = totalLen + stuInParam.stuPushPicInfos[1].nLength; // 总的长度是各个图的累加
		}

		stuInParam.nBinBufLen = totalLen;
		stuInParam.pBinBuf = new Memory(totalLen); // 分配内存
		stuInParam.pBinBuf.clear(totalLen); // 清理内存

		if (testFileBuffer1 != null) // 第一张图写入缓存
			stuInParam.pBinBuf.write(stuInParam.stuPushPicInfos[0].nOffset, testFileBuffer1, 0,
					stuInParam.stuPushPicInfos[0].nLength);
		if (testFileBuffer2 != null) // 第二张图写入缓存
			stuInParam.pBinBuf.write(stuInParam.stuPushPicInfos[1].nOffset, testFileBuffer2, 0,
					stuInParam.stuPushPicInfos[1].nLength);

		// 出参
		NetSDKLib.NET_OUT_PUSH_ANALYSE_PICTURE_FILE stuOutParam = new NetSDKLib.NET_OUT_PUSH_ANALYSE_PICTURE_FILE();

		if (netSdk.CLIENT_PushAnalysePictureFile(loginHandle, stuInParam, stuOutParam, 5000)) {
			System.out.println("PushAnalysePictureFile Succeed!");
		} else {
			System.err.printf("PushAnalysePictureFile Failed!Last Error: %s\n", ToolKits.getErrorCode());
		}
	}

	/**
	 * 智能分析结果订阅函数原型
	 */
	private static class CbfAnalyseTaskResultCallBack implements NetSDKLib.fAnalyseTaskResultCallBack {

		private final File picturePath;

		private CbfAnalyseTaskResultCallBack() {

			picturePath = new File("./AnalyzerPicture/result/");
			if (!picturePath.exists()) {
				picturePath.mkdirs();
			}
		}

		private static class CallBackHolder {
			private static CbfAnalyseTaskResultCallBack instance = new CbfAnalyseTaskResultCallBack();
		}

		public static CbfAnalyseTaskResultCallBack getInstance() {
			return CbfAnalyseTaskResultCallBack.CallBackHolder.instance;
		}

		@Override
		public int invoke(NetSDKLib.LLong lAttachHandle, Pointer pstAnalyseTaskResult, Pointer pBuf, int dwBufSize,
				Pointer dwUser) {
			NetSDKLib.NET_CB_ANALYSE_TASK_RESULT_INFO task = new NetSDKLib.NET_CB_ANALYSE_TASK_RESULT_INFO();
			ToolKits.GetPointerData(pstAnalyseTaskResult, task);

			System.out.println("进入回调-----------------------------------");

			System.out.println("task.nTaskResultNum:" + task.nTaskResultNum);
			for (int i = 0; i < task.nTaskResultNum; i++) {
				for (int j = 0; j < task.stuTaskResultInfos[0].nEventCount; j++) {
					int emEventType = task.stuTaskResultInfos[i].stuEventInfos[i].emEventType;
					switch (emEventType) {

					case NetSDKLib.EM_ANALYSE_EVENT_TYPE.EM_ANALYSE_EVENT_DIALRECOGNITION: {
						System.out.println(" 仪表检测事件");
						DEV_EVENT_DIALRECOGNITION_INFO msg = new DEV_EVENT_DIALRECOGNITION_INFO();

						ToolKits.GetPointerData(task.stuTaskResultInfos[0].stuEventInfos[j].pstEventInfo, msg);

						int emType = msg.emType;
						System.out.println("emType:" + emType);

						byte[] szTaskID = msg.szTaskID;

						System.out.println("szTaskID:" + new String(szTaskID));
						System.out.println("nPresetID:" + msg.nPresetID);

						System.out.println("nChannelID:" + msg.nChannelID);

						System.out.println("nRetImageInfoNum:" + msg.nRetImageInfoNum);

						NET_IMAGE_INFO[] stuImgaeInfo = msg.stuImgaeInfo;

						for (int m = 0; m < msg.nRetImageInfoNum; m++) {
							NET_IMAGE_INFO net_image_info = stuImgaeInfo[m];
							/**
							 * 图片类型 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_PIC_TYPE}
							 */
							System.out.println("emPicType:" + net_image_info.emPicType);
							System.out.println("net_image_info.length:" + net_image_info.nLength);
							System.out.println("net_image_info.szFilePath:" + new String(net_image_info.szFilePath));

							// 图片
							if (net_image_info != null && net_image_info.nLength > 0) {
								String picture = picturePath + "/" + System.currentTimeMillis() + "_stuImgaeInfo.jpg";
								ToolKits.savePicture(pBuf, net_image_info.nOffset, net_image_info.nLength, picture);
							}

						}

						System.out.println("szDialResult:" + new String(msg.szDialResult));

						// 原始图片
						if (msg.nOriginalImageLength > 0) {
							String picture = picturePath + "/" + System.currentTimeMillis() + "_OriginalImage.jpg";
							ToolKits.savePicture(pBuf, msg.nOriginalImageOffset, msg.nOriginalImageLength, picture);
						}

						/**
						 * 告警类型：0-该字段无效;1-数值异常;2-定时上报
						 */
						System.out.println("nAlarmType:" + msg.nAlarmType);

						/**
						 * EM_ANALYSE_EVENT_DIALRECOGNITION 仪表检测事件 DEV_EVENT_DIALRECOGNITION_INFO
						 * EM_ANALYSE_EVENT_ELECTRICFAULT_DETECT 仪表类缺陷检测事件
						 * DEV_EVENT_ELECTRICFAULTDETECT_INFO
						 *
						 */
						break;
					}
					case NetSDKLib.EM_ANALYSE_EVENT_TYPE.EM_ANALYSE_EVENT_ELECTRICFAULT_DETECT: { //

						System.out.println(" 仪表类缺陷检测事件");

						DEV_EVENT_ELECTRICFAULTDETECT_INFO msg = new DEV_EVENT_ELECTRICFAULTDETECT_INFO();

						ToolKits.GetPointerData(task.stuTaskResultInfos[0].stuEventInfos[j].pstEventInfo, msg);
						/**
						 * 智能事件所属大类 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_CLASS_TYPE}
						 */
						System.out.println("emClassType:" + msg.emClassType);

						System.out.println("szName:" + new String(msg.szName));

						NetSDKLib.SCENE_IMAGE_INFO stuSceneImageInfo = msg.stuSceneImageInfo;
						// 大图
						if (stuSceneImageInfo != null && stuSceneImageInfo.nLength > 0) {
							String picture = picturePath + "/" + System.currentTimeMillis() + "_stuSceneImageInfo.jpg";
							ToolKits.savePicture(pBuf, stuSceneImageInfo.nOffSet, stuSceneImageInfo.nLength, picture);
						}

						int nDialDetectNum = msg.nDialDetectNum;

						System.out.println("表盘检测结果个数: " + nDialDetectNum);

						NET_DIAL_DETECT[] stuDialDetectInfo = msg.stuDialDetectInfo;

						for (int m = 0; m < nDialDetectNum; m++) {
							NET_DIAL_DETECT detect = stuDialDetectInfo[m];

							/**
							 * 表盘状态 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_DIAL_STATE}
							 */

							int emDialState = detect.emDialState;
							System.out.println("表盘状态:" + emDialState);

							NET_RECT stuBoundingBox = detect.stuBoundingBox;

							System.out.println("stuBoundingBox: " + stuBoundingBox);
						}

						break;
					}
					case NetSDKLib.EM_ANALYSE_EVENT_TYPE.EM_ANALYSE_EVENT_XRAY_DETECTION: { //
						// X光机检测事件, 对应结构体 DEV_EVENT_XRAY_DETECTION_INFO
						System.out.println("X光机检测事件 ");

						DEV_EVENT_XRAY_DETECTION_INFO msg = new DEV_EVENT_XRAY_DETECTION_INFO();

						ToolKits.GetPointerData(task.stuTaskResultInfos[0].stuEventInfos[j].pstEventInfo, msg);
						// 通道号
						int nChannelID = msg.nChannelID;
						System.out.println("nChannelID:" + nChannelID);
						// 事件名称
						byte[] szName = msg.szName;
						System.out.println("szName:" + new String(szName));
						// 相对事件时间戳(单位是毫秒)
						double pts = msg.PTS;
						System.out.println("pts:" + new Date((long) pts));
						// UTC时间
						NET_TIME_EX UTC = msg.UTC;
						System.out.println("UTC:" + UTC.toStringTime());
						// 所属大类
						int emClassType = msg.emClassType;
						System.out.println("emClassType:" + emClassType);
						// 事件id
						int nEventID = msg.nEventID;
						System.out.println("nEventID:" + nEventID);
						// 危险等级
						int emDangerGrade = msg.stuPacketInfo.emDangerGrade;
						System.out.println("stuPacketInfo.emDangerGrade:" + emDangerGrade);
						// 主视角包裹内物品个数
						int nObjectNum = msg.nObjectNum;
						System.out.println("nObjectNum:" + nObjectNum);
						// 主视角包裹内物品信息
						for (int n = 0; n < nObjectNum; n++) {
							NET_INSIDE_OBJECT stuInsideObj = msg.stuInsideObj[n];
							System.out.println("stuInsideObj[" + n + "]:" + stuInsideObj.toString());
						}
						// 从视角包裹内物品个数
						int nSlaveViewObjectNum = msg.nSlaveViewObjectNum;
						System.out.println("nSlaveViewObjectNum:" + nSlaveViewObjectNum);
						// 从视角包裹内物品信息
						for (int n = 0; n < nSlaveViewObjectNum; n++) {
							NET_INSIDE_OBJECT stuSlaveViewInsideObj = msg.stuSlaveViewInsideObj[n];
							System.out.println("stuSlaveViewInsideObj[" + n + "]:" + stuSlaveViewInsideObj.toString());
						}
						// 图片数量
						int nImageCount = msg.nImageCount;
						System.out.println("nImageCount:" + nImageCount);
						int picSizes = 0;
						// 图片信息
						for (int n = 0; n < nImageCount; n++) {
							NET_XRAY_IMAGE_INFO stuImageInfo = msg.stuImageInfo[n];
							System.out.println("stuImageInfo[" + n + "]:" + stuImageInfo.toString());
							// 图片
							if (stuImageInfo != null && stuImageInfo.nLength > 0) {

								picSizes += stuImageInfo.nLength;

								String picture = picturePath + "\\" + System.currentTimeMillis() + "related.jpg";
								ToolKits.savePicture(pBuf, stuImageInfo.nOffset, stuImageInfo.nLength, picture);
							}
						}

						// 客户自定义信息个数
						int nViewCustomInfoNum = msg.nViewCustomInfoNum;
						System.out.println("nViewCustomInfoNum:" + nViewCustomInfoNum);
						// 客户自定义信息, X光机定制专用
						for (int n = 0; n < nViewCustomInfoNum; n++) {
							NetSDKLib.NET_XRAY_CUSTOM_INFO stuViewCustomInfo = msg.stuViewCustomInfo[n];
							System.out.println("stuViewCustomInfo[" + n + "]:" + stuViewCustomInfo.toString());
						}
						// 包裹标识
						String szPackageTag = Arrays.toString(msg.szPackageTag);
						System.out.println("szPackageTag:" + szPackageTag);
						// 包裹产生方式
						int emPackageMode = msg.emPackageMode;
						System.out.println("emPackageMode:" + emPackageMode);
						// 关联图片数量
						int nRelatedImageNum = msg.nRelatedImageNum;
						System.out.println("nRelatedImageNum:" + nRelatedImageNum);
						// 客户自定义信息, X光机定制专用
						for (int n = 0; n < nRelatedImageNum; n++) {
							NET_XRAY_RELATED_IMAGE_INFO stuRelatedImageInfo = msg.stuRelatedImageInfo[n];
							System.out.println("stuRelatedImageInfo[" + n + "]:" + stuRelatedImageInfo.toString());
							// 图片 stuRelatedImageInfo
							if (stuRelatedImageInfo != null && stuRelatedImageInfo.nLength > 0) {

								picSizes += stuRelatedImageInfo.nLength;

								String picture = picturePath + "\\" + System.currentTimeMillis() + "related2.jpg";
								ToolKits.savePicture(pBuf, stuRelatedImageInfo.nOffset, stuRelatedImageInfo.nLength,
										picture);
							}
						}
						break;
					}
					default:
						System.out.println(" emEventType:" + emEventType);
						break;

					}
				}
			}

			return 0;
		}
	}

	public void RunTest() {
		CaseMenu menu = new CaseMenu();
		menu.addItem(new CaseMenu.Item(this, "getXRayMultiLevelDetectCFG", "getXRayMultiLevelDetectCFG"));
		menu.addItem(new CaseMenu.Item(this, "setXRayMultiLevelDetectCFG", "setXRayMultiLevelDetectCFG"));
		menu.addItem(new CaseMenu.Item(this, "findAnalyseTask", "findAnalyseTask"));

		menu.addItem(new CaseMenu.Item(this, "AttachAnalyseTaskResult", "AttachAnalyseTaskResult"));

		menu.addItem(new CaseMenu.Item(this, "detachAnalyseTaskResult", "detachAnalyseTaskResult"));

		menu.addItem(new CaseMenu.Item(this, "PushAnalysePictureFile", "PushAnalysePictureFile"));

		menu.run();
	}

	public static void main(String[] args) {
		InitTest("10.35.35.50", 37777, "admin", "admin123");

		byte[] sSerialNumber = m_stDeviceInfo.sSerialNumber;

		System.out.println("序列号:" + new String(sSerialNumber));

		new XRayMultiLevelDemo().RunTest();
		LoginOut();
	}
}
