package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.DEV_EVENT_TRAFFIC_ELETAGINFO_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_RFIDELETAG_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_TIME_EX;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Pointer;

import java.io.File;
import java.io.UnsupportedEncodingException;

/**
 * @author 291189
 * @version 1.0
 * @description ERR221011003
 * @date 2022/10/13 14:47
 */
public class TrafficEletaginfoDemo extends Initialization {
    int channel=-1;
    NetSDKLib.LLong    attachHandle=new NetSDKLib.LLong(0);
    /**
     * 订阅智能任务
     */
    public NetSDKLib.LLong AttachEventRealLoadPic() {
        // 先退订，设备不会对重复订阅作校验，重复订阅后会有重复的事件返回
        if(attachHandle.longValue()!=0){
            this.DetachEventRealLoadPic();
        }

        // 需要图片
        int bNeedPicture = 1;
        attachHandle = netSdk.CLIENT_RealLoadPictureEx(loginHandle, channel, NetSDKLib.EVENT_IVS_TRAFFIC_ELETAGINFO, bNeedPicture,
                AnalyzerDataCB.getInstance(), null, null);
        if (attachHandle.longValue() != 0) {
            System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Success\n", channel);
        } else {
            System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Failed!LastError = %s\n", channel,
                    ToolKits.getErrorCode());
        }

        return attachHandle;
    }
    /**
     * 报警事件（智能）回调
     */
    private static class AnalyzerDataCB implements NetSDKLib.fAnalyzerDataCallBack {
        private final File picturePath;
        private static AnalyzerDataCB instance;

        private AnalyzerDataCB() {
            picturePath = new File("./AnalyzerPicture/");
            if (!picturePath.exists()) {
                picturePath.mkdirs();
            }
        }

        public static AnalyzerDataCB getInstance() {
            if (instance == null) {
                synchronized (AnalyzerDataCB.class) {
                    if (instance == null) {
                        instance = new AnalyzerDataCB();
                    }
                }
            }
            return instance;
        }

        @Override
        public int invoke(NetSDKLib.LLong lAnalyzerHandle, int dwAlarmType, Pointer pAlarmInfo, Pointer pBuffer, int dwBufSize,
                          Pointer dwUser, int nSequence, Pointer reserved) {
            if (lAnalyzerHandle == null || lAnalyzerHandle.longValue() == 0) {
                return -1;
            }

            switch (dwAlarmType) {
                case NetSDKLib.EVENT_IVS_TRAFFIC_ELETAGINFO : {  // RFID电子车牌标签事件(对应 DEV_EVENT_TRAFFIC_ELETAGINFO_INFO)
                    System.out.println("RFID电子车牌标签事件");
                    DEV_EVENT_TRAFFIC_ELETAGINFO_INFO msg=new DEV_EVENT_TRAFFIC_ELETAGINFO_INFO();
                    ToolKits.GetPointerData(pAlarmInfo, msg);
                    /**
                     0:脉冲 1:开始 2:停止
                     */
                    int nAction = msg.nAction;
                    System.out.println("nAction:"+nAction);

                    /**
                     通道号
                     */
                    int nChannelID = msg.nChannelID;
                    System.out.println("nChannelID:"+nChannelID);

                    /**
                     事件名称
                     */
                    byte[] szName = msg.szName;
                    try {
                        System.out.println("szName:"+new String(szName,encode));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    /**
                     时间戳(单位是毫秒)
                     */
                    double pts = msg.PTS;
                    System.out.println("pts:"+pts);

                    /**
                     事件发生的时间
                     */
                    NET_TIME_EX utc = msg.UTC;
                    System.out.println("utc:"+utc.toStringTime());

                    /**
                     RFID电子车牌标签信息
                     */
                    NET_RFIDELETAG_INFO stuRFIDEleTagInfo = msg.stuRFIDEleTagInfo;

                    /**
                     卡号
                     */
                    byte[] szCardID = stuRFIDEleTagInfo.szCardID;
                    try {
                        System.out.println("szCardID:"+new String(szCardID,encode));
                    /**
                     卡号类型, 0:交通管理机关发行卡, 1:新车出厂预装卡
                     */
                        int nCardType = stuRFIDEleTagInfo.nCardType;
                        System.out.println("nCardType:"+nCardType);
                    /**
                     卡号省份 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_CARD_PROVINCE}
                     */
                        int emCardPrivince = stuRFIDEleTagInfo.emCardPrivince;
                        System.out.println("emCardPrivince:"+emCardPrivince);
                        /**
                         车牌号码
                         */
                        byte[] szPlateNumber = stuRFIDEleTagInfo.szPlateNumber;
                        System.out.println("szPlateNumber:"+new String(szPlateNumber,encode));
                        /**
                         出厂日期
                         */
                        byte[] szProductionDate = stuRFIDEleTagInfo.szProductionDate;
                        System.out.println("szProductionDate:"+new String(szProductionDate,encode));

                        /**
                         车辆类型 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_CAR_TYPE}
                         */
                        int emCarType = stuRFIDEleTagInfo.emCarType;
                        System.out.println("emCarType:"+emCarType);

                        /**
                         功率,单位：千瓦时，功率值范围0~254；255表示该车功率大于可存储的最大功率值
                         */
                        int nPower = stuRFIDEleTagInfo.nPower;
                        System.out.println("nPower:"+nPower);

                        /**
                         排量,单位：百毫升，排量值范围0~254；255表示该车排量大于可存储的最大排量值
                         */
                        int nDisplacement = stuRFIDEleTagInfo.nDisplacement;
                        System.out.println("nDisplacement:"+nDisplacement);
                        /**
                         天线ID，取值范围:1~4
                         */
                        int nAntennaID = stuRFIDEleTagInfo.nAntennaID;
                        System.out.println("nAntennaID:"+nAntennaID);
                        /**
                         号牌种类 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_PLATE_TYPE}
                         */
                        int emPlateType = stuRFIDEleTagInfo.emPlateType;
                        System.out.println("emPlateType:"+emPlateType);

                        /**
                         检验有效期，年-月
                         */
                        byte[] szInspectionValidity = stuRFIDEleTagInfo.szInspectionValidity;
                        System.out.println("szInspectionValidity:"+new String(szInspectionValidity,encode));

                        /**
                         逾期未年检标志, 0:已年检, 1:逾期未年检
                         */
                        int nInspectionFlag = stuRFIDEleTagInfo.nInspectionFlag;
                        System.out.println("nInspectionFlag:"+nInspectionFlag);

                        /**
                         强制报废期，从检验有效期开始，距离强制报废期的年数
                         */
                        int nMandatoryRetirement = stuRFIDEleTagInfo.nMandatoryRetirement;
                        System.out.println("nMandatoryRetirement:"+nMandatoryRetirement);

                        /**
                         车身颜色 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_CAR_COLOR_TYPE}
                         */
                        int emCarColor = stuRFIDEleTagInfo.emCarColor;
                        System.out.println("emCarColor:"+emCarColor);
                        /**
                         核定载客量，该值<0时：无效；此值表示核定载客，单位为人
                         */
                        int nApprovedCapacity = stuRFIDEleTagInfo.nApprovedCapacity;
                        System.out.println("nApprovedCapacity:"+nApprovedCapacity);

                        /**
                         此值表示总质量，单位为百千克；该值<0时：无效；该值的有效范围为0~0x3FF，0x3FF（1023）表示数据值超过了可存储的最大值
                         */
                        int nApprovedTotalQuality = stuRFIDEleTagInfo.nApprovedTotalQuality;
                        System.out.println("nApprovedTotalQuality:"+nApprovedTotalQuality);

                        /**
                         过车时间
                         */
                        NET_TIME_EX stuThroughTime = stuRFIDEleTagInfo.stuThroughTime;
                        System.out.println("stuThroughTime:"+stuThroughTime.toStringTime());

                        /**
                         使用性质 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_USE_PROPERTY_TYPE}
                         */
                        int emUseProperty = stuRFIDEleTagInfo.emUseProperty;
                        System.out.println("emUseProperty:"+emUseProperty);
                        /**
                         发牌代号，UTF-8编码
                         */
                        byte[] szPlateCode = stuRFIDEleTagInfo.szPlateCode;
                        System.out.println("szPlateCode:"+new String(szPlateCode,"UTF-8"));
                        /**
                         号牌号码序号，UTF-8编码
                         */
                        byte[] szPlateSN = stuRFIDEleTagInfo.szPlateSN;
                        System.out.println("szPlateSN:"+new String(szPlateSN,"UTF-8"));
                        /**
                         标签(唯一标识), UTF-8编码
                         */
                        byte[] szTID = stuRFIDEleTagInfo.szTID;
                        System.out.println("szTID:"+new String(szTID,"UTF-8"));

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }



                    break;
                }
                default:
                    System.out.println("其他事件--------------------"+ dwAlarmType);
                    break;
            }
            return 0;
        }
    }


    /**
     * 停止侦听智能事件
     */
    public void DetachEventRealLoadPic() {
        if (this.attachHandle.longValue() != 0) {
            netSdk.CLIENT_StopLoadPic(this.attachHandle);
        }
    }
    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "AttachEventRealLoadPic" , "AttachEventRealLoadPic")));
        menu.addItem((new CaseMenu.Item(this , "DetachEventRealLoadPic" , "DetachEventRealLoadPic")));

        menu.run();
    }

    public static void main(String[] args) {
        TrafficEletaginfoDemo trafficEletaginfoDemo=new TrafficEletaginfoDemo();
        InitTest("20.2.36.32",37777,"admin","admin123");
        trafficEletaginfoDemo.RunTest();
        LoginOut();

    }

}
