package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.*;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import java.io.UnsupportedEncodingException;

/**
 * @author 291189
 * @version 1.0
 * @description GIP220817015 
 * @date 2022/8/31 14:27
 */
public class LowRateWPANDemo extends Initialization {


        public void getAndSetAccessoryInfo(){
            NET_GET_ACCESSORY_INFO info=new NET_GET_ACCESSORY_INFO();

            info.nSNNum=0;
            info.nMaxInfoNum=2;

            NET_WPAN_ACCESSORY_INFO[] wpInfo=new NET_WPAN_ACCESSORY_INFO[info.nMaxInfoNum];
            for(int i=0;i<wpInfo.length;i++){
                wpInfo[i]=new NET_WPAN_ACCESSORY_INFO();
            }
            int len
                    = new NET_WPAN_ACCESSORY_INFO().size() * wpInfo.length;
            Pointer wpInfoPointer = new Memory(len);
            wpInfoPointer.clear(len);
            ToolKits.SetStructArrToPointerData(wpInfo,wpInfoPointer);

            info.pstuInfo=wpInfoPointer;

            Pointer params = new Memory(info.size());
            params.clear(info.size());
            ToolKits.SetStructDataToPointer(info,params,0);
            IntByReference intRetLen = new IntByReference(0);
            boolean bRet = netSdk.CLIENT_QueryDevState(loginHandle, NetSDKLib.NET_DEVSTATE_GET_ACCESSORY_INFO, //获取配件信息(对应 NET_GET_ACCESSORY_INFO)
                    params, info.size(), intRetLen, 5000);

            if(bRet) {
                ToolKits.GetPointerData(params,info);
                Native.free(Pointer.nativeValue(params));
               Pointer.nativeValue(params, 0);

                System.out.println("CLIENT_QueryDevState success!");

                int nInfoNum = info.nInfoNum;

                System.out.println("nInfoNum:"+nInfoNum);
                ToolKits.GetPointerDataToStructArr( info.pstuInfo,wpInfo);
                Native.free(Pointer.nativeValue(wpInfoPointer));
                Pointer.nativeValue(wpInfoPointer, 0);

                for(int i=0;i<nInfoNum;i++){
                    NET_WPAN_ACCESSORY_INFO net_wpan_accessory_info
                            = wpInfo[i];
                    System.out.println("["+i+"]bArmingWithoutPassword:"+net_wpan_accessory_info.bArmingWithoutPassword);

                    System.out.println("["+i+"]bRecordEnable:"+net_wpan_accessory_info.bRecordEnable);

                    System.out.println("["+i+"]bExternalAlarmEnable:"+net_wpan_accessory_info.bExternalAlarmEnable);

                    try {
                        System.out.println("["+i+"]szSN:"+new String(net_wpan_accessory_info.szSN,encode) );
                        System.out.println("["+i+"]szName:"+new String(net_wpan_accessory_info.szName,encode) );
                        System.out.println("["+i+"]szSN:"+new String(net_wpan_accessory_info.szVersion,encode) );
                        System.out.println("["+i+"]szName:"+new String(net_wpan_accessory_info.szModel,encode) );
                        System.out.println("["+i+"]szSN:"+new String(net_wpan_accessory_info.szAlarmTone,encode) );
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                }

            } else {
                Native.free(Pointer.nativeValue(params));
                Pointer.nativeValue(params, 0);

                System.err.println("CLIENT_QueryDevState Failed!" + ToolKits.getErrorCode());
              return;
            }

            NET_IN_CTRL_LOWRATEWPAN_ACCESSORY_PARAM paramInput = new NET_IN_CTRL_LOWRATEWPAN_ACCESSORY_PARAM();

            paramInput.stuInfo=wpInfo[0];

            Pointer paramInputPointer = new Memory(paramInput.size());
            paramInputPointer.clear(paramInput.size());
            ToolKits.SetStructDataToPointer(paramInput,paramInputPointer,0);

            boolean flg = netSdk.CLIENT_ControlDevice(loginHandle, NetSDKLib.CtrlType.CTRL_LOWRATEWPAN_SET_ACCESSORY_PARAM, paramInputPointer,5000);
            if (flg) {
                System.out.println("设置配件信息功能成功");
            }else{
                System.err.println("设置配件信息功能失败:" +ToolKits.getErrorCode());

            }
            Native.free(Pointer.nativeValue(paramInputPointer));
            Pointer.nativeValue(paramInputPointer, 0);
        }
//获取设备状态
        public void GetUnifiedStatus(){

            NET_IN_UNIFIEDINFOCOLLECT_GET_DEVSTATUS inPut=new NET_IN_UNIFIEDINFOCOLLECT_GET_DEVSTATUS();

            Pointer paramInputPointer = new Memory(inPut.size());
            paramInputPointer.clear(inPut.size());
            ToolKits.SetStructDataToPointer(inPut,paramInputPointer,0);


            NET_OUT_UNIFIEDINFOCOLLECT_GET_DEVSTATUS outPut=new NET_OUT_UNIFIEDINFOCOLLECT_GET_DEVSTATUS();

            Pointer paramOutPointer = new Memory(outPut.size());
            paramOutPointer.clear(outPut.size());
            ToolKits.SetStructDataToPointer(outPut,paramOutPointer,0);


            boolean flg = netSdk.CLIENT_GetUnifiedStatus(loginHandle, paramInputPointer, paramOutPointer, 5000);
            if (flg) {
                ToolKits.GetPointerData(paramOutPointer,outPut);
                Native.free(Pointer.nativeValue(paramInputPointer));
                Pointer.nativeValue(paramInputPointer, 0);
                System.out.println("获取设备状态成功 ");

                int emTamperState = outPut.emTamperState;

                NET_DEVSTATUS_POWER_INFO stuPowerInfo
                        = outPut.stuPowerInfo;

                System.out.println("emTamperState:"+emTamperState);
                System.out.println("emPowerType:"+stuPowerInfo.emPowerType);
                System.out.println("nBatteryPercent:"+stuPowerInfo.nBatteryPercent);
                System.out.println("emTamperState:"+emTamperState);

                try {
                    System.out.println("szVersion:"+new String(outPut.szVersion,encode));

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                NET_DEVSTATUS_NET_INFO stuNetInfo = outPut.stuNetInfo;

                System.out.println("nWifiIntensity:"+stuNetInfo.nWifiIntensity);
                System.out.println("nWifiSignal:"+stuNetInfo.nWifiSignal);
                System.out.println("nCellulSignal:"+stuNetInfo.nCellulSignal);
                System.out.println("nCellulIntensity:"+stuNetInfo.nCellulIntensity);

                System.out.println("emEthState:"+stuNetInfo.emEthState);

                System.out.println("n3Gflux:"+stuNetInfo.n3Gflux);

                System.out.println("n3GfluxByTime:"+stuNetInfo.n3GfluxByTime);

                System.out.println("emWifiState:"+stuNetInfo.emWifiState);

                System.out.println("emCellularstate:"+stuNetInfo.emCellularstate);

                System.out.println("nSimNum:"+stuNetInfo.nSimNum);

                NET_DEVSTATUS_SIM_INFO[] stuSimInfo = stuNetInfo.stuSimInfo;

                for(int i=0;i<stuNetInfo.nSimNum;i++){
                    NET_DEVSTATUS_SIM_INFO sim_info = stuSimInfo[i];

                    byte byIndex = sim_info.byIndex;

                    int emStatus = sim_info.emStatus;

                    System.out.println("byIndex:"+byIndex);

                    System.out.println("emStatus:"+emStatus);

                }


            }else {
                Native.free(Pointer.nativeValue(paramInputPointer));
                Pointer.nativeValue(paramInputPointer, 0);
                System.err.println("获取设备状态失败:" +ToolKits.getErrorCode());

            }

        }


    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "getAndSetAccessoryInfo" , "getAndSetAccessoryInfo")));
        menu.addItem((new CaseMenu.Item(this , "GetUnifiedStatus" , "GetUnifiedStatus")));
        menu.run();
    }

    public static void main(String[] args) {
        LowRateWPANDemo lowRateWPANDemo=new LowRateWPANDemo();
        InitTest("172.3.3.135",37777,"admin","admin123");
        lowRateWPANDemo.RunTest();
        LoginOut();

    }

}
