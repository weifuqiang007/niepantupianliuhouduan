package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_IN_ATTACH_GYRO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_NOTIFY_GYRO_DATA_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_OUT_ATTACH_GYRO;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 291189
 * @version 1.0
 * @description GIP220929012 
 * @date 2022/10/26 16:13
 */
public class AttachGyroDemo extends Initialization {

    NetSDKLib.LLong lAttachHandle=new   NetSDKLib.LLong();

    static List<Pointer> pointers=new ArrayList<>();
    /**
     * 订阅陀螺仪数据接口
     */
    public void attachGyro(){

        String dwUser="{"+"ip:"+ipAddr+",port:"+port+"}";

// CLIENT_AttachGyro接口入参
        NET_IN_ATTACH_GYRO input=new NET_IN_ATTACH_GYRO();
        Pointer dwUserData
                = GetStringToPointer(dwUser, encode);
        input.cbNotifyGyroData=NotifyGyroData.getInstance();
        input.dwUser=dwUserData;
         Pointer inputPointer =new Memory(input.size());

         inputPointer.clear(input.size());

         ToolKits.SetStructDataToPointer(input,inputPointer,0);

       //CLIENT_AttachGyro接口出参
         NET_OUT_ATTACH_GYRO outPut=new NET_OUT_ATTACH_GYRO();

        Pointer outPutPointer =new Memory(outPut.size());

        outPutPointer.clear(outPut.size());

        ToolKits.SetStructDataToPointer(outPut,outPutPointer,0);

        lAttachHandle = netSdk.CLIENT_AttachGyro(loginHandle,inputPointer,outPutPointer,3000 );

        if(lAttachHandle.longValue()!=0){
            System.out.println(" CLIENT_AttachGyro Success");
            pointers.add(dwUserData);
        }else {
            System.out.println("CLIENT_AttachGyro Failed!LastError "+
                    ToolKits.getErrorCode());
        }
    }

    /**
     * 取消陀螺仪数据订阅接口
     */
    public void detachGyro(){

        if(lAttachHandle.longValue()!=0){

            boolean b
                    = netSdk.CLIENT_DetachGyro(lAttachHandle);

            if(b){
                System.out.println(" CLIENT_DetachGyro Success");

            }else {
                System.out.println("CLIENT_DetachGyro Failed!LastError "+
                        ToolKits.getErrorCode());
            }

        }


    }

    /**
     *   订阅陀螺仪数据接口回调函数原型, lAttachGyroHandle为CLIENT_AttachGyro接口的返回值
     */
    private static class NotifyGyroData implements NetSDKLib.fNotifyGyroData {
        private final File picturePath;
        private static NotifyGyroData instance;

        private NotifyGyroData() {
            picturePath = new File("./AnalyzerPicture/");
            if (!picturePath.exists()) {
                picturePath.mkdirs();
            }
        }

        public static NotifyGyroData getInstance() {
            if (instance == null) {
                synchronized (NotifyGyroData.class) {
                    if (instance == null) {
                        instance = new NotifyGyroData();
                    }
                }
            }
            return instance;
        }


        @Override
        public void invoke(NetSDKLib.LLong lAttachGyroHandle, Pointer pstuGyroDataInfo, Pointer dwUser) {

            String s = GetPointerDataToString(dwUser,encode);
            System.out.println("dwUser:"+s);
            NET_NOTIFY_GYRO_DATA_INFO msg=new NET_NOTIFY_GYRO_DATA_INFO();

            ToolKits.GetPointerData(pstuGyroDataInfo,msg);

            System.out.println("msg:"+msg.toString());


        }
    }

    public static Pointer GetStringToPointer(String src,String charset) {
        Pointer pointer = null;
        try {
            byte[] b = src.getBytes(charset);

            pointer = new Memory(b.length + 1);
            pointer.clear(b.length + 1);

            pointer.write(0, b, 0, b.length);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return pointer;
    }

    public static String GetPointerDataToString(Pointer pointer,String charset) {
        String str = "";
        if (pointer == null) {
            return str;
        }

        int length = 0;
        byte[] bufferPlace = new byte[1];

        for (int i = 0; i < 2048; i++) {
            pointer.read(i, bufferPlace, 0, 1);
            if (bufferPlace[0] == '\0') {
                length = i;
                break;
            }
        }

        if (length > 0) {
            byte[] buffer = new byte[length];
            pointer.read(0, buffer, 0, length);
            try {
                str = new String(buffer, charset).trim();
            } catch (UnsupportedEncodingException e) {
                return str;
            }
        }

        return str;
    }
    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();
        menu.addItem(new CaseMenu.Item(this , "订阅陀螺仪数据接口" , "attachGyro"));
        menu.addItem(new CaseMenu.Item(this , "取消陀螺仪数据订阅接口" , "detachGyro"));
        menu.run();
    }

    private static String ipAddr 			= "10.55.157.161";
    private static int    port 				= 37777;
    private static String user 			    = "admin";
    private static String password 		    = "admin123";

    public static void main(String[] args){
        InitTest(ipAddr, port, user, password);
        AttachGyroDemo attachGyroDemo=new AttachGyroDemo();
        attachGyroDemo.RunTest();
        LoginOut();
    }
}
