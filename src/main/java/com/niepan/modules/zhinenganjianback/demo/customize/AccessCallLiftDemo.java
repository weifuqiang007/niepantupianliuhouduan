package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_CTRL_ACCESS_CALL_LIFT;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import java.io.UnsupportedEncodingException;


/**
 * @author 291189
 * @version 1.0
 * @description ERR221010116 
 * @date 2022/10/19 13:43
 */
public class AccessCallLiftDemo extends Initialization {

    public static void StringToByteArr(String src, byte[] dst,String charset) {
        try {
            byte[] GBKBytes = src.getBytes(charset);
            for (int i = 0; i < GBKBytes.length; i++) {
                dst[i] = (byte) GBKBytes[i];
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void callLift() {
        NET_CTRL_ACCESS_CALL_LIFT callLift=new NET_CTRL_ACCESS_CALL_LIFT();

        /**
         呼叫电梯的方式 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_CALLLIFT_ACTION}
         */
        callLift.emCallLiftAction=2;
        /**
         呼叫电梯命令 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_CALLLIFT_CMD}
         */
        callLift.emCallLiftCmd=1;

        String srcFloor="0100";

        StringToByteArr(srcFloor,callLift.szSrcFloor,encode);

        String destFloor="0201";

        StringToByteArr(destFloor,callLift.szDestFloor,encode);

        callLift.write();
        boolean result = netSdk.CLIENT_ControlDevice(loginHandle,
                NetSDKLib.CtrlType.CTRLTYPE_CTRL_ACCESS_CALL_LIFT, callLift.getPointer(), 5000);
        if(!result){
            System.err.println("remove fingerprint failed." + ToolKits.getErrorCode());
            return;
        }
        callLift.read();
            /**
             通道号
             */
        int nChannelID
                = callLift.nChannelID;

    System.out.println("nChannelID:"+nChannelID);

        /**
         用户ID
         */
        byte[] szUserID
                = callLift.szUserID;
        try {
            System.out.println("szUserID:"+new String(szUserID,encode));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        /**
         呼叫电梯个数
         */
        int nCallLiftCount
                = callLift.nCallLiftCount;
        System.out.println("nCallLiftCount:"+nCallLiftCount);
        /**
         呼叫电梯号
         */
        int[] nCallLiftNo = callLift.nCallLiftNo;
        for(int i=0;i<nCallLiftCount;i++){
            System.out.println("["+i+"]:"+nCallLiftNo[i]);
        }
        /**
         呼梯类型 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_LIFT_CALLER_TYPE}
         */
        System.out.println("emLiftCaller:"+callLift.emLiftCaller);

    }


    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();
        menu.addItem(new CaseMenu.Item(this , "门禁控制-呼梯" , "callLift"));
        menu.run();
    }


    private static String ipAddr 			= "172.3.11.41";
    private static int    port 				= 37777;
    private static String user 			    = "admin";
    private static String password 		    = "admin123";

    public static void main(String[] args){
        InitTest(ipAddr, port, user, password);
        AccessCallLiftDemo accessCallLiftDemo=new AccessCallLiftDemo();
        accessCallLiftDemo.RunTest();
        LoginOut();
    }
}
