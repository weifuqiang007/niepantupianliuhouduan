package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_DEV_EVENT_ANTI_COUNTERFEIT_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_TIME_EX;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_UINT_POINT;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Pointer;
import java.io.File;
import java.io.UnsupportedEncodingException;
import static com.niepan.modules.zhinenganjianback.lib.NetSDKLib.EVENT_IVS_ANTI_COUNTERFEIT;

/**
 * @author 291189
 * @version 1.0
 * @description  GIP220808005  
 * @date 2022/8/24 14:58
 */
public class CounterFeitDemo extends Initialization {

    int channel = -1;
    NetSDKLib.LLong attachHandle = new NetSDKLib.LLong(0);

    /**
     * 订阅智能任务
     */

    public NetSDKLib.LLong AttachEventRealLoadPic() {
        // 先退订，设备不会对重复订阅作校验，重复订阅后会有重复的事件返回
        if (attachHandle.longValue() != 0) {
            this.DetachEventRealLoadPic();
        }

        // 需要图片
        int bNeedPicture = 1;
        attachHandle = netSdk.CLIENT_RealLoadPictureEx(loginHandle, channel, EVENT_IVS_ANTI_COUNTERFEIT, bNeedPicture,
                AnalyzerDataCB.getInstance(), null, null);
        if (attachHandle.longValue() != 0) {
            System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Success\n", channel);
        } else {
            System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Failed!LastError = %s\n", channel,
                    ToolKits.getErrorCode());
        }

        return attachHandle;
    }

    /**
     * 报警事件（智能）回调
     */
    private static class AnalyzerDataCB implements NetSDKLib.fAnalyzerDataCallBack {
        private final File picturePath;
        private static AnalyzerDataCB instance;

        private AnalyzerDataCB() {
            picturePath = new File("./AnalyzerPicture/");
            if (!picturePath.exists()) {
                picturePath.mkdirs();
            }
        }

        public static AnalyzerDataCB getInstance() {
            if (instance == null) {
                synchronized (AnalyzerDataCB.class) {
                    if (instance == null) {
                        instance = new AnalyzerDataCB();
                    }
                }
            }
            return instance;
        }


        @Override
        public int invoke(NetSDKLib.LLong lAnalyzerHandle, int dwAlarmType, Pointer pAlarmInfo, Pointer pBuffer, int dwBufSize,
                          Pointer dwUser, int nSequence, Pointer reserved) {
            if (lAnalyzerHandle == null || lAnalyzerHandle.longValue() == 0) {
                return -1;
            }

            switch (dwAlarmType) {
                case EVENT_IVS_ANTI_COUNTERFEIT: { //  防造假检测事件(对应 NET_DEV_EVENT_ANTI_COUNTERFEIT_INFO)

                    System.out.println("防造假检测事件");

                    NET_DEV_EVENT_ANTI_COUNTERFEIT_INFO msg=new NET_DEV_EVENT_ANTI_COUNTERFEIT_INFO();

                    ToolKits.GetPointerData(pAlarmInfo, msg);

                    int nAction = msg.nAction;
                    System.out.println("nAction:"+nAction);

                    int nChannelID = msg.nChannelID;
                    System.out.println("nChannelID:"+nChannelID);

                    byte[] szName = msg.szName;
                    byte[] szClass = msg.szClass;
                    try {
                        System.out.println("szName utf-8:"+new String(szName,"UTF-8"));
                        System.out.println("szName gbk:"+new String(szName,"GBK"));

                        System.out.println("szClass utf-8:"+new String(szClass,"UTF-8"));
                        System.out.println("szClass gbk:"+new String(szClass,"GBK"));

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    int nRuleID = msg.nRuleID;
                    System.out.println("nRuleID:"+nRuleID);

                    int nGroupID = msg.nGroupID;
                    System.out.println("nGroupID:"+nGroupID);

                    int nCountInGroup = msg.nCountInGroup;
                    System.out.println("nCountInGroup:"+nCountInGroup);

                    int nIndexInGroup = msg.nIndexInGroup;
                    System.out.println("nIndexInGroup:"+nIndexInGroup);

                    NET_TIME_EX utc = msg.UTC;
                    System.out.println("utc:"+utc.toStringTime());

                    int nEventID = msg.nEventID;
                    System.out.println("nEventID:"+nEventID);

                    int nSequence1 = msg.nSequence;
                    System.out.println("nSequence1:"+nSequence1);


                    int nFrameSequence = msg.nFrameSequence;
                    System.out.println("nFrameSequence:"+nFrameSequence);


                    int nDetectRegionNum = msg.nDetectRegionNum;
                    System.out.println("nDetectRegionNum:"+nDetectRegionNum);

                    NET_UINT_POINT[] stuDetectRegion = msg.stuDetectRegion;

                    for(int i=0;i<nDetectRegionNum;i++){
                        NET_UINT_POINT point = stuDetectRegion[i];
                        System.out.println("["+i+"]:"+point.nx+":"+point.ny);
                    }

                    byte[] szDirection = msg.szDirection;
                    byte[] szAction = msg. szAction;
                    try {
                        System.out.println("szDirection utf-8:"+new String(szDirection,"UTF-8"));
                        System.out.println("szDirection gbk:"+new String(szDirection,"GBK"));

                        System.out.println("szAction utf-8:"+new String(szAction,"UTF-8"));
                        System.out.println("szAction gbk:"+new String(szAction,"GBK"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    NetSDKLib.SCENE_IMAGE_INFO stuSceneImage = msg.stuSceneImage;

                    if ( stuSceneImage.nLength > 0) {
                        String sencePicture = picturePath + "\\" + System.currentTimeMillis() + "sence.jpg";
                        ToolKits.savePicture(pBuffer, stuSceneImage.nOffSet, stuSceneImage.nLength, sencePicture);
                    }

                    NetSDKLib.NET_MSG_OBJECT stuObject = msg.stuObject;

                    //  物体对应图片信息
                    NetSDKLib.NET_PIC_INFO stPicInfo = stuObject.stPicInfo;
                    if ( stPicInfo.dwFileLenth > 0) {
                        String picture = picturePath + "\\" + System.currentTimeMillis() + "pic.jpg";
                        ToolKits.savePicture(pBuffer, stPicInfo.dwOffSet, stPicInfo.dwFileLenth, picture);
                    }


                    int nMark = msg.nMark;
                    System.out.println("nMark:"+nMark);

                    int nSource = msg.nSource;
                    System.out.println("nSource:"+nSource);

                    int nEventSeq = msg.nEventSeq;
                    System.out.println("nEventSeq:"+nEventSeq);

                    break;
                }
                default:
                    System.out.println("其他事件--------------------" + dwAlarmType);
                    break;
            }
            return 0;
        }
    }

    /**
     * 停止侦听智能事件
     */
    public void DetachEventRealLoadPic() {
        if (this.attachHandle.longValue() != 0) {
            netSdk.CLIENT_StopLoadPic(this.attachHandle);
        }
    }

    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "AttachEventRealLoadPic" , "AttachEventRealLoadPic")));
        menu.addItem((new CaseMenu.Item(this , "DetachEventRealLoadPic" , "DetachEventRealLoadPic")));
        menu.run();
    }

    public static void main(String[] args) {
        CounterFeitDemo counterFeitDemo=new CounterFeitDemo();
        InitTest("172.12.8.67",37777,"admin","admin123");
        counterFeitDemo.RunTest();
        LoginOut();

    }
}
