package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Memory;

/**
 * @author 291189
 * @version 1.0
 * @description ERR221212121
 * @date 2022/12/15 16:05
 */
public class FaceOpenDoorDemo extends Initialization {

    /**
     * 人脸开门
     */

    public void Test_FaceOpenDoor(){
        if(loginHandle.longValue()==0){
            return;
        }
        //入参
        NetSDKLib.NET_IN_FACE_OPEN_DOOR pInParam=new NetSDKLib.NET_IN_FACE_OPEN_DOOR();

        pInParam.nChannel=0;
        /**
         *        比对结果, {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_COMPARE_RESULT}
          */
        pInParam.emCompareResult=1;

        NetSDKLib.NET_OPENDOOR_MATCHINFO stuMatchInfo
                = pInParam.stuMatchInfo;

        NetSDKLib.NET_CUSTOM_EDUCATION_INFO   info=new  NetSDKLib.NET_CUSTOM_EDUCATION_INFO();

        String content="hello";

        StringToByteArr(content,info.szInfoContent,encode);

        stuMatchInfo.pstuCustomEducationInfo=new Memory(info.size());

        stuMatchInfo.pstuCustomEducationInfo.clear( info.size() );

        ToolKits.SetStructDataToPointer(info,stuMatchInfo.pstuCustomEducationInfo,0);


        //出参
        NetSDKLib.NET_OUT_FACE_OPEN_DOOR pOutParam=new NetSDKLib.NET_OUT_FACE_OPEN_DOOR();

        boolean bRet =netSdk.CLIENT_FaceOpenDoor(loginHandle, pInParam, pOutParam, 3000);
        if (!bRet)
        {
            System.err.printf("Test_FaceOpenDoor field Fail.Error[%s]\n", ToolKits.getErrorCode());
            return;
        }
    }


    public static void StringToByteArr(String src, byte[] dst,String charset) {
        try {
            byte[] GBKBytes = src.getBytes(charset);
            for (int i = 0; i < GBKBytes.length; i++) {
                dst[i] = (byte) GBKBytes[i];
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "Test_FaceOpenDoor" , "Test_FaceOpenDoor")));

        menu.run();
    }

    public static void main(String[] args) {
        FaceOpenDoorDemo demo=new FaceOpenDoorDemo();
        InitTest("172.10.6.182",37777,"admin","admin123");
        demo.RunTest();
        LoginOut();
    }
}
