package com.niepan.modules.zhinenganjianback.demo.customize;

import static com.niepan.modules.zhinenganjianback.lib.Utils.getOsPrefix;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Objects;
import java.util.Scanner;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.LLong;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.NET_GET_CHANNEL_INFO;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.NET_GET_DEVICE_INFO;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.NET_IN_GET_CHANNEL_INFO;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.NET_IN_GET_DEVICE_LIST_INFO;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.NET_OUT_GET_CHANNEL_INFO;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.NET_OUT_GET_DEVICE_LIST_INFO;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultDisconnectCallback;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultHaveReconnectCallBack;
import com.niepan.modules.zhinenganjianback.lib.enumeration.EM_EVENT_IVS_TYPE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_IN_ATTACH_VIDEO_STATISTICS;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_OUT_ATTACH_VIDEO_STATISTICS;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_PEOPLE_REGION_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_VIDEOSTAT_CROWD_DISTRI_MAP;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;

public class AttachVideoStatisticsDemo {
	// SDk对象初始化
	public static final NetSDKLib netsdk = NetSDKLib.NETSDK_INSTANCE;
	public static final NetSDKLib configsdk = NetSDKLib.CONFIG_INSTANCE;

	// 判断是否初始化
	private static boolean bInit = false;
	// 判断log是否打开
	private static boolean bLogOpen = false;
	// 设备信息
	private NetSDKLib.NET_DEVICEINFO_Ex deviceInfo = new NetSDKLib.NET_DEVICEINFO_Ex();
	// 登录句柄
	private NetSDKLib.LLong m_hLoginHandle = new NetSDKLib.LLong(0);
	// 订阅客流统计服务实时数据句柄
	private NetSDKLib.LLong attachHandle = new NetSDKLib.LLong(0);
	
	// 智能事件订阅句柄
	private NetSDKLib.LLong m_attachHandle = new NetSDKLib.LLong(0);

	// 回调函数需要是静态的，防止被系统回收
	// 断线回调
	private static NetSDKLib.fDisConnect disConnectCB = DefaultDisconnectCallback.getINSTANCE();
	// 重连回调
	private static NetSDKLib.fHaveReConnect haveReConnectCB = DefaultHaveReconnectCallBack.getINSTANCE();

	// 编码格式
	public static String encode;

	static {
		String osPrefix = getOsPrefix();
		if (osPrefix.toLowerCase().startsWith("win32-amd64")) {
			encode = "GBK";
		} else if (osPrefix.toLowerCase().startsWith("linux-amd64")) {
			encode = "UTF-8";
		}
	}

	/**
	 * 获取当前时间
	 */
	public static String GetDate() {
		SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return simpleDate.format(new java.util.Date()).replaceAll("[^0-9]", "-");
	}

	/**
	 * 初始化SDK库
	 */
	public static boolean Init() {
		bInit = netsdk.CLIENT_Init(disConnectCB, null);// 进程启动时，初始化一次
		if (!bInit) {
			System.out.println("Initialize SDK failed");
			return false;
		}
		// 配置日志
		AttachVideoStatisticsDemo.enableLog();

		// 设置断线重连回调接口, 此操作为可选操作，但建议用户进行设置
		netsdk.CLIENT_SetAutoReconnect(haveReConnectCB, null);

		// 设置登录超时时间和尝试次数，可选
		// 登录请求响应超时时间设置为3S
		int waitTime = 3000;
		// 登录时尝试建立链接 1 次
		int tryTimes = 1;
		netsdk.CLIENT_SetConnectTime(waitTime, tryTimes);
		// 设置更多网络参数， NET_PARAM 的nWaittime ， nConnectTryNum 成员与 CLIENT_SetConnectTime
		// 接口设置的登录设备超时时间和尝试次数意义相同,可选
		NetSDKLib.NET_PARAM netParam = new NetSDKLib.NET_PARAM();
		// 登录时尝试建立链接的超时时间
		netParam.nConnectTime = 10000;
		// 设置子连接的超时时间
		netParam.nGetConnInfoTime = 3000;
		netsdk.CLIENT_SetNetworkParam(netParam);
		return true;
	}

	/**
	 * 打开 sdk log
	 */
	private static void enableLog() {
		NetSDKLib.LOG_SET_PRINT_INFO setLog = new NetSDKLib.LOG_SET_PRINT_INFO();
		File path = new File("sdklog/");
		if (!path.exists())
			path.mkdir();

		// 这里的log保存地址依据实际情况自己调整
		String logPath = path.getAbsoluteFile().getParent() + "\\sdklog\\" + "sdklog" + GetDate() + ".log";
		setLog.nPrintStrategy = 0;
		setLog.bSetFilePath = 1;
		System.arraycopy(logPath.getBytes(), 0, setLog.szLogFilePath, 0, logPath.getBytes().length);
		System.out.println(logPath);
		setLog.bSetPrintStrategy = 1;
		bLogOpen = netsdk.CLIENT_LogOpen(setLog);
		if (!bLogOpen)
			System.err.println("Failed to open NetSDK log");
	}

	/**
	 * 高安全登录
	 */
	public void loginWithHighLevel() {
		// 输入结构体参数
		NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY pstlnParam = new NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY() {
			{
				szIP = m_strIpAddr.getBytes();
				nPort = m_nPort;
				szUserName = m_strUser.getBytes();
				szPassword = m_strPassword.getBytes();
			}
		};
		// 输出结构体参数
		NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY pstOutParam = new NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY();

		// 写入sdk
		m_hLoginHandle = netsdk.CLIENT_LoginWithHighLevelSecurity(pstlnParam, pstOutParam);
		if (m_hLoginHandle.longValue() == 0) {
			System.err.printf("Login Device[%s] Port[%d]Failed. %s\n", m_strIpAddr, m_nPort,
					netsdk.CLIENT_GetLastError());
		} else {
			deviceInfo = pstOutParam.stuDeviceInfo; // 获取设备信息
			System.out.println("Login Success");
			System.out.println("Device Address：" + m_strIpAddr);
			System.out.println("设备包含：" + deviceInfo.byChanNum + "个通道");
		}
	}

	/**
	 * 退出
	 */
	public void logOut() {
		if (m_hLoginHandle.longValue() != 0) {
			netsdk.CLIENT_Logout(m_hLoginHandle);
			System.out.println("LogOut Success");
		}
	}

	/**
	 * 清理sdk环境并退出
	 */
	public static void cleanAndExit() {
		if (bLogOpen) {
			netsdk.CLIENT_LogClose(); // 关闭sdk日志打印
		}
		netsdk.CLIENT_Cleanup(); // 进程关闭时，调用一次
		System.exit(0);
	}

	/**
	 * 获取已添加的设备信息
	 */
	public boolean getAllDeviceInfo() {
		String[] deviceIDs = null;//设备id列表为空，默认查全部
		//String[] deviceIDs = { "CXbGbmlFA1E8GQ7A6P1C9R" };
		return getDeviceInfo(deviceIDs);
	}

	public boolean getDeviceInfo(String[] deviceIDs) { // 获取已添加的设备信息

		NET_IN_GET_DEVICE_LIST_INFO pstInParam = new NET_IN_GET_DEVICE_LIST_INFO();
		NET_OUT_GET_DEVICE_LIST_INFO pstOutParam = new NET_OUT_GET_DEVICE_LIST_INFO();

		if (deviceIDs != null) {
			pstInParam.nCount = deviceIDs.length;
			for (int i = 0; i < pstInParam.nCount; i++) {
				System.arraycopy(deviceIDs[i].getBytes(), 0, pstInParam.szDeviceIDsArr[i].szDeviceID, 0,
						deviceIDs[i].getBytes().length);
			}
			pstOutParam.nMaxCount = pstInParam.nCount;
		} else {
			pstInParam.nCount = 0;
			// pstOutParam.nMaxCount = NetSDKLib.MAX_LINK_DEVICE_NUM;// 用户申请的设备个数
			pstOutParam.nMaxCount = 100;// 用户申请的设备个数
		}

		NET_GET_DEVICE_INFO[] deviceInfo = new NET_GET_DEVICE_INFO[pstOutParam.nMaxCount];
		for (int i = 0; i < pstOutParam.nMaxCount; i++) {
			deviceInfo[i] = new NET_GET_DEVICE_INFO();
		}
		pstOutParam.pstuDeviceInfo = new Memory(deviceInfo[0].size() * pstOutParam.nMaxCount);
		pstOutParam.pstuDeviceInfo.clear(deviceInfo[0].size() * pstOutParam.nMaxCount);
		ToolKits.SetStructArrToPointerData(deviceInfo, pstOutParam.pstuDeviceInfo); // 将数组内存拷贝到Pointer

		boolean bRet = netsdk.CLIENT_GetDeviceInfo(m_hLoginHandle, pstInParam, pstOutParam, 3000);
		if (!bRet) {
			System.err.printf("GetDeviceInfo Fail.Last Error[0x%x]\n", netsdk.CLIENT_GetLastError());
			return false;
		}

		ToolKits.GetPointerDataToStructArr(pstOutParam.pstuDeviceInfo, deviceInfo); // 将 Pointer 的内容 输出到 数组
		System.out.println("用户申请的设备个数:" + pstOutParam.nMaxCount);
		System.out.println("实际返回的设备个数:" + pstOutParam.nRetCount);
		for (int i = 0; i < pstOutParam.nRetCount; i++) {
			System.out.println("设备ID:" + new String(deviceInfo[i].szDeviceID).trim());
			System.out.println("url:" + new String(deviceInfo[i].szUrl).trim());
			System.out.println("设备序列号:" + new String(deviceInfo[i].szSerialNo).trim());
			System.out.println("设备大类:" + new String(deviceInfo[i].szDeviceClass).trim());
			System.out.println("设备类型:" + new String(deviceInfo[i].szDeviceType).trim());
			System.out.println("设备名称:" + new String(deviceInfo[i].szDeviceName).trim());
			System.out.println();
		}
		return true;
	}

	/**
	 * 获取设备通道信息
	 * 
	 * @throws UnsupportedEncodingException
	 */
	public void getChannelInfo() throws UnsupportedEncodingException {
		String deviceID = "CXbGbmlFA1E8GQ7A6P1C9R";
		NET_IN_GET_CHANNEL_INFO pstInParam = new NET_IN_GET_CHANNEL_INFO();
		System.arraycopy(deviceID.getBytes(), 0, pstInParam.szDeviceID, 0, deviceID.getBytes().length);

		NET_GET_CHANNEL_INFO[] channelInfo = new NET_GET_CHANNEL_INFO[NetSDKLib.MAX_DEVICE_CHANNEL_NUM];
		for (int i = 0; i < channelInfo.length; i++) {
			channelInfo[i] = new NET_GET_CHANNEL_INFO();
		}

		NET_OUT_GET_CHANNEL_INFO pstOutParam = new NET_OUT_GET_CHANNEL_INFO();
		pstOutParam.nMaxCount = channelInfo.length; // 用户申请的通道个数
		pstOutParam.pstuChannelInfo = new Memory(channelInfo[0].size() * pstOutParam.nMaxCount);
		pstOutParam.pstuChannelInfo.clear(channelInfo[0].size() * pstOutParam.nMaxCount);
		ToolKits.SetStructArrToPointerData(channelInfo, pstOutParam.pstuChannelInfo); // 将数组内存拷贝到Pointer

		boolean bRet = netsdk.CLIENT_GetChannelInfo(m_hLoginHandle, pstInParam, pstOutParam, 3000);
		if (!bRet) {
			System.err.printf("GetChannelInfo Fail.Last Error[0x%x]\n", netsdk.CLIENT_GetLastError());
			return;
		}
		ToolKits.GetPointerDataToStructArr(pstOutParam.pstuChannelInfo, channelInfo);// 将 Pointer 的内容 输出到 数组
		System.out.println("用户申请的通道个数:" + pstOutParam.nMaxCount);
		System.out.println("实际返回的通道个数:" + pstOutParam.nRetCount);
		for (int i = 0; i < pstOutParam.nRetCount; i++) {
			System.out.println("--------获取设备通道信息结果信息---------");
			System.out.println("远程通道号:" + channelInfo[i].nRemoteChannel);
			System.out.println("分配的逻辑通道,-1表示未分配:" + channelInfo[i].nLogicChannel);
			System.out.println("通道名称:" + new String(channelInfo[i].szName, encode));			
		}
	}

	int channel = 27;
	/**
	 * 订阅客流统计服务实时数据
	 */
	public void attachVideoStatistics() {
		NET_IN_ATTACH_VIDEO_STATISTICS input = new NET_IN_ATTACH_VIDEO_STATISTICS();
		input.nChannelID = channel;//逻辑通道号
		input.emType = 1;//业务类型,枚举{@link NET_EM_VS_TYPE}
		input.cbCallBack = VideoStatisticsInfoCallBack.getInstance();
		input.write();

		NET_OUT_ATTACH_VIDEO_STATISTICS outPut = new NET_OUT_ATTACH_VIDEO_STATISTICS();
		outPut.write();

		attachHandle = netsdk.CLIENT_AttachVideoStatistics(m_hLoginHandle, input.getPointer(), outPut.getPointer(),3000);
		if (attachHandle.longValue() == 0) {
			System.out.printf("Chn[%d] CLIENT_AttachVideoStatistics Failed!LastError = %s\n", channel,
					ToolKits.getErrorCode());
		} else {
			System.out.printf("Chn[%d] CLIENT_AttachVideoStatistics Success\n", channel);
		}

	}
	/**
	 * 取消订阅客流统计服务实时数据
	 */
	public void detachVideoStatistics() {
		boolean isSuccess = netsdk.CLIENT_DetachVideoStatistics(attachHandle);
		if (isSuccess) {
			System.out.println(" CLIENT_AttachVideoStatistics Success");
		} else {
			System.out.printf("Chn[%d] CLIENT_AttachVideoStatistics Failed!LastError = %s\n", channel,
					ToolKits.getErrorCode());
		}
	}

	/**
	 * 立体行为-视频统计信息回调函数原形，
	 */
	private static class VideoStatisticsInfoCallBack implements NetSDKLib.fVideoStatisticsInfoCallBack {
		private static VideoStatisticsInfoCallBack instance;

		public static VideoStatisticsInfoCallBack getInstance() {
			if (instance == null) {
				synchronized (VideoStatisticsInfoCallBack.class) {
					if (instance == null) {
						instance = new VideoStatisticsInfoCallBack();
					}
				}
			}
			return instance;
		}

		@Override
		public void invoke(LLong lAttachHandle, int emType, Pointer pBuf, int dwBufLen, Pointer dwUser) {
			// 请不要在回调函数进行耗时操作
			/**
			 * lAttachHandle 是 CLIENT_AttachVideoStatistics返回值
			 * emType 是枚举{@link NET_EM_VS_TYPE}的值
			 * pBuf 是对应结构体数据指针，参考枚举值描述
			 */

			switch (emType) {			
			case 1:{ // NET_EM_VS_TYPE_CROWD_DISTRI_MAP(1, "人群分布图报表实时数据,对应 NET_VIDEOSTAT_CROWD_DISTRI_MAP"),
				System.out.println("----------人群分布图报表实时数据-----------");
				NET_VIDEOSTAT_CROWD_DISTRI_MAP msg = new NET_VIDEOSTAT_CROWD_DISTRI_MAP();
                ToolKits.GetPointerData(pBuf, msg);
                System.out.println("时间:"+msg.UTC.toString());
                System.out.println("通道号:"+msg.nChannelID);
                System.out.println("预置点编号:"+msg.nPtzPresetId);
                System.out.println("统计区个数:"+msg.nRegionNum);
                NET_PEOPLE_REGION_INFO[] stuRegionList = msg.stuRegionList;
                for (int i = 0; i < msg.nRegionNum; i++) {
					try {
						System.out.println("第"+(i+1)+"个区域名称："+new String(stuRegionList[i].szRegionName,encode));
						System.out.println("第"+(i+1)+"个当前时间点统计区内人数："+stuRegionList[i].nRegionPeopleNum);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
                break;
			}
			default:
				System.out.println("其他值：--------------------"+ emType);
				break;
			}							
		}
	}
	
	/**
	 * 选择通道
	 */
	private int channelId = -1;// 逻辑通道

	public void setChannelID() {
		System.out.println("请输入通道，从0开始计数，-1表示全部");
		Scanner sc = new Scanner(System.in);
		this.channelId = sc.nextInt();
	}

	/**
	 * 订阅智能任务
	 */
	public void AttachEventRealLoadPic() {
		// 先退订，设备不会对重复订阅作校验，重复订阅后会有重复的事件返回
		this.DetachEventRealLoadPic();
		// 需要图片
		int bNeedPicture = 1;
		m_attachHandle = netsdk.CLIENT_RealLoadPictureEx(m_hLoginHandle, channelId, NetSDKLib.EVENT_IVS_ALL, bNeedPicture,
				AnalyzerDataCB.getInstance(), null, null);
		if (m_attachHandle.longValue() != 0) {
			System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Success\n", channelId);
		} else {
			System.out.printf("Ch[%d] CLIENT_RealLoadPictureEx Failed!LastError = %s\n", channelId,
					ToolKits.getErrorCode());
		}
	}

	/**
	 * 报警事件（智能）回调
	 */
	private static class AnalyzerDataCB implements NetSDKLib.fAnalyzerDataCallBack {
		private final File picturePath;
		private static AnalyzerDataCB instance;

		private AnalyzerDataCB() {
			picturePath = new File("./AnalyzerPicture/");
			if (!picturePath.exists()) {
				picturePath.mkdirs();
			}
		}

		public static AnalyzerDataCB getInstance() {
			if (instance == null) {
				synchronized (AnalyzerDataCB.class) {
					if (instance == null) {
						instance = new AnalyzerDataCB();
					}
				}
			}
			return instance;
		}

		public int invoke(LLong m_attachHandle, int dwAlarmType, Pointer pAlarmInfo, Pointer pBuffer, int dwBufSize,
				Pointer dwUser, int nSequence, Pointer reserved) {
			if (m_attachHandle == null || m_attachHandle.longValue() == 0) {
				return -1;
			}

			switch (Objects.requireNonNull(EM_EVENT_IVS_TYPE.getEventType(dwAlarmType))) {			
			case EVENT_IVS_RETROGRADEDETECTION: {//人员逆行事件(对应 DEV_EVENT_RETROGRADEDETECTION_INFO)
				NetSDKLib.DEV_EVENT_RETROGRADEDETECTION_INFO msg = new NetSDKLib.DEV_EVENT_RETROGRADEDETECTION_INFO();
                ToolKits.GetPointerData(pAlarmInfo, msg);
                System.out.println(" 人员逆行事件(UTC)：" + msg.UTC + " 通道号:" + msg.nChannelID +" 枚举值"+msg.stuIntelliCommInfo.emClassType);
                //8 
                // EM_CLASS_ELEVATOR_ACCESS_CONTROL  = 65,  // 电梯门禁 "ElevatorAccessControl"		
                break;
			}
			case  EVENT_IVS_TUMBLE_DETECTION: {// 倒地报警事件(对应 DEV_EVENT_TUMBLE_DETECTION_INFO)
				NetSDKLib.DEV_EVENT_TUMBLE_DETECTION_INFO msg = new NetSDKLib.DEV_EVENT_TUMBLE_DETECTION_INFO();
                ToolKits.GetPointerData(pAlarmInfo, msg);
                System.out.println(" 倒地报警事件(UTC)：" + msg.UTC + " 通道号:" + msg.nChannelID +" 枚举值"+msg.emClassType);
                //8 
                // EM_CLASS_ELEVATOR_ACCESS_CONTROL  = 65,  // 电梯门禁 "ElevatorAccessControl"		
                break;
			}
			default:
				//System.out.println("其他事件--------------------"+ dwAlarmType);
				break;
			}
			
			return 0;
		}		
	}

	/**
	 * 停止侦听智能事件
	 */
	public void DetachEventRealLoadPic() {
		if (m_attachHandle.longValue() != 0) {
			netsdk.CLIENT_StopLoadPic(m_attachHandle);
		}
	}

	/******************************** 测试控制台 ***************************************/

	// 配置登陆地址，端口，用户名，密码
	private String m_strIpAddr = "172.13.76.115";
	private int m_nPort = 37777;
	private String m_strUser = "admin";
	private String m_strPassword = "admin123";

	public static void main(String[] args) {
		AttachVideoStatisticsDemo demo = new AttachVideoStatisticsDemo();
		demo.InitTest();
		demo.RunTest();
		demo.EndTest();

	}

	/**
	 * 初始化测试
	 */
	public void InitTest() {
		AttachVideoStatisticsDemo.Init();
		this.loginWithHighLevel();
	}

	/**
	 * 加载测试内容
	 */
	public void RunTest() {
		CaseMenu menu = new CaseMenu();
		// 设备通道信息获取
		menu.addItem(new CaseMenu.Item(this, "获取已添加的设备信息", "getAllDeviceInfo"));
		menu.addItem(new CaseMenu.Item(this, "获取设备通道信息", "getChannelInfo"));
		
		// 人群密度接口
		menu.addItem(new CaseMenu.Item(this, "订阅客流统计服务实时数据", "attachVideoStatistics"));
		menu.addItem(new CaseMenu.Item(this, "取消订阅客流统计服务实时数据", "detachVideoStatistics"));
		
		// 智能事件订阅上报
		menu.addItem(new CaseMenu.Item(this, "选择通道", "setChannelID"));
		menu.addItem(new CaseMenu.Item(this, "订阅智能事件", "AttachEventRealLoadPic"));
		menu.addItem(new CaseMenu.Item(this, "停止侦听智能事件", "DetachEventRealLoadPic"));
		
		menu.run();
	}

	/**
	 * 结束测试
	 */
	public void EndTest() {
		System.out.println("End Test");
		this.logOut(); // 登出设备
		System.out.println("See You...");
		AttachVideoStatisticsDemo.cleanAndExit(); // 清理资源并退出
	}
	/******************************** 结束 ***************************************/
}
