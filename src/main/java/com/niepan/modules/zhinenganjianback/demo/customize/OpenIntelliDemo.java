package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultDisconnectCallback;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultHaveReconnectCallBack;
import com.niepan.modules.zhinenganjianback.lib.enumeration.NET_EM_ATTITUDE_TYPE;
import com.niepan.modules.zhinenganjianback.lib.structure.*;
import com.sun.jna.Pointer;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;

import static com.niepan.modules.zhinenganjianback.lib.NetSDKLib.EVENT_IVS_OPEN_INTELLI;
import static com.niepan.modules.zhinenganjianback.lib.Utils.getOsPrefix;

/**
 * @author 291189
 * @version 1.0
 * @description ERR230203064
 * @date 2023/2/6 14:47
 */
public class OpenIntelliDemo {

    // SDk对象初始化
    public static final NetSDKLib netsdk = NetSDKLib.NETSDK_INSTANCE;
    public static final NetSDKLib configsdk = NetSDKLib.CONFIG_INSTANCE;

    // 判断是否初始化
    private static boolean bInit = false;
    // 判断log是否打开
    private static boolean bLogOpen = false;
    // 设备信息
    private NetSDKLib.NET_DEVICEINFO_Ex deviceInfo = new NetSDKLib.NET_DEVICEINFO_Ex();
    // 登录句柄
    private NetSDKLib.LLong m_hLoginHandle = new NetSDKLib.LLong(0);

    // 智能事件订阅句柄
    private NetSDKLib.LLong m_attachHandle = new NetSDKLib.LLong(0);

    // 回调函数需要是静态的，防止被系统回收
    // 断线回调
    private static NetSDKLib.fDisConnect disConnectCB = DefaultDisconnectCallback.getINSTANCE();
    // 重连回调
    private static NetSDKLib.fHaveReConnect haveReConnectCB = DefaultHaveReconnectCallBack.getINSTANCE();

    // 编码格式
    public static String encode;

    static {
        String osPrefix = getOsPrefix();
        if (osPrefix.toLowerCase().startsWith("win32-amd64")) {
            encode = "GBK";
        } else if (osPrefix.toLowerCase().startsWith("linux-amd64")) {
            encode = "UTF-8";
        }
    }

    /**
     * 获取当前时间
     */
    public static String GetDate() {
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDate.format(new java.util.Date()).replaceAll("[^0-9]", "-");
    }

    /**
     * 初始化SDK库
     */
    public static boolean Init() {
        bInit = netsdk.CLIENT_Init(disConnectCB, null);// 进程启动时，初始化一次
        if (!bInit) {
            System.out.println("Initialize SDK failed");
            return false;
        }
        // 配置日志
        OpenIntelliDemo.enableLog();

        // 设置断线重连回调接口, 此操作为可选操作，但建议用户进行设置
        netsdk.CLIENT_SetAutoReconnect(haveReConnectCB, null);

        // 设置登录超时时间和尝试次数，可选
        // 登录请求响应超时时间设置为3S
        int waitTime = 3000;
        // 登录时尝试建立链接 1 次
        int tryTimes = 1;
        netsdk.CLIENT_SetConnectTime(waitTime, tryTimes);
        // 设置更多网络参数， NET_PARAM 的nWaittime ， nConnectTryNum 成员与 CLIENT_SetConnectTime
        // 接口设置的登录设备超时时间和尝试次数意义相同,可选
        NetSDKLib.NET_PARAM netParam = new NetSDKLib.NET_PARAM();
        // 登录时尝试建立链接的超时时间
        netParam.nConnectTime = 10000;
        // 设置子连接的超时时间
        netParam.nGetConnInfoTime = 3000;
        netsdk.CLIENT_SetNetworkParam(netParam);
        return true;
    }

    /**
     * 打开 sdk log
     */
    private static void enableLog() {
        NetSDKLib.LOG_SET_PRINT_INFO setLog = new NetSDKLib.LOG_SET_PRINT_INFO();
        File path = new File("sdklog/");
        if (!path.exists())
            path.mkdir();

        // 这里的log保存地址依据实际情况自己调整
        String logPath = path.getAbsoluteFile().getParent() + "\\sdklog\\" + "sdklog" + GetDate() + ".log";
        setLog.nPrintStrategy = 0;
        setLog.bSetFilePath = 1;
        System.arraycopy(logPath.getBytes(), 0, setLog.szLogFilePath, 0, logPath.getBytes().length);
        System.out.println(logPath);
        setLog.bSetPrintStrategy = 1;
        bLogOpen = netsdk.CLIENT_LogOpen(setLog);
        if (!bLogOpen)
            System.err.println("Failed to open NetSDK log");
    }

    /**
     * 高安全登录
     */
    public void loginWithHighLevel() {
        // 输入结构体参数
        NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY pstlnParam = new NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY() {
            {
                szIP = m_strIpAddr.getBytes();
                nPort = m_nPort;
                szUserName = m_strUser.getBytes();
                szPassword = m_strPassword.getBytes();
            }
        };
        // 输出结构体参数
        NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY pstOutParam = new NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY();

        // 写入sdk
        m_hLoginHandle = netsdk.CLIENT_LoginWithHighLevelSecurity(pstlnParam, pstOutParam);
        if (m_hLoginHandle.longValue() == 0) {
            System.err.printf("Login Device[%s] Port[%d]Failed. %s\n", m_strIpAddr, m_nPort,
                    netsdk.CLIENT_GetLastError());
        } else {
            deviceInfo = pstOutParam.stuDeviceInfo; // 获取设备信息
            System.out.println("Login Success");
            System.out.println("Device Address：" + m_strIpAddr);
            System.out.println("设备包含：" + deviceInfo.byChanNum + "个通道");
        }
    }

    /**
     * 退出
     */
    public void logOut() {
        if (m_hLoginHandle.longValue() != 0) {
            netsdk.CLIENT_Logout(m_hLoginHandle);
            System.out.println("LogOut Success");
        }
    }

    /**
     * 清理sdk环境并退出
     */
    public static void cleanAndExit() {
        if (bLogOpen) {
            netsdk.CLIENT_LogClose(); // 关闭sdk日志打印
        }
        netsdk.CLIENT_Cleanup(); // 进程关闭时，调用一次
        System.exit(0);
    }


    /**
     * 选择通道
     */
    private int channelId = -1;// 逻辑通道


    /**
     * 订阅智能任务
     */
    public void AttachEventRealLoadPic() {
        // 先退订，设备不会对重复订阅作校验，重复订阅后会有重复的事件返回
        this.DetachEventRealLoadPic();
        // 需要图片
        int bNeedPicture = 1;
        m_attachHandle = netsdk.CLIENT_RealLoadPictureEx(m_hLoginHandle, channelId, EVENT_IVS_OPEN_INTELLI,
                bNeedPicture, AnalyzerDataCB.getInstance(), null, null);
        if (m_attachHandle.longValue() != 0) {
            System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Success\n", channelId);
        } else {
            System.out.printf("Ch[%d] CLIENT_RealLoadPictureEx Failed!LastError = %s\n", channelId,
                    ToolKits.getErrorCode());
        }
    }

    /**
     * 报警事件（智能）回调
     */
    private static class AnalyzerDataCB implements NetSDKLib.fAnalyzerDataCallBack {
        private final File picturePath;
        private static AnalyzerDataCB instance;

        private AnalyzerDataCB() {
            picturePath = new File("./AnalyzerPicture/");
            if (!picturePath.exists()) {
                picturePath.mkdirs();
            }
        }

        public static AnalyzerDataCB getInstance() {
            if (instance == null) {
                synchronized (AnalyzerDataCB.class) {
                    if (instance == null) {
                        instance = new AnalyzerDataCB();
                    }
                }
            }
            return instance;
        }

        @Override
        public int invoke(NetSDKLib.LLong lAnalyzerHandle, int dwAlarmType, Pointer pAlarmInfo, Pointer pBuffer, int dwBufSize,
                          Pointer dwUser, int nSequence, Pointer reserved) {
            if (lAnalyzerHandle == null || lAnalyzerHandle.longValue() == 0) {
                return -1;
            }

            switch (dwAlarmType) {
                case EVENT_IVS_OPEN_INTELLI : { //开放智能事件(对应 DEV_EVENT_OPEN_INTELLI_INFO)
                    System.out.println("开放智能事件");
                    DEV_EVENT_OPEN_INTELLI_INFO msg = new DEV_EVENT_OPEN_INTELLI_INFO();
                    ToolKits.GetPointerData(pAlarmInfo, msg);
                    /**
                     通道号
                     */
                    int nChannelID = msg.nChannelID;
                    System.out.println("nChannelID:"+nChannelID);
                    /**
                     0:脉冲,1:开始, 2:停止
                     */
                    int action = msg.nAction;
                    System.out.println("action:"+action);

                    /**
                     所属开放算法的Id
                     */
                    byte[] szOpenCode = msg.szOpenCode;

                    try {
                        String openCode
                                = new String(szOpenCode, encode);
                        System.out.println("openCode:"+openCode);

                        /**
                         所属开放算法的名称
                         */
                        byte[] szOpenName
                                = msg.szOpenName;
                        String openName
                                = new String(szOpenName, encode);
                        System.out.println("openName:"+openName);

                        /**
                         所属开放算法的规则类型, 仅支持: 拌线入侵CrossLineDetection(EVENT_IVS_CROSSLINEDETECTION)、区域入侵CrossRegionDetection(EVENT_IVS_CROSSREGIONDETECTION)、滞留检测StayDetection(EVENT_IVS_STAYDETECTION)、数量统计ObjectNumDetection(EVENT_IVS_OBJECT_NUM_DETECTION)、通用姿态行为事件 GenealAttitude(EVENT_IVS_GENEAL_ATTITUDE)
                         */
                        byte[] szRuleType
                                = msg.szRuleType;
                        String ruleType
                                = new String(szRuleType, encode);
                        System.out.println("ruleType:"+ruleType);

                        Pointer pstuOpenData
                                = msg.pstuOpenData;

                        if("CrossLineDetection".equals(ruleType.trim())){ // 拌线入侵
                            System.out.println("拌线入侵");

                            NetSDKLib.DEV_EVENT_CROSSLINE_INFO info=new   NetSDKLib.DEV_EVENT_CROSSLINE_INFO();

                            ToolKits.GetPointerData(pstuOpenData, info);

                            int nChannelID1
                                    = info.nChannelID;
                     System.out.println("nChannelID1:"+nChannelID1);
                            /**
                             事件名称
                             */
                            byte[] szName
                                    = info.szName;

                            String name
                                    = new String(szName, encode);

                            System.out.println("name:"+name);

                            System.out.println("UTC:"+info.UTC.toStringTime());


                            // 全景广角图
                            if (info.stuSceneImage != null && info.stuSceneImage.nLength > 0) {
                                String bigPicture = picturePath + "\\" + "CrossLineDetection_" + System.currentTimeMillis()
                                        + ".jpg";
                                ToolKits.savePicture(pBuffer, info.stuSceneImage.nOffSet, info.stuSceneImage.nLength, bigPicture);
                            }

                        }else if("GenealAttitude".equals(ruleType.trim())){ //通用姿态行为事件
                            System.out.println("通用姿态行为事件");
                            DEV_EVENT_GENEAL_ATTITUDE_INFO info=new DEV_EVENT_GENEAL_ATTITUDE_INFO();

                            ToolKits.GetPointerData(pstuOpenData, info);


                            int nChannelID1
                                    = info.nChannelID;
                            System.out.println("nChannelID1:"+nChannelID1);
                            /**
                             事件名称
                             */
                            byte[] szName
                                    = info.szName;

                            String name
                                    = new String(szName, encode);

                            System.out.println("name:"+name);

                            System.out.println("UTC:"+info.stuUTC.toStringTime());
                            /**
                             姿态类型 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.NET_EM_ATTITUDE_TYPE}
                             */
                            int emAttitudeType
                                    = info.emAttitudeType;

                            System.out.println("emAttitudeType:"+emAttitudeType+":"+ NET_EM_ATTITUDE_TYPE.getNoteByValue(emAttitudeType));

                            int nLength = info.stuSceneImage.nLength;
                                System.out.println("nLength: "+nLength);

                            String bigPicture = picturePath + "\\" + "GenealAttitude_" + System.currentTimeMillis()
                                    + ".jpg";
                            ToolKits.savePicture(pBuffer, 0, dwBufSize, bigPicture);

                        }else if("CrossRegionDetection".equals(ruleType.trim())){ //区域入侵

                            System.out.println("区域入侵");

                            NetSDKLib.DEV_EVENT_CROSSREGION_INFO info=new NetSDKLib.DEV_EVENT_CROSSREGION_INFO();

                            ToolKits.GetPointerData(pstuOpenData, info);

                            int nChannelID1
                                    = info.nChannelID;
                            System.out.println("nChannelID1:"+nChannelID1);
                            /**
                             事件名称
                             */
                            byte[] szName
                                    = info.szName;

                            String name
                                    = new String(szName, encode);

                            System.out.println("name:"+name);

                            System.out.println("UTC:"+info.UTC.toStringTime());


                            // 全景广角图
                            if (info.stuSceneImage != null && info.stuSceneImage.nLength > 0) {
                                String bigPicture = picturePath + "\\" + "CrossRegionDetection_" + System.currentTimeMillis()
                                        + ".jpg";
                                ToolKits.savePicture(pBuffer, info.stuSceneImage.nOffSet, info.stuSceneImage.nLength, bigPicture);
                            }

                        }else if("StayDetection".equals(ruleType.trim())){//   滞留检测StayDetection(EVENT_IVS_STAYDETECTION) - DEV_EVENT_STAY_INFO

                            System.out.println("滞留检测");

                            NetSDKLib.DEV_EVENT_STAY_INFO info=new NetSDKLib.DEV_EVENT_STAY_INFO();

                            ToolKits.GetPointerData(pstuOpenData, info);
                            int nChannelID1
                                    = info.nChannelID;
                            System.out.println("nChannelID1:"+nChannelID1);
                            /**
                             事件名称
                             */
                            byte[] szName
                                    = info.szName;

                            String name
                                    = new String(szName, encode);

                            System.out.println("name:"+name);

                            System.out.println("UTC:"+info.UTC.toStringTime());


                            String bigPicture = picturePath + "\\" + "StayDetection_" + System.currentTimeMillis()
                                    + ".jpg";
                            ToolKits.savePicture(pBuffer, 0, dwBufSize, bigPicture);
                        }else if("ObjectNumDetection".equals(ruleType.trim())){ //
                            // 数量统计ObjectNumDetection(EVENT_IVS_OBJECT_NUM_DETECTION) - DEV_EVENT_OBJECT_NUM_DETECTION_INFO
                            System.out.println("数量统计");

                            DEV_EVENT_OBJECT_NUM_DETECTION_INFO info=new DEV_EVENT_OBJECT_NUM_DETECTION_INFO();

                            ToolKits.GetPointerData(pstuOpenData, info);

                            int nChannelID1 = info.nChannelID;

                            System.out.println("nChannelID1:"+nChannelID1);
                            /**
                             事件名称
                             */
                            byte[] szName = info.szName;

                            String name = new String(szName, encode);

                            System.out.println("name:"+name);

                            System.out.println("UTC:"+info.stuUTC.toStringTime());

                            /**
                             表示区域内物体的个数
                             */
                            System.out.println("UTC:"+info.nNumber);


                            // 全景广角图
                            if (info.stuSceneImage != null && info.stuSceneImage.nLength > 0) {
                                String bigPicture = picturePath + "\\" + "ObjectNumDetection_" + System.currentTimeMillis()
                                        + ".jpg";
                                ToolKits.savePicture(pBuffer, info.stuSceneImage.nOffSet, info.stuSceneImage.nLength, bigPicture);
                            }
                        }else if("CrossLineStat".equals(ruleType.trim())){// 绊线计数事件 CrossLineStat(EVENT_IVS_CROSSLINE_STAT) - NET_DEV_EVENT_CROSSLINE_STAT_INFO
                            System.out.println("绊线计数事件");
                            NET_DEV_EVENT_CROSSLINE_STAT_INFO info=new NET_DEV_EVENT_CROSSLINE_STAT_INFO();
                            System.out.println("size:"+info.size());
                            ToolKits.GetPointerData(pstuOpenData, info);


                            int nChannelID1
                                    = info.nChannelID;
                            System.out.println("nChannelID1:"+nChannelID1);
                            /**
                             事件名称
                             */
                            byte[] szName
                                    = info.szName;

                            String name
                                    = new String(szName, encode);

                            System.out.println("name:"+name);

                            System.out.println("UTC:"+info.stuUTC.toStringTime());

                            /**
                             表示区域内物体的个数
                             */
                            System.out.println("nNumber:"+info.nNumber);
                            /**
                             详细计数信息个数
                             */
                            int nStatDetailNum = info.nStatDetailNum;
                            System.out.println("nStatDetailNum:"+nStatDetailNum);


                            NET_STAT_DETAIL[] stuStatDetail = info.stuStatDetail;

                            for(int i=0;i<nStatDetailNum;i++){
                                    NET_STAT_DETAIL detail
                                            = stuStatDetail[i];
                                    //目标类型名称
                                    byte[] szType = detail.szType;
                                    String type
                                            = new String(szType, encode);
                                    System.out.println("type:"+type);

                                int nAttributeNumber = detail.nAttributeNumber;
                                System.out.println("nAttributeNumber:"+nAttributeNumber);


                                NET_STAT_ATTRIBUTE[] stuAttribute
                                        = detail.stuAttribute;

                                for(int n=0;n<nAttributeNumber;n++){
                                    NET_STAT_ATTRIBUTE attribute
                                            = stuAttribute[n];
                                    int nAttrMapNumber
                                            = attribute.nAttrMapNumber;

                                    System.out.println("nAttrMapNumber:"+nAttrMapNumber);

                                    NET_STAT_ATTRMAP[] stuAttrMap
                                            = attribute.stuAttrMap;

                                    for(int m=0;m<nAttrMapNumber;m++){

                                        NET_STAT_ATTRMAP attrmap = stuAttrMap[m];

                                        System.out.println("szAttrType:"+new String(attrmap.szAttrType,encode));

                                        System.out.println("szValue:"+new String(attrmap.szValue,encode));

                                    }

                                }
                            }

                        }else if("RegionNumStat".equals(ruleType.trim())){ //域内计数事件 RegionNumStat(EVENT_IVS_REGIONNUM_STAT) - NET_DEV_EVENT_REGIONNUM_STAT_INFO
                            System.out.println("域内计数事件");
                            NET_DEV_EVENT_REGIONNUM_STAT_INFO info=new NET_DEV_EVENT_REGIONNUM_STAT_INFO();

                            ToolKits.GetPointerData(pstuOpenData, info);
                            int nChannelID1
                                    = info.nChannelID;
                            System.out.println("nChannelID1:"+nChannelID1);
                            /**
                             事件名称
                             */
                            byte[] szName
                                    = info.szName;

                            String name
                                    = new String(szName, encode);

                            System.out.println("name:"+name);

                            System.out.println("UTC:"+info.stuUTC.toStringTime());

                            /**
                             表示区域内物体的个数
                             */
                            System.out.println("UTC:"+info.nNumber);
                            /**
                             详细计数信息个数
                             */
                            int nStatDetailNum = info.nStatDetailNum;
                            System.out.println("nStatDetailNum:"+nStatDetailNum);

                            NET_STAT_DETAIL[] stuStatDetail = info.stuStatDetail;
                            for(int i=0;i<nStatDetailNum;i++){
                                NET_STAT_DETAIL detail
                                        = stuStatDetail[i];
                                //目标类型名称
                                byte[] szType = detail.szType;
                                String type
                                        = new String(szType, encode);
                                System.out.println("type:"+type);

                            }


                        }else if("AnythingDetect".equals(ruleType.trim())){// 全物体类型检测事件 AnythingDetect(EVENT_IVS_ANYTHING_DETECT) - DEV_EVENT_ANYTHING_DETECT_INFO
                            System.out.println("全物体类型检测事件");
                            DEV_EVENT_ANYTHING_DETECT_INFO info=new DEV_EVENT_ANYTHING_DETECT_INFO();

                            ToolKits.GetPointerData(pstuOpenData, info);
                            int nChannelID1
                                    = info.nChannelID;
                            System.out.println("nChannelID1:"+nChannelID1);
                            /**
                             事件名称
                             */
                            byte[] szName = info.szName;

                            String name
                                    = new String(szName, encode);

                            System.out.println("name:"+name);

                            System.out.println("UTC:"+info.UTC.toStringTime());
                            // 全景广角图
                            if (info.stuSceneImage != null && info.stuSceneImage.nLength > 0) {
                                String bigPicture = picturePath + "\\" + "AnythingDetect_" + System.currentTimeMillis()
                                        + ".jpg";
                                ToolKits.savePicture(pBuffer, info.stuSceneImage.nOffSet, info.stuSceneImage.nLength, bigPicture);
                            }
                        }

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }


                    break;
                }
                default:
                    System.out.println("其他事件--------------------"+ dwAlarmType);
                    break;
            }
            return 0;
        }
    }

    /**
     * 停止侦听智能事件
     */
    public void DetachEventRealLoadPic() {
        if (m_attachHandle.longValue() != 0) {
            netsdk.CLIENT_StopLoadPic(m_attachHandle);
        }
    }
    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "AttachEventRealLoadPic" , "AttachEventRealLoadPic")));
        menu.addItem((new CaseMenu.Item(this , "DetachEventRealLoadPic" , "DetachEventRealLoadPic")));

        menu.run();
    }
    // 配置登陆地址，端口，用户名，密码
    private String m_strIpAddr = "172.12.12.215";
    private int m_nPort = 37777;
    private String m_strUser = "admin";
    private String m_strPassword = "admin123";
    public static void main(String[] args) {
        OpenIntelliDemo demo=new OpenIntelliDemo();
        demo.InitTest();
        demo.RunTest();
        demo.EndTest();

    }

    /**
     * 初始化测试
     */
    public void InitTest() {
        OpenIntelliDemo.Init();
        this.loginWithHighLevel();
    }

    /**
     * 结束测试
     */
    public void EndTest() {
        System.out.println("End Test");
        this.logOut(); // 登出设备
        System.out.println("See You...");
        OpenIntelliDemo.cleanAndExit(); // 清理资源并退出
    }
}
