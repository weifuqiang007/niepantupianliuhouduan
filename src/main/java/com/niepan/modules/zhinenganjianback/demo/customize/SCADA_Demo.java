package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_IN_ATTACH_SCADA_DATA_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_NOTIFY_SCADA_DATA_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_OUT_ATTACH_SCADA_DATA_INFO;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;

import java.io.UnsupportedEncodingException;

public class SCADA_Demo extends Initialization {

    NetSDKLib.LLong lAttachHandle;

    public static class fNotifySCADADataCB implements NetSDKLib.fNotifySCADAData {
        public static fNotifySCADADataCB instance;
        public static fNotifySCADADataCB getInstance(){
            if(null == instance){
                synchronized (fNotifySCADADataCB.class) {
                    if(null == instance){
                        instance = new fNotifySCADADataCB();
                    }
                }
            }
            return instance;
        }
        @Override
        public void invoke(NetSDKLib.LLong lSCADADataHandle, Pointer pstuSCADADataNotifyInfo, Pointer dwUser) {
            System.out.println("fNotifySCADADataCB received! Handle is " + lSCADADataHandle);
            NET_NOTIFY_SCADA_DATA_INFO msg=new NET_NOTIFY_SCADA_DATA_INFO();
            ToolKits.GetPointerData(pstuSCADADataNotifyInfo,msg);
            System.out.println("szDeviceId = " + new String(msg.szDeviceId).trim());
            System.out.println("nDataInfoNum = " + msg.nDataInfoNum);
            for(int i = 0; i < msg.nDataInfoNum; i ++){
                System.out.println("stuDataInfo[" + i + "].fMeasuredVal = " + msg.stuDataInfo[i].fMeasuredVal);
                System.out.println("stuDataInfo[" + i + "].fSetupVal = " + msg.stuDataInfo[i].fSetupVal);
                System.out.println("stuDataInfo[" + i + "].szID = " + new String(msg.stuDataInfo[i].szID).trim());
                System.out.println("stuDataInfo[" + i + "].szAddress = " + new String(msg.stuDataInfo[i].szAddress).trim());
                System.out.println("stuDataInfo[" + i + "].szType = " + new String(msg.stuDataInfo[i].szType).trim());
                try {
                    System.out.println("stuDataInfo[" + i + "].szUnit = " + new String(msg.stuDataInfo[i].szUnit,"UTF-8").trim());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                System.out.println("stuDataInfo[" + i + "].nStatus = " + msg.stuDataInfo[i].nStatus);
                System.out.println("stuDataInfo[" + i + "].stuTime = " + msg.stuDataInfo[i].stuTime.toStringTime());
            }
        }
    }

    public void AttachSCADA() {
        NET_IN_ATTACH_SCADA_DATA_INFO stIn = new NET_IN_ATTACH_SCADA_DATA_INFO();
        stIn.cbfNotifySCADAData = fNotifySCADADataCB.getInstance();
        NET_OUT_ATTACH_SCADA_DATA_INFO stOut = new NET_OUT_ATTACH_SCADA_DATA_INFO();

        Pointer inputPoiner = new Memory(stIn.size());
        inputPoiner.clear(stIn.size());
        ToolKits.SetStructDataToPointer(stIn, inputPoiner, 0);
        Pointer outPointer = new Memory(stOut.size());
        outPointer.clear(stOut.size());
        ToolKits.SetStructDataToPointer(stOut, outPointer, 0);

        lAttachHandle = netSdk.CLIENT_AttachSCADAData(loginHandle, inputPoiner, outPointer, 5000);
        if (lAttachHandle.longValue() != 0) {
            System.out.printf("CLIENT_AttachSCADAData Success\n");
        } else {
            System.out.printf("CLIENT_AttachSCADAData Failed!LastError = \n" +
                    ToolKits.getErrorCode());
        }
    }

    public void DetachSCADA() {
        if (netSdk.CLIENT_DetachSCADAData(lAttachHandle)) {
            System.out.printf("CLIENT_DetachSCADAData Success\n");
        } else {
            System.out.printf("CLIENT_DetachSCADAData Failed!LastError = \n" +
                    ToolKits.getErrorCode());
        }
    }

    public void RunTest() {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();

        menu.addItem((new CaseMenu.Item(this, "AttachSCADA", "AttachSCADA")));
        menu.addItem((new CaseMenu.Item(this, "DetachSCADA", "DetachSCADA")));

        menu.run();
    }

    public static void main(String[] args) {
        SCADA_Demo scadaDemo = new SCADA_Demo();
        InitTest("172.3.3.171", 37777, "admin", "admin123");
        scadaDemo.RunTest();
        LoginOut();

    }
}

