package com.niepan.modules.zhinenganjianback.demo.customize.transmitInfoDemo.module;

import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.CFG_DVRIP_INFO;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.LLong;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

public class ConfigModule {
	static NetSDKLib NetSdk = NetSDKLib.NETSDK_INSTANCE;

	public static void QueryOnlineState(NetSDKLib.LLong lLoginID) {
		Pointer p = new Memory(Integer.SIZE);
		p.clear(Integer.SIZE);
		boolean ret = NetSdk.CLIENT_QueryDevState(lLoginID, NetSDKLib.NET_DEVSTATE_ONLINE, p, Integer.SIZE,
				new IntByReference(0), 3000);
		if (!ret) {
			System.err.println("查询设备在线状态失败, " + ToolKits.getErrorCode());
			return;
		}
		int[] buffer = new int[1];
		p.read(0, buffer, 0, 1);
		// 1 表示在线, 0 表示断线
		System.out.println(buffer[0] == 1 ? "设备在线" : "设备断线");
	}
	/**
	 * 获取网络协议
	 * @param lLoginID 登录句柄
	 * @return
	 */
	public static CFG_DVRIP_INFO getDVRIPConfig(NetSDKLib.LLong lLoginID) {
		String command = NetSDKLib.CFG_CMD_DVRIP;
		int channel = -1;
		CFG_DVRIP_INFO msg = new CFG_DVRIP_INFO();

		// 获取
		if (ToolKits.GetDevConfig(lLoginID, channel, command, msg)) {
			return msg;
		} else {
			System.err.println("Get DVRIP Failed!" + NetSdk.CLIENT_GetLastError());
			return null;
		}
	}
	
	
	/**
	 * 网络协议配置
	 * @param m_hLoginHandle 登陆句柄
	 * @param enable  使能
	 * @param address 服务器地址
	 * @param nPort  服务器端口号
	 * @param deviceId  设备ID
	 * @param info 获取到的网络协议配置
	 * @return
	 */
	public static boolean setDVRIPConfig(LLong m_hLoginHandle,CFG_DVRIP_INFO info) {
		CFG_DVRIP_INFO msg = info;				
		return ToolKits.SetDevConfig(m_hLoginHandle, -1, NetSDKLib.CFG_CMD_DVRIP, msg);
	}

}
