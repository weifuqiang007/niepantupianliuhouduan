package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.CFG_TRAFFIC_OVERSPEED_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.CFG_TRAFFIC_UNDERSPEED_INFO;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Memory;

import java.io.UnsupportedEncodingException;

/**
 * @author 291189
 * @version 1.0
 * @description ERR220831112 
 * @date 2022/9/7 14:44
 */
public class VideoAnalyseRuleNewDemo extends Initialization {

    /**
     * 查询和修改智能规则配置信息
     */
    public void getCongigRuleInfo() {
        int channel = 0; // 通道号
        String command = NetSDKLib.CFG_CMD_ANALYSERULE;//视频分析规则配置(对应 CFG_ANALYSERULES_INFO)

        int ruleCount = 10; // 事件规则个数
        NetSDKLib.CFG_RULE_INFO[] ruleInfo = new NetSDKLib.CFG_RULE_INFO[ruleCount];
        for (int i = 0; i < ruleCount; i++) {
            ruleInfo[i] = new NetSDKLib.CFG_RULE_INFO();
        }

        NetSDKLib.CFG_ANALYSERULES_INFO analyse = new NetSDKLib.CFG_ANALYSERULES_INFO();
        analyse.nRuleLen = 1024 * 1024 * 40;
        analyse.pRuleBuf = new Memory(1024 * 1024 * 40); // 申请内存
        analyse.pRuleBuf.clear(1024 * 1024 * 40);

        // 查询
        if (ToolKits.GetDevConfig(loginHandle, channel, command, analyse)) {
            int offset = 0;
            System.out.println("设备返回的事件规则个数:" + analyse.nRuleCount);

            int count = analyse.nRuleCount < ruleCount ? analyse.nRuleCount : ruleCount;

            for (int i = 0; i < count; i++) {
                // 每个视频输入通道对应的所有事件规则：缓冲区pRuleBuf填充多个事件规则信息，每个事件规则信息内容为 CFG_RULE_INFO +
                // "事件类型对应的规则配置结构体"。
                ToolKits.GetPointerDataToStruct(analyse.pRuleBuf, offset, ruleInfo[i]);

                offset += ruleInfo[0].size(); // 智能规则偏移量

                switch (ruleInfo[i].dwRuleType) {
                                 //CFG_TRAFFIC_OVERSPEED_INFO
                    case NetSDKLib.EVENT_IVS_TRAFFIC_OVERSPEED: { //交通违章-超速
                        System.out.println("交通违章-超速----------------------------");
                      CFG_TRAFFIC_OVERSPEED_INFO msg = new CFG_TRAFFIC_OVERSPEED_INFO();
                        ToolKits.GetPointerDataToStruct(analyse.pRuleBuf, offset, msg);

                        int nSpeedUpperLimit = msg.nSpeedUpperLimit;

                        int nSpeedLowerLimit = msg.nSpeedLowerLimit;

                        System.out.println("nSpeedUpperLimit:"+nSpeedUpperLimit);

                        System.out.println("nSpeedLowerLimit:"+nSpeedLowerLimit);

                        msg.nSpeedUpperLimit=200;

                        msg.nSpeedLowerLimit=120;

                        try {
                            System.out.println("szRuleName:"+new String(msg.szRuleName,encode));

                            String szRuleName = "超速规则111";

                            ToolKits.ByteArrZero(msg.szRuleName);

                            System.arraycopy(szRuleName.getBytes(encode), 0, msg.szRuleName, 0, szRuleName.getBytes(encode).length);

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                        ToolKits.SetStructDataToPointer(msg, analyse.pRuleBuf, offset);
                        break;
                    }
                    case NetSDKLib.EVENT_IVS_TRAFFIC_UNDERSPEED: { //交通违章-低速
                        System.out.println("交通违章-低速-----------------------------");
                        CFG_TRAFFIC_UNDERSPEED_INFO msg = new CFG_TRAFFIC_UNDERSPEED_INFO();
                        ToolKits.GetPointerDataToStruct(analyse.pRuleBuf, offset, msg);

                        int nSpeedUpperLimit = msg.nSpeedUpperLimit;

                        int nSpeedLowerLimit = msg.nSpeedLowerLimit;

                        System.out.println("nSpeedUpperLimit:"+nSpeedUpperLimit);

                        System.out.println("nSpeedLowerLimit:"+nSpeedLowerLimit);

                        msg.nSpeedUpperLimit=80;

                        msg.nSpeedLowerLimit=60;

                        try {
                            System.out.println("szRuleName:"+new String(msg.szRuleName,encode));

                            String szRuleName = "低速规则222";

                            ToolKits.ByteArrZero(msg.szRuleName);

                            System.arraycopy(szRuleName.getBytes(encode), 0, msg.szRuleName, 0, szRuleName.getBytes(encode).length);

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                        ToolKits.SetStructDataToPointer(msg, analyse.pRuleBuf, offset);
                        break;
                    }
                    default:
                        break;
                }
                offset += ruleInfo[i].nRuleSize; // 智能事件偏移量
            }

            // 设置
            if (ToolKits.SetDevConfig(loginHandle, channel, command, analyse)) {
                System.out.println("修改智能事件规则成功!");
            } else {
                System.err.println("修改智能事件规则失败!" + ToolKits.getErrorCode());
            }

        } else {
            System.err.println("查询智能事件规则信息失败!" + ToolKits.getErrorCode());
        }
    }

    /**
     * 加载测试内容
     */
    public void RunTest() {
        CaseMenu menu = new CaseMenu();
        menu.addItem(new CaseMenu.Item(this, "getCongigRuleInfo", "getCongigRuleInfo"));
        menu.run();
    }

    public static void main(String[] args) {
        Initialization.InitTest("172.24.2.10", 37777, "admin", "admin123");
        new VideoAnalyseRuleNewDemo().RunTest();
        Initialization.LoginOut();
    }
}
