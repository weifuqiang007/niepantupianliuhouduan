package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_DEV_EVENT_POSITION_SNAP_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_EVENT_INFO_EXTEND;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_TIME_EX;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Pointer;
import java.io.File;
import java.io.UnsupportedEncodingException;

import static com.niepan.modules.zhinenganjianback.lib.NetSDKLib.EVENT_IVS_POSITION_SNAP;

/**
 * @author 291189
 * @version 1.0
 * @description ERR221027111
 * @date 2022/11/15 9:35
 */
public class PosiTionSnapDemo extends Initialization {
    int channel=0;
    NetSDKLib.LLong    attachHandle=new NetSDKLib.LLong(0);
    /**
     * 订阅智能任务
     */

    public NetSDKLib.LLong AttachEventRealLoadPic() {
        // 先退订，设备不会对重复订阅作校验，重复订阅后会有重复的事件返回
        if(attachHandle.longValue()!=0){
            this.DetachEventRealLoadPic();
        }

        // 需要图片
        int bNeedPicture = 1;
        attachHandle = netSdk.CLIENT_RealLoadPictureEx(loginHandle, channel, EVENT_IVS_POSITION_SNAP, bNeedPicture,
                AnalyzerDataCB.getInstance(), null, null);
        if (attachHandle.longValue() != 0) {
            System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Success\n", channel);
        } else {
            System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Failed!LastError = %s\n", channel,
                    ToolKits.getErrorCode());
        }

        return attachHandle;
    }
    /**
     * 报警事件（智能）回调
     */
    private static class AnalyzerDataCB implements NetSDKLib.fAnalyzerDataCallBack {
        private final File picturePath;
        private static AnalyzerDataCB instance;

        private AnalyzerDataCB() {
            picturePath = new File("./AnalyzerPicture/");
            if (!picturePath.exists()) {
                picturePath.mkdirs();
            }
        }

        public static AnalyzerDataCB getInstance() {
            if (instance == null) {
                synchronized (AnalyzerDataCB.class) {
                    if (instance == null) {
                        instance = new AnalyzerDataCB();
                    }
                }
            }
            return instance;
        }


        @Override
        public int invoke(NetSDKLib.LLong lAnalyzerHandle, int dwAlarmType, Pointer pAlarmInfo, Pointer pBuffer, int dwBufSize,
                          Pointer dwUser, int nSequence, Pointer reserved) {
            if (lAnalyzerHandle == null || lAnalyzerHandle.longValue() == 0) {
                return -1;
            }

            switch (dwAlarmType) {
                case EVENT_IVS_POSITION_SNAP : {
                    System.out.println("按位置抓图事件"); //  NET_DEV_EVENT_POSITION_SNAP_INFO
                    NET_DEV_EVENT_POSITION_SNAP_INFO msg = new NET_DEV_EVENT_POSITION_SNAP_INFO();

                    ToolKits.GetPointerData(pAlarmInfo, msg);

                    int chanId=  msg.nChannelID;

                    System.out.println("chanId:"+chanId);

                    int action= msg.nAction;
                    System.out.println("action:"+action);

                    NET_TIME_EX stuUTC= msg.stuUTC;
                    System.out.println("stuUTC:"+stuUTC);


                    NET_EVENT_INFO_EXTEND stuEventInfoEx= msg.stuEventInfoEx;
                    System.out.println("stuEventInfoEx:"+stuEventInfoEx.toString());

                    try {
                        System.out.println("szType:"+new String( msg. szType,encode));

                        System.out.println("szPresetName:"+new String( msg. szPresetName,encode));


                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    System.out.println("nPresetID:"+msg.nPresetID);

                    System.out.println("stuGPSInfo:"+msg.stuGPSInfo);

                  int[]   nAbsposition=msg.nAbsposition;
                    for (int p:nAbsposition
                         ) {
                        System.out.println(p);
                    }

                    break;
                }
                default:
                    System.out.println("其他事件--------------------"+ dwAlarmType);
                    break;
            }
            return 0;
        }
    }


    /**
     * 停止侦听智能事件
     */
    public void DetachEventRealLoadPic() {
        if (this.attachHandle.longValue() != 0) {
            netSdk.CLIENT_StopLoadPic(this.attachHandle);
        }
    }
    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "AttachEventRealLoadPic" , "AttachEventRealLoadPic")));
        menu.addItem((new CaseMenu.Item(this , "DetachEventRealLoadPic" , "DetachEventRealLoadPic")));
        menu.run();
    }

    public static void main(String[] args) {
        PosiTionSnapDemo posiTionSnapDemo=new PosiTionSnapDemo();
        InitTest("172.32.0.152",37777,"admin","admin123");
        posiTionSnapDemo.RunTest();
        LoginOut();

    }
}
