package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.enumeration.NET_EM_CFG_OPERATE_TYPE;
import com.niepan.modules.zhinenganjianback.lib.structure.*;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;

import static com.niepan.modules.zhinenganjianback.lib.NetSDKLib.*;

/**
 * @author 291189
 * @version 1.0
 * @description ERR220913031 
 * @date 2022/9/15 14:27
 */
public class StartVideoDiagnosisDemo extends Initialization {

    Scanner scanner=new Scanner(System.in);

    //视频诊断参数表
    public void VideoDiagnosisProfileConfig() {
        String strCmd = NetSDKLib.CFG_CMD_VIDEODIAGNOSIS_PROFILE; // 视频诊断参数表命令
        int nChn = 0;     // 通道号

        NetSDKLib.CFG_VIDEODIAGNOSIS_PROFILE   profiles=new     NetSDKLib.CFG_VIDEODIAGNOSIS_PROFILE();
        profiles.nTotalProfileNum=10;



        NetSDKLib.CFG_VIDEO_DIAGNOSIS_PROFILE[]    profileArr=new NetSDKLib.CFG_VIDEO_DIAGNOSIS_PROFILE[profiles.nTotalProfileNum];

        for(int i=0;i<profileArr.length;i++){
            profileArr[i]=new NetSDKLib.CFG_VIDEO_DIAGNOSIS_PROFILE();

            NetSDKLib.CFG_VIDEO_DITHER_DETECTION cfg_pstDither = new NetSDKLib.CFG_VIDEO_DITHER_DETECTION();

            profileArr[i].pstDither=new Memory(cfg_pstDither.size());

            profileArr[i].pstDither.clear(cfg_pstDither.size());

            ToolKits.SetStructDataToPointer(cfg_pstDither, profileArr[i].pstDither,0);


            NetSDKLib.CFG_VIDEO_STRIATION_DETECTION cfg_pstStriation   =new NetSDKLib.CFG_VIDEO_STRIATION_DETECTION();

            profileArr[i].pstStriation=new Memory(cfg_pstStriation.size());

            profileArr[i].pstStriation.clear(cfg_pstStriation.size());

            ToolKits.SetStructDataToPointer(cfg_pstStriation, profileArr[i].pstStriation,0);

            NetSDKLib.CFG_VIDEO_LOSS_DETECTION cfg_pstLoss   =new NetSDKLib.CFG_VIDEO_LOSS_DETECTION();

            profileArr[i].pstLoss=new Memory(cfg_pstLoss.size());

            profileArr[i].pstLoss.clear(cfg_pstLoss.size());


            ToolKits.SetStructDataToPointer(cfg_pstLoss, profileArr[i].pstLoss,0);


            NetSDKLib.CFG_VIDEO_COVER_DETECTION cfg_pstCover   =new NetSDKLib.CFG_VIDEO_COVER_DETECTION();

            profileArr[i].pstCover=new Memory(cfg_pstCover.size());

            profileArr[i].pstCover.clear(cfg_pstCover.size());

            ToolKits.SetStructDataToPointer(cfg_pstCover, profileArr[i].pstCover,0);


            NetSDKLib.CFG_VIDEO_FROZEN_DETECTION cfg_pstFrozen   =new NetSDKLib.CFG_VIDEO_FROZEN_DETECTION();

            profileArr[i].pstFrozen=new Memory(cfg_pstFrozen.size());

            profileArr[i].pstFrozen.clear(cfg_pstFrozen.size());

            ToolKits.SetStructDataToPointer(cfg_pstFrozen, profileArr[i].pstFrozen,0);


            NetSDKLib.CFG_VIDEO_BRIGHTNESS_DETECTION cfg_pstBrightness   =new NetSDKLib.CFG_VIDEO_BRIGHTNESS_DETECTION();

            profileArr[i].pstBrightness=new Memory(cfg_pstBrightness.size());

            profileArr[i].pstBrightness.clear(cfg_pstBrightness.size());


            ToolKits.SetStructDataToPointer(cfg_pstBrightness, profileArr[i].pstBrightness,0);


            NetSDKLib.CFG_VIDEO_CONTRAST_DETECTION cfg_pstContrast   =new NetSDKLib.CFG_VIDEO_CONTRAST_DETECTION();

            profileArr[i].pstContrast=new Memory(cfg_pstContrast.size());

            profileArr[i].pstContrast.clear(cfg_pstContrast.size());

            ToolKits.SetStructDataToPointer(cfg_pstContrast, profileArr[i].pstContrast,0);


            NetSDKLib.CFG_VIDEO_UNBALANCE_DETECTION cfg_pstUnbalance  =new NetSDKLib.CFG_VIDEO_UNBALANCE_DETECTION();

            profileArr[i].pstUnbalance=new Memory(cfg_pstUnbalance.size());

            profileArr[i].pstUnbalance.clear(cfg_pstUnbalance.size());

            ToolKits.SetStructDataToPointer(cfg_pstUnbalance, profileArr[i].pstUnbalance,0);



            NetSDKLib.CFG_VIDEO_NOISE_DETECTION cfg_pstNoise  =new NetSDKLib.CFG_VIDEO_NOISE_DETECTION();

            profileArr[i].pstNoise=new Memory(cfg_pstNoise.size());

            profileArr[i].pstNoise.clear(cfg_pstNoise.size());

            ToolKits.SetStructDataToPointer(cfg_pstNoise, profileArr[i].pstNoise,0);


            NetSDKLib.CFG_VIDEO_BLUR_DETECTION cfg_pstBlur =new NetSDKLib.CFG_VIDEO_BLUR_DETECTION();

            profileArr[i].pstBlur=new Memory(cfg_pstBlur.size());

            profileArr[i].pstBlur.clear(cfg_pstBlur.size());

            ToolKits.SetStructDataToPointer(cfg_pstBlur, profileArr[i].pstBlur,0);



            NetSDKLib.CFG_VIDEO_SCENECHANGE_DETECTION cfg_pstSceneChange=new NetSDKLib.CFG_VIDEO_SCENECHANGE_DETECTION();

            profileArr[i].pstSceneChange=new Memory(cfg_pstSceneChange.size());

            profileArr[i].pstSceneChange.clear(cfg_pstSceneChange.size());

            ToolKits.SetStructDataToPointer(cfg_pstSceneChange, profileArr[i].pstSceneChange,0);



            NetSDKLib.CFG_VIDEO_DELAY_DETECTION cfg_pstVideoDelay=new NetSDKLib.CFG_VIDEO_DELAY_DETECTION();

            profileArr[i].pstVideoDelay=new Memory(cfg_pstVideoDelay.size());

            profileArr[i].pstVideoDelay.clear(cfg_pstVideoDelay.size());

            ToolKits.SetStructDataToPointer(cfg_pstVideoDelay, profileArr[i].pstVideoDelay,0);



            NetSDKLib.CFG_PTZ_MOVING_DETECTION cfg_pstPTZMoving=new NetSDKLib.CFG_PTZ_MOVING_DETECTION();

            profileArr[i].pstPTZMoving=new Memory(cfg_pstPTZMoving.size());

            profileArr[i].pstPTZMoving.clear(cfg_pstPTZMoving.size());

            ToolKits.SetStructDataToPointer(cfg_pstPTZMoving, profileArr[i].pstPTZMoving,0);


            NetSDKLib.CFG_VIDEO_BLACKWHITE_DETECTION cfg_pstBlackAndWhite=new NetSDKLib.CFG_VIDEO_BLACKWHITE_DETECTION();

            profileArr[i].pstBlackAndWhite=new Memory(cfg_pstBlackAndWhite.size());

            profileArr[i].pstBlackAndWhite.clear(cfg_pstBlackAndWhite.size());

            ToolKits.SetStructDataToPointer(cfg_pstBlackAndWhite, profileArr[i].pstBlackAndWhite,0);


            NetSDKLib.CFG_VIDEO_DRAMATICCHANGE_DETECTION cfg_pstDramaticChange=new NetSDKLib.CFG_VIDEO_DRAMATICCHANGE_DETECTION();

            profileArr[i].pstDramaticChange=new Memory(cfg_pstDramaticChange.size());

            profileArr[i].pstDramaticChange.clear(cfg_pstDramaticChange.size());

            ToolKits.SetStructDataToPointer(cfg_pstDramaticChange, profileArr[i].pstDramaticChange,0);


            CFG_VIDEO_AVAILABILITY_DETECTION cfg_pstVideoAvailability=new CFG_VIDEO_AVAILABILITY_DETECTION();

            profileArr[i].pstVideoAvailability=new Memory(cfg_pstVideoAvailability.size());

            profileArr[i].pstVideoAvailability.clear(cfg_pstVideoAvailability.size());

            ToolKits.SetStructDataToPointer(cfg_pstVideoAvailability, profileArr[i].pstVideoAvailability,0);


            CFG_VIDEO_SNOWFLAKE_DETECTION cfg_pstSnowflake=new CFG_VIDEO_SNOWFLAKE_DETECTION();

            profileArr[i].pstSnowflake=new Memory(cfg_pstSnowflake.size());

            profileArr[i].pstSnowflake.clear(cfg_pstSnowflake.size());

            ToolKits.SetStructDataToPointer(cfg_pstSnowflake, profileArr[i].pstSnowflake,0);


            CFG_VIDEO_ALGORITHMTYPE_DETECTION cfg_pstVideoAlgorithmType=new CFG_VIDEO_ALGORITHMTYPE_DETECTION();

            profileArr[i].pstVideoAlgorithmType=new Memory(cfg_pstVideoAlgorithmType.size());

            profileArr[i].pstVideoAlgorithmType.clear(cfg_pstVideoAlgorithmType.size());

            ToolKits.SetStructDataToPointer(cfg_pstVideoAlgorithmType, profileArr[i].pstVideoAlgorithmType,0);


            CFG_VIDEO_FILCKERING_DETECTION cfg_pstuVideoFilckeringDetection=new CFG_VIDEO_FILCKERING_DETECTION();

            profileArr[i].pstuVideoFilckeringDetection=new Memory(cfg_pstuVideoFilckeringDetection.size());

            profileArr[i].pstuVideoFilckeringDetection.clear(cfg_pstuVideoFilckeringDetection.size());

            ToolKits.SetStructDataToPointer(cfg_pstuVideoFilckeringDetection, profileArr[i].pstuVideoFilckeringDetection,0);


            CFG_VIDEO_LOSS_FRAME_DETECTION cfg_pstuVideoLossFrameDetection=new CFG_VIDEO_LOSS_FRAME_DETECTION();

            profileArr[i].pstuVideoLossFrameDetection=new Memory(cfg_pstuVideoLossFrameDetection.size());

            profileArr[i].pstuVideoLossFrameDetection.clear(cfg_pstuVideoLossFrameDetection.size());

            ToolKits.SetStructDataToPointer(cfg_pstuVideoLossFrameDetection, profileArr[i].pstuVideoLossFrameDetection,0);
        }

        NetSDKLib.CFG_VIDEO_DIAGNOSIS_PROFILE    profile=new NetSDKLib.CFG_VIDEO_DIAGNOSIS_PROFILE();
        profiles.pstProfiles=new Memory(profile.size()*profiles.nTotalProfileNum);
        profiles.pstProfiles.clear(profile.size()*profiles.nTotalProfileNum);

        ToolKits.SetStructArrToPointerData(profileArr,profiles.pstProfiles);


        boolean b
                = ToolKits.GetDevConfig(loginHandle, nChn, strCmd, profiles);

        if(b){
            int nReturnProfileNum = profiles.nReturnProfileNum;

            printlns("nReturnProfileNum:"+nReturnProfileNum);

            Pointer pstProfiles = profiles.pstProfiles;

        ToolKits.GetPointerDataToStructArr(pstProfiles,profileArr);


                NetSDKLib.CFG_VIDEO_DIAGNOSIS_PROFILE cfg_profile = profileArr[0];
                try {

                    printlns("szName:"+new String(cfg_profile.szName,encode));


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                NetSDKLib.CFG_VIDEO_DITHER_DETECTION  dither=new NetSDKLib.CFG_VIDEO_DITHER_DETECTION();

                Pointer pstDither = cfg_profile.pstDither;

                ToolKits.GetPointerData(pstDither,dither);
                printlns("dither.bEnable:"+dither.bEnable);
                printlns("dither.nMinDuration:"+dither.nMinDuration);
                printlns("dither.byThrehold1:"+dither.byThrehold1);
                printlns("dither.byThrehold2:"+dither.byThrehold2);

                NetSDKLib.CFG_VIDEO_STRIATION_DETECTION  striation=new NetSDKLib.CFG_VIDEO_STRIATION_DETECTION();

                Pointer pstStriation = cfg_profile.pstStriation;

                ToolKits.GetPointerData(pstStriation,striation);

                printlns("striation.bEnable:"+striation.bEnable);
                printlns("striation.nMinDuration:"+striation.nMinDuration);
                printlns("striation.byThrehold1:"+striation.byThrehold1);
                printlns("striation.byThrehold2:"+striation.byThrehold2);
                printlns("striation.bUVDetection:"+striation.bUVDetection);


                ToolKits.ByteArrZero(cfg_profile.szName);//byte[]清空

                String name="test001";

                ToolKits.StringToByteArray(name,cfg_profile.szName);

                dither.nMinDuration=10;

                ToolKits.SetStructDataToPointer(dither,pstDither,0);


                ToolKits.SetStructDataToPointer(striation,pstStriation,0);


                ToolKits.SetStructArrToPointerData(profileArr,profiles.pstProfiles);

                if(ToolKits.SetDevConfig(loginHandle, nChn, strCmd, profiles)) {

                    System.out.println("设置视频诊断参数成功!");
                } else {
                    System.err.println("设置视频诊断参数失败, " + ToolKits.getErrorCode());
                }

            }


    }


    /**
     * 获取任务名称
     */
    ArrayList<String> nameList = new ArrayList<String>();
    public void GetMemberNames(String command) {

        nameList.clear();

        int nNameCount = 10;  // 任务名称个数

        NetSDKLib.NET_ARRAY[] arrays = new NetSDKLib.NET_ARRAY[nNameCount];
        for(int i = 0; i < nNameCount; i++) {
            arrays[i] = new NetSDKLib.NET_ARRAY();
            arrays[i].pArray = new Memory(260);   //  缓冲区 目前最小260字节, 需要用户自己申请
            arrays[i].pArray.clear(260);
            arrays[i].dwArrayLen = 260;
        }

        /*
         * 入参
         */
        NetSDKLib.NET_IN_MEMBERNAME stIn  = new NetSDKLib.NET_IN_MEMBERNAME();
        stIn.szCommand = command/*NetSDKLib.CFG_CMD_VIDEODIAGNOSIS_TASK*/;   // 配置命令, 获取任务的成员名称

        /*
         * 出参
         */
        NetSDKLib.NET_OUT_MEMBERNAME stOut = new NetSDKLib.NET_OUT_MEMBERNAME();
        stOut.nTotalNameCount = nNameCount;
        stOut.pstNames = new Memory(arrays[0].size() * nNameCount);
        stOut.pstNames.clear(arrays[0].size() * nNameCount);

        ToolKits.SetStructArrToPointerData(arrays, stOut.pstNames);

        if(netSdk.CLIENT_GetMemberNames(loginHandle, stIn, stOut, 3000)) {
            ToolKits.GetPointerDataToStructArr(stOut.pstNames, arrays);
            System.out.println("nRetNameCount:"+stOut.nRetNameCount);
            for(int i = 0; i < stOut.nRetNameCount; i++) {
                System.out.println("成员名称:" + ToolKits.GetPointerDataToGBKString(arrays[i].pArray, arrays[i].dwArrayLen));
                nameList.add(ToolKits.GetPointerDataToGBKString(arrays[i].pArray, arrays[i].dwArrayLen));
            }
        } else {
            System.err.println("获取配置成员名称失败, " + ToolKits.getErrorCode());
        }
    }

    /**
     * 视频诊断任务获取
     */

    public void VideoDiagnosisTaskGet() {

        // 单个获取
            for(int i=0;i<nameList.size();i++){
                VideoDiagnosisTaskGetSingle(nameList.get(i));
            }

    }

    // 获取所有计划名称，然后再获取每个计划的具体信息
    public void GetVideoDiagnosisTask() {
        // 获取所有任务名称，然后再获取每个任务的具体信息
        GetMemberNames(NetSDKLib.CFG_CMD_VIDEODIAGNOSIS_TASK);

    }


    /**
     * 根据任务名称，获取具体的信息
     */
    public void VideoDiagnosisTaskGetSingle(String taskName) {
        /////////////////////  获取单个任务的具体信息    //////////////////////////////////
        int nSourceNum = 128;  	  // 任务数据源的个数，用户自己定义
        int nChn = 0;  // 通道号
        String strCmd = "VideoDiagnosisTask." + taskName;
        printlns("strCmd:"+strCmd);
        // 任务数据源
        NetSDKLib.CFG_TAST_SOURCES[] sources = new NetSDKLib.CFG_TAST_SOURCES[nSourceNum];
        for(int i = 0; i < nSourceNum; i++) {
            sources[i] = new NetSDKLib.CFG_TAST_SOURCES();
        }

        // 任务
        NetSDKLib.CFG_DIAGNOSIS_TASK[] tasks = new NetSDKLib.CFG_DIAGNOSIS_TASK[1];
        tasks[0] = new NetSDKLib.CFG_DIAGNOSIS_TASK();
        tasks[0].nTotalSourceNum = nSourceNum;
        tasks[0].pstSources = new Memory(sources[0].size() * nSourceNum);
        tasks[0].pstSources.clear(sources[0].size() * nSourceNum);

        ToolKits.SetStructArrToPointerData(sources, tasks[0].pstSources);

        /*
         * 视频诊断任务表
         */
        NetSDKLib.CFG_VIDEODIAGNOSIS_TASK msg = new NetSDKLib.CFG_VIDEODIAGNOSIS_TASK();
        msg.nTotalTaskNum = 1;
        msg.pstTasks = new Memory(tasks[0].size());
        msg.pstTasks.clear(tasks[0].size());

        ToolKits.SetStructArrToPointerData(tasks, msg.pstTasks);

        /*
         *  获取
         */
        if(GetDevConfig(loginHandle, nChn, strCmd, msg)) {
            ToolKits.GetPointerDataToStructArr(msg.pstTasks, tasks);
            ToolKits.GetPointerDataToStructArr(tasks[0].pstSources, sources);

            // 输出打印
            printlns("任务名称:" + taskName);
            try {
                printlns("诊断参数表名:" + new String(tasks[0].szProfileName,encode).trim());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            for(int i = 0; i < tasks[0].nReturnSourceNum; i++) {
                printlns("持续诊断时间:" + sources[i].nDuration);
                printlns("视频通道号:" + sources[i].nVideoChannel);
                try {
                    printlns("设备地址:" + new String(sources[i].stRemoteDevice.szAddress,encode).trim());
                    printlns("视频流地址:" + new String(sources[i].szPath,encode).trim());
                    //szPath
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            System.out.println();

        } else {
            System.err.println("获取任务失败, " + ToolKits.getErrorCode());
        }
    }


    /*
     * 获取，只适用  诊断任务的获取
     */
    public boolean GetDevConfig(NetSDKLib.LLong hLoginHandle, int nChn, String strCmd, NetSDKLib.SdkStructure cmdObject) {
        boolean result = false;
        IntByReference error = new IntByReference(0);
        int nBufferLen = 2*1024*1024;
        byte[] strBuffer = new byte[nBufferLen];

        if(netSdk.CLIENT_GetNewDevConfig(hLoginHandle, strCmd, nChn, strBuffer, nBufferLen,error,3000)) {
            cmdObject.write();
            if (config.CLIENT_ParseData(NetSDKLib.CFG_CMD_VIDEODIAGNOSIS_TASK_ONE, strBuffer, cmdObject.getPointer(),
                    cmdObject.size(), null)) {
                cmdObject.read();
                result = true;
            } else {
                System.err.println("Parse VideoDiagnosisTask.x, Config Failed!" + ToolKits.getErrorCode());
                result = false;
            }
        } else {
            System.err.println("Get" + strCmd + "Config Failed!" + ToolKits.getErrorCode());
            result = false;
        }
        return result;
    }
    /*
     * 设置，只适用  诊断任务的设置
     */
    public boolean SetDevConfig(NetSDKLib.LLong hLoginHandle, int nChn, String strCmd, NetSDKLib.SdkStructure cmdObject) {
        boolean result = false;
        int nBufferLen = 2*1024*1024;
        byte szBuffer[] = new byte[nBufferLen];
        for(int i=0; i<nBufferLen; i++)szBuffer[i]=0;
        IntByReference error = new IntByReference(0);
        IntByReference restart = new IntByReference(0);

        cmdObject.write();
        if (config.CLIENT_PacketData(NetSDKLib.CFG_CMD_VIDEODIAGNOSIS_TASK_ONE, cmdObject.getPointer(), cmdObject.size(),
                szBuffer, nBufferLen)) {
            cmdObject.read();
            if( netSdk.CLIENT_SetNewDevConfig(hLoginHandle, strCmd, nChn, szBuffer, nBufferLen, error, restart, 3000)) {
                result = true;
            } else {
                System.err.printf("Set %s Config Failed! Last Error = %x\n" , strCmd , ToolKits.getErrorCode());
                result = false;
            }
        } else {
            System.err.println("Packet VideoDiagnosisTask.x Config Failed!");
            result = false;
        }
        return result;
    }

    /**
     * 视频诊断任务设置
     */
    public void VideoDiagnosisTaskSet() {
        String[] address = {"172.23.12.248"};
        printlns("新增1，修改2");  //task名称相同更新 不同为新增
        int nextInt = scanner.nextInt();
        if(nextInt==1){
            String task = scanner.next();
            VideoDiagnosisTaskSetSingle(task, address, 30, "test001");

        }else if(nextInt==2){
            if(nameList.size()==0){
            }else {
                VideoDiagnosisTaskSetSingle(nameList.get(0), address, 60, "test001");
            }
        }


    }
    /**
     * 视频诊断任务单个设置
     * 设备登陆后，根据 deviceinfo.byChanNum  获取通道个数
     * 如果 172.25.242.120  这个设备有16个通道，则有16个数据源，通道从0~15
     * @param taskName  任务名称
     * @param address  数据源数组
     * @param nDuration  持续时间
     * @param profileName  配置参数表名
     */
    public void VideoDiagnosisTaskSetSingle(String taskName, String[] address, int nDuration, String profileName) {
        int nChn = 0;  // 通道号
        String strCmd = "VideoDiagnosisTask." + taskName;

        int nSourceNum = address.length;   // 数据源个数

        // 任务数据源
        NetSDKLib.CFG_TAST_SOURCES[] sources = new NetSDKLib.CFG_TAST_SOURCES[nSourceNum];
        for(int i = 0; i < nSourceNum; i++) {
            sources[i] = new NetSDKLib.CFG_TAST_SOURCES();
        }

        for(int i = 0; i < nSourceNum; i++) {
            sources[i].abRemoteDevice = 1;
            try {
                System.arraycopy(address[i].getBytes(encode), 0, sources[i].szDeviceID, 0, address[i].getBytes(encode).length);
                System.arraycopy(address[i].getBytes(encode), 0, sources[i].stRemoteDevice.szAddress, 0, address[i].getBytes(encode).length);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            byte[] szPath = sources[i].szPath;
            String path="rtsp://admin:admin123@172.23.12.248:554/cam/realmonitor?channel=1&subtype=0";
            ToolKits.ByteArrZero(szPath);
            try {
                System.arraycopy(path.getBytes(encode), 0, szPath, 0, path.getBytes(encode).length);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            sources[i].nVideoChannel = i;
            sources[i].nDuration = nDuration;
        }

        // 任务
        NetSDKLib.CFG_DIAGNOSIS_TASK[] tasks = new NetSDKLib.CFG_DIAGNOSIS_TASK[1];
        tasks[0] = new NetSDKLib.CFG_DIAGNOSIS_TASK();
        tasks[0].nTotalSourceNum = nSourceNum;
        tasks[0].pstSources = new Memory(sources[0].size() * nSourceNum);
        tasks[0].pstSources.clear(sources[0].size() * nSourceNum);

        // 设置诊断表名称
        try {
            System.arraycopy(profileName.getBytes(encode), 0, tasks[0].szProfileName, 0, profileName.getBytes(encode).length);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        ToolKits.SetStructArrToPointerData(sources, tasks[0].pstSources);

        /*
         * 视频诊断任务表
         */
        NetSDKLib.CFG_VIDEODIAGNOSIS_TASK msg = new NetSDKLib.CFG_VIDEODIAGNOSIS_TASK();
        msg.nTotalTaskNum = 1;
        msg.pstTasks = new Memory(tasks[0].size());
        msg.pstTasks.clear(tasks[0].size());

        ToolKits.SetStructArrToPointerData(tasks, msg.pstTasks);

        if(SetDevConfig(loginHandle, nChn, strCmd, msg)) {
            System.out.println("设置诊断任务成功!");
        } else {
            System.err.println("设置诊断任务失败, " + ToolKits.getErrorCode());
        }
    }
   // 获取所有计划名称，然后再获取每个计划的具体信息
    public void GetVideoDiagnosisProject() {
        // 获取所有计划名称，然后再获取每个计划的具体信息
        GetMemberNames(NetSDKLib.CFG_CMD_VIDEODIAGNOSIS_PROJECT);
    }

    public void VideoDiagnosisProjectGet() {

        for(int i=0;i<nameList.size();i++){
            VideoDiagnosisProjectGetSingle(nameList.get(i));
        }
    }

    String[] dates = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
    public void VideoDiagnosisProjectGetSingle(String projectName) {
        int nChn = 0;
        int strCmd = NET_EM_CFG_OPERATE_TYPE.NET_EM_VIDEODIAGNOSIS_PROJECT;

        NET_VIDEODIAGNOSIS_PROJECT_INFO project =new NET_VIDEODIAGNOSIS_PROJECT_INFO();
        try {
            System.arraycopy(projectName.getBytes(encode), 0, project.szProjectName, 0, projectName.getBytes(encode).length);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        project.nTotalTaskNum  = 10;
        NET_PROJECT_TASK[] task = new NET_PROJECT_TASK[10];
        for (int i = 0; i < 10; i ++ ) {
            task[i] = new NET_PROJECT_TASK();
        }
        project.pstProjectTasks = new Memory(task[0].size()*10);
        ToolKits.SetStructArrToPointerData(task, project.pstProjectTasks);
        project.write();

        if(netSdk.CLIENT_GetConfig(loginHandle, strCmd, nChn, project.getPointer(), project.size(), 5000, null)) {
            System.out.println("获取计划成功!");
            project.read();

            for (int i = 0; i < project.nReturnTaskNum; i ++ ) {
                int offset = 0;
                ToolKits.GetPointerDataToStruct(project.pstProjectTasks, offset, task[i]);
                offset += task[i].size();
            }


            for(int i = 0; i < project.nReturnTaskNum; i++) {
                //System.out.println("计划名称:" + new String(project.szProjectName).trim());
                System.out.println("任务使能：" + task[i].bEnable);
                System.out.println("任务名称：" + new String(task[i].szTaskName).trim());

                // 时间第一维代表，周日~周六，第二维代表时间段，可以设置6个，目前的设置，只设置第一个有效
                for(int j = 0; j < 7; j++) {
                    System.out.println("日期：" + dates[j]);
                    System.out.println("时间：" + task[i].Section[j].stuTimeSection[0].startTime() + "-" +
                            task[i].Section[j].stuTimeSection[0].endTime());
                }
            }}else {
            System.err.println("获取计划失败, " + ToolKits.getErrorCode());
        }
    }



    /**
     * 视频诊断计划设置
     */
    public void VideoDiagnosisProjectSet() {
        PROJECT_INFO[] projectInfo = new PROJECT_INFO[1];
        printlns("输入task名称");
        String task
                = scanner.next();
        printlns("输入project名称");
        String projectName
                = scanner.next();
        projectInfo[0] = new PROJECT_INFO(projectName, task, "00:00:00", "23:59:59");

        // 诊断计划设置
        SetVideoDiagnosisProject(projectInfo);
    }

    public void SetVideoDiagnosisProject(PROJECT_INFO[] projectInfo) {
        int nChn = 0;
        int strCmd = NET_EM_CFG_OPERATE_TYPE.NET_EM_VIDEODIAGNOSIS_PROJECT;
        int nProjectNum = projectInfo.length;   // 计划个数，用户自己设置

        for(int i = 0; i < nProjectNum; i++) {

            NET_VIDEODIAGNOSIS_PROJECT_INFO project =new NET_VIDEODIAGNOSIS_PROJECT_INFO();

            try {
                System.arraycopy(projectInfo[i].projectName.getBytes(encode), 0, project.szProjectName, 0, projectInfo[i].projectName.getBytes(encode).length);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            project.nTotalTaskNum=1;

            NET_PROJECT_TASK task = new NET_PROJECT_TASK();
            task.bEnable = 1;
            task.bIsCycle=1;
            task.bIsRepeat=1;
            task.nCycleInterval=10;
            project.pstProjectTasks = new Memory(task.size()*project.nTotalTaskNum);

            try {
                System.arraycopy(projectInfo[i].taskName.getBytes(encode), 0, task.szTaskName, 0, projectInfo[i].taskName.getBytes(encode).length);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            for(int j = 0; j < 7; j++) {
                task.Section[j].stuTimeSection[0].bEnable = 1;
                task.Section[j].stuTimeSection[0].iBeginHour = projectInfo[i].startHour;
                task.Section[j].stuTimeSection[0].iBeginMin = projectInfo[i].startMinute;
                task.Section[j].stuTimeSection[0].iBeginSec = projectInfo[i].startSecond;
                task.Section[j].stuTimeSection[0].iEndHour = projectInfo[i].endHour;
                task.Section[j].stuTimeSection[0].iEndMin = projectInfo[i].endMinute;
                task.Section[j].stuTimeSection[0].iEndSec = projectInfo[i].endSecond;
            }

            ToolKits.SetStructDataToPointer(task, project.pstProjectTasks,0);
            project.write();

            if(netSdk.CLIENT_SetConfig(loginHandle, strCmd, nChn, project.getPointer(), project.size(), 5000, null, null)) {
                System.out.println("设置计划成功!");
            }else {
                System.err.println("设置计划失败, " + ToolKits.getErrorCode());
            }

        }
    }

    class PROJECT_INFO {
        // 计划名称
        String projectName;

        // 任务名称
        String taskName;

        // 开始/结束时间
        int startHour;
        int startMinute;
        int startSecond;
        int endHour;
        int endMinute;
        int endSecond;

        public PROJECT_INFO(String projectName, String taskName, String startTime, String endTime) {
            this.projectName = projectName;
            this.taskName = taskName;
            this.startHour = Integer.parseInt(startTime.split(":")[0]);
            this.startMinute = Integer.parseInt(startTime.split(":")[1]);
            this.startSecond = Integer.parseInt(startTime.split(":")[2]);
            this.endHour = Integer.parseInt(endTime.split(":")[0]);
            this.endMinute = Integer.parseInt(endTime.split(":")[1]);
            this.endSecond = Integer.parseInt(endTime.split(":")[2]);
        }
    }


    private static NetSDKLib.LLong m_lDiagnosisHandle = new NetSDKLib.LLong(0);

    /**
     * 视频诊断订阅，当诊断完成后，会收到事件，一个计划只收到一个事件
     */
    public void StartVideoDiagnosis() {
        /*
         * 入参
         */
        NetSDKLib.NET_IN_VIDEODIAGNOSIS stIn = new NetSDKLib.NET_IN_VIDEODIAGNOSIS();
        stIn.nDiagnosisID = -1;
        stIn.dwWaitTime = 5000;
        stIn.cbVideoDiagnosis = RealVideoDiagnosis.getInstance();

        /*
         * 出参
         */
        NetSDKLib.NET_OUT_VIDEODIAGNOSIS stOut = new NetSDKLib.NET_OUT_VIDEODIAGNOSIS();

        if(netSdk.CLIENT_StartVideoDiagnosis(loginHandle, stIn, stOut)) {
            m_lDiagnosisHandle = stOut.lDiagnosisHandle;
            System.out.println("视频诊断订阅成功！");
        } else {
            System.err.println("订阅失败, " + ToolKits.getErrorCode());
        }
    }

    // 取消订阅
    public void StopVideoDiagnosis() {
        if(m_lDiagnosisHandle.longValue() != 0) {
            if(netSdk.CLIENT_StopVideoDiagnosis(m_lDiagnosisHandle)) {
                System.out.println("取消视频诊断订阅！");
                m_lDiagnosisHandle.setValue(0);
            }
        }
    }


    private static class RealVideoDiagnosis implements NetSDKLib.fRealVideoDiagnosis {
        private RealVideoDiagnosis() {}

        private static class RealVideoDiagnosisHolder {
            private static RealVideoDiagnosis instance = new RealVideoDiagnosis();
        }

        private static RealVideoDiagnosis getInstance() {
            return RealVideoDiagnosis.RealVideoDiagnosisHolder.instance;
        }

        @Override
        public int invoke(NetSDKLib.LLong lDiagnosisHandle,
                          NetSDKLib.NET_REAL_DIAGNOSIS_RESULT pDiagnosisInfo, Pointer pBuf,
                          int nBufLen, Pointer dwUser) {

            NetSDKLib.NET_VIDEODIAGNOSIS_COMMON_INFO commons = new NetSDKLib.NET_VIDEODIAGNOSIS_COMMON_INFO();

            ToolKits.GetPointerData(pDiagnosisInfo.pstDiagnosisCommonInfo, commons);

            int dwBufSize = pDiagnosisInfo.dwBufSize;

            System.out.println("dwBufSize:"+dwBufSize);

            System.out.println("nTypeCount:"+pDiagnosisInfo.nTypeCount);

            Pointer pDiagnosisResult
                    = pDiagnosisInfo.pDiagnosisResult;

            int count=0;

            for(int i=0;i<pDiagnosisInfo.nTypeCount;i++){

                NetSDKLib.NET_DIAGNOSIS_RESULT_HEADER header=new NetSDKLib.NET_DIAGNOSIS_RESULT_HEADER();

                ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,header);


                byte[] szDiagnosisType
                        = header.szDiagnosisType;
                int nDiagnosisTypeLen
                        = header.nDiagnosisTypeLen;

                count+=header.dwSize;

                if(NET_DIAGNOSIS_DITHER.equals(new String(szDiagnosisType).trim())){
                    System.out.println(" 视频抖动检测:");
                    NetSDKLib.NET_VIDEO_DITHER_DETECTIONRESULT result=new NetSDKLib.NET_VIDEO_DITHER_DETECTIONRESULT();

                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);

                    count+=result.dwSize;
                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);

                }else if(NET_DIAGNOSIS_STRIATION.equals(new String(szDiagnosisType).trim())){
                    System.out.println(" 视频条纹检测:");
                    NetSDKLib.NET_VIDEO_STRIATION_DETECTIONRESULT result=new NetSDKLib.NET_VIDEO_STRIATION_DETECTIONRESULT();

                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);
                    count+=result.dwSize;
                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);
                    System.out.println(" szPicUrl:"+new String(result.szPicUrl) );
                }else if(NET_DIAGNOSIS_LOSS.equals(new String(szDiagnosisType).trim())){

                    System.out.println(" 视频丢失检测:");
                    NetSDKLib.NET_VIDEO_LOSS_DETECTIONRESULT result=new NetSDKLib.NET_VIDEO_LOSS_DETECTIONRESULT();

                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);


                    count+=result.dwSize;

                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);
                    System.out.println(" szPicUrl:"+new String(result.szPicUrl) );
                }else if(NET_DIAGNOSIS_COVER.equals(new String(szDiagnosisType).trim())){

                    System.out.println(" 视频遮挡检测:");

                    NET_VIDEO_COVER_DETECTIONRESULT  result=new NET_VIDEO_COVER_DETECTIONRESULT();
                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);
                    count+=result.dwSize;
                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);
                }else if(NET_DIAGNOSIS_FROZEN.equals(new String(szDiagnosisType).trim())){

                    System.out.println(" 视频冻结检测:");
                    NET_VIDEO_FROZEN_DETECTIONRESULT  result=new NET_VIDEO_FROZEN_DETECTIONRESULT();
                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);

                    count+=result.dwSize;
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);
                }else if(NET_DIAGNOSIS_BRIGHTNESS.equals(new String(szDiagnosisType).trim())){

                    System.out.println(" 视频亮度异常检测:");
                    NET_VIDEO_BRIGHTNESS_DETECTIONRESULT     result=new NET_VIDEO_BRIGHTNESS_DETECTIONRESULT();

                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);
                    count+=result.dwSize;

                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);
                    System.out.println(" szPicUrl:"+new String(result.szPicUrl) );

                }else if(NET_DIAGNOSIS_CONTRAST.equals( new String(szDiagnosisType).trim())){
                    System.out.println(" 视频对比度异常检测:");
                    NET_VIDEO_CONTRAST_DETECTIONRESULT  result=new NET_VIDEO_CONTRAST_DETECTIONRESULT();


                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);
                    count+=result.dwSize;

                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);
                    System.out.println(" szPicUrl:"+new String(result.szPicUrl) );
                }else if(NET_DIAGNOSIS_UNBALANCE.equals(new String(szDiagnosisType).trim())){
                    System.out.println(" 视频偏色检测:");
                    NET_VIDEO_UNBALANCE_DETECTIONRESULT result=new NET_VIDEO_UNBALANCE_DETECTIONRESULT();
                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);
                    count+=result.dwSize;

                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);
                    System.out.println(" szPicUrl:"+new String(result.szPicUrl) );
                }else if(NET_DIAGNOSIS_NOISE.equals(new String(szDiagnosisType).trim())){
                    System.out.println(" 视频噪声检测:");
                    NET_VIDEO_NOISE_DETECTIONRESULT  result=new NET_VIDEO_NOISE_DETECTIONRESULT();

                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);
                    count+=result.dwSize;

                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);
                    System.out.println(" szPicUrl:"+new String(result.szPicUrl) );


                }else if(NET_DIAGNOSIS_BLUR.equals(new String(szDiagnosisType).trim()) ){
                    System.out.println(" 视频模糊检测:");
                    NET_VIDEO_BLUR_DETECTIONRESULT result=new NET_VIDEO_BLUR_DETECTIONRESULT();

                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);
                    count+=result.dwSize;

                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);
                    System.out.println(" szPicUrl:"+new String(result.szPicUrl) );

                }else if(NET_DIAGNOSIS_SCENECHANGE.equals(new String(szDiagnosisType).trim())){
                    System.out.println(" 视频场景变化检测:");

                    NET_VIDEO_SCENECHANGE_DETECTIONRESULT result=new NET_VIDEO_SCENECHANGE_DETECTIONRESULT();

                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);
                    count+=result.dwSize;
                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);
                }else if(NET_DIAGNOSIS_VIDEO_DELAY.equals(new String(szDiagnosisType).trim())){
                    System.out.println(" 视频延时检测:");
                    NET_VIDEO_DELAY_DETECTIONRESUL result=new NET_VIDEO_DELAY_DETECTIONRESUL();

                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);

                    count+=result.dwSize;
                    System.out.println(" nSignalDelay:"+result.nSignalDelay);
                    System.out.println(" nStreamDelay:"+result.nStreamDelay);
                    System.out.println(" nIFrameDelay:"+result.nIFrameDelay);


                }else if(NET_DIAGNOSIS_PTZ_MOVING.equals( new String(szDiagnosisType).trim() )){
                    System.out.println(" 云台移动检测:");

                    NET_PTZ_MOVING_DETECTIONRESULT result=new NET_PTZ_MOVING_DETECTIONRESULT();

                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);
                    count+=result.dwSize;

                    System.out.println(" emPTZMovingUp:"+result.emPTZMovingUp);
                    System.out.println(" emPTZMovingDown:"+result.emPTZMovingDown);
                    System.out.println(" emPTZMovingLeft:"+result.emPTZMovingLeft);
                    System.out.println(" emPTZMovingRight:"+result.emPTZMovingRight);
                    System.out.println(" emPTZMovingZoomWide:"+result.emPTZMovingZoomWide);
                    System.out.println(" emPTZMovingZoomTele:"+result.emPTZMovingZoomTele);

                }else if(NET_DIAGNOSIS_BLACK_WHITE.equals(new String(szDiagnosisType).trim())){
                    System.out.println(" 黑白图像检测:");
                    NET_BLACK_WHITE_DETECTIONRESULT result=new NET_BLACK_WHITE_DETECTIONRESULT();

                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);

                    count+=result.dwSize;

                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);

                    System.out.println(" szPicUrl:"+ new String(result.szPicUrl) );


                }else if(NET_DIAGNOSIS_DRAMATIC_CHANGE.equals( new String(szDiagnosisType).trim() )){
                    System.out.println(" 场景剧变检测:");
                    NET_DIAGNOSIS_DRAMATIC_DETECTIONRESULT result=new NET_DIAGNOSIS_DRAMATIC_DETECTIONRESULT();
                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);
                    count+=result.dwSize;

                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);


                }else if(NET_DIAGNOSIS_VIDEO_AVAILABILITY.equals(new String(szDiagnosisType).trim())){
                    System.out.println(" 视频完好率监测:");

                    NET_VIDEO_AVAILABILITY_DETECTIONRESULT   result=new NET_VIDEO_AVAILABILITY_DETECTIONRESULT();
                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);
                    count+=result.dwSize;

                    System.out.println(" nSignalDelay:"+result.nSignalDelay);
                    System.out.println(" nStreamDelay:"+result.nStreamDelay);
                    System.out.println(" nIFrameDelay:"+result.nIFrameDelay);

                } else if(NET_DIAGNOSIS_SNOWFLAKE.equals(new String(szDiagnosisType).trim())){
                    System.out.println(" 雪花屏检测:");
                    NET_VIDEO_SNOWFLAKE_DETECTIONRESULT result=new NET_VIDEO_SNOWFLAKE_DETECTIONRESULT();
                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);

                    count+=result.dwSize;

                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);

                    System.out.println(" szPicUrl:"+new String(result.szPicUrl));
                }else if(NET_DIAGNOSIS_VIDEO_ALGORITHMTYPE.equals( new String(szDiagnosisType).trim() )){
                    System.out.println(" 视频算法类型检测:");
                    NET_VIDEO_ALGORITHMTYPE_DETECTIONRESULT result=new NET_VIDEO_ALGORITHMTYPE_DETECTIONRESULT();
                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);
                    count+=result.dwSize;
                    System.out.println(" bFaceAlgorithm:"+result.bFaceAlgorithm);
                    System.out.println(" nFaceAlgorithmValue:"+result.nFaceAlgorithmValue);
                    System.out.println(" bHumanBodyAlgorithm:"+result.bHumanBodyAlgorithm);

                    System.out.println(" nHumanBodyAlgorithmValue:"+result.nHumanBodyAlgorithmValue);
                    System.out.println(" bVehicleAlgorithm:"+result.bVehicleAlgorithm);

                    System.out.println(" nVehicleAlgorithmValue:"+result.nVehicleAlgorithmValue);

                    System.out.println(" bPlateNumAlgorithm:"+result.bPlateNumAlgorithm);

                    System.out.println(" nPlateNumAlgorithmValue:"+result.nPlateNumAlgorithmValue);

                }else if(NET_DIAGNOSIS_VIDEO_FILCKERING_DETECTION.equals( new String(szDiagnosisType).trim() )){
                    System.out.println(" 视频闪频检测:");
                    NET_VIDEO_FILCKERING_DETECTION_RESULT result=new NET_VIDEO_FILCKERING_DETECTION_RESULT();

                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);

                    count+=result.dwSize;

                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);

                } else if(NET_DIAGNOSIS_VIDEO_LOSS_FRAME_DETECTION.equals(new String(szDiagnosisType).trim())){
                    NET_VIDEO_LOSS_FRAME_DETECTION_RESULT result=new NET_VIDEO_LOSS_FRAME_DETECTION_RESULT();
                    ToolKits.GetPointerDataToStruct(pDiagnosisResult,count,result);

                    count+=result.dwSize;

                    System.out.println(" nValue:"+result.nValue);
                    System.out.println(" emState:"+result.emState);
                    System.out.println(" nDuration:"+result.nDuration);

                }else {
                    System.out.println("other szDiagnosisType:"+new String(szDiagnosisType));
                    count+=nDiagnosisTypeLen;
                }

            }


            System.out.println("计划名称:" + ToolKits.GetPointerDataToGBKString(commons.stProject.pArray, commons.stProject.dwArrayLen));
            System.out.println("任务名称:" + ToolKits.GetPointerDataToGBKString(commons.stTask.pArray, commons.stTask.dwArrayLen));
            System.out.println("参数表名称:" + ToolKits.GetPointerDataToGBKString(commons.stProfile.pArray, commons.stProfile.dwArrayLen));
            System.out.println("诊断设备ID:" + ToolKits.GetPointerDataToGBKString(commons.stDeviceID.pArray, commons.stDeviceID.dwArrayLen));
            System.out.println("诊断通道:" + commons.nVideoChannelID);
            System.out.println("诊断开始时间:" + commons.stStartTime.toStringTime());
            System.out.println("诊断结束时间:" + commons.stEndTime.toStringTime());
            System.out.println("诊断码流:" + commons.emVideoStream);  // 参考  NET_STREAM_TYPE
            System.out.println("诊断结果类型:" + commons.emResultType);  // 参考  NET_VIDEODIAGNOSIS_RESULT_TYPE
            System.out.println("诊断结果:" + commons.bCollectivityState); // 诊断结果, 1-true, 0-false
            System.out.println("失败原因:" + commons.emFailedCause);  // 参考 NET_VIDEODIAGNOSIS_FAIL_TYPE
            System.out.println("失败原因描述:" + new String(commons.szFailedCode).trim());
            System.out.println("诊断结果存放地址:" + new String(commons.szResultAddress).trim());

            System.out.println("诊断结果存放地址扩展:" + new String(commons.szResultAddressEx).trim());
            for(int i = 0; i < commons.nBackPic; i++) {
                System.out.println("背景图片路径:" + new String(commons.szBackPicAddressArr[i].szBackPicAddress).trim());
            }
            return 0;
        }

    }



    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "视频诊断参数表" , "VideoDiagnosisProfileConfig")));
        menu.addItem((new CaseMenu.Item(this , "获取诊断任务名称" , "GetVideoDiagnosisTask")));
        menu.addItem((new CaseMenu.Item(this , "视频诊断任务获取" , "VideoDiagnosisTaskGet")));
        menu.addItem((new CaseMenu.Item(this , "视频诊断任务设置" , "VideoDiagnosisTaskSet")));
        menu.addItem((new CaseMenu.Item(this , "获取诊断计划名称" , "GetVideoDiagnosisProject")));
        menu.addItem((new CaseMenu.Item(this , "视频诊断计划获取" , "VideoDiagnosisProjectGet")));
        menu.addItem((new CaseMenu.Item(this , "视频诊断计划设置" , "VideoDiagnosisProjectSet")));
        menu.addItem((new CaseMenu.Item(this , "视频诊断订阅" , "StartVideoDiagnosis")));
        menu.addItem((new CaseMenu.Item(this , "取消视频诊断订阅" , "StopVideoDiagnosis")));
        menu.run();
    }

    public static void main(String[] args) {
        StartVideoDiagnosisDemo demo=new StartVideoDiagnosisDemo();
        InitTest("10.38.51.1",37777,"admin","dahuacloud123");
        demo.RunTest();
        LoginOut();

    }

}
