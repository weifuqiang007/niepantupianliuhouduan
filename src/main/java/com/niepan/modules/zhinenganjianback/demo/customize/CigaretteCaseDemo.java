package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_CIGARETTE_CASE_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_DEV_EVENT_CIGARETTE_CASE_DETECTION_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_TIME_EX;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Pointer;
import java.io.File;
import java.io.UnsupportedEncodingException;

/**
 * @author 291189
 * @version 1.0
 * @description ERR221213111
 * @date 2022/12/14 14:17
 */
public class CigaretteCaseDemo extends Initialization {


    public static   NetSDKLib.LLong AttachHandle=new NetSDKLib.LLong(0);

    static   int ChannelId=0;
    /**
     * 订阅智能事件
     * @return
     */
    public static void AttachEventRealLoadPic() {
        //先关闭，再开启
        if(AttachHandle.intValue()!=0){
            DetachEventRealLoadPic();
        }
        int bNeedPicture = 1; // 是否需要图片
        AttachHandle =netSdk.CLIENT_RealLoadPictureEx(loginHandle, ChannelId, netSdk.EVENT_IVS_CIGARETTE_CASE_DETECTION,
                bNeedPicture , AnalyzerDataCB.getInstance() , null , null);
        if( AttachHandle.longValue() != 0  ) {
            System.out.println("CLIENT_RealLoadPictureEx Success  ChannelId : \n" + ChannelId);
        } else {
            System.out.println("CLIENT_RealLoadPictureEx Failed  ChannelId : \n" + ToolKits.getErrorCode());
        }

    }
    /**
     * 停止侦听智能事件
     */
    public static void DetachEventRealLoadPic() {
        if (AttachHandle.longValue() != 0) {
            netSdk.CLIENT_StopLoadPic(AttachHandle);
        }
    }
    /** 写成静态主要是防止被回收 */
    private static class AnalyzerDataCB implements NetSDKLib.fAnalyzerDataCallBack {

        private String m_imagePath;
        private AnalyzerDataCB() {
            m_imagePath = "./PlateNumber/";
            File path = new File(m_imagePath);
            if (!path.exists()) {
                path.mkdir();
            }
        }

        private static class AnalyzerDataCBHolder {
            private static final AnalyzerDataCB instance = new AnalyzerDataCB();
        }

        public static AnalyzerDataCB getInstance() {
            return AnalyzerDataCB.AnalyzerDataCBHolder.instance;
        }

        public int invoke(
                NetSDKLib.LLong lAnalyzerHandle,
                int dwAlarmType,
                Pointer pAlarmInfo,
                Pointer pBuffer,
                int dwBufSize,
                Pointer dwUser,
                int nSequence,
                Pointer reserved) {
            if (lAnalyzerHandle.longValue() == 0 || pAlarmInfo == null) {
                return -1;
            }

            switch (dwAlarmType) {

                case NetSDKLib.EVENT_IVS_CIGARETTE_CASE_DETECTION: // 烟盒检测事件(对应 NET_DEV_EVENT_CIGARETTE_CASE_DETECTION_INFO)
                {
                    System.out.println("烟盒检测事件");
                    NET_DEV_EVENT_CIGARETTE_CASE_DETECTION_INFO msg = new NET_DEV_EVENT_CIGARETTE_CASE_DETECTION_INFO();
                    ToolKits.GetPointerData(pAlarmInfo, msg);
                //Demo中需要体现：销售日期YYYY-MM-DD、销售时间YYYY-MM-DD HH:MM:SS、商品名称、销售数量（单位包）；
                    byte[] szName = msg.szName;

                    try {
                        System.out.println("事件名称:"+new String(szName,encode));

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    System.out.println("通道号:"+msg.nChannelID);

                    NET_TIME_EX stuUTC = msg.stuUTC;

                    System.out.println("销售日期:"+stuUTC.dwYear+"-"+stuUTC.dwMonth+"-"+stuUTC.dwDay);

                    System.out.println("销售时间:"+stuUTC.dwYear+"-"+stuUTC.dwMonth+"-"+stuUTC.dwDay+" "
                            +stuUTC.dwHour+":"+stuUTC.dwMinute+":"+stuUTC.dwSecond);

                    int nCigaretteCaseNum
                            = msg.nCigaretteCaseNum;
                    System.out.println("销售香烟信息数量:"+nCigaretteCaseNum);


                    NET_CIGARETTE_CASE_INFO[] stuCigaretteCaseInfo = msg.stuCigaretteCaseInfo;

                    for(int i=0;i<nCigaretteCaseNum;i++){
                        NET_CIGARETTE_CASE_INFO info
                                = stuCigaretteCaseInfo[i];

                        int cigaretteNum = info.CigaretteNum;
                        System.out.println("销售烟盒数量，单位为盒:"+cigaretteNum);

                        byte[] szCigaretteType = info.szCigaretteType;

                        try {
                            System.out.println("销售烟盒种类:"+new String(szCigaretteType,encode));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }

                    break;
                }
                default:
                    System.out.println("其他事件：" + dwAlarmType);
                    break;
            }

            return 0;
        }

    }

    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "AttachEventRealLoadPic" , "AttachEventRealLoadPic")));
        menu.addItem((new CaseMenu.Item(this , "DetachEventRealLoadPic" , "DetachEventRealLoadPic")));
        menu.run();
    }

    public static void main(String[] args) {
        CigaretteCaseDemo cigaretteCaseDemo=new CigaretteCaseDemo();
        InitTest("172.29.5.28",37777,"admin","admin123");
        cigaretteCaseDemo.RunTest();
        LoginOut();

    }
}
