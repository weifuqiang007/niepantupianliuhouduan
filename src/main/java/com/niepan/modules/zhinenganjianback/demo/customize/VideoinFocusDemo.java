package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.CFG_VIDEO_IN_FOCUS;
import com.niepan.modules.zhinenganjianback.lib.structure.CFG_VIDEO_IN_FOCUS_UNIT;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;

/**
 * @author 291189
 * @version 1.0
 * @description ERR221028089 
 * @date 2022/11/1 11:05
 */
public class VideoinFocusDemo extends Initialization {

/**
 * 聚焦设置
 */
public void videoFocus() {

    int chanl=0;
    CFG_VIDEO_IN_FOCUS focus=new CFG_VIDEO_IN_FOCUS();

    boolean b
            = ToolKits.GetDevConfig(loginHandle, chanl, NetSDKLib.CFG_CMD_VIDEOIN_FOCUS, focus);
    if (!b) {
        System.err.println("CFG_CMD_VIDEOIN_FOCUS GetDevConfig Failed." + ToolKits.getErrorCode());
        return;
    }else {
        System.out.println("CFG_CMD_VIDEOIN_FOCUS  GetDevConfig success");
    }
    /**
     通道号
     */
    System.out.println("nChannelIndex:"+focus.nChannelIndex);
    /**
     配置使用个数
     */
    int nVideoInFocusRealNum
            = focus.nVideoInFocusRealNum;

    System.out.println("nVideoInFocusRealNum:"+nVideoInFocusRealNum);
    CFG_VIDEO_IN_FOCUS_UNIT[] stVideoInFocusUnit
            = focus.stVideoInFocusUnit;

    for(int i=0;i<nVideoInFocusRealNum;i++){
        CFG_VIDEO_IN_FOCUS_UNIT focus_unit
                = stVideoInFocusUnit[i];


        System.out.println("nMode:"+focus_unit.nMode);

        System.out.println("nSensitivity:"+focus_unit.nSensitivity);

        System.out.println("nIRCorrection:"+focus_unit.nIRCorrection);

        System.out.println("emFocusMode:"+focus_unit.emFocusMode);

        System.out.println("nFocusLimit:"+focus_unit.nFocusLimit);

    }


    CFG_VIDEO_IN_FOCUS_UNIT focus_unit = focus.stVideoInFocusUnit[0];
    /**
     聚焦模式, 0-关闭, 1-辅助聚焦, 2-自动聚焦, 3-半自动聚焦, 4-手动聚焦
     */
    focus_unit.nMode=1;
    /**
     聚焦灵敏度, 0-高, 1-默认, 2-低
     */
    focus_unit.nSensitivity=2;
    /**
     红外光聚焦修正, 0-不修正, 1-修正, 2-自动修正
     */
    focus_unit.nIRCorrection=1;
    /**
     聚焦极限值, 单位毫米
     */
    focus_unit.nFocusLimit=100;

     b = ToolKits.SetDevConfig(loginHandle, chanl, NetSDKLib.CFG_CMD_VIDEOIN_FOCUS, focus);
    if (!b) {
        System.err.println("CFG_CMD_VIDEOIN_FOCUS SetDevConfig Failed." + ToolKits.getErrorCode());
        return;
    }else {
        System.out.println("CFG_CMD_VIDEOIN_FOCUS SetDevConfig success");
    }

}


    /**
     * 云台通道变倍配置
     */
    public void videoZoom() {

        int chanl=0;

        NetSDKLib.CFG_VIDEO_IN_ZOOM zoom=new NetSDKLib.CFG_VIDEO_IN_ZOOM();

        boolean b = ToolKits.GetDevConfig(loginHandle, chanl, NetSDKLib.CFG_CMD_VIDEO_IN_ZOOM, zoom);

        if (!b) {
            System.err.println("CFG_CMD_VIDEO_IN_ZOOM GetDevConfig Failed." + ToolKits.getErrorCode());
            return;
        }else {
            System.out.println("CFG_CMD_VIDEO_IN_ZOOM  GetDevConfig success");
        }

        System.out.println("nChannelIndex:"+zoom.nChannelIndex);

        int nVideoInZoomRealNum = zoom.nVideoInZoomRealNum;

        System.out.println("nVideoInZoomRealNum:"+nVideoInZoomRealNum);

        NetSDKLib.CFG_VIDEO_IN_ZOOM_UNIT[] stVideoInZoomUnit= zoom.stVideoInZoomUnit;


        for(int i=0;i<nVideoInZoomRealNum;i++){
            NetSDKLib.CFG_VIDEO_IN_ZOOM_UNIT zoom_unit = stVideoInZoomUnit[i];
            //变倍速率(0~7)
            System.out.println("nSpeed:"+zoom_unit.nSpeed);
            //是否数字变倍, 类型为BOOL, 取值0或者1
            System.out.println("bDigitalZoom:"+zoom_unit.bDigitalZoom);
            //当前速率下最大变倍上限(0~13)
            System.out.println("nZoomLimit:"+zoom_unit.nZoomLimit);
            //光照场景名称 EM_LIGHT_SCENE
            /**
             * {@link NetSDKLib.EM_LIGHT_SCENE}
             */
            System.out.println("emLightScene:"+zoom_unit.emLightScene);
        }

        NetSDKLib.CFG_VIDEO_IN_ZOOM_UNIT cfg_zoom_unit
                = stVideoInZoomUnit[0];
        cfg_zoom_unit.nSpeed=2;

        cfg_zoom_unit.bDigitalZoom=1;

        cfg_zoom_unit.nZoomLimit=5;

        cfg_zoom_unit.emLightScene=1;


        b = ToolKits.SetDevConfig(loginHandle, chanl, NetSDKLib.CFG_CMD_VIDEO_IN_ZOOM, zoom);
        if (!b) {
            System.err.println("CFG_CMD_VIDEOIN_FOCUS SetDevConfig Failed." + ToolKits.getErrorCode());
            return;
        }else {
            System.out.println("CFG_CMD_VIDEOIN_FOCUS SetDevConfig success");
        }

    }


    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();
        menu.addItem(new CaseMenu.Item(this , "聚焦设置" , "videoFocus"));

        menu.addItem(new CaseMenu.Item(this , "云台通道变倍配置" , "videoZoom"));

        menu.run();
    }


    private static String ipAddr 			= "10.35.249.231";
    private static int    port 				= 37777;
    private static String user 			    = "admin";
    private static String password 		    = "admin123";

    public static void main(String[] args){
        InitTest(ipAddr, port, user, password);
        VideoinFocusDemo videoinFocusDemo=new VideoinFocusDemo();
        videoinFocusDemo.RunTest();
        LoginOut();
    }

}
