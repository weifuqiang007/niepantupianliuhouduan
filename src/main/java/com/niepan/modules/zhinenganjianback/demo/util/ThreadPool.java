package com.niepan.modules.zhinenganjianback.demo.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadPool{
    public static class ThreadInfo{
        Runnable runnable;
        Thread thread;
        Semaphore semaphore = new Semaphore(0);
    };

    List<ThreadInfo> threadInfoList = new ArrayList<>();
    List<ThreadInfo> idleThreadInfoList = new ArrayList<>();
    List<Runnable> runnableList = new ArrayList<>();
    ReentrantLock reentrantLock = new ReentrantLock();

    public void addRunnable(Runnable runnable){
        reentrantLock.lock();
        runnableList.add(runnable);
        processRunnable();
        reentrantLock.unlock();
    }

    public void start(int threadCount){
        for(int i = 0; i < threadCount; i++){
            ThreadInfo threadInfo = new ThreadInfo();
            threadInfo.thread = getNewThread(threadInfo);
            threadInfo.thread.start();
            threadInfoList.add(threadInfo);
            idleThreadInfoList.add(threadInfo);
        }
    }

    private Thread getNewThread(ThreadInfo threadInfo){
        return new Thread() {
            public void run(){
                threadProc(threadInfo);
            }
        };
    }
    private void threadProc(ThreadInfo threadInfo){
        do {
            try
            {
                threadInfo.semaphore.acquire();
                threadInfo.runnable.run();
                threadInfo.runnable = null;
                reentrantLock.lock();
                idleThreadInfoList.add(threadInfo);
                processRunnable();
                reentrantLock.unlock();
            }
            catch (InterruptedException e)
            {
            }
        }while (true);
    }

    private void processRunnable(){
        while (true){
            if(idleThreadInfoList.size() > 0 && runnableList.size() > 0){
                ThreadInfo threadInfo = idleThreadInfoList.get(0);
                Runnable runnable = runnableList.get(0);
                idleThreadInfoList.remove(0);
                runnableList.remove(0);
                threadInfo.runnable = runnable;
                threadInfo.semaphore.release();
            }else{
                break;
            }
        }

    }
}
