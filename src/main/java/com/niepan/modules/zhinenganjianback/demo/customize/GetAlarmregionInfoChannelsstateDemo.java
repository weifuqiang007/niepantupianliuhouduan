package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.enumeration.*;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_CHANNELS_STATE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_IN_GET_CHANNELS_STATE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_OUT_GET_CHANNELS_STATE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_SENSOR_STATE;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;

import java.io.UnsupportedEncodingException;

/**
 * @author 291189
 * @version 1.0
 * @description  ERR220926212-RC01
 * @date 2022/10/9 10:00
 */
public class GetAlarmregionInfoChannelsstateDemo extends Initialization {


    public static void getAlarmregionInfoChannelsstate(){

        // 入参
        NET_IN_GET_CHANNELS_STATE stuIn = new NET_IN_GET_CHANNELS_STATE();
        /**
         通道类型 {@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_CHANNELS_STATE_TYPE}
         */
        stuIn.stuCondition.emType=1;
        stuIn.write();

        // 出参
        NET_OUT_GET_CHANNELS_STATE stuOut = new NET_OUT_GET_CHANNELS_STATE();
        stuOut.write();
        Boolean bRet = netSdk.CLIENT_GetAlarmRegionInfo(loginHandle, NET_EM_GET_ALARMREGION_INFO.NET_EM_GET_ALARMREGION_INFO_CHANNELSSTATE, stuIn.getPointer(), stuOut.getPointer(), 3000);
        if (!bRet){
            System.err.println("获取通道状态 失败：" + ToolKits.getErrorCode());
            return;
        }else{
            stuOut.read();
            System.out.println("获取通道状态 成功");
            System.out.println("通道状态个数:"+stuOut.nChannelsStatesCount);
            NET_CHANNELS_STATE[] stuChannelsStates = stuOut.stuChannelsStates;
            for (int i = 0; i < stuOut.nChannelsStatesCount; i++) {
                System.out.println("Area号:"+(i+1));
                System.out.println("通道类型:"+ EM_CHANNELS_STATE_TYPE.getNoteByValue(stuChannelsStates[i].emType) );
                System.out.println("通道号:"+ stuChannelsStates[i].nIndex );
                System.out.println("在线状态:"+ EM_DEV_STATUS.getNoteByValue(stuChannelsStates[i].emOnlineState));
                System.out.println("报警状态:"+ EM_ZONE_STATUS.getNoteByValue(stuChannelsStates[i].emAlarmState));
                System.out.println("输出状态:"+ EM_OUTPUT_STATE.getNoteByValue(stuChannelsStates[i].emOutputState));

                try {
                    System.out.println("通道对应名称:"+ new  String(stuChannelsStates[i].szName,encode) );

                    System.out.println("通道对应SN号:"+ new  String(stuChannelsStates[i].szSN,encode) );
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                //探测器状态信息
                NET_SENSOR_STATE stuSensorState = stuChannelsStates[i].stuSensorState;
                //外接电源连接状态 : 0:正常, 1:未连接
                System.out.println("外接电源连接状态:"+ stuSensorState.nExPowerState);
                // 配件防拆状态 : 0:正常, 1:打开
                System.out.println("配件防拆状态:"+ stuSensorState.nTamper);
                //  电池电量状态 : 0:正常, 1:低电量, 2:掉电
                System.out.println("电池电量状态:"+ stuSensorState.nLowPowerState);
            }

        }
    }

    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;

        menu.addItem((new CaseMenu.Item(this , "getAlarmregionInfoChannelsstate" , "getAlarmregionInfoChannelsstate")));

        menu.run();
    }

    public static void main(String[] args) {
        GetAlarmregionInfoChannelsstateDemo getAlarmregionInfoChannelsstateDemo=new GetAlarmregionInfoChannelsstateDemo();
        InitTest("172.3.0.185",37777,"admin","admin123");
        getAlarmregionInfoChannelsstateDemo.RunTest();
        LoginOut();

    }

}
