package com.niepan.modules.zhinenganjianback.demo.udp;

import com.niepan.common.utils.logs.LogUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * @author: liuchenyu
 * @date: 2023/4/8
 */
public class UDPNewServer{

    private final static Logger logger = LoggerFactory.getLogger(UDPNewServer.class);

    public static DatagramSocket socket = null;

    private static int port = 9000;

    //左摆
    public static final byte[] leftBytes = new byte[]{(byte) 0xAA, (byte) 0xAA,0x01,0x00,0x00,0x00,(byte) 0x0F, 0x00,0x3A,0x01,0x1A,0x00,0x20,0x00,0x01};

    //右摆
    public static final byte[] rightBytes = new byte[]{(byte) 0xAA, (byte) 0xAA,0x01,0x00,0x00,0x00,(byte) 0x0F, 0x00,0x3A,0x01,0x1A,0x01,0x20,0x00,0x00};

    //回正
    public static final byte[] backBytes = new byte[]{(byte) 0xAA, (byte) 0xAA,0x01,0x00,0x00,0x00,(byte) 0x0F, 0x00,0x3A,0x01,0x1A,0x01,0x10,0x00,0x02};


    /**
     * 自动回复消息
     *
     * @param bytes
     */
    public static void returnMsg(byte[] bytes) {
        try {
            if (socket == null) {
                synchronized (DatagramSocket.class) {
                    if (socket == null) {
                        socket = new DatagramSocket();
                    }
                }
            }
            InetAddress address = InetAddress.getByName("192.168.0.49");
            DatagramPacket packet = new DatagramPacket(bytes, bytes.length, address, port);
            socket.send(packet);

            logger.info("已经发送指令");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
