package com.niepan.modules.zhinenganjianback.demo;


import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.enumeration.EM_INSIDE_OBJECT_TYPE;
import com.niepan.modules.zhinenganjianback.lib.enumeration.ENUMERROR;
import com.niepan.modules.zhinenganjianback.lib.structure.*;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Component
public class DeviceClient implements NetSDKLib.fAnalyseTaskResultCallBack {

    private final static Logger log= LoggerFactory.getLogger(DeviceClient.class);

    static NetSDKLib netSdk = NetSDKLib.NETSDK_INSTANCE;
    static NetSDKLib config =  NetSDKLib.CONFIG_INSTANCE;
    static final int   ClientStateInit =0;
    static final int   ClientStateLogined =1;
    static final int   ClientStateTaskAttached =2;
    DeviceData deviceData  = new DeviceData();



    public static class PictureInfo{
        String id;
        byte[] data;
    };

    public static class PictureResult{
        String sn;
        //        DEV_EVENT_XRAY_DETECTION_INFO detectInfo;
        byte[] resultBuf;
        int resultBufLen;
        List<NET_INSIDE_OBJECT> objectList = new ArrayList<>();
    };

    public interface PictureResultCallBack  {
        public int onResult(PictureInfo info, PictureResult result);
    };
    public static class DeviceInfo{
        Boolean idle;
        String ip;
    };
    public DeviceInfo getInfo(){
        DeviceInfo info = new DeviceInfo();
        info.ip = deviceData.ip;
        info.idle = false;
        if(deviceData.state == ClientStateTaskAttached){
            if(deviceData.waitResult){
                if(deviceData.pushTime + 500 < System.currentTimeMillis()){
                    info.idle = true;
                }
            }
            else{
                info.idle = true;
            }
        }
        return info;
    }

    // todo
    public static class DeviceData{
        String ip;
        int port;
        String user;
        String password;

        NetSDKLib.LLong loginHandle = new NetSDKLib.LLong(0);
        NetSDKLib.LLong attachHandle = new NetSDKLib.LLong(0);
        int taskID=0;
        int state = ClientStateInit;

        NetSDKLib.NET_DEVICEINFO_Ex stDeviceInfo = new NetSDKLib.NET_DEVICEINFO_Ex();

        //        Boolean hasResult = false;
//        DEV_EVENT_XRAY_DETECTION_INFO detectInfo;
        File picturePath = new File("./AnalyzerPicture/result/");
//        byte[] resultBuf;
//        int resultBufLen;

        long pushTime;
        //todo
        boolean waitResult;

        PictureInfo pictureInfo;
        PictureResultCallBack callBack = null;
        void init(){
            if (!picturePath.exists()) {
                picturePath.mkdirs();
            }
            loginHandle = new NetSDKLib.LLong(0);
            attachHandle = new NetSDKLib.LLong(0);
            taskID=0;
            state = ClientStateInit;
//            resultBuf = null;
            waitResult = false;
            pushTime = 0;
        }
    }
    public NetSDKLib.LLong getLoginHandle(){
        return deviceData.loginHandle;
    }
    public  Boolean isReady(){
        return deviceData.state == ClientStateTaskAttached;
    }

    public void init(String ip,int port,String user,String password, PictureResultCallBack callback){
        deviceData.ip = ip;
        deviceData.port = port;
        deviceData.user = user;
        deviceData.password = password;
        deviceData.init();
        deviceData.callBack = callback;
    }

    private Boolean login() {
        // 输入结构体参数
        NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY pstlnParam = new NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY() {
            {
                szIP = deviceData.ip.getBytes();
                nPort = deviceData.port;
                szUserName = deviceData.user.getBytes();
                szPassword = deviceData.password.getBytes();
            }
        };

        // 输出结构体参数
        NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY pstOutParam = new NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY();

        // 写入sdk
        deviceData.loginHandle = netSdk.CLIENT_LoginWithHighLevelSecurity(pstlnParam, pstOutParam);
        if (deviceData.loginHandle.longValue() == 0) {
            //  System.err.printf("Login Device[%s] Port[%d]Failed. %s\n", ip, port, netSdk.CLIENT_GetLastError());
            System.out.println("login Device[%s] Port[%d]Failed. "+ deviceData.ip+":"+ deviceData.port+":"+ netSdk.CLIENT_GetLastError());
            return false;
        } else {
            NetSDKLib.NET_DEVICEINFO_Ex   deviceInfo = pstOutParam.stuDeviceInfo; // 获取设备信息
            System.out.println("login Success Device Address：" + deviceData.ip + " handle "+ deviceData.loginHandle);
            deviceData.stDeviceInfo = pstOutParam.stuDeviceInfo;
            return true;
        }
    }


    public Boolean loginNew() {

        System.out.println("deviceData:"+deviceData.user+","+deviceData.ip);
        // 输入结构体参数
        NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY pstlnParam = new NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY() {
            {
                szIP = deviceData.ip.getBytes();
                nPort = deviceData.port;
                szUserName = deviceData.user.getBytes();
                szPassword = deviceData.password.getBytes();
            }
        };

        // 输出结构体参数
        NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY pstOutParam = new NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY();

        // 写入sdk
        deviceData.loginHandle = netSdk.CLIENT_LoginWithHighLevelSecurity(pstlnParam, pstOutParam);
        if (deviceData.loginHandle.longValue() == 0) {
            //  System.err.printf("Login Device[%s] Port[%d]Failed. %s\n", ip, port, netSdk.CLIENT_GetLastError());
            System.out.println("login Device[%s] Port[%d]Failed. "+ deviceData.ip+":"+ deviceData.port+":"+ netSdk.CLIENT_GetLastError());
            return false;
        } else {
            NetSDKLib.NET_DEVICEINFO_Ex   deviceInfo = pstOutParam.stuDeviceInfo; // 获取设备信息
            System.out.println("login Success Device Address：" + deviceData.ip + " handle "+ deviceData.loginHandle);
            deviceData.stDeviceInfo = pstOutParam.stuDeviceInfo;
            return true;
        }
    }

    private void logout(){
        System.out.println("logout Device Address：" + deviceData.ip);
        netSdk.CLIENT_Logout(deviceData.loginHandle);
        deviceData.loginHandle = new NetSDKLib.LLong(0);;
    }
    public static boolean logoutNew(NetSDKLib.LLong lLoginID){
       // System.out.println("logout Device Address：" + deviceData.ip);
        boolean b = netSdk.CLIENT_Logout(lLoginID);
        return b;
    }

    private Boolean findAnalyseTaskID() {
        NetSDKLib.NET_IN_FIND_ANALYSE_TASK stuInParam = new NetSDKLib.NET_IN_FIND_ANALYSE_TASK();
        NetSDKLib.NET_OUT_FIND_ANALYSE_TASK stuOutParam = new NetSDKLib.NET_OUT_FIND_ANALYSE_TASK();
        if (netSdk.CLIENT_FindAnalyseTask(deviceData.loginHandle, stuInParam, stuOutParam, 5000)) {
            //System.out.println("FindAnalyseTask Succeed!" + "智能分析任务个数" + stuOutParam.nTaskNum);
            // ID和状态 可以从 stuTaskInfos 中获取
            for (int i = 0; i < stuOutParam.nTaskNum; i++) {   // 状态值参考 EM_ANALYSE_STATE

                deviceData.taskID=stuOutParam.stuTaskInfos[i].nTaskID;
                System.out.printf("任务%d: %d, 状态：%d\n", (i + 1), stuOutParam.stuTaskInfos[i].nTaskID, stuOutParam.stuTaskInfos[i].emAnalyseState);
                return true;
            }

        } else {
            System.err.printf("FindAnalyseTask Failed!Last Error:%s\n", ToolKits.getErrorCode());
        }
        return false;
    }
//todo
    public Boolean attachAnalyseTaskResult() {
        // 入参
        NetSDKLib.NET_IN_ATTACH_ANALYSE_RESULT pInParam = new NetSDKLib.NET_IN_ATTACH_ANALYSE_RESULT();
        pInParam.cbAnalyseTaskResult = this;

        pInParam.nTaskIdNum = 1;
        pInParam.nTaskIDs[0] = deviceData.taskID;

        pInParam.stuFilter.nEventNum = 1;    // 只获取 EVENT_IVS_FEATURE_ABSTRACT 特征提取事件
        pInParam.stuFilter.dwAlarmTypes[0] = NetSDKLib.EVENI_IVS_XRAY_DETECTION;
System.out.println("deviceData.loginHandle:"+deviceData.loginHandle);
        deviceData.attachHandle = netSdk.CLIENT_AttachAnalyseTaskResult(deviceData.loginHandle, pInParam, 5000);
        if (deviceData.attachHandle.longValue() != 0) {
            System.out.println("AttachAnalyseTaskResult Succeed!"+deviceData.attachHandle.longValue());
        } else {
            System.err.printf("AttachAnalyseTaskResult Failed!Last Error[0x%x]\n", netSdk.CLIENT_GetLastError());
        }
        return deviceData.attachHandle.longValue() != 0;
    }

    public  Boolean attachAnalyseTaskResultNew(NetSDKLib.LLong lLoginID) {
        // 入参
        NetSDKLib.NET_IN_ATTACH_ANALYSE_RESULT pInParam = new NetSDKLib.NET_IN_ATTACH_ANALYSE_RESULT();
        pInParam.cbAnalyseTaskResult = this;

        pInParam.nTaskIdNum = 1;
        pInParam.nTaskIDs[0] = deviceData.taskID;

        pInParam.stuFilter.nEventNum = 1;    // 只获取 EVENT_IVS_FEATURE_ABSTRACT 特征提取事件
        pInParam.stuFilter.dwAlarmTypes[0] = NetSDKLib.EVENI_IVS_XRAY_DETECTION;

        deviceData.attachHandle = netSdk.CLIENT_AttachAnalyseTaskResult(lLoginID, pInParam, 5000);
        if (deviceData.attachHandle.longValue() != 0) {
            System.out.println("AttachAnalyseTaskResult Succeed!"+deviceData.attachHandle.longValue());
        } else {
            System.err.printf("AttachAnalyseTaskResult Failed!Last Error[0x%x]\n", netSdk.CLIENT_GetLastError());
        }
        return deviceData.attachHandle.longValue() != 0;
    }

    private Boolean detachAnalyseTaskResult() {
        Boolean result = netSdk.CLIENT_DetachAnalyseTaskResult(deviceData.attachHandle);
        if (!result) {
            System.out.println("detachAnalyseTaskResult failed. error is " + ENUMERROR.getErrorMessage());
        }else {
            System.out.println("detachAnalyseTaskResult Succeed");
        }
        return  result;
    }

    public Boolean prepare(){
        if(deviceData.state == ClientStateTaskAttached){
            System.out.println(deviceData.ip + " prepare already");
            return true;
        }
        if(deviceData.state!=ClientStateLogined){

            if(!login()){
                System.out.println(deviceData.ip + " prepare fail login fail");
                return false;
            }

        }

    /*    if(!login()){
            System.out.println(deviceData.ip + " prepare fail login fail");
            return false;
        }*/

        if(!findAnalyseTaskID()){
            System.out.println(deviceData.ip + " prepare fail findAnalyseTaskID fail");
            logout();
            return false;
        }

        if(!attachAnalyseTaskResult())
        {
            System.out.println(deviceData.ip + " prepare fail attachAnalyseTaskResult fail");
            logout();
            return  false;
        }
        deviceData.state = ClientStateTaskAttached;
        System.out.println(deviceData.ip + " prepare success");
        return  true;
    }

    public void finish(){
        if(deviceData.state == ClientStateInit) {
            return;
        }
        if(deviceData.state == ClientStateTaskAttached){
            detachAnalyseTaskResult();
            deviceData.state = ClientStateLogined;
        }
        if(deviceData.state == ClientStateLogined){
            logout();
            deviceData.state = ClientStateInit;
        }
        deviceData.init();
        System.out.println(deviceData.ip + " finish");
    }

    void freePointer(Pointer pointer){
        Native.free(Pointer.nativeValue(pointer)); //清理内存
        Pointer.nativeValue(pointer, 0);   //防止gc重复回收
    }

    Pointer allocMemory(long size){
        Pointer pointer = new Memory(size);
        pointer.clear(size);
        return pointer;
    }

    Pointer toMemoryPointer(Structure pJavaStu){
        Pointer pointer = allocMemory(pJavaStu.size());
        ToolKits.SetStructDataToPointer(pJavaStu,pointer,0);
        return pointer;
    }

    void initMultiLevelMemory(NET_OUT_GET_XRAY_MULTILEVEL_DETECT_INFO multilevelDetectInfo){
        NET_MULTI_LEVEL_INFO tempMultiLevelInfo = new NET_MULTI_LEVEL_INFO();
        multilevelDetectInfo.stuInfo.nMultiLevelConfigMax = 10;
        multilevelDetectInfo.stuInfo.pstuMultiLevelConfig =
                allocMemory(tempMultiLevelInfo.size()*multilevelDetectInfo.stuInfo.nMultiLevelConfigMax);
    }
    void freeMultiLevelMemory(NET_OUT_GET_XRAY_MULTILEVEL_DETECT_INFO multilevelDetectInfo){
        freePointer(multilevelDetectInfo.stuInfo.pstuMultiLevelConfig);
    }

    Boolean getXRayMultiLevelDetectCFG(NET_OUT_GET_XRAY_MULTILEVEL_DETECT_INFO multilevelDetectInfo){
        long beginTime = System.currentTimeMillis();
        Boolean ret = false;
        if(multilevelDetectInfo == null){
            System.out.println("getXRayMultiLevelDetectCFG ret " + ret + " param null ");
            return ret;
        }
        NET_IN_GET_XRAY_MULTILEVEL_DETECT_INFO input=new NET_IN_GET_XRAY_MULTILEVEL_DETECT_INFO();
        Pointer pointerInput = toMemoryPointer(input);
        Pointer pointerOutPut = toMemoryPointer(multilevelDetectInfo);
        NetSDKLib.LLong result = netSdk.CLIENT_GetXRayMultiLevelDetectCFG(deviceData.loginHandle, pointerInput, pointerOutPut, 3000);
        if(result.longValue()!=0){
            ret = true;
            ToolKits.GetPointerData(pointerOutPut,multilevelDetectInfo);
        }
        freePointer(pointerInput);
        freePointer(pointerOutPut);


        long endTime = System.currentTimeMillis();
        System.out.println("getXRayMultiLevelDetectCFG ret " + ret + " " + (!ret ? ("error "+  ToolKits.getErrorCode()) : "") + " use " + (endTime-beginTime));
        return ret;
    }

    Boolean  setXRayMultiLevelDetectCFG(NET_IN_SET_XRAY_MULTILEVEL_DETECT_INFO inMultiLevelInfo){
        long beginTime = System.currentTimeMillis();
        Boolean ret = false;
        if(inMultiLevelInfo == null){
            System.out.println("setXRayMultiLevelDetectCFG ret " + ret + " param null");
            return false;
        }
        Pointer pointerInput = toMemoryPointer(inMultiLevelInfo);
        Pointer pointerOutput = toMemoryPointer(new NET_OUT_SET_XRAY_MULTILEVEL_DETECT_INFO());
        //todo
        NetSDKLib.LLong result = netSdk.CLIENT_SetXRayMultiLevelDetectCFG(deviceData.loginHandle, pointerInput, pointerOutput, 3000);
        if(result.longValue() != 0){
            ret = true;
        }
        freePointer(pointerInput);
        freePointer(pointerOutput);
        long endTime = System.currentTimeMillis();
        System.out.println("setXRayMultiLevelDetectCFG ret " + ret + " " + (!ret ? ("error "+  ToolKits.getErrorCode()) : "") + " use " + (endTime-beginTime) );
        return ret;
    }

    public static class  ObjectAttribute{
        EM_INSIDE_OBJECT_TYPE type;
        int	detectThreshold;//阈值 0~100
        int bEnable;//开启为1  关闭为 0
    }

    public Boolean getObjectsAttribute(List<ObjectAttribute> list){
        long beginTime = System.currentTimeMillis();
        NET_OUT_GET_XRAY_MULTILEVEL_DETECT_INFO multilevelDetectInfo = new NET_OUT_GET_XRAY_MULTILEVEL_DETECT_INFO();
        initMultiLevelMemory(multilevelDetectInfo);
        if(!getXRayMultiLevelDetectCFG(multilevelDetectInfo)){
            System.out.println("getObjectsAttribute ret 0 getconfig fail");
            freeMultiLevelMemory(multilevelDetectInfo);
            return false;
        }
        list.clear();
        NET_MULTI_LEVEL_INFO levelConfig = new NET_MULTI_LEVEL_INFO();
        ToolKits.GetPointerDataToStruct(multilevelDetectInfo.stuInfo.pstuMultiLevelConfig
                , multilevelDetectInfo.stuInfo.emCurrentLevel * levelConfig.size(), levelConfig);
        List<String> types=new ArrayList<>();

        for(int i = 0; i< levelConfig.nObjectGroupsNum; i++)
        {
            NET_XRAY_OBJECT_GROUP group  = levelConfig.stuObjectGroups[i];
            ObjectAttribute attribute = new ObjectAttribute();

            attribute.type = EM_INSIDE_OBJECT_TYPE.getEnum(group.emGroupType);

            NET_XRAY_OBJECT_INFO stuObject = group.stuObjects[i];

            if(!types.contains( EM_INSIDE_OBJECT_TYPE.getNoteByValue(stuObject.emType))){
                types.add(EM_INSIDE_OBJECT_TYPE.getNoteByValue(stuObject.emType));
            }
            attribute.detectThreshold = group.stuObjects[0].nDetectThreshold;
            attribute.bEnable = group.stuObjects[0].bEnable;
            list.add(attribute);
        }

        freeMultiLevelMemory(multilevelDetectInfo);
        long endTime = System.currentTimeMillis();
        System.out.println("getObjectsAttribute ret "+true + " use " + (endTime-beginTime));
        return true;
    }

    public Boolean setObjectsAttribute(List<ObjectAttribute> list){
        if(list == null){
            System.out.println("setObjectsAttribute ret 0 list null");
            return false;
        }
        long beginTime = System.currentTimeMillis();
        NET_OUT_GET_XRAY_MULTILEVEL_DETECT_INFO multilevelDetectInfo = new NET_OUT_GET_XRAY_MULTILEVEL_DETECT_INFO();
        initMultiLevelMemory(multilevelDetectInfo);
        if(!getXRayMultiLevelDetectCFG(multilevelDetectInfo)){
            System.out.println("setObjectsAttribute ret 0 getConfig fail");
            freeMultiLevelMemory(multilevelDetectInfo);
            return false;
        }
        NET_XRAY_MULTILEVEL_DETECT_CFG_INFO	stuInfo = multilevelDetectInfo.stuInfo;
        stuInfo.nMultiLevelConfigMax = stuInfo.nMultiLevelConfigCnt;
        NET_MULTI_LEVEL_INFO levelConfig = new NET_MULTI_LEVEL_INFO();
        ToolKits.GetPointerDataToStruct(stuInfo.pstuMultiLevelConfig
                , stuInfo.emCurrentLevel * levelConfig.size(), levelConfig);

        //todo 修改阈值
        for(int k=0;k<list.size();k++) {
            ObjectAttribute attribute = list.get(k);
            for(int i = 0; i <levelConfig.nObjectGroupsNum ; i++){
                NET_XRAY_OBJECT_GROUP group = levelConfig.stuObjectGroups[i];
                if(group.emGroupType == attribute.type.getValue()){
                    for(int j = 0; j < group.nObjectsNum; j++){
                        group.stuObjects[j].nDetectThreshold = attribute.detectThreshold;
                        //修改使能（是否开启检测）
                        group.stuObjects[j].bEnable = attribute.bEnable;
                        group.bGroupEnable = attribute.bEnable;
                    }
                    break;
                }
            }
        }
        ToolKits.SetStructDataToPointer(levelConfig, stuInfo.pstuMultiLevelConfig, stuInfo.emCurrentLevel * levelConfig.size());
        NET_IN_SET_XRAY_MULTILEVEL_DETECT_INFO input = new NET_IN_SET_XRAY_MULTILEVEL_DETECT_INFO();
        input.stuInfo = stuInfo;
        Boolean ret = setXRayMultiLevelDetectCFG(input);
        freeMultiLevelMemory(multilevelDetectInfo);
        long endTime = System.currentTimeMillis();
        System.out.println("setObjectsAttribute ret " + ret + " use " + (endTime -beginTime));
        return  ret;
    }

    public Boolean pushPicture(PictureInfo pictureInfo) {
        if(pictureInfo == null){
            System.out.println("pushPicture fail pictureInfo null");
            return false;
        }
        deviceData.pictureInfo = pictureInfo;
        //入参
        NetSDKLib.NET_IN_PUSH_ANALYSE_PICTURE_FILE stuInParam = new NetSDKLib.NET_IN_PUSH_ANALYSE_PICTURE_FILE();
        stuInParam.nTaskID = deviceData.taskID;
        stuInParam.nPicNum = 1;    // 以两张图为例
        String pictureID = String.valueOf(pictureInfo.id);
        byte[] fileId = pictureID.getBytes();// 这个ID是图片的标识符，设备分析完毕返回的数据也以这个ID区分
        System.arraycopy(fileId, 0, stuInParam.stuPushPicInfos[0].szFileID, 0, fileId.length);

        stuInParam.stuPushPicInfos[0].nLength = pictureInfo.data.length;
        stuInParam.stuPushPicInfos[0].nOffset = 0; // 偏移量，有多张图时这里要特别注意
        int totalLen = stuInParam.stuPushPicInfos[0].nLength;  // 总的长度是各个图的累加
        stuInParam.nBinBufLen = totalLen;
        stuInParam.pBinBuf = allocMemory(totalLen); // 分配内存
        stuInParam.pBinBuf.write(stuInParam.stuPushPicInfos[0].nOffset, pictureInfo.data, 0, stuInParam.stuPushPicInfos[0].nLength);

        deviceData.pushTime = System.currentTimeMillis();
        deviceData.waitResult = true;
        // 出参
        NetSDKLib.NET_OUT_PUSH_ANALYSE_PICTURE_FILE stuOutParam = new NetSDKLib.NET_OUT_PUSH_ANALYSE_PICTURE_FILE();
        //根据机器的唯一标识码
        Boolean ret = netSdk.CLIENT_PushAnalysePictureFile(deviceData.loginHandle, stuInParam, stuOutParam, 0);
        if(!ret){
            deviceData.waitResult = false;
            deviceData.pushTime = 0;
        }
        freePointer(stuInParam.pBinBuf);
        System.out.println("pushPicture ret " + ret + " " + (!ret ? ("error "+  ToolKits.getErrorCode()) : "") );
        return ret;
    }

    public int invoke(NetSDKLib.LLong lAttachHandle, Pointer pstAnalyseTaskResult, Pointer pBuf, int dwBufSize,
                      Pointer dwUser) {
        NetSDKLib.NET_CB_ANALYSE_TASK_RESULT_INFO task = new NetSDKLib.NET_CB_ANALYSE_TASK_RESULT_INFO();
        ToolKits.GetPointerData(pstAnalyseTaskResult, task);
//        System.out.println("进入回调 lAttachHandle"+ lAttachHandle);
//        System.out.println("task.nTaskResultNum:"+task.nTaskResultNum);
        if(!deviceData.waitResult){
            System.out.println("invoke invalid response");
            return 0;
        }
        for (int i = 0; i < task.nTaskResultNum; i++) {
            for (int j = 0; j < task.stuTaskResultInfos[i].nEventCount; j++) {
                String fileID = new String(task.stuTaskResultInfos[i].szFileID).trim();
                String fileID2 = String.valueOf(deviceData.pictureInfo.id);
//                System.out.println(fileID + ","+ fileID2 + ","+ (fileID.equals(fileID2)));
                if(!fileID.equals(fileID2)){
                    continue;
                }
                int emEventType = task.stuTaskResultInfos[i].stuEventInfos[j].emEventType;
                switch (emEventType) {
                    case NetSDKLib.EM_ANALYSE_EVENT_TYPE.EM_ANALYSE_EVENT_XRAY_DETECTION: { //
                        //  X光机检测事件, 对应结构体 DEV_EVENT_XRAY_DETECTION_INFO
//                        System.out.println("X光机检测事件 ");
                        DEV_EVENT_XRAY_DETECTION_INFO detectInfo = new DEV_EVENT_XRAY_DETECTION_INFO();
                        ToolKits.GetPointerData(task.stuTaskResultInfos[i].stuEventInfos[j].pstEventInfo, detectInfo);
                        if(deviceData.callBack != null){
                            PictureResult result = new PictureResult();
                            result.sn = new String(deviceData.stDeviceInfo.sSerialNumber);
                            result.resultBuf = pBuf.getByteArray(0, dwBufSize);
                            result.resultBufLen = dwBufSize;
                            result.objectList = new ArrayList<>();
                            // 主视角包裹内物品信息
                            for(int n = 0; n < detectInfo.nObjectNum; n ++){
                                NET_INSIDE_OBJECT stuInsideObj = detectInfo.stuInsideObj[n];
                                result.objectList.add(stuInsideObj);
                            }
                            deviceData.callBack.onResult(deviceData.pictureInfo, result);
                            deviceData.pushTime = 0;
                            deviceData.waitResult = false;
                        }
                        break;
                    }
                    default:
                        System.out.println(" emEventType:"+emEventType);
                        break;
                }
            }
        }
        return 0;
    }

    public Boolean newPushPicture(PictureInfo pictureInfo, NetSDKLib.LLong lLong) {
        if(pictureInfo == null){
            log.info("pushPicture fail pictureInfo null");
            return false;
        }
        deviceData.pictureInfo = pictureInfo;
        //入参
        NetSDKLib.NET_IN_PUSH_ANALYSE_PICTURE_FILE stuInParam = new NetSDKLib.NET_IN_PUSH_ANALYSE_PICTURE_FILE();
        stuInParam.nTaskID = deviceData.taskID;
        stuInParam.nPicNum = 1;    // 以两张图为例
        String pictureID = String.valueOf(pictureInfo.id);
        byte[] fileId = pictureID.getBytes();// 这个ID是图片的标识符，设备分析完毕返回的数据也以这个ID区分
        System.arraycopy(fileId, 0, stuInParam.stuPushPicInfos[0].szFileID, 0, fileId.length);

        stuInParam.stuPushPicInfos[0].nLength = pictureInfo.data.length;
        stuInParam.stuPushPicInfos[0].nOffset = 0; // 偏移量，有多张图时这里要特别注意
        int totalLen = stuInParam.stuPushPicInfos[0].nLength;  // 总的长度是各个图的累加
        stuInParam.nBinBufLen = totalLen;
        stuInParam.pBinBuf = allocMemory(totalLen); // 分配内存
        stuInParam.pBinBuf.write(stuInParam.stuPushPicInfos[0].nOffset, pictureInfo.data, 0, stuInParam.stuPushPicInfos[0].nLength);

        deviceData.pushTime = System.currentTimeMillis();
        deviceData.waitResult = true;
        // 出参
        NetSDKLib.NET_OUT_PUSH_ANALYSE_PICTURE_FILE stuOutParam = new NetSDKLib.NET_OUT_PUSH_ANALYSE_PICTURE_FILE();
        //todo 根据机器的唯一标识码
        Boolean ret = netSdk.CLIENT_PushAnalysePictureFile(lLong, stuInParam, stuOutParam, 0);
        if(!ret){
            deviceData.waitResult = false;
            deviceData.pushTime = 0;
        }
        freePointer(stuInParam.pBinBuf);
        log.info("pushPicture ret " + ret + " " + (!ret ? ("error "+  ToolKits.getErrorCode()) : "") );
        log.info(lLong + "处理");
        return ret;
    }
}
