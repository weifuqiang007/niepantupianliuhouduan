package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.enumeration.EM_GLASS_STATE_TYPE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_FACE_ORIGINAL_SIZE;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Pointer;
import java.io.File;
import static com.niepan.modules.zhinenganjianback.lib.NetSDKLib.EVENT_IVS_FACERECOGNITION;


/**
 * @author 291189
 * @version 1.0
 * @description ERR220926212
 * @date 2022/9/28 10:38
 */
public class TargetRecognitionNewDemo extends Initialization {
    int channel=1;
    NetSDKLib.LLong    attachHandle=new NetSDKLib.LLong(0);
    /**
     * 订阅智能任务
     */

    public NetSDKLib.LLong AttachEventRealLoadPic() {
        // 先退订，设备不会对重复订阅作校验，重复订阅后会有重复的事件返回
        if(attachHandle.longValue()!=0){
            this.DetachEventRealLoadPic();
        }

        // 需要图片
        int bNeedPicture = 1;
        attachHandle = netSdk.CLIENT_RealLoadPictureEx(loginHandle, channel, NetSDKLib.EVENT_IVS_FACERECOGNITION , bNeedPicture,
             AnalyzerDataCB.getInstance(), null, null);
        if (attachHandle.longValue() != 0) {
            System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Success\n", channel);
        } else {
            System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Failed!LastError = %s\n", channel,
                    ToolKits.getErrorCode());
        }

        return attachHandle;
    }
    /**
     * 报警事件（智能）回调
     */
    private static class AnalyzerDataCB implements NetSDKLib.fAnalyzerDataCallBack {
        private final File picturePath;
        private static AnalyzerDataCB instance;

        @Override
        public void finalize() throws Throwable {
            super.finalize();
            System.out.println("AnalyzerDataCB 对象已经被回收");
        }


        private AnalyzerDataCB() {
            picturePath = new File("./AnalyzerPicture/");
            if (!picturePath.exists()) {
                picturePath.mkdirs();
            }
        }

        public static AnalyzerDataCB getInstance() {
            if (instance == null) {
                synchronized (AnalyzerDataCB.class) {
                    if (instance == null) {
                        instance = new AnalyzerDataCB();
                    }
                }
            }
            return instance;
        }

        @Override
        public int invoke(NetSDKLib.LLong lAnalyzerHandle, int dwAlarmType, Pointer pAlarmInfo, Pointer pBuffer, int dwBufSize,
                          Pointer dwUser, int nSequence, Pointer reserved) {
            if (lAnalyzerHandle == null || lAnalyzerHandle.longValue() == 0) {
                return -1;
            }

            switch (dwAlarmType) {
                case EVENT_IVS_FACERECOGNITION: {
                    System.out.println("目标识别事件");

                    NetSDKLib.DEV_EVENT_FACERECOGNITION_INFO  msg=new NetSDKLib.DEV_EVENT_FACERECOGNITION_INFO();


                    ToolKits.GetPointerDataToStruct(pAlarmInfo,0,msg);
                    NetSDKLib.NET_FACE_DATA stuFaceData = msg.stuFaceData;

                    int nAge = stuFaceData.nAge;

                    System.out.println("nAge:"+nAge);

                    int emMask = stuFaceData.emMask;
                    System.out.println("emMask:"+emMask);

                    int emBeard = stuFaceData.emBeard;
                    System.out.println("emBeard:"+emBeard);

                    int emSex = stuFaceData.emSex;
                    System.out.println("emSex:"+emSex);

                    /** 是否人体测温 */
                    int bAnatomyTempDetect = stuFaceData.bAnatomyTempDetect;
                    System.out.println("bAnatomyTempDetect:"+bAnatomyTempDetect);
                    /** 温度单位 {@link  NetSDKLib.EM_HUMAN_TEMPERATURE_UNIT}*/
                    int emTemperatureUnit = stuFaceData.emTemperatureUnit;
                    System.out.println("emTemperatureUnit:"+emTemperatureUnit);

                    double dbTemperature = stuFaceData.dbTemperature;
                    System.out.println("dbTemperature:"+dbTemperature);

                    /** 算法人脸分析时的实际人脸图片尺寸, 宽高为0时无效 */
                    NET_FACE_ORIGINAL_SIZE stuOriginalSize
                            = stuFaceData.stuOriginalSize;

                    System.out.println("stuOriginalSize.nWidth:"+stuOriginalSize.nWidth);
                    System.out.println("stuOriginalSize.nHeight:"+stuOriginalSize.nHeight);

                    /** 戴眼镜状态 {@link  EM_GLASS_STATE_TYPE} */
                    int emGlass = stuFaceData.emGlass;
                    System.out.println("emGlass:"+emGlass);
                    break;
                }
                default:
                    System.out.println("其他事件--------------------"+ dwAlarmType);
                    break;
            }
            return 0;
        }
    }


    /**
     * 停止侦听智能事件
     */
    public void DetachEventRealLoadPic() {
        if (this.attachHandle.longValue() != 0) {
            netSdk.CLIENT_StopLoadPic(this.attachHandle);
        }
    }
    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "AttachEventRealLoadPic" , "AttachEventRealLoadPic")));
        menu.addItem((new CaseMenu.Item(this , "DetachEventRealLoadPic" , "DetachEventRealLoadPic")));

        menu.run();
    }

    public static void main(String[] args) {
        TargetRecognitionNewDemo faceRecognitionNewDemo=new TargetRecognitionNewDemo();
        InitTest("172.12.97.97",37777,"admin","admin123");
        faceRecognitionNewDemo.RunTest();
        LoginOut();

    }
}
