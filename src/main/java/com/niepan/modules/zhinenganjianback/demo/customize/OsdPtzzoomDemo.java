package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultDisconnectCallback;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultHaveReconnectCallBack;
import com.niepan.modules.zhinenganjianback.lib.enumeration.ENUMERROR;
import com.niepan.modules.zhinenganjianback.lib.enumeration.NET_EM_CFG_OPERATE_TYPE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_COLOR_RGBA;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_OSD_PTZZOOM_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_RECT;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import java.io.File;
import java.text.SimpleDateFormat;

import static com.niepan.modules.zhinenganjianback.lib.Utils.getOsPrefix;

/**
 * @author 291189
 * @version 1.0
 * @description ERR230228101
 * @date 2023/3/1 9:44
 */
public class OsdPtzzoomDemo {

    // SDk对象初始化
    public static final NetSDKLib netsdk = NetSDKLib.NETSDK_INSTANCE;
    public static final NetSDKLib configsdk = NetSDKLib.CONFIG_INSTANCE;

    // 判断是否初始化
    private static boolean bInit = false;
    // 判断log是否打开
    private static boolean bLogOpen = false;
    // 设备信息
    private NetSDKLib.NET_DEVICEINFO_Ex deviceInfo = new NetSDKLib.NET_DEVICEINFO_Ex();
    // 登录句柄
    private NetSDKLib.LLong m_hLoginHandle = new NetSDKLib.LLong(0);


    // 回调函数需要是静态的，防止被系统回收
    // 断线回调
    private static NetSDKLib.fDisConnect disConnectCB = DefaultDisconnectCallback.getINSTANCE();
    // 重连回调
    private static NetSDKLib.fHaveReConnect haveReConnectCB = DefaultHaveReconnectCallBack.getINSTANCE();

    // 编码格式
    public static String encode;

    static {
        String osPrefix = getOsPrefix();
        if (osPrefix.toLowerCase().startsWith("win32-amd64")) {
            encode = "GBK";
        } else if (osPrefix.toLowerCase().startsWith("linux-amd64")) {
            encode = "UTF-8";
        }
    }

    /**
     * 获取当前时间
     */
    public static String GetDate() {
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDate.format(new java.util.Date()).replaceAll("[^0-9]", "-");
    }

    /**
     * 初始化SDK库
     */
    public static boolean Init() {
        bInit = netsdk.CLIENT_Init(disConnectCB, null);// 进程启动时，初始化一次
        if (!bInit) {
            System.out.println("Initialize SDK failed");
            return false;
        }
        // 配置日志
        OsdPtzzoomDemo.enableLog();

        // 设置断线重连回调接口, 此操作为可选操作，但建议用户进行设置
        netsdk.CLIENT_SetAutoReconnect(haveReConnectCB, null);

        // 设置登录超时时间和尝试次数，可选
        // 登录请求响应超时时间设置为3S
        int waitTime = 3000;
        // 登录时尝试建立链接 1 次
        int tryTimes = 1;
        netsdk.CLIENT_SetConnectTime(waitTime, tryTimes);
        // 设置更多网络参数， NET_PARAM 的nWaittime ， nConnectTryNum 成员与 CLIENT_SetConnectTime
        // 接口设置的登录设备超时时间和尝试次数意义相同,可选
        NetSDKLib.NET_PARAM netParam = new NetSDKLib.NET_PARAM();
        // 登录时尝试建立链接的超时时间
        netParam.nConnectTime = 10000;
        // 设置子连接的超时时间
        netParam.nGetConnInfoTime = 3000;
        netsdk.CLIENT_SetNetworkParam(netParam);
        return true;
    }

    /**
     * 打开 sdk log
     */
    private static void enableLog() {
        NetSDKLib.LOG_SET_PRINT_INFO setLog = new NetSDKLib.LOG_SET_PRINT_INFO();
        File path = new File("sdklog/");
        if (!path.exists())
            path.mkdir();

        // 这里的log保存地址依据实际情况自己调整
        String logPath = path.getAbsoluteFile().getParent() + "\\sdklog\\" + "sdklog" + GetDate() + ".log";
        setLog.nPrintStrategy = 0;
        setLog.bSetFilePath = 1;
        System.arraycopy(logPath.getBytes(), 0, setLog.szLogFilePath, 0, logPath.getBytes().length);
        System.out.println(logPath);
        setLog.bSetPrintStrategy = 1;
        bLogOpen = netsdk.CLIENT_LogOpen(setLog);
        if (!bLogOpen)
            System.err.println("Failed to open NetSDK log");
    }

    /**
     * 高安全登录
     */
    public void loginWithHighLevel() {
        // 输入结构体参数
        NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY pstlnParam = new NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY() {
            {
                szIP = m_strIpAddr.getBytes();
                nPort = m_nPort;
                szUserName = m_strUser.getBytes();
                szPassword = m_strPassword.getBytes();
            }
        };
        // 输出结构体参数
        NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY pstOutParam = new NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY();

        // 写入sdk
        m_hLoginHandle = netsdk.CLIENT_LoginWithHighLevelSecurity(pstlnParam, pstOutParam);
        if (m_hLoginHandle.longValue() == 0) {
            System.err.printf("Login Device[%s] Port[%d]Failed. %s\n", m_strIpAddr, m_nPort,
                    netsdk.CLIENT_GetLastError());
        } else {
            deviceInfo = pstOutParam.stuDeviceInfo; // 获取设备信息
            System.out.println("Login Success");
            System.out.println("Device Address：" + m_strIpAddr);
            System.out.println("设备包含：" + deviceInfo.byChanNum + "个通道");
        }
    }

    /**
     * 退出
     */
    public void logOut() {
        if (m_hLoginHandle.longValue() != 0) {
            netsdk.CLIENT_Logout(m_hLoginHandle);
            System.out.println("LogOut Success");
        }
    }

    /**
     * 清理sdk环境并退出
     */
    public static void cleanAndExit() {
        if (bLogOpen) {
            netsdk.CLIENT_LogClose(); // 关闭sdk日志打印
        }
        netsdk.CLIENT_Cleanup(); // 进程关闭时，调用一次
        System.exit(0);
    }


    /**
     * 选择通道
     */
    private int channelId = 0;// 逻辑通道

    NET_OSD_PTZZOOM_INFO config = new NET_OSD_PTZZOOM_INFO();

// 获取变倍叠加配置
    public void  getOsdPtzzoom(){


        Pointer pointer = new Memory(config.size());
        pointer.clear(config.size());
        ToolKits.SetStructDataToPointer(config, pointer, 0);
        boolean result = netsdk.CLIENT_GetConfig(m_hLoginHandle, NET_EM_CFG_OPERATE_TYPE.NET_EM_CFG_OSD_PTZZOOM, channelId,
                pointer, config.size(), 5000, null);
        if (!result) {
            System.out.println("获取 变倍叠加配置 配置失败:" + ENUMERROR.getErrorMessage());
            Native.free(Pointer.nativeValue(pointer)); //清理内存
            Pointer.nativeValue(pointer, 0);  //防止gc重复回收
        } else {

            System.out.println("获取 变倍叠加配置 配置成功");

            ToolKits.GetPointerData(pointer, config);

            Native.free(Pointer.nativeValue(pointer)); //清理内存
            Pointer.nativeValue(pointer, 0);  //防止gc重复回收

            /**
             是否叠加到主码流 ,1 是 ，0 否
             */
            int bMainBlend = config.bMainBlend;

            System.out.println("bMainBlend:"+bMainBlend);

            /**
             是否叠加到预览码流
             */
            int bPreviewBlend=  config.bPreviewBlend;

            System.out.println("bPreviewBlend:"+bPreviewBlend);

            /**
             前景色
             */
            NET_COLOR_RGBA stuFrontColor
                    = config.stuFrontColor;
            System.out.println("stuFrontColor:"+stuFrontColor.toString());

            /**
             背景色
             */
            NET_COLOR_RGBA					stuBackColor=config.stuBackColor;

            System.out.println("stuBackColor:"+stuBackColor.toString());


            /**
             区域, 坐标取值0~8191, 仅使用left和top值, 点(left,top)应和(right,bottom)设置成同样的点
             */
             NET_RECT stuRect=config.stuRect;
            System.out.println("stuRect:"+stuRect.toString());

            /**
             显示时间，单位为妙，0表示一直显示
             */
         	int				nDisplayTime=config.nDisplayTime;
            System.out.println("nDisplayTime:"+nDisplayTime);
        }


    }

    // 设置变倍叠加配置
    public void  setOsdPtzzoom(){

        /**
         是否叠加到主码流
         */
        config.bMainBlend=1;
        /**
         是否叠加到预览码流
         */
        config.bPreviewBlend=1;

        /**
         显示时间，单位为妙，0表示一直显示
         */
        config.nDisplayTime=0;

        /**
         区域, 坐标取值0~8191, 仅使用left和top值, 点(left,top)应和(right,bottom)设置成同样的点
         */
        NET_RECT stuRect
                = config.stuRect;

        stuRect.nLeft=200;

        stuRect.nTop=2000;


        Pointer pointer = new Memory(config.size());
        pointer.clear(config.size());
        ToolKits.SetStructDataToPointer(config, pointer, 0);
        int nChannelID = 0;// 通道号
        boolean result = netsdk.CLIENT_SetConfig(m_hLoginHandle, NET_EM_CFG_OPERATE_TYPE.NET_EM_CFG_OSD_PTZZOOM, nChannelID,
                pointer, config.size(),5000, new IntByReference(0), null);
        if (!result) {
            System.err.println("设置变倍叠加配置失败:" + ENUMERROR.getErrorMessage());
        } else {
            System.out.println("设置变倍叠加配置成功");
        }
        Native.free(Pointer.nativeValue(pointer)); //清理内存
        Pointer.nativeValue(pointer, 0);  //防止gc重复回收
    }

    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "获取变倍叠加配置" , "getOsdPtzzoom")));
        menu.addItem((new CaseMenu.Item(this , "设置变倍叠加配置" , "setOsdPtzzoom")));
        menu.run();
    }
    // 配置登陆地址，端口，用户名，密码
    private String m_strIpAddr = "172.32.5.38";
    private int m_nPort = 37777;
    private String m_strUser = "admin";
    private String m_strPassword = "admin123";
    public static void main(String[] args) {
        OsdPtzzoomDemo demo=new OsdPtzzoomDemo();
        demo.InitTest();
        demo.RunTest();
        demo.EndTest();

    }

    /**
     * 初始化测试
     */
    public void InitTest() {
        OsdPtzzoomDemo.Init();
        this.loginWithHighLevel();
    }

    /**
     * 结束测试
     */
    public void EndTest() {
        System.out.println("End Test");
        this.logOut(); // 登出设备
        System.out.println("See You...");
        OsdPtzzoomDemo.cleanAndExit(); // 清理资源并退出
    }

}
