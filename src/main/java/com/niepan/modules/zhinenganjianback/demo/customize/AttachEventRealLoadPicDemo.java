package com.niepan.modules.zhinenganjianback.demo.customize;

import static com.niepan.modules.zhinenganjianback.lib.Utils.getOsPrefix;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Objects;
import java.util.Scanner;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.LLong;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.NET_IN_ATTACH_VIDEOSTAT_SUM;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.NET_OUT_ATTACH_VIDEOSTAT_SUM;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.NET_VIDEOSTAT_SUMMARY;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultDisconnectCallback;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultHaveReconnectCallBack;
import com.niepan.modules.zhinenganjianback.lib.enumeration.EM_EVENT_IVS_TYPE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_DEV_EVENT_GARBAGE_PLASTICBAG_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_DEV_EVENT_ILLEGAL_CARRIAGE_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_POINT_EX;
import com.sun.jna.Pointer;

public class AttachEventRealLoadPicDemo {
	// SDk对象初始化
	public static final NetSDKLib netsdk = NetSDKLib.NETSDK_INSTANCE;
	public static final NetSDKLib configsdk = NetSDKLib.CONFIG_INSTANCE;

	// 判断是否初始化
	private static boolean bInit = false;
	// 判断log是否打开
	private static boolean bLogOpen = false;
	// 设备信息
	private NetSDKLib.NET_DEVICEINFO_Ex deviceInfo = new NetSDKLib.NET_DEVICEINFO_Ex();
	// 登录句柄
	private NetSDKLib.LLong m_hLoginHandle = new NetSDKLib.LLong(0);
	
	// 智能事件订阅句柄
	private NetSDKLib.LLong m_attachHandle = new NetSDKLib.LLong(0);

	// 回调函数需要是静态的，防止被系统回收
	// 断线回调
	private static NetSDKLib.fDisConnect disConnectCB = DefaultDisconnectCallback.getINSTANCE();
	// 重连回调
	private static NetSDKLib.fHaveReConnect haveReConnectCB = DefaultHaveReconnectCallBack.getINSTANCE();

	// 编码格式
	public static String encode;

	static {
		String osPrefix = getOsPrefix();
		if (osPrefix.toLowerCase().startsWith("win32-amd64")) {
			encode = "GBK";
		} else if (osPrefix.toLowerCase().startsWith("linux-amd64")) {
			encode = "UTF-8";
		}
	}

	/**
	 * 获取当前时间
	 */
	public static String GetDate() {
		SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return simpleDate.format(new java.util.Date()).replaceAll("[^0-9]", "-");
	}

	/**
	 * 初始化SDK库
	 */
	public static boolean Init() {
		bInit = netsdk.CLIENT_Init(disConnectCB, null);// 进程启动时，初始化一次
		if (!bInit) {
			System.out.println("Initialize SDK failed");
			return false;
		}
		// 配置日志
		AttachEventRealLoadPicDemo.enableLog();

		// 设置断线重连回调接口, 此操作为可选操作，但建议用户进行设置
		netsdk.CLIENT_SetAutoReconnect(haveReConnectCB, null);

		// 设置登录超时时间和尝试次数，可选
		// 登录请求响应超时时间设置为3S
		int waitTime = 3000;
		// 登录时尝试建立链接 1 次
		int tryTimes = 1;
		netsdk.CLIENT_SetConnectTime(waitTime, tryTimes);
		// 设置更多网络参数， NET_PARAM 的nWaittime ， nConnectTryNum 成员与 CLIENT_SetConnectTime
		// 接口设置的登录设备超时时间和尝试次数意义相同,可选
		NetSDKLib.NET_PARAM netParam = new NetSDKLib.NET_PARAM();
		// 登录时尝试建立链接的超时时间
		netParam.nConnectTime = 10000;
		// 设置子连接的超时时间
		netParam.nGetConnInfoTime = 3000;
		netsdk.CLIENT_SetNetworkParam(netParam);
		return true;
	}

	/**
	 * 打开 sdk log
	 */
	private static void enableLog() {
		NetSDKLib.LOG_SET_PRINT_INFO setLog = new NetSDKLib.LOG_SET_PRINT_INFO();
		File path = new File("sdklog/");
		if (!path.exists())
			path.mkdir();

		// 这里的log保存地址依据实际情况自己调整
		String logPath = path.getAbsoluteFile().getParent() + "\\sdklog\\" + "sdklog" + GetDate() + ".log";
		setLog.nPrintStrategy = 0;
		setLog.bSetFilePath = 1;
		System.arraycopy(logPath.getBytes(), 0, setLog.szLogFilePath, 0, logPath.getBytes().length);
		System.out.println(logPath);
		setLog.bSetPrintStrategy = 1;
		bLogOpen = netsdk.CLIENT_LogOpen(setLog);
		if (!bLogOpen)
			System.err.println("Failed to open NetSDK log");
	}

	/**
	 * 高安全登录
	 */
	public void loginWithHighLevel() {
		// 输入结构体参数
		NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY pstlnParam = new NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY() {
			{
				szIP = m_strIpAddr.getBytes();
				nPort = m_nPort;
				szUserName = m_strUser.getBytes();
				szPassword = m_strPassword.getBytes();
			}
		};
		// 输出结构体参数
		NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY pstOutParam = new NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY();

		// 写入sdk
		m_hLoginHandle = netsdk.CLIENT_LoginWithHighLevelSecurity(pstlnParam, pstOutParam);
		if (m_hLoginHandle.longValue() == 0) {
			System.err.printf("Login Device[%s] Port[%d]Failed. %s\n", m_strIpAddr, m_nPort,
					netsdk.CLIENT_GetLastError());
		} else {
			deviceInfo = pstOutParam.stuDeviceInfo; // 获取设备信息
			System.out.println("Login Success");
			System.out.println("Device Address：" + m_strIpAddr);
			System.out.println("设备包含：" + deviceInfo.byChanNum + "个通道");
		}
	}

	/**
	 * 退出
	 */
	public void logOut() {
		if (m_hLoginHandle.longValue() != 0) {
			netsdk.CLIENT_Logout(m_hLoginHandle);
			System.out.println("LogOut Success");
		}
	}

	/**
	 * 清理sdk环境并退出
	 */
	public static void cleanAndExit() {
		if (bLogOpen) {
			netsdk.CLIENT_LogClose(); // 关闭sdk日志打印
		}
		netsdk.CLIENT_Cleanup(); // 进程关闭时，调用一次
		System.exit(0);
	}

	/**
	 * 选择通道
	 */
	private int channelId = -1;// 逻辑通道

	public void setChannelID() {
		System.out.println("请输入通道，从0开始计数，-1表示全部");
		Scanner sc = new Scanner(System.in);
		this.channelId = sc.nextInt();
	}

	/**
	 * 订阅智能任务
	 */
	public void AttachEventRealLoadPic() {
		// 先退订，设备不会对重复订阅作校验，重复订阅后会有重复的事件返回
		this.DetachEventRealLoadPic();
		// 需要图片
		int bNeedPicture = 1;
		m_attachHandle = netsdk.CLIENT_RealLoadPictureEx(m_hLoginHandle, channelId, NetSDKLib.EVENT_IVS_ALL, bNeedPicture,
				AnalyzerDataCB.getInstance(), null, null);
		if (m_attachHandle.longValue() != 0) {
			System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Success\n", channelId);
		} else {
			System.out.printf("Ch[%d] CLIENT_RealLoadPictureEx Failed!LastError = %s\n", channelId,
					ToolKits.getErrorCode());
		}
	}

	/**
	 * 报警事件（智能）回调
	 */
	private static class AnalyzerDataCB implements NetSDKLib.fAnalyzerDataCallBack {
		private final File picturePath;
		private static AnalyzerDataCB instance;

		private AnalyzerDataCB() {
			picturePath = new File("./AnalyzerPicture/");
			if (!picturePath.exists()) {
				picturePath.mkdirs();
			}
		}

		public static AnalyzerDataCB getInstance() {
			if (instance == null) {
				synchronized (AnalyzerDataCB.class) {
					if (instance == null) {
						instance = new AnalyzerDataCB();
					}
				}
			}
			return instance;
		}

		public int invoke(LLong m_attachHandle, int dwAlarmType, Pointer pAlarmInfo, Pointer pBuffer, int dwBufSize,
				Pointer dwUser, int nSequence, Pointer reserved) {
			/**
			 * 回调函数使用注意（防止卡回调）
			 * 1.不能再回调进行耗时操作，建议将数据、图片流、视频流等通过中间件和多线程抛出处理
			 * 2.不能再回调调用netsdk的其他接口
			 */	  	
			if (m_attachHandle == null || m_attachHandle.longValue() == 0) {
				return -1;
			}

			switch (Objects.requireNonNull(EM_EVENT_IVS_TYPE.getEventType(dwAlarmType))) {			
			case EVENT_IVS_ELEVATOR_ABNORMAL: {// 电动扶梯运行异常事件 (对应DEV_EVENT_ELEVATOR_ABNORMAL_INFO)
				NetSDKLib.DEV_EVENT_ELEVATOR_ABNORMAL_INFO msg = new NetSDKLib.DEV_EVENT_ELEVATOR_ABNORMAL_INFO();
                ToolKits.GetPointerData(pAlarmInfo, msg);
                String Picture = picturePath + "\\" +"EVENT_IVS_ELEVATOR_ABNORMAL_"+ System.currentTimeMillis() + ".jpg";
                ToolKits.savePicture(pBuffer, 0, dwBufSize, Picture);
                System.out.println(" 电动扶梯运行异常事件(UTC)：" + msg.UTC + " 通道号:" + msg.nChannelID +" 枚举值"+msg.emClassType); 
                // 规则框坐标
                System.out.println(" 扶梯检测区顶点数:"+msg.nDetectRegionPointNum);
                NetSDKLib.NET_POINT[] stuDetectRegion = msg.stuDetectRegion;               
                for (int i = 0; i < msg.nDetectRegionPointNum; i++) {
					//  扶梯检测区多边形类型，多边形中每个顶点的坐标归一化到[0,8191]区间
                	System.out.println("nx: "+stuDetectRegion[i].nx+";"+" ny:"+stuDetectRegion[i].ny);                	
				}                               
                break;
			}
			case  EVENT_IVS_TUMBLE_DETECTION: {// 倒地报警事件(对应 DEV_EVENT_TUMBLE_DETECTION_INFO)
				NetSDKLib.DEV_EVENT_TUMBLE_DETECTION_INFO msg = new NetSDKLib.DEV_EVENT_TUMBLE_DETECTION_INFO();
                ToolKits.GetPointerData(pAlarmInfo, msg);
                String Picture = picturePath + "\\" +"EVENT_IVS_TUMBLE_DETECTION_"+ System.currentTimeMillis() + ".jpg";
                ToolKits.savePicture(pBuffer, 0, dwBufSize, Picture);
                System.out.println(" 倒地报警事件(UTC)：" + msg.UTC + " 通道号:" + msg.nChannelID +" 枚举值："+msg.emClassType);	
                // emClassType 8是人员倒地    65是扶梯上人员异常
                // 规则框坐标
                System.out.println(" 检测区个数:"+msg.nDetectRegionNum);
                NET_POINT_EX[] stuDetectRegion = msg.stuDetectRegion;               
                for (int i = 0; i < msg.nDetectRegionNum; i++) {
					// 检测区二维空间坐标点
                	System.out.println("nx: "+stuDetectRegion[i].nx+";"+" ny:"+stuDetectRegion[i].ny);                	
				}
                // 目标框坐标 
                System.out.println(" 物体包围盒top left bottom right:"+msg.stuBoundingBox.top+","+msg.stuBoundingBox.left+
						","+msg.stuBoundingBox.bottom+","+msg.stuBoundingBox.right);                
                break;
			}
			case  EVENT_IVS_FIGHTDETECTION: {// 斗殴事件(对应 DEV_EVENT_FIGHT_INFO)
				NetSDKLib.DEV_EVENT_FIGHT_INFO msg = new NetSDKLib.DEV_EVENT_FIGHT_INFO();
                ToolKits.GetPointerData(pAlarmInfo, msg);
                String Picture = picturePath + "\\" +"EVENT_IVS_FIGHTDETECTION_"+ System.currentTimeMillis() + ".jpg";
                ToolKits.savePicture(pBuffer, 0, dwBufSize, Picture);
                System.out.println(" 斗殴事件(UTC)：" + msg.UTC + " 通道号:" + msg.nChannelID );	
                // 规则框坐标
                System.out.println(" 规则检测区域顶点数:"+msg.nDetectRegionNum);
                NetSDKLib.NET_POINT[] DetectRegion = msg.DetectRegion;               
                for (int i = 0; i < msg.nDetectRegionNum; i++) {
                	// 规则检测区域
                	System.out.println("nx: "+DetectRegion[i].nx+";"+" ny:"+DetectRegion[i].ny);                	
				}
                // 目标框坐标 
                System.out.println(" 检测到的物体个数:"+msg.nObjectNum);
                NetSDKLib.NET_MSG_OBJECT[] stuObjectIDs= msg.stuObjectIDs;
                for (int i = 0; i < msg.nObjectNum; i++) {
                	System.out.println(" 物体包围盒top left bottom right:"+ stuObjectIDs[i].BoundingBox.top+","+stuObjectIDs[i].BoundingBox.left+
    						","+stuObjectIDs[i].BoundingBox.bottom+","+stuObjectIDs[i].BoundingBox.right);
				}                               
                break;
			}
			case  EVENT_IVS_CROWDDETECTION: {// 人群密度检测事件(对应结构体 DEV_EVENT_CROWD_DETECTION_INFO)
				NetSDKLib.DEV_EVENT_CROWD_DETECTION_INFO msg = new NetSDKLib.DEV_EVENT_CROWD_DETECTION_INFO();
                ToolKits.GetPointerData(pAlarmInfo, msg);
                String Picture = picturePath + "\\" +"EVENT_IVS_CROWDDETECTION_"+ System.currentTimeMillis() + ".jpg";
                ToolKits.savePicture(pBuffer, 0, dwBufSize, Picture);
                System.out.println(" 人群密度检测事件(UTC)：" + msg.UTC + " 通道号:" + msg.nChannelID );
                // 全局密度超限报警规则框坐标
                System.out.println(" 返回的全局拥挤人群密度列表个数 (矩形描述)：" + msg.nCrowdRectListNum);
                NetSDKLib.NET_CROWD_RECT_LIST_INFO[] stuCrowdRectList = msg.stuCrowdRectList;
                for (int i = 0; i < msg.nCrowdRectListNum; i++) {
                	// 矩形的左上角点与右下角点,8192坐标系，表示矩形的人群密度矩形框
                	NetSDKLib.NET_POINT[] stuRectPoint = stuCrowdRectList[i].stuRectPoint;
                	for (int j = 0; j < stuRectPoint.length; j++) {
                		System.out.println("nx: "+stuRectPoint[j].nx+";"+" ny:"+stuRectPoint[j].ny);  
					}               	    
				}
                
                // 局部人数超限报警规则框坐标 
                System.out.println(" 返回的人数超限的报警区域ID列表个数：" + msg.nRegionListNum);
                NetSDKLib.NET_REGION_LIST_INFO[] stuRegionList=  msg.stuRegionList;
                for (int i = 0; i < msg.nRegionListNum; i++) {
                	System.out.println(" 配置的区域下标:"+stuRegionList[i].nRegionID+" --------------");
                	System.out.println(" 配置的检测区域坐标个数:"+stuRegionList[i].nDetectRegionNum);                	
                	NetSDKLib.DH_POINT[] stuDetectRegion = stuRegionList[i].stuDetectRegion;
                	for (int j = 0; j < stuRegionList[i].nDetectRegionNum; j++) {
                    	// 配置的检测区域坐标
                    	System.out.println("nx: "+stuDetectRegion[j].nx+";"+" ny:"+stuDetectRegion[j].ny); 
					}
				}                                
                break;
			}
			case  EVENT_IVS_ILLEGAL_CARRIAGE: {// 非法运输事件(对应 NET_DEV_EVENT_ILLEGAL_CARRIAGE_INFO)
				NET_DEV_EVENT_ILLEGAL_CARRIAGE_INFO msg = new NET_DEV_EVENT_ILLEGAL_CARRIAGE_INFO();
                ToolKits.GetPointerData(pAlarmInfo, msg);
                String Picture = picturePath + "\\" +"EVENT_IVS_ILLEGAL_CARRIAGE_"+ System.currentTimeMillis() + ".jpg";
                ToolKits.savePicture(pBuffer, 0, dwBufSize, Picture);
                System.out.println(" 非法运输事件：" +  " 通道号:" + msg.nChannelID );	
                System.out.println(" 检测到的车牌信息个数(配合stuObjects使用):" + msg.nObjectsNum );	
                NetSDKLib.NET_MSG_OBJECT_EX2[] stuObjects = msg.stuObjects;
                for (int i = 0; i < msg.nObjectsNum; i++) {
					System.out.println("---------第"+(i+1)+"个车牌信息------");
					try {
						System.out.println(" 物体类型:"+new String(stuObjects[i].szObjectType,encode).trim()+"车牌信息:"+new String(stuObjects[i].szText,encode).trim());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
              // 规则框坐标
                System.out.println(" 检测区个数:"+msg.nDetectRegionNum);
                NET_POINT_EX[] stuDetectRegion = msg.stuDetectRegion;               
                for (int i = 0; i < msg.nDetectRegionNum; i++) {
					// 检测区二维空间坐标点
                	System.out.println("nx: "+stuDetectRegion[i].nx+";"+" ny:"+stuDetectRegion[i].ny);                	
				}     
                // 目标框坐标 
                System.out.println(" 检测到的车辆信息个数(配合stuVehicles使用):" + msg.nVehiclesNum );	
                NetSDKLib.NET_MSG_OBJECT_EX2[] stuVehicles = msg.stuVehicles;
                for (int i = 0; i < msg.nVehiclesNum; i++) {
                	// 目标框信息字段
					System.out.println("---------第"+(i+1)+"一个车辆信息------");
					System.out.println(" 包围盒top left bottom right:"+stuVehicles[i].BoundingBox.top+","+stuVehicles[i].BoundingBox.left+
							","+stuVehicles[i].BoundingBox.bottom+","+stuVehicles[i].BoundingBox.right);
				}
                break;
			}
			case  EVENT_IVS_GARBAGE_EXPOSURE: {// 垃圾暴露检测事件 (对应 DEV_EVENT_GARBAGE_EXPOSURE_INFO)
				NetSDKLib.DEV_EVENT_GARBAGE_EXPOSURE_INFO msg = new NetSDKLib.DEV_EVENT_GARBAGE_EXPOSURE_INFO();
                ToolKits.GetPointerData(pAlarmInfo, msg);
                String Picture = picturePath + "\\" +"EVENT_IVS_GARBAGE_EXPOSURE_"+ System.currentTimeMillis() + ".jpg";
                ToolKits.savePicture(pBuffer, 0, dwBufSize, Picture);
                System.out.println(" 垃圾暴露检测事件(UTC)：" + msg.UTC + " 通道号:" + msg.nChannelID);	
                // 规则框坐标
                System.out.println(" 规则检测区域顶点数:"+msg.nDetectRegionNum);
                NetSDKLib.NET_POINT[] DetectRegion = msg.DetectRegion;               
                for (int i = 0; i < msg.nDetectRegionNum; i++) {
                	// 规则检测区域
                	System.out.println("nx: "+DetectRegion[i].nx+";"+" ny:"+DetectRegion[i].ny);                	
				}
                // 目标框坐标 
                System.out.println(" 检测到的物体个数:"+msg.nObjectNum);
                NetSDKLib.NET_MSG_OBJECT[] stuObjects= msg.stuObjects;
                for (int i = 0; i < msg.nObjectNum; i++) {
                	System.out.println(" 物体包围盒top left bottom right:"+ stuObjects[i].BoundingBox.top+","+stuObjects[i].BoundingBox.left+
    						","+stuObjects[i].BoundingBox.bottom+","+stuObjects[i].BoundingBox.right);
				}    
                break;
			}
			case  EVENT_IVS_GARBAGE_PLASTICBAG: {// 打包垃圾检测事件(对应 NET_DEV_EVENT_GARBAGE_PLASTICBAG_INFO)
				NET_DEV_EVENT_GARBAGE_PLASTICBAG_INFO msg = new NET_DEV_EVENT_GARBAGE_PLASTICBAG_INFO();
                ToolKits.GetPointerData(pAlarmInfo, msg);
                String Picture = picturePath + "\\" +"EVENT_IVS_GARBAGE_PLASTICBAG_"+ System.currentTimeMillis() + ".jpg";
                ToolKits.savePicture(pBuffer, 0, dwBufSize, Picture);
                System.out.println(" 打包垃圾检测事件"  + " 通道号:" + msg.nChannelID +"stuUTC:"+msg.stuUTC);
                // 规则框坐标
                System.out.println(" 规则检测区域顶点数:"+msg.nDetectRegionCount);
                NET_POINT_EX[] stuDetectRegion = msg.stuDetectRegion;               
                for (int i = 0; i < msg.nDetectRegionCount; i++) {
                	// 规则检测区域
                	System.out.println("nx: "+stuDetectRegion[i].nx+";"+" ny:"+stuDetectRegion[i].ny);                	
				}
                // 目标框坐标 
                System.out.println(" 检测到的物体个数:"+msg.nObjectCount);
                NetSDKLib.DH_MSG_OBJECT[] stuObjects= msg.stuObjects;
                for (int i = 0; i < msg.nObjectCount; i++) {
                	System.out.println(" 物体包围盒top left bottom right:"+ stuObjects[i].BoundingBox.top+","+stuObjects[i].BoundingBox.left+
    						","+stuObjects[i].BoundingBox.bottom+","+stuObjects[i].BoundingBox.right);
				}    
                break;
			}
			case EVENT_IVS_MAN_NUM_DETECTION : {// 立体视觉区域内人数统计事件(对应 DEV_EVENT_MANNUM_DETECTION_INFO)
				NetSDKLib.DEV_EVENT_MANNUM_DETECTION_INFO msg = new NetSDKLib.DEV_EVENT_MANNUM_DETECTION_INFO();
                ToolKits.GetPointerData(pAlarmInfo, msg);
                String Picture = picturePath + "\\" +"EVENT_IVS_MAN_NUM_DETECTION_"+ System.currentTimeMillis() + ".jpg";
                ToolKits.savePicture(pBuffer, 0, dwBufSize, Picture);
                System.out.println(" 立体视觉区域内人数统计事件 时间(UTC)：" + msg.UTC + " 通道号:" + msg.nChannelID +
                        " 区域人员列表数量:" + msg.nManListCount + " 人员身高:" + msg.stuManList[0].nStature+" 变化前人数:"+
                		msg.nPrevNumber+" 当前人数:"+msg.nCurrentNumber+" 事件关联ID:"+new String(msg.szSourceID).trim() +" 规则名称:"+new String(msg.szRuleName).trim());
                // 规则框坐标
                System.out.println(" 检测区个数:"+msg.nDetectRegionNum);
                NET_POINT_EX[] stuDetectRegion = msg.stuDetectRegion;               
                for (int i = 0; i < msg.nDetectRegionNum; i++) {
                	// 规则检测区域
                	System.out.println("nx: "+stuDetectRegion[i].nx+";"+" ny:"+stuDetectRegion[i].ny);                	
				}
                // 目标框坐标 
                System.out.println(" 区域人员列表数量:"+msg.nManListCount);
                NetSDKLib.MAN_NUM_LIST_INFO[] stuManList= msg.stuManList;
                for (int i = 0; i < msg.nManListCount; i++) {
                	System.out.println(" 人员包围盒,8192坐标系top left bottom right:"+ stuManList[i].stuBoudingBox.top+","+stuManList[i].stuBoudingBox.left+
    						","+stuManList[i].stuBoudingBox.bottom+","+stuManList[i].stuBoudingBox.right);
				}                                 
                break;
			}
			default:
				//System.out.println("其他事件--------------------"+ dwAlarmType);
				break;
			}			
			return 0;
		}		
	}

	/**
	 * 停止侦听智能事件
	 */
	public void DetachEventRealLoadPic() {
		if (m_attachHandle.longValue() != 0) {
			netsdk.CLIENT_StopLoadPic(m_attachHandle);
		}
	}
	
	
	
	
	/** 订阅视频统计 句柄 */
	private LLong videoStatHandle = new LLong(0);

	/**
	 * 订阅
	 */
	public void attachVideoStatSummary() {
		if (m_hLoginHandle.longValue() == 0) {
			return;
		}

		NET_IN_ATTACH_VIDEOSTAT_SUM videoStatIn = new NET_IN_ATTACH_VIDEOSTAT_SUM();
		videoStatIn.nChannel = 7;// 通道号
		videoStatIn.cbVideoStatSum = VideoStatSumCallback.getInstance();

		NET_OUT_ATTACH_VIDEOSTAT_SUM videoStatOut = new NET_OUT_ATTACH_VIDEOSTAT_SUM();

		videoStatHandle = netsdk.CLIENT_AttachVideoStatSummary(m_hLoginHandle, videoStatIn, videoStatOut, 5000);
		if (videoStatHandle.longValue() == 0) {
			System.err.printf("Attach Failed!LastError = %x\n", netsdk.CLIENT_GetLastError());
			return;
		}

		System.out.printf("Attach Success!Wait Device Notify Information\n");
	}

	/**
	 * 退订
	 */
	public void detachVideoStatSummary() {
		if (videoStatHandle.longValue() != 0) {
			netsdk.CLIENT_DetachVideoStatSummary(videoStatHandle);
			videoStatHandle.setValue(0);
		}
	}

	private static class VideoStatSumCallback implements NetSDKLib.fVideoStatSumCallBack {
		private static VideoStatSumCallback instance = new VideoStatSumCallback();

		private VideoStatSumCallback() {
		}

		public static VideoStatSumCallback getInstance() {
			return instance;
		}

		public void invoke(LLong lAttachHandle, NET_VIDEOSTAT_SUMMARY stVideoState, int dwBufLen, Pointer dwUser) {
			/**
			 * 回调函数使用注意（防止卡回调）
			 * 1.不能再回调进行耗时操作，建议将数据、图片流、视频流等通过中间件和多线程抛出处理
			 * 2.不能再回调调用netsdk的其他接口
			 */	  			  		
	  		System.out.printf("Channel[%d] GetTime[%s]\n" +
	  				"People In  Information[Total[%d] Hour[%d] Today[%d]]\n" +
	  				"People Out Information[Total[%d] Hour[%d] Today[%d]]\n", 
	  				stVideoState.nChannelID , stVideoState.stuTime.toStringTime() , 
	  				stVideoState.stuEnteredSubtotal.nToday , 
	  				stVideoState.stuEnteredSubtotal.nHour , 
	  				stVideoState.stuEnteredSubtotal.nTotal , 
	  				stVideoState.stuExitedSubtotal.nToday , 
	  				stVideoState.stuExitedSubtotal.nHour , 
	  				stVideoState.stuExitedSubtotal.nTotal );
	  		
	  		try {	  			 											
				System.out.println("规则类型名称:"+new String(stVideoState.szRuleName,encode)); 
				System.out.println("区域ID:"+stVideoState.nAreaID);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
	}

	/******************************** 测试控制台 ***************************************/

	// 配置登陆地址，端口，用户名，密码
	private String m_strIpAddr = "172.25.31.27";
	private int m_nPort = 37777;
	private String m_strUser = "admin";
	private String m_strPassword = "Admin123";

	public static void main(String[] args) {
		AttachEventRealLoadPicDemo demo = new AttachEventRealLoadPicDemo();
		demo.InitTest();
		demo.RunTest();
		demo.EndTest();

	}

	/**
	 * 初始化测试
	 */
	public void InitTest() {
		AttachEventRealLoadPicDemo.Init();
		this.loginWithHighLevel();
	}

	/**
	 * 加载测试内容
	 */
	public void RunTest() {
		CaseMenu menu = new CaseMenu();
		
		// 智能事件订阅上报
		menu.addItem(new CaseMenu.Item(this, "选择通道", "setChannelID"));
		menu.addItem(new CaseMenu.Item(this, "订阅智能事件", "AttachEventRealLoadPic"));
		menu.addItem(new CaseMenu.Item(this, "停止侦听智能事件", "DetachEventRealLoadPic"));
		
		// 支持视频统计摘要信息
		menu.addItem(new CaseMenu.Item(this, "订阅视频统计摘要信息", "attachVideoStatSummary"));
		menu.addItem(new CaseMenu.Item(this, "取消订阅视频统计摘要信息", "detachVideoStatSummary"));
		
		menu.run();
	}

	/**
	 * 结束测试
	 */
	public void EndTest() {
		System.out.println("End Test");
		this.logOut(); // 登出设备
		System.out.println("See You...");
		AttachEventRealLoadPicDemo.cleanAndExit(); // 清理资源并退出
	}
	/******************************** 结束 ***************************************/
}
