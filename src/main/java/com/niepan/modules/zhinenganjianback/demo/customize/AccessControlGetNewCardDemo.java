package com.niepan.modules.zhinenganjianback.demo.customize;

import static com.niepan.modules.zhinenganjianback.lib.NetSDKLib.NET_ALARM_ACCESS_CTL_EVENT;
import static com.niepan.modules.zhinenganjianback.lib.Utils.getOsPrefix;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Objects;
import java.util.Scanner;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.DEV_EVENT_CITIZEN_PICTURE_COMPARE_INFO;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib.LLong;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultDisconnectCallback;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultHaveReconnectCallBack;
import com.niepan.modules.zhinenganjianback.lib.enumeration.EM_EVENT_IVS_TYPE;
import com.niepan.modules.zhinenganjianback.lib.enumeration.EM_GATHER_TYPE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_IN_ACCESSCONTROL_CAPTURE_CMD;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_IN_ACCESSCONTROL_CAPTURE_NEWCARD;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_IN_PERSON_INFO_INPUT_RESULT;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_OUT_ACCESSCONTROL_CAPTURE_CMD;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_OUT_ACCESSCONTROL_CAPTURE_NEWCARD;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_OUT_PERSON_INFO_INPUT_RESULT;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;

public class AccessControlGetNewCardDemo {
	// SDk对象初始化
	public static final NetSDKLib netsdk = NetSDKLib.NETSDK_INSTANCE;
	public static final NetSDKLib configsdk = NetSDKLib.CONFIG_INSTANCE;

	// 判断是否初始化
	private static boolean bInit = false;
	// 判断log是否打开
	private static boolean bLogOpen = false;
	// 设备信息
	private NetSDKLib.NET_DEVICEINFO_Ex deviceInfo = new NetSDKLib.NET_DEVICEINFO_Ex();
	// 登录句柄
	private NetSDKLib.LLong m_hLoginHandle = new NetSDKLib.LLong(0);

	// 智能事件订阅句柄
	private NetSDKLib.LLong m_attachHandle = new NetSDKLib.LLong(0);

	// 回调函数需要是静态的，防止被系统回收
	// 断线回调
	private static NetSDKLib.fDisConnect disConnectCB = DefaultDisconnectCallback.getINSTANCE();
	// 重连回调
	private static NetSDKLib.fHaveReConnect haveReConnectCB = DefaultHaveReconnectCallBack.getINSTANCE();

	// 编码格式
	public static String encode;

	static {
		String osPrefix = getOsPrefix();
		if (osPrefix.toLowerCase().startsWith("win32-amd64")) {
			encode = "GBK";
		} else if (osPrefix.toLowerCase().startsWith("linux-amd64")) {
			encode = "UTF-8";
		}
	}

	/**
	 * 获取当前时间
	 */
	public static String GetDate() {
		SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return simpleDate.format(new java.util.Date()).replaceAll("[^0-9]", "-");
	}

	/**
	 * 初始化SDK库
	 */
	public static boolean Init() {
		bInit = netsdk.CLIENT_Init(disConnectCB, null);// 进程启动时，初始化一次
		if (!bInit) {
			System.out.println("Initialize SDK failed");
			return false;
		}
		// 配置日志
		AccessControlGetNewCardDemo.enableLog();

		// 设置断线重连回调接口, 此操作为可选操作，但建议用户进行设置
		netsdk.CLIENT_SetAutoReconnect(haveReConnectCB, null);

		// 设置登录超时时间和尝试次数，可选
		// 登录请求响应超时时间设置为3S
		int waitTime = 3000;
		// 登录时尝试建立链接 1 次
		int tryTimes = 1;
		netsdk.CLIENT_SetConnectTime(waitTime, tryTimes);
		// 设置更多网络参数， NET_PARAM 的nWaittime ， nConnectTryNum 成员与 CLIENT_SetConnectTime
		// 接口设置的登录设备超时时间和尝试次数意义相同,可选
		NetSDKLib.NET_PARAM netParam = new NetSDKLib.NET_PARAM();
		// 登录时尝试建立链接的超时时间
		netParam.nConnectTime = 10000;
		// 设置子连接的超时时间
		netParam.nGetConnInfoTime = 3000;
		netsdk.CLIENT_SetNetworkParam(netParam);
		return true;
	}

	/**
	 * 打开 sdk log
	 */
	private static void enableLog() {
		NetSDKLib.LOG_SET_PRINT_INFO setLog = new NetSDKLib.LOG_SET_PRINT_INFO();
		File path = new File("sdklog/");
		if (!path.exists())
			path.mkdir();

		// 这里的log保存地址依据实际情况自己调整
		String logPath = path.getAbsoluteFile().getParent() + "\\sdklog\\" + "sdklog" + GetDate() + ".log";
		setLog.nPrintStrategy = 0;
		setLog.bSetFilePath = 1;
		System.arraycopy(logPath.getBytes(), 0, setLog.szLogFilePath, 0, logPath.getBytes().length);
		System.out.println(logPath);
		setLog.bSetPrintStrategy = 1;
		bLogOpen = netsdk.CLIENT_LogOpen(setLog);
		if (!bLogOpen)
			System.err.println("Failed to open NetSDK log");
	}

	/**
	 * 高安全登录
	 */
	public void loginWithHighLevel() {
		// 输入结构体参数
		NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY pstlnParam = new NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY() {
			{
				szIP = m_strIpAddr.getBytes();
				nPort = m_nPort;
				szUserName = m_strUser.getBytes();
				szPassword = m_strPassword.getBytes();
			}
		};
		// 输出结构体参数
		NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY pstOutParam = new NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY();

		// 写入sdk
		m_hLoginHandle = netsdk.CLIENT_LoginWithHighLevelSecurity(pstlnParam, pstOutParam);
		if (m_hLoginHandle.longValue() == 0) {
			System.err.printf("Login Device[%s] Port[%d]Failed. %s\n", m_strIpAddr, m_nPort,
					netsdk.CLIENT_GetLastError());
		} else {
			deviceInfo = pstOutParam.stuDeviceInfo; // 获取设备信息
			System.out.println("Login Success");
			System.out.println("Device Address：" + m_strIpAddr);
			System.out.println("设备包含：" + deviceInfo.byChanNum + "个通道");
		}
	}

	/**
	 * 退出
	 */
	public void logOut() {
		if (m_hLoginHandle.longValue() != 0) {
			netsdk.CLIENT_Logout(m_hLoginHandle);
			System.out.println("LogOut Success");
		}
	}

	/**
	 * 清理sdk环境并退出
	 */
	public static void cleanAndExit() {
		if (bLogOpen) {
			netsdk.CLIENT_LogClose(); // 关闭sdk日志打印
		}
		netsdk.CLIENT_Cleanup(); // 进程关闭时，调用一次
		System.exit(0);
	}

	/**
	 * 订阅报警信息
	 * 
	 */
	public void startListen() {
		// 设置报警回调函数设置一次即可
		netsdk.CLIENT_SetDVRMessCallBack(fAlarmAccessDataCB.getInstance(), null);

		// 订阅报警
		boolean bRet = netsdk.CLIENT_StartListenEx(m_hLoginHandle);
		if (!bRet) {
			System.err.println("订阅报警失败! LastError = 0x%x\n" + netsdk.CLIENT_GetLastError());
		} else {
			System.out.println("订阅报警成功.");
		}
	}

	/**
	 * 报警事件回调
	 */
	private static class fAlarmAccessDataCB implements NetSDKLib.fMessCallBack {
		private static fAlarmAccessDataCB instance = new fAlarmAccessDataCB();

		private fAlarmAccessDataCB() {
		}

		public static fAlarmAccessDataCB getInstance() {
			return instance;
		}

		public boolean invoke(int lCommand, LLong lLoginID, Pointer pStuEvent, int dwBufLen, String strDeviceIP,
				NativeLong nDevicePort, Pointer dwUser) {
			switch (lCommand) {
			case NET_ALARM_ACCESS_CTL_EVENT: { /// -> 门禁事件
				System.out.println("门禁事件------------");
				NetSDKLib.ALARM_ACCESS_CTL_EVENT_INFO msg = new NetSDKLib.ALARM_ACCESS_CTL_EVENT_INFO();
				ToolKits.GetPointerData(pStuEvent, msg);

				/////////////// 事件信息 ///////////////
				try {	
					System.out.println("卡类型: "+msg.emCardType);
					System.out.println("卡号: "+new String(msg.szCardNo, encode));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				break;
			}
			default:
				System.out.println("其他报警事件--------------------" + lCommand);
				break;
			}
			return true;
		}
	}

	/**
	 * 取消订阅报警信息
	 * 
	 */
	public void stopListen() {
		// 停止订阅报警
		boolean bRet = netsdk.CLIENT_StopListen(m_hLoginHandle);
		if (bRet) {
			System.out.println("取消订阅报警信息.");
		}
	}

	/**
	 * 选择通道
	 */
	private int channelId = -1;// 逻辑通道

	public void setChannelID() {
		System.out.println("请输入通道，从0开始计数，-1表示全部");
		Scanner sc = new Scanner(System.in);
		this.channelId = sc.nextInt();
	}

	/**
	 * 订阅智能任务
	 */
	public void AttachEventRealLoadPic() {
		// 先退订，设备不会对重复订阅作校验，重复订阅后会有重复的事件返回
		this.DetachEventRealLoadPic();
		// 需要图片
		int bNeedPicture = 1;
		m_attachHandle = netsdk.CLIENT_RealLoadPictureEx(m_hLoginHandle, channelId, NetSDKLib.EVENT_IVS_ALL,
				bNeedPicture, AnalyzerDataCB.getInstance(), null, null);
		if (m_attachHandle.longValue() != 0) {
			System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Success\n", channelId);
		} else {
			System.out.printf("Ch[%d] CLIENT_RealLoadPictureEx Failed!LastError = %s\n", channelId,
					ToolKits.getErrorCode());
		}
	}

	/**
	 * 报警事件（智能）回调
	 */
	private static class AnalyzerDataCB implements NetSDKLib.fAnalyzerDataCallBack {
		private final File picturePath;
		private static AnalyzerDataCB instance;

		private AnalyzerDataCB() {
			picturePath = new File("./AnalyzerPicture/");
			if (!picturePath.exists()) {
				picturePath.mkdirs();
			}
		}

		public static AnalyzerDataCB getInstance() {
			if (instance == null) {
				synchronized (AnalyzerDataCB.class) {
					if (instance == null) {
						instance = new AnalyzerDataCB();
					}
				}
			}
			return instance;
		}

		public int invoke(LLong m_attachHandle, int dwAlarmType, Pointer pAlarmInfo, Pointer pBuffer, int dwBufSize,
				Pointer dwUser, int nSequence, Pointer reserved) {
			if (m_attachHandle == null || m_attachHandle.longValue() == 0) {
				return -1;
			}

			switch (Objects.requireNonNull(EM_EVENT_IVS_TYPE.getEventType(dwAlarmType))) {
			case EVENT_IVS_CITIZEN_PICTURE_COMPARE: {// 人证比对事件: (对应 DEV_EVENT_CITIZEN_PICTURE_COMPARE_INFO)
				DEV_EVENT_CITIZEN_PICTURE_COMPARE_INFO msg = new DEV_EVENT_CITIZEN_PICTURE_COMPARE_INFO();
				ToolKits.GetPointerData(pAlarmInfo, msg);
				try {
					System.out.println("通道号 :" + msg.nChannelID);
					System.out.println("事件名称 :" + new String(msg.szName).trim());
					System.out.println("比对结果:" + msg.bCompareResult);

				} catch (Exception e) {
					e.printStackTrace();
				}
				// 拍摄照片
				String strFileName = picturePath + "\\" + System.currentTimeMillis() + "citizen_shoot.jpg";
				ToolKits.savePicture(pBuffer, msg.stuImageInfo[0].dwOffSet, msg.stuImageInfo[0].dwFileLenth,
						strFileName);
				break;
			}
			default:
				System.out.println("其他智能事件--------------------" + dwAlarmType);
				break;
			}
			return 0;
		}
	}

	/**
	 * 停止侦听智能事件
	 */
	public void DetachEventRealLoadPic() {
		if (m_attachHandle.longValue() != 0) {
			netsdk.CLIENT_StopLoadPic(m_attachHandle);
		}
	}

	/**
	 * 门禁设备开始捕获新卡
	 */
	public void accessControlCaptureNewCard() {
		NET_IN_ACCESSCONTROL_CAPTURE_NEWCARD pstInParam = new NET_IN_ACCESSCONTROL_CAPTURE_NEWCARD();
		// 授权时间,单位秒,0表示由服务端控制时间
		pstInParam.nAllowTime = 30;
		pstInParam.write();

		NET_OUT_ACCESSCONTROL_CAPTURE_NEWCARD pstOutParam = new NET_OUT_ACCESSCONTROL_CAPTURE_NEWCARD();
		pstOutParam.write();

		boolean bRet = netsdk.CLIENT_AccessControlCaptureNewCard(m_hLoginHandle, pstInParam.getPointer(),
				pstOutParam.getPointer(), 3000);
		if (!bRet) {
			System.err.printf("CLIENT_AccessControlCaptureNewCard Fail.Last Error[0x%x]\n",
					netsdk.CLIENT_GetLastError());
			return;
		} else {
			System.out.printf("CLIENT_AccessControlCaptureNewCard Success");
		}
	}

	/**
	 * 门禁人证设备获取人脸、证件
	 */
	public void accessControlCaptureCmd() {
		NET_IN_ACCESSCONTROL_CAPTURE_CMD pstInParam = new NET_IN_ACCESSCONTROL_CAPTURE_CMD();
		// 采集类型,{@link com.niepan.modules.zhinenganjianback.lib.enumeration.EM_GATHER_TYPE}
		//pstInParam.emGathertype = EM_GATHER_TYPE.EM_GATHER_TYPE_FACE.getValue(); // 1, "人脸采集" 2, "证件采集"
		pstInParam.emGathertype = EM_GATHER_TYPE.EM_GATHER_TYPE_IDCARD.getValue(); // 1, "人脸采集" 2, "证件采集"
		pstInParam.write();

		NET_OUT_ACCESSCONTROL_CAPTURE_CMD pstOutParam = new NET_OUT_ACCESSCONTROL_CAPTURE_CMD();
		pstOutParam.write();

		boolean bRet = netsdk.CLIENT_AccessControlCaptureCmd(m_hLoginHandle, pstInParam.getPointer(),
				pstOutParam.getPointer(), 3000);
		if (!bRet) {
			System.err.printf("CLIENT_AccessControlCaptureCmd Fail.Last Error[0x%x]\n", netsdk.CLIENT_GetLastError());
			return;
		} else {
			System.out.printf("CLIENT_AccessControlCaptureCmd Success");
		}
	}

	/**
	 * 下发人员信息录入结果
	 */
	public void setPersonInfoInputResult() {

		NET_IN_PERSON_INFO_INPUT_RESULT input = new NET_IN_PERSON_INFO_INPUT_RESULT();
		input.nChannelID = 0;// 通道ID
		input.nResult = 0;// 0 : 成功 1 :人员信息不在人员库中
		input.write();

		NET_OUT_PERSON_INFO_INPUT_RESULT out = new NET_OUT_PERSON_INFO_INPUT_RESULT();
		out.write();

		boolean isSuccess = netsdk.CLIENT_SetPersonInfoInputResult(m_hLoginHandle, input.getPointer(), out.getPointer(),
				3000);

		if (isSuccess) {
			System.out.println("CLIENT_SetPersonInfoInputResult success");
		} else {
			System.err.println("CLIENT_SetPersonInfoInputResult fail " + ToolKits.getErrorCode());
		}

	}

	/******************************** 测试控制台 ***************************************/

	// 配置登陆地址，端口，用户名，密码
	private String m_strIpAddr = "172.5.98.148";
	private int m_nPort = 37777;
	private String m_strUser = "admin";
	private String m_strPassword = "admin123";

	public static void main(String[] args) {
		AccessControlGetNewCardDemo demo = new AccessControlGetNewCardDemo();
		demo.InitTest();
		demo.RunTest();
		demo.EndTest();

	}

	/**
	 * 初始化测试
	 */
	public void InitTest() {
		AccessControlGetNewCardDemo.Init();
		this.loginWithHighLevel();
	}

	/**
	 * 加载测试内容
	 */
	public void RunTest() {
		CaseMenu menu = new CaseMenu();
		// 智能事件订阅上报
		menu.addItem(new CaseMenu.Item(this, "选择通道", "setChannelID"));
		menu.addItem(new CaseMenu.Item(this, "订阅智能事件", "AttachEventRealLoadPic"));
		menu.addItem(new CaseMenu.Item(this, "停止侦听智能事件", "DetachEventRealLoadPic"));
        // 报警事件上报
		menu.addItem(new CaseMenu.Item(this, "订阅报警信息", "startListen"));
		menu.addItem(new CaseMenu.Item(this, "取消订阅报警信息", "stopListen"));

		// 门禁设备开始捕获新卡
		// 备注：下发后采集信息通过门禁事件上报
		menu.addItem(new CaseMenu.Item(this, "门禁设备开始捕获新卡", "accessControlCaptureNewCard"));
		// 门禁人证设备获取人脸
		// 备注：下发后采集信息通过人证比对事件上报
		menu.addItem(new CaseMenu.Item(this, "门禁人证设备获取人脸", "accessControlCaptureCmd"));
		// 下发人员信息录入结果
		// 备注：收到设备上报的数据的时候，立刻下发该结果，不然设备会报失败（为防止卡回调，请不要直接在回调函数调用该接口，另起线程或者触发式调用）
		menu.addItem(new CaseMenu.Item(this, "下发人员信息录入结果", "setPersonInfoInputResult"));
		menu.run();
	}

	/**
	 * 结束测试
	 */
	public void EndTest() {
		System.out.println("End Test");
		this.logOut(); // 登出设备
		System.out.println("See You...");
		AccessControlGetNewCardDemo.cleanAndExit(); // 清理资源并退出
	}
	/******************************** 结束 ***************************************/
}
