package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.BYTE_32;
import com.niepan.modules.zhinenganjianback.lib.structure.CFG_DETECT_REGION;
import com.niepan.modules.zhinenganjianback.lib.structure.CFG_MOTION_INFO;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;

import java.io.UnsupportedEncodingException;

/**
 * @author 291189
 * @version 1.0
 * @description
 * @date 2022/9/3 11:03
 */
public class MotionDetectDemo extends Initialization {



    public void SetVideoAnalyseModule() {
        int channel = 0;
        String command = NetSDKLib.CFG_CMD_MOTIONDETECT;

        CFG_MOTION_INFO msg = new CFG_MOTION_INFO();

        // 获取
        if (ToolKits.GetDevConfig(loginHandle, channel, command, msg)) {
            System.out.println("bEnable:"+msg.bEnable);
            int nRegionCount = msg.nRegionCount;

            CFG_DETECT_REGION[] stuRegion = msg.stuRegion;

            for(int i=0;i<nRegionCount;i++){
                CFG_DETECT_REGION region = stuRegion[i];

                System.out.println("nRegionID:"+region.nRegionID);
                try {
                    System.out.println("szRegionName:"+ new String(region.szRegionName,encode) );
                    System.out.println("nThreshold:"+region.nThreshold);
                    System.out.println("nSenseLevel:"+region.nSenseLevel);
                    System.out.println("nMotionRow:"+region.nMotionRow);

                    System.out.println("nMotionCol:"+region.nMotionCol);
                    BYTE_32[] byRegion
                            = region.byRegion;

                    for(int n=0;n<region.nMotionRow;n++){//动态检测区域的行数

                        for(int m=0;m<region.nMotionCol;m++){//动态检测区域的列数

                            System.out.println("byRegion:["+n+"]["+m+"]"+byRegion[n].SN_32[m]);
                        }

                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
            stuRegion[0].byRegion[0].SN_32[0]=0;
            stuRegion[0].byRegion[0].SN_32[1]=0;

            msg.bEnable = 1;
                //nRegionCount和stuRegion
            if (ToolKits.SetDevConfig(loginHandle, channel, command, msg)) {
                System.out.println("设置使能成功!");
            } else {
                System.err.println("设置使能失败!" + ToolKits.getErrorCode());
            }
        }
    }

    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();
        menu.addItem((new CaseMenu.Item(this , "动态检测报警配置" , "SetVideoAnalyseModule")));
        menu.run();
    }

    public static void main(String[] args) {
        MotionDetectDemo motionDetectDemo=new MotionDetectDemo();
        InitTest("172.8.3.17",37777,"admin","admin123");
        motionDetectDemo.RunTest();
        LoginOut();
    }
}
