package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.CFG_RTSP_INFO_IN;
import com.niepan.modules.zhinenganjianback.lib.structure.CFG_RTSP_INFO_OUT;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import java.io.File;
/**
 * @author 291189
 * @version 1.0
 * @description ERR221214079
 * @date 2022/12/15 17:01
 */
public class CfgRtspDemo {

    public static final NetSDKLib netSdk = NetSDKLib.NETSDK_INSTANCE;

    // 登陆句柄
    private NetSDKLib.LLong loginHandle = new NetSDKLib.LLong(0);

    // 登陆句柄
    private NetSDKLib.LLong lFindID = new NetSDKLib.LLong(0);

    // 设备信息扩展
    private NetSDKLib.NET_DEVICEINFO_Ex deviceInfo = new NetSDKLib.NET_DEVICEINFO_Ex();


    public void InitTest(){
        // 初始化SDK库
        netSdk.CLIENT_Init(DisConnectCallBack.getInstance(), null);

        // 设置断线重连成功回调函数
        netSdk.CLIENT_SetAutoReconnect(HaveReConnectCallBack.getInstance(), null);

        //打开日志，可选
        NetSDKLib.LOG_SET_PRINT_INFO setLog = new NetSDKLib.LOG_SET_PRINT_INFO();
        String logPath = new File(".").getAbsoluteFile().getParent() + File.separator + "sdk_log" + File.separator + "sdk.log";
        setLog.bSetFilePath = 1;
        System.arraycopy(logPath.getBytes(), 0, setLog.szLogFilePath, 0, logPath.getBytes().length);
        setLog.bSetPrintStrategy = 1;
        setLog.nPrintStrategy = 0;
        if (!netSdk.CLIENT_LogOpen(setLog)){
            System.err.println("Open SDK Log Failed!!!");
        }

        Login();
    }

    public void Login(){
        // 登陆设备
        int nSpecCap = NetSDKLib.EM_LOGIN_SPAC_CAP_TYPE.EM_LOGIN_SPEC_CAP_TCP;    // TCP登入
        IntByReference nError = new IntByReference(0);
        loginHandle = netSdk.CLIENT_LoginEx2(m_strIp, m_nPort, m_strUser,
                m_strPassword ,nSpecCap, null, deviceInfo, nError);
        if (loginHandle.longValue() != 0) {
            System.out.printf("Login Device[%s] Success!\n", m_strIp);
        }
        else {
            System.err.printf("Login Device[%s] Fail.Error[%s]\n", m_strIp, ToolKits.getErrorCode());
            LoginOut();
        }
    }


    public void LoginOut(){
        System.out.println("End Test");
        if( loginHandle.longValue() != 0)
        {
            netSdk.CLIENT_Logout(loginHandle);
        }
        System.out.println("See You...");

        netSdk.CLIENT_Cleanup();
        System.exit(0);
    }
    /**
     * 设备断线回调
     */
    private static class DisConnectCallBack implements NetSDKLib.fDisConnect {

        private DisConnectCallBack() {
        }

        private static class CallBackHolder {
            private static DisConnectCallBack instance = new DisConnectCallBack();
        }

        public static DisConnectCallBack getInstance() {
            return DisConnectCallBack.CallBackHolder.instance;
        }

        public void invoke(NetSDKLib.LLong lLoginID, String pchDVRIP, int nDVRPort, Pointer dwUser) {
            System.out.printf("Device[%s] Port[%d] DisConnect!\n", pchDVRIP, nDVRPort);
        }
    }

    /**
     * 设备重连回调
     */
    private static class HaveReConnectCallBack implements NetSDKLib.fHaveReConnect {
        private HaveReConnectCallBack() {
        }

        private static class CallBackHolder {
            private static HaveReConnectCallBack instance = new HaveReConnectCallBack();
        }

        public static HaveReConnectCallBack getInstance() {
            return HaveReConnectCallBack.CallBackHolder.instance;
        }

        public void invoke(NetSDKLib.LLong m_hLoginHandle, String pchDVRIP, int nDVRPort, Pointer dwUser) {
            System.out.printf("ReConnect Device[%s] Port[%d]\n", pchDVRIP, nDVRPort);

        }
    }


    /**
     * RTSP的配置
     */
    public void cfgRtsp() {

        int chanl=0;

        CFG_RTSP_INFO_OUT outPut=new CFG_RTSP_INFO_OUT();

        boolean b
                = ToolKits.GetDevConfig(loginHandle, chanl, NetSDKLib.CFG_CMD_RTSP, outPut);
        if (!b) {
            System.err.println("CFG_CMD_RTSP GetDevConfig Failed." + ToolKits.getErrorCode());
            return;
        }else {
            System.out.println("CFG_CMD_RTSP  GetDevConfig success");
        }
        System.out.println("outPut:"+outPut.toString());
        CFG_RTSP_INFO_IN input=new CFG_RTSP_INFO_IN();

        input.bEnable=1;

        input.bHttpEnable=outPut.bHttpEnable;

        input.nHttpPort=outPut.nHttpPort;

        input.nPort=outPut.nPort;

        input.nRtpEndPort=100;

        input.nRtpStartPort=10000;

        System.out.println("input:"+input);
        b = ToolKits.SetDevConfig(loginHandle, chanl, NetSDKLib.CFG_CMD_RTSP, input);
        if (!b) {
            System.err.println("CFG_CMD_RTSP SetDevConfig Failed." + ToolKits.getErrorCode());
            return;
        }else {
            System.out.println("CFG_CMD_RTSP SetDevConfig success");
        }

    }
    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "RTSP的配置" , "cfgRtsp")));

        menu.run();
    }
    ////////////////////////////////////////////////////////////////
    private String m_strIp 				    = "172.8.2.253";
    private int    m_nPort 				    = 37777;
    private String m_strUser 			    = "admin";
    private String m_strPassword 		    = "admin123";
    ////////////////////////////////////////////////////////////////
    public static void main(String[] args) {
        CfgRtspDemo demo=new CfgRtspDemo();
        demo.InitTest();
        demo.RunTest();
        demo.LoginOut();
    }
}
