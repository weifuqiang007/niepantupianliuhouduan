package com.niepan.modules.zhinenganjianback.demo.customize;

import static com.niepan.modules.zhinenganjianback.lib.Utils.getOsPrefix;

import java.io.File;
import java.text.SimpleDateFormat;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultDisconnectCallback;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultHaveReconnectCallBack;
import com.niepan.modules.zhinenganjianback.lib.enumeration.NET_EM_CFG_OPERATE_TYPE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_CFG_DISABLE_LINKAGE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_CFG_DISABLE_LINKAGE_TIME_SECTION;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_CFG_REMOTE_DISABLE_LINKAGE_TIME_SECTION_SYNC_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.TIME_SECTION_6;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

public class RemoteDisableLinkageSyncDemo {
	// SDk对象初始化
	public static final NetSDKLib netsdk = NetSDKLib.NETSDK_INSTANCE;
	public static final NetSDKLib configsdk = NetSDKLib.CONFIG_INSTANCE;

	// 判断是否初始化
	private static boolean bInit = false;
	// 判断log是否打开
	private static boolean bLogOpen = false;
	// 设备信息
	private NetSDKLib.NET_DEVICEINFO_Ex deviceInfo = new NetSDKLib.NET_DEVICEINFO_Ex();
	// 登录句柄
	private NetSDKLib.LLong m_hLoginHandle = new NetSDKLib.LLong(0);

	// 回调函数需要是静态的，防止被系统回收
	// 断线回调
	private static NetSDKLib.fDisConnect disConnectCB = DefaultDisconnectCallback.getINSTANCE();
	// 重连回调
	private static NetSDKLib.fHaveReConnect haveReConnectCB = DefaultHaveReconnectCallBack.getINSTANCE();

	// 编码格式
	public static String encode;

	static {
		String osPrefix = getOsPrefix();
		if (osPrefix.toLowerCase().startsWith("win32-amd64")) {
			encode = "GBK";
		} else if (osPrefix.toLowerCase().startsWith("linux-amd64")) {
			encode = "UTF-8";
		}
	}

	/**
	 * 获取当前时间
	 */
	public static String GetDate() {
		SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return simpleDate.format(new java.util.Date()).replaceAll("[^0-9]", "-");
	}

	/**
	 * 初始化SDK库
	 */
	public static boolean Init() {
		bInit = netsdk.CLIENT_Init(disConnectCB, null);// 进程启动时，初始化一次
		if (!bInit) {
			System.out.println("Initialize SDK failed");
			return false;
		}
		// 配置日志
		RemoteDisableLinkageSyncDemo.enableLog();

		// 设置断线重连回调接口, 此操作为可选操作，但建议用户进行设置
		netsdk.CLIENT_SetAutoReconnect(haveReConnectCB, null);

		// 设置登录超时时间和尝试次数，可选
		// 登录请求响应超时时间设置为3S
		int waitTime = 3000;
		// 登录时尝试建立链接 1 次
		int tryTimes = 1;
		netsdk.CLIENT_SetConnectTime(waitTime, tryTimes);
		// 设置更多网络参数， NET_PARAM 的nWaittime ， nConnectTryNum 成员与 CLIENT_SetConnectTime
		// 接口设置的登录设备超时时间和尝试次数意义相同,可选
		NetSDKLib.NET_PARAM netParam = new NetSDKLib.NET_PARAM();
		// 登录时尝试建立链接的超时时间
		netParam.nConnectTime = 10000;
		// 设置子连接的超时时间
		netParam.nGetConnInfoTime = 3000;
		netsdk.CLIENT_SetNetworkParam(netParam);
		return true;
	}

	/**
	 * 打开 sdk log
	 */
	private static void enableLog() {
		NetSDKLib.LOG_SET_PRINT_INFO setLog = new NetSDKLib.LOG_SET_PRINT_INFO();
		File path = new File("sdklog/");
		if (!path.exists())
			path.mkdir();

		// 这里的log保存地址依据实际情况自己调整
		String logPath = path.getAbsoluteFile().getParent() + "\\sdklog\\" + "sdklog" + GetDate() + ".log";
		setLog.nPrintStrategy = 0;
		setLog.bSetFilePath = 1;
		System.arraycopy(logPath.getBytes(), 0, setLog.szLogFilePath, 0, logPath.getBytes().length);
		System.out.println(logPath);
		setLog.bSetPrintStrategy = 1;
		bLogOpen = netsdk.CLIENT_LogOpen(setLog);
		if (!bLogOpen)
			System.err.println("Failed to open NetSDK log");
	}

	/**
	 * 高安全登录
	 */
	public void loginWithHighLevel() {
		// 输入结构体参数
		NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY pstlnParam = new NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY() {
			{
				szIP = m_strIpAddr.getBytes();
				nPort = m_nPort;
				szUserName = m_strUser.getBytes();
				szPassword = m_strPassword.getBytes();
			}
		};
		// 输出结构体参数
		NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY pstOutParam = new NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY();

		// 写入sdk
		m_hLoginHandle = netsdk.CLIENT_LoginWithHighLevelSecurity(pstlnParam, pstOutParam);
		if (m_hLoginHandle.longValue() == 0) {
			System.err.printf("Login Device[%s] Port[%d]Failed. %s\n", m_strIpAddr, m_nPort,
					netsdk.CLIENT_GetLastError());
		} else {
			deviceInfo = pstOutParam.stuDeviceInfo; // 获取设备信息
			System.out.println("Login Success");
			System.out.println("Device Address：" + m_strIpAddr);
			System.out.println("设备包含：" + deviceInfo.byChanNum + "个通道");
		}
	}

	/**
	 * 退出
	 */
	public void logOut() {
		if (m_hLoginHandle.longValue() != 0) {
			netsdk.CLIENT_Logout(m_hLoginHandle);
			System.out.println("LogOut Success");
		}
	}

	/**
	 * 清理sdk环境并退出
	 */
	public static void cleanAndExit() {
		if (bLogOpen) {
			netsdk.CLIENT_LogClose(); // 关闭sdk日志打印
		}
		netsdk.CLIENT_Cleanup(); // 进程关闭时，调用一次
		System.exit(0);
	}

	/**
	 * 一键撤防配置
	 */
	public void setCfgDisableLinkage() {
		NET_CFG_DISABLE_LINKAGE config = new NET_CFG_DISABLE_LINKAGE();
		config.write();
		/** 配置获取 **/
		boolean result = netsdk.CLIENT_GetConfig(m_hLoginHandle, NET_EM_CFG_OPERATE_TYPE.NET_EM_CFG_DISABLE_LINKAGE, -1,
				config.getPointer(), config.size(), 5000, null);
		if (!result) {
			System.err.println("获取一键撤防配置 失败:" + ToolKits.getErrorCode());
		} else {
			System.out.println("获取一键撤防配置 成功");
			config.read();
			/** 打印相关参数 **/
			System.out.println("撤防联动项功能总开关： " + config.bEnable);
			/** 修改相关参数 **/
			config.bEnable = 0;

			config.write();
			/** 配置下发 **/
			boolean bRet = netsdk.CLIENT_SetConfig(m_hLoginHandle, NET_EM_CFG_OPERATE_TYPE.NET_EM_CFG_DISABLE_LINKAGE,
					-1, config.getPointer(), config.size(), 5000, new IntByReference(0), null);
			if (!bRet) {
				System.err.println("下发 一键撤防配置 失败:" + ToolKits.getErrorCode());
			} else {
				System.out.println("下发 一键撤防配置 成功");
			}
		}

	}

	/**
	 * 周期撤防联动项功能总开关
	 */
	public void setCfgDisableTimeSection() {
		NET_CFG_DISABLE_LINKAGE_TIME_SECTION config = new NET_CFG_DISABLE_LINKAGE_TIME_SECTION();
		config.write();
		/** 配置获取 **/
		boolean result = netsdk.CLIENT_GetConfig(m_hLoginHandle,
				NET_EM_CFG_OPERATE_TYPE.NET_EM_CFG_DISABLE_LINKAGE_TIME_SECTION, -1, config.getPointer(), config.size(),
				5000, null);
		if (!result) {
			System.err.println("获取周期撤防联动项功能总开关配置 失败:" + ToolKits.getErrorCode());
		} else {
			System.out.println("获取周期撤防联动项功能总开关配置 成功");
			config.read();
			/** 打印相关参数 **/
			System.out.println("周期撤防联动项功能总开关： " + config.bEnable);
			TIME_SECTION_6[] stuTimeSection = config.stuTimeSection;// 时间段周期设置, 第一维前7个元素对应每周7天，第8个元素对应节假日，每天最多6个时间段
			for (int i = 0; i < stuTimeSection.length; i++) {
				System.out.println("第" + (i + 1) + "个时间段  ");
				NetSDKLib.NET_TSECT[] timeSection = stuTimeSection[i].timeSection;
				for (int j = 0; j < timeSection.length; j++) {
					System.out.println("开始时间:" + timeSection[j].startTime() + " 结束时间:" + timeSection[j].endTime());
				}
			}
			/** 修改相关参数 **/
			config.bEnable = 1;
			config.stuTimeSection[0].timeSection[0].iBeginHour = 1;

			config.write();
			/** 配置下发 **/
			boolean bRet = netsdk.CLIENT_SetConfig(m_hLoginHandle,
					NET_EM_CFG_OPERATE_TYPE.NET_EM_CFG_DISABLE_LINKAGE_TIME_SECTION, -1, config.getPointer(),
					config.size(), 5000, new IntByReference(0), null);
			if (!bRet) {
				System.err.println("下发 周期撤防联动项功能总开关配置 失败:" + ToolKits.getErrorCode());
			} else {
				System.out.println("下发 周期撤防联动项功能总开关配置 成功");
			}
		}

	}

	/**
	 * 远程通道是否同步本地端'周期一键布撤防'配置
	 */
	public void setCfgDisableTimeSectionSync() {
		NET_CFG_REMOTE_DISABLE_LINKAGE_TIME_SECTION_SYNC_INFO config = new NET_CFG_REMOTE_DISABLE_LINKAGE_TIME_SECTION_SYNC_INFO();
		// 设备通道号数
		int byChanNum = deviceInfo.byChanNum;
		int[] pRemoteChannelsArr = new int[byChanNum];
		
		Pointer pRemoteChannels = new Memory(pRemoteChannelsArr.length * 4);
		pRemoteChannels.clear(pRemoteChannelsArr.length * 4);
		pRemoteChannels.write(0, pRemoteChannelsArr, 0, pRemoteChannelsArr.length);
		config.pRemoteChannels = pRemoteChannels;
		config.nRemoteChannelsCnt = deviceInfo.byChanNum; // pRemoteChannels指向的有效通道个数
		config.write();
		/** 配置获取 **/
		boolean result = netsdk.CLIENT_GetConfig(m_hLoginHandle,
				NET_EM_CFG_OPERATE_TYPE.NET_EM_CFG_REMOTE_DISABLE_LINKAGE_TIME_SECTION_SYNC, -1, config.getPointer(),
				config.size(), 5000, null);
		if (!result) {
			System.err.println("获取远程通道是否同步本地端'周期一键布撤防'配置 失败:" + ToolKits.getErrorCode());
		} else {
			config.read();
			/** 打印相关参数 **/
			System.out.println("远程通道是否同步本地端'周期一键布撤防'配置功能总开关： " + config.bEnable);
			System.out.println("获取配置时,返回的有效通道号个数： " + config.nRetRemoteChannelsCnt);
			config.pRemoteChannels.read(0, pRemoteChannelsArr, 0, pRemoteChannelsArr.length);
			String arr = "";
			for (int i = 0; i < config.nRetRemoteChannelsCnt; i++) {
				arr += pRemoteChannelsArr[i] + ",";
			}
			System.out.println("有效通道号如下：" + arr);
			/** 修改相关参数 **/
			// 下发的数据为全覆盖，请重新组装.以下数据为简单的修改
			config.bEnable = 1;
			config.nRemoteChannelsCnt =  config.nRetRemoteChannelsCnt+1; // pRemoteChannels指向的有效通道个数
			pRemoteChannelsArr[config.nRetRemoteChannelsCnt] = 10;
			
			Pointer pRemoteChannels1 = new Memory(pRemoteChannelsArr.length * 4);
			pRemoteChannels1.clear(pRemoteChannelsArr.length * 4);
			pRemoteChannels1.write(0, pRemoteChannelsArr, 0, pRemoteChannelsArr.length);
			config.pRemoteChannels = pRemoteChannels1;									
			config.write();
			/** 配置下发 **/
			boolean bRet = netsdk.CLIENT_SetConfig(m_hLoginHandle,
					NET_EM_CFG_OPERATE_TYPE.NET_EM_CFG_REMOTE_DISABLE_LINKAGE_TIME_SECTION_SYNC, -1, config.getPointer(),
					config.size(), 5000, new IntByReference(0), null);
			if (!bRet) {
				System.err.println("下发 远程通道是否同步本地端'周期一键布撤防'配置 失败:" + ToolKits.getErrorCode());
			} else {
				System.out.println("下发 远程通道是否同步本地端'周期一键布撤防'配置  成功");
			}

		}
	}

	/******************************** 测试控制台 ***************************************/

	// 配置登陆地址，端口，用户名，密码
	private String m_strIpAddr = "172.11.3.6";
	private int m_nPort = 37777;
	private String m_strUser = "admin";
	private String m_strPassword = "admin123";

	public static void main(String[] args) {
		RemoteDisableLinkageSyncDemo demo = new RemoteDisableLinkageSyncDemo();
		demo.InitTest();
		demo.RunTest();
		demo.EndTest();

	}

	/**
	 * 初始化测试
	 */
	public void InitTest() {
		REIDConfigDemo.Init();
		this.loginWithHighLevel();
	}

	/**
	 * 加载测试内容
	 */
	public void RunTest() {
		CaseMenu menu = new CaseMenu();
		menu.addItem(new CaseMenu.Item(this, "一键撤防配置", "setCfgDisableLinkage"));
		menu.addItem(new CaseMenu.Item(this, "周期撤防联动项功能总开关", "setCfgDisableTimeSection"));
		menu.addItem(new CaseMenu.Item(this, "远程通道是否同步本地端'周期一键布撤防'配置", "setCfgDisableTimeSectionSync"));
		menu.run();
	}

	/**
	 * 结束测试
	 */
	public void EndTest() {
		System.out.println("End Test");
		this.logOut(); // 登出设备
		System.out.println("See You...");
		RemoteDisableLinkageSyncDemo.cleanAndExit(); // 清理资源并退出
	}
	/******************************** 结束 ***************************************/
}
