package com.niepan.modules.zhinenganjianback.demo;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.niepan.common.tcpUtil.SocketServerUtil;
import com.niepan.common.tcpUtil.TcpServer;
import com.niepan.common.tcpUtil.TcpServerUtil;
import com.niepan.common.utils.ConfigConstant;
import com.niepan.common.utils.logs.LogUtils;
import com.niepan.common.utils.redis.RedisUtils;
import com.niepan.common.utils.uuid.SequentialUuidHexGenerator;
import com.niepan.common.websocket.server.WarningWebSocketServer;
import com.niepan.modules.sys.service.SysConfigService;
import com.niepan.modules.zhinenganjianback.VO.JsonObjectVO;
import com.niepan.modules.zhinenganjianback.VO.ThresholdVO;
import com.niepan.modules.zhinenganjianback.dao.*;
import com.niepan.modules.zhinenganjianback.demo.ftp.FTPUtils;
//import com.niepan.modules.zhinenganjianback.demo.udp.UdpServer;
import com.niepan.modules.zhinenganjianback.demo.udp.UdpServer;
import com.niepan.modules.zhinenganjianback.demo.util.ThreadPoolUtil;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.enumeration.EM_INSIDE_OBJECT_TYPE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_INSIDE_OBJECT;
import com.niepan.modules.zhinenganjianback.model.ImageIdentificationRecords;
import com.niepan.modules.zhinenganjianback.model.PackageItemInfo;
import com.niepan.modules.zhinenganjianback.model.PullData;
import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
import com.niepan.common.utils.SpringUtil.SpringUtil;
import com.niepan.modules.zhinenganjianback.service.PullDataService;
import com.sun.jna.Pointer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.niepan.modules.zhinenganjianback.demo.DeviceClient.ClientStateLogined;

@Component
public class DeviceManage implements DeviceClient.PictureResultCallBack{
    private static final Logger log= LoggerFactory.getLogger(DeviceManage.class);

    @Autowired
    private DeviceManagerDao deviceManagerDao;

    @Autowired
    private PackageItemInfoRepository packageItemInfoRepository;

    @Autowired
    private SysConfigService configService;


    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private PullDataService pullDataService;

//    @Autowired
//    private MongoTemplate mongoTemplate;

    @Autowired
    WarningWebSocketServer warningWebSocketServer;

    //上报数据公司id
    @Value("${external.imageCheckServer.requestCompanyId}")
    private String companyId;

    //请求密钥
    @Value("${external.imageCheckServer.requestSecretKey}")
    private String requestSecretKey;

    @Value("${external.imageCheckServer.msgType}")
    private String msgType;

    @Value("${external.imageCheckServer.filePath}")
    private String filePath;

    @Value("${file.picPath}")
    private String picPath;

    @Value("${FTP.hostname}")
    private String hostname;

    @Value("${FTP.port}")
    private String port;

    @Value("${FTP.username}")
    private String username;

    @Value("${FTP.password}")
    private String password;

    @Value("${FTP.workingPath}")
    private String workingPath;

    //给中通上传的url
    public static String url = null;

    public static String areaCode = null;

    //下面四个是摆轮需要的
    static String swing = null;
    static Long swingTime = null;
    static Long backTime = null;
    static int enable = 1;

    static NetSDKLib netSdk = NetSDKLib.NETSDK_INSTANCE;
    static NetSDKLib config =  NetSDKLib.CONFIG_INSTANCE;
    static DeviceManage manage = new DeviceManage();

    DeviceDisconnectCallback deviceDisconnectCallback = new DeviceDisconnectCallback();
    DeviceReconnectCallback deviceReconnectCallback = new DeviceReconnectCallback();
    static List<DeviceClient> deviceClients=new ArrayList<>();
    Thread thread;
    //默认值为0，如果有包裹数据进来，修改为1，
    private static volatile AtomicInteger isBusy=new AtomicInteger(-1);

    public static ThreadPoolExecutor pool =
            new ThreadPoolExecutor(10, 30, 10L, TimeUnit.SECONDS, new LinkedBlockingDeque<>(20));

    public static ThreadPoolExecutor pool2 =
            new ThreadPoolExecutor(10, 30, 10L, TimeUnit.SECONDS, new LinkedBlockingDeque<>(20));

    public static ThreadPoolExecutor pool3 =
            new ThreadPoolExecutor(10, 30, 10L, TimeUnit.SECONDS, new LinkedBlockingDeque<>(20));

    //线程池
    public static ThreadPoolExecutor poolBase =
            new ThreadPoolExecutor(10, 50, 60L, TimeUnit.SECONDS, new LinkedBlockingDeque<>(1000));


    private static Boolean b1 = false;

    private static Boolean b2 = false;
    //摆轮协议
    private static String BALANCE_PROTOCOL=StringUtils.EMPTY;
    //是否启用ftp上报
    private static String ftpEnable=StringUtils.EMPTY;
    private static Map<String,Object> deviceList_db = MapUtils.EMPTY_MAP;

    /**
     * 断线重连
     */
    private static class DeviceDisconnectCallback implements NetSDKLib.fDisConnect {
        public void invoke(NetSDKLib.LLong lLoginID, String pchDVRIP, int nDVRPort, Pointer dwUser) {
            manage.onDisconnect(lLoginID, pchDVRIP, nDVRPort, dwUser);
            DeviceClient deviceClient=new DeviceClient();
            DeviceClient.DeviceData deviceData = deviceClient.deviceData;
            deviceData.ip=pchDVRIP;
            deviceData.port=nDVRPort;
            deviceData.password="admin123";
            deviceData.user="admin";
            deviceData.callBack=manage;

            while (true){
                Boolean aBoolean = deviceClient.loginNew();
                log.info("loginL:"+deviceData.loginHandle);
                if(aBoolean){
                    log.info("登录成功");
                    break;
                }else {
                    log.info("登录失败，10s继续");
                    try {
                        Thread.sleep(10*1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
            for (int i = 0; i < deviceClients.size(); i++) {
                DeviceClient client = deviceClients.get(i);
                if(pchDVRIP.equals(client.deviceData.ip)){
                    deviceData.state=ClientStateLogined;
                    client.deviceData=deviceData;
                    break;
                }
            }
        }
    }
    private static class DeviceReconnectCallback implements NetSDKLib.fHaveReConnect {
        public void invoke(NetSDKLib.LLong lLoginID, String pchDVRIP, int nDVRPort, Pointer dwUser) {
            log.info("manage："+JSON.toJSON(manage));
        }
    }

    public void onDisconnect(NetSDKLib.LLong lLoginID, String pchDVRIP, int nDVRPort, Pointer dwUser){
        log.info("login id " + lLoginID);
        System.out.printf("Device[%s:%d] Disconnect!\n", pchDVRIP, nDVRPort);
        for(int i=0;i<deviceClients.size();i++) {
            DeviceClient device = deviceClients.get(i);
            if(device.getLoginHandle() == lLoginID){
                device.finish();
                break;
            }
        }
    }

    public void onReconnect(NetSDKLib.LLong lLoginID, String pchDVRIP, int nDVRPort, Pointer dwUser){
        System.out.printf("Device[%s:%d] HaveReconnected!\n", pchDVRIP, nDVRPort);
    }

   public void checkDeviceOnline(){
        while(!thread.isInterrupted()){
            for(int i=0;i<deviceClients.size();i++) {
                checkDevice(deviceClients.get(i));
            }
            int count = 0;
            while(!thread.isInterrupted() && count < 30){
                try {
                    Thread.sleep(1000);
                }catch (InterruptedException exception){
                    //exception.printStackTrace();
                    break;
                }
                count++;
            }
        }
    }

    void checkDevice(DeviceClient device){
        if(device.isReady()) {
            return;
        }
        if(device.prepare()) {
        }
    }

    void initSDk(){
        // 初始化SDK库
        netSdk.CLIENT_Init(deviceDisconnectCallback, null);
        //打开日志，可选0
        NetSDKLib.LOG_SET_PRINT_INFO setLog = new NetSDKLib.LOG_SET_PRINT_INFO();
        String logPath = new File(".").getAbsoluteFile().getParent() + File.separator + "sdk_log" + File.separator + "sdk.log";
        log.info("logPath:"+logPath);
        setLog.bSetFilePath = 1;
        System.arraycopy(logPath.getBytes(), 0, setLog.szLogFilePath, 0, logPath.getBytes().length);
        setLog.bSetPrintStrategy = 1;
        setLog.nPrintStrategy = 0;
        if (!netSdk.CLIENT_LogOpen(setLog)){
            System.err.println("Open SDK Log Failed!!!");
        }
        Map<String,Object> requestURL = deviceManagerDao.getRequestURL();
        url = (String) requestURL.get("requesturl");
        areaCode = (String) requestURL.get("area_code");
        // todo 需要操作摆轮的时候将下面一行放开
        balanceStart();
        // 设置断线重连成功回调函数
    //    netSdk.CLIENT_SetAutoReconnect(deviceReconnectCallback, null);
    }

    public void run(){
        initSDk();
        thread = new Thread(){
            public void run(){
                checkDeviceOnline();
            }
        };
        thread.start();
    }

    public void finish(){
        for(int i=0;i<deviceClients.size();i++) {
            deviceClients.get(i).finish();
        }
        netSdk.CLIENT_Cleanup();
        System.exit(0);
    }

    /**
     * {
     *     msg : {
     *         device : device.getInfo().ip,
     *         idle : device.getInfo().idle
     *         loginHandle ： loginHandle
     *     }
     * }
     */
    public void queryDeviceState(){
        for(int i=0;i<deviceClients.size();i++) {
            DeviceClient device = deviceClients.get(i);
            log.info("device "+ device.getInfo().ip + " idle " + device.getInfo().idle);
            NetSDKLib.LLong loginHandle = device.getLoginHandle();
            log.info("loginHandle = " + loginHandle);
        }
    }

    public List<Map<String,Object>> queryAttribute(){
        List<Map<String,Object>> result = new ArrayList<>();
        for(int i=0;i<deviceClients.size();i++) {
            List<Map<String,Object>> res = new ArrayList<>();
            DeviceClient device = deviceClients.get(i);
            log.info("device "+ device.getInfo().ip);
            List<DeviceClient.ObjectAttribute> list = new ArrayList<>();
            device.getObjectsAttribute(list);

            Map resMap = new HashMap<>();
            resMap.put("device",device.getInfo().ip);
            List<Map<String,Object>> list1 = deviceManagerDao.getDevice(device.getInfo().ip);
            for(int j=0;j<list.size();j++) {
                Map<String,Object> map = new HashMap<>();
                DeviceClient.ObjectAttribute attribute = list.get(j);
                log.info("type " + attribute.type.getValue() + " " + EM_INSIDE_OBJECT_TYPE.getNoteByValue(attribute.type.getValue())
                        + " Threshold " + attribute.detectThreshold);
                map.put("type",attribute.type.getValue());
                map.put("name", EM_INSIDE_OBJECT_TYPE.getNoteByValue(attribute.type.getValue()));
                map.put("threshold",attribute.detectThreshold);
                map.put("enable",attribute.bEnable);
                res.add(map);
            }
            for (Map<String, Object> stringObjectMap : list1) {
                String province = (String) stringObjectMap.get("province");
                resMap.put("province",province);
                String city = (String) stringObjectMap.get("city");
                resMap.put("city",city);
                String center = (String) stringObjectMap.get("center");
                resMap.put("center",center);
                String channel = (String) stringObjectMap.get("channel");
                resMap.put("channel",channel);
            }
            resMap.put("data",res);
            result.add(resMap);
        }
        return result;
    }

    public void setAttribute(List<ThresholdVO> thresholdVOList/*int threshod*/){
        for(int i=0;i<deviceClients.size();i++) {
            //todo 这里可以用if对ip进行判断，进而按照ip对机器进行修改阈值
            if (thresholdVOList.get(0).getDeviceIP().equals(deviceClients.get(i).getInfo().ip)){
                DeviceClient device = deviceClients.get(i);
                log.info("device "+ device.getInfo().ip);
                List<DeviceClient.ObjectAttribute> list = new ArrayList<>();
                if(!device.getObjectsAttribute(list)) {
                    continue;
                }
                for(int j=0;j<list.size();j++) {
                    Integer threshold = thresholdVOList.get(j).getThreshold();
                    String type = thresholdVOList.get(j).getType();
                    EM_INSIDE_OBJECT_TYPE em_inside_object_type = EM_INSIDE_OBJECT_TYPE.getEnum(Integer.valueOf(type));
                    list.get(j).detectThreshold = threshold;
                    list.get(j).type = em_inside_object_type;
                    list.get(j).bEnable = thresholdVOList.get(j).getEnable();
                }
                device.setObjectsAttribute(list);
            }
        }
    }

    public void addDevice(String ip,int port,String user,String password){
        DeviceClient device = new DeviceClient();
        device.init(ip, port, user, password, this);
        deviceClients.add(device);
    }

    public Boolean pushPicture(){
        for(int i=0;i<deviceClients.size();i++) {
            DeviceClient device = deviceClients.get(i);
            // if(device.getInfo().ip==0){};
            if(!device.getInfo().idle){
                continue;
            }
            DeviceClient.PictureInfo pictureInfo = new DeviceClient.PictureInfo();
//            pictureInfo.id = pictureId++;
            pictureInfo.data = ToolKits.readPictureToByteArray("1.jpg");
            log.info("push to device " + device.getInfo().ip + " picture " + pictureInfo.id);
            device.pushPicture(pictureInfo);
        }
        return true;
    }

    /**
     * 图片处理回调接口
     * @param pictureInfo
     * @param result
     * @return
     */
    @Override
    public int onResult(DeviceClient.PictureInfo pictureInfo, DeviceClient.PictureResult result) {
        try {
            log.error("接收数据,图片信息{}，sn信息:{}",pictureInfo.id,result.sn);
            //指定isBusy 忙碌状态为1
            isBusy.getAndSet(1);
            ApplicationContext applicationContext = SpringUtil.getApplicationContext();
            SecurityMachineDao securityMachineDaoBean = applicationContext.getBean(SecurityMachineDao.class);
            SysConfigService sysConfigService = applicationContext.getBean(SysConfigService.class);
            Set set = new HashSet();
            log.info("onResult " + " picture " + pictureInfo.id + " object count " + result.objectList.size()
                    + " sn " + result.sn);
            Map<String, Object> map = new HashMap<>();
            map.put("picture", pictureInfo.id);
            map.put("count", result.objectList.size());
            Date date1 = new Date();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
            String date = df.format(date1);//2023-02-17 13:49:08
//        securityMachineDaoBean.replenishPicture((String)map.get("picture"),(int)map.get("count"),date);

            SecurityMachinePicture securityMachinePicture = redisUtils.get(result.sn.trim()+"-"+pictureInfo.id, SecurityMachinePicture.class);
            if(Objects.nonNull(securityMachinePicture)){
                redisUtils.delete(result.sn.trim()+"-"+pictureInfo.id);
            }
            securityMachinePicture.setCount((int) map.get("count"));
            securityMachinePicture.setCompleteTime(date);
            Integer twice = securityMachinePicture.getTwice();

            ThreadPoolUtil.getThreadPool().submit(()->{
                if (twice == 1) {
                    securityMachineDaoBean.insertInfo(securityMachinePicture);
                } else if (twice == 2) {
                    securityMachineDaoBean.insertInfoTwice(securityMachinePicture);
                }
            });

            //给自己的统计服务器发送信息
//        Thread uploadForUsThread = new Thread(){
//            public void run(){
//                uploadForUs(securityMachinePicture);
//            }
//        };
//        uploadForUsThread.start();

            ThreadPoolUtil.getThreadPool().submit(()-> {
                for (int n = 0; n < result.objectList.size(); n++) {
                    PackageItemInfo packageItemInfo = new PackageItemInfo();
                    NET_INSIDE_OBJECT stuInsideObj = result.objectList.get(n);
                    log.info("stuInsideObj[" + n + "]:" + stuInsideObj.toString());
                    //以下为新增
                    //物品危险等级
                    int emDangerGrade = stuInsideObj.emDangerGrade;
                    //物品类型
                    int emObjType = stuInsideObj.emObjType;

                    set.add(emObjType);
                    //物品名称
                    String emObjName = EM_INSIDE_OBJECT_TYPE.getEnum(emObjType).getNote();
                    //相似度
                    int nSimilarity = stuInsideObj.nSimilarity;
                    //包围盒
                    String stuBoundingBoxs = stuInsideObj.stuBoundingBox.toString();

                    Map<String, Object> stuInsideObjMap = new HashMap<>();
                    stuInsideObjMap.put("emDangerGrade", emDangerGrade);
                    stuInsideObjMap.put("emObjType", emObjType);
                    stuInsideObjMap.put("nSimilarity", nSimilarity);
                    stuInsideObjMap.put("emObjName", emObjName);
                    stuInsideObjMap.put("stuBoundingBox", stuBoundingBoxs);

                    map.put("stuInsideObj[" + n + "]:", stuInsideObjMap);

                    packageItemInfo.setEmDangerGrade(emDangerGrade);
                    packageItemInfo.setEmObjType(emObjType);
                    packageItemInfo.setItemName(emObjName);
                    packageItemInfo.setImageIdentificationRecordsId((String) map.get("picture"));
                    packageItemInfo.setNSimilarity(nSimilarity);
                    packageItemInfo.setStuBoundingBox(stuBoundingBoxs);
                    packageItemInfo.setUnique_code(areaCode);

                    //将有问题的图片属性存储到数据库中
                    DeviceManagerDao deviceManagerDaoBean = applicationContext.getBean(DeviceManagerDao.class);
                    deviceManagerDaoBean.insertImage(emDangerGrade, emObjType, (String) stuInsideObjMap.get("emObjName"), nSimilarity, stuBoundingBoxs, (String) map.get("picture"), areaCode);

                    //给自己的统计服务器发送信息
//            Thread uploadForUsDangerThread = new Thread(){
//                public void run(){
//                    uploadForUsDanger(packageItemInfo);
//                }
//            };
//            uploadForUsDangerThread.start();
                }
            });

            if (result.objectList.size() > 0) {
                //控制摆轮 todo 需要我们自己控制摆轮的话就放开
                if(StringUtils.isBlank(BALANCE_PROTOCOL)){
                    BALANCE_PROTOCOL= sysConfigService.getValue(ConfigConstant.BALANCE_PROTOCOL);
                }
                log.info("启动摆轮："+BALANCE_PROTOCOL);
                if(StringUtils.equalsIgnoreCase(BALANCE_PROTOCOL,"udp")){
                    //UDP摆轮
                    UDPBalance();
                }else if(StringUtils.equalsIgnoreCase(BALANCE_PROTOCOL,"tcp")) {
                    //TCP摆轮
                    TCPBalance();
                }else{
                    //没有获取到就是不操作摆轮
                }
                try {
                    //将解析的图片信息交给需要的服务器  http需连接服务器
                    ImageIdentificationRecords imageIdentificationRecords = new ImageIdentificationRecords();
                    imageIdentificationRecords.setEmDangerGrade(result.objectList.get(0).emDangerGrade);
                    imageIdentificationRecords.setImageId(String.valueOf(pictureInfo.id));
//                    imageIdentificationRecords.setScanTime(date1);
//                    imageIdentificationRecords.setEmClassType(stringBuffer.deleteCharAt(stringBuffer.lastIndexOf(",")));
                    //set变成字符串并且去掉括号
                    imageIdentificationRecords.setEmClassType(set.toString().replaceAll("[\\[\\]]", ""));
                    imageIdentificationRecords.setImageId(pictureInfo.id);
//                    Map<String,Object> map1 = deviceManagerDao.getDeviceIp(String.valueOf(pictureInfo.id));
//                    String deviceIp = (String) map1.get("device_ip");
//                    String ajj_time = (String) map1.get("ajj_time");
                    String deviceIp = securityMachinePicture.getDeviceIp();
                    String ajj_time = securityMachinePicture.getAjjTime();
                    String substring = ajj_time.substring(0, 19);
                    Timestamp timestamp = Timestamp.valueOf(substring);
                    imageIdentificationRecords.setScanTime(timestamp);
                    String sn = result.sn;
                    imageIdentificationRecords.setSnCode(sn.trim());
                    uploadImageIdentifyData(deviceIp, imageIdentificationRecords, true);
                } catch (Exception e) {
                    LogUtils.error("图片信息上传失败,ip端口为" + url);
                    log.error("违禁品图片信息上传失败,ip端口为",e);

                }
            } else {//无违禁品
                try {
                    //将解析的图片信息交给需要的服务器  http需连接服务器
                    ImageIdentificationRecords imageIdentificationRecords = new ImageIdentificationRecords();
                    imageIdentificationRecords.setEmDangerGrade(0);
                    imageIdentificationRecords.setImageId(String.valueOf(pictureInfo.id));
                    imageIdentificationRecords.setEmClassType("0");
                    imageIdentificationRecords.setImageId(pictureInfo.id);
                    String deviceIp = securityMachinePicture.getDeviceIp();
                    String ajj_time = securityMachinePicture.getAjjTime();
                    String substring = ajj_time.substring(0, 19);
                    Timestamp timestamp = Timestamp.valueOf(substring);
                    imageIdentificationRecords.setScanTime(timestamp);
                    String sn = result.sn;
                    imageIdentificationRecords.setSnCode(sn.trim());
                    uploadImageIdentifyData(deviceIp, imageIdentificationRecords, false);
                } catch (Exception e) {
                    LogUtils.error("图片信息上传失败,ip端口为" + url);
                    log.error("非违禁品图片信息上传失败,ip端口为",e);
                }
            }

            if (0 == (Integer) map.get("count") || (Integer) map.get("count") == null) {
//            deviceManagerDao.updateHttpNoDanger(pictureInfo.id);
            }
            return 0;
        } catch (Exception e) {
            log.error("onResult",e);
            return 0;
        }
    }

    /**
     * 每间隔十秒，判断机器是否在忙碌状态，
     * 如果在忙碌状态，则不进行推送非违禁品数据到中通，如果不在忙碌状态，则每次拉去100条非违禁品数据进行推送
     */
    @Scheduled(cron = "0/10 * * * * * ")
    public void pushIsBusyToZto(){
        //如果是默认值0，则不执行任何事情，
        if(isBusy.get()==0){
            return;
        }
        //如果是1 ，则说明在运行中，赋值为-1，且当前不执行推送，等待下次定时任务轮询，
        if(isBusy.get()==1){
            isBusy.getAndSet(-1);
            return;
        }
        //如果当前值为-1，则判断为空闲状态，进行数据上报
        List<PullData> pullDataList= pullDataService.listByPushState();
        if(CollectionUtil.isEmpty(pullDataList)){
            return;
        }
        if(pullDataList.size()<1000){
            //不够1000时，这下次不再查询sql
            isBusy.set(0);
        }
//        ThreadPoolUtil.getThreadPool().submit(()->{
            for (PullData item:pullDataList){
                JSONObject data = new JSONObject();
                //厂家设备SN码
                data.put("snCode", item.getSnCode());
                //DWS设备SN码
                data.put("dwsCode", item.getDwsCode());
                //安检机图片ID, 目前无法采集到
                data.put("forbidId", item.getImageId());
                //违禁品标记,只有发现违禁品才会上报，默认为true
                data.put("isForbid", Boolean.valueOf(item.getIsForbid()));
                //违禁品类型编号
                data.put("forbidCode", item.getForbidCode());
                //违禁品类型名称
                data.put("forbidType", item.getForbidType());
                //扫描时间
                data.put("scanTime", item.getScanTime());
                //公司Id
                data.put("companyId", companyId);
                //扩展字段，待定 TODO
                data.put("extendedField", item.getExtendedField());
                LogUtils.info("data : " + data);
                LogUtils.info("key : " + requestSecretKey);

                String json = data.toJSONString();
                String data_digest = DigestUtil.md5Hex(json+requestSecretKey);
                Map<String,Object> map = new HashMap<>();
                map.put("data",json);
                map.put("data_digest",data_digest);
                map.put("msg_type",msgType);
                map.put("company_id",companyId);
                log.info("map = " + map);
                String result= pullZto(map,5000);
                if ("SUCCESS".equals(result)){
                    item.setPullState(1);
                }else{
                    item.setPullState(-1);
                }
                item.setPullTime(DateUtil.date());
                pullDataService.updatePullState(item);
            }
//        });

    }


    static void printTip(){
        log.info("");
        log.info("0 查看设备状态");
        log.info("1 查询阈值");
        log.info("2 设置阈值");
        log.info("3 推送图片");
        log.info("4 退出");
    }

    //测试主方法，生存环境没用
    public void main(String[] args) {
        manage.addDevice("192.168.0.20", 37777, "admin", "admin123");
        manage.addDevice("192.168.0.21", 37777, "admin", "admin123");
        manage.run();
        Scanner scanner = new Scanner(System.in);
        while(true){
            printTip();
            int value = scanner.nextInt();
            if(value == 0){
                manage.queryDeviceState();
            }else if(value == 1){
                manage.queryAttribute();
            }else if(value == 2){
                log.info("输入阈值: 0~100");
                int threshold = scanner.nextInt();
//                manage.setAttribute(threshold);
            }else if(value == 3){
                manage.pushPicture();
            }else if(value == 4){
                break;
            }
        }
        manage.finish();
    }

    int deviceNum = 0; //轮询算法的定位量

    public Boolean newPushPicture(MultipartFile multipartFiles){
        try {
            //将该服务器连接的所有的大华机器获取到
            List<NetSDKLib.LLong> list = new ArrayList();
            for(int i=0;i<deviceClients.size();i++){
                DeviceClient device = deviceClients.get(i);
                NetSDKLib.LLong loginHandle = device.getLoginHandle();
                list.add(loginHandle);
                log.info("loginHandle = " + loginHandle);
            }

            DeviceClient device = deviceClients.get(deviceNum % 1);
            NetSDKLib.LLong lLong = list.get(deviceNum % 1);
            deviceNum++;

            if(!device.getInfo().idle){
                return null;
            }
            DeviceClient.PictureInfo pictureInfo = new DeviceClient.PictureInfo();
//            pictureInfo.id = pictureId++;
            pictureInfo.data = multipartFiles.getBytes();
            log.info("push to device " + device.getInfo().ip + " picture " + pictureInfo.id);

            //将图片存储到本地指定地址
            String originalName = multipartFiles.getOriginalFilename();
            String newName = UUID.randomUUID().toString().replaceAll("-","") + originalName;
            File file = new File(filePath + newName);
            if (!file.getParentFile().exists()) file.getParentFile().mkdirs();
            byte[] bytes = multipartFiles.getBytes();
            for (int k=0 ; k < bytes.length; k++){
                if (bytes[k] < 0){
                    bytes[k] += 256;
                }
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bytes);
            fileOutputStream.flush();
            fileOutputStream.close();

            //推送给大华机器
            device.newPushPicture(pictureInfo,lLong);
            return true;
        } catch (IOException e) {
            log.error("newPushPicture",e);
            return false;
        }
    }

    public Boolean newPushPicture(JsonObjectVO jsonObjectVO){
        try {
            //将该服务器连接的所有的大华机器获取到
            List<DeviceClient> list = new ArrayList();

            if(MapUtils.isEmpty(deviceList_db)) {
                deviceList_db = deviceManagerDao.getAllDevice(jsonObjectVO.getDevice_sn());
            }

            for(int i=0;i<deviceClients.size();i++){
                DeviceClient device = deviceClients.get(i);
                NetSDKLib.LLong loginHandle = device.getLoginHandle();
                list.add(device);
                log.info("loginHandle = " + loginHandle);
            }

            for (DeviceClient device : list) {
                if (deviceList_db.get("ip").equals(device.getInfo().ip)){
//                    poolBase.submit(()-> {
                        DeviceClient.PictureInfo pictureInfo = new DeviceClient.PictureInfo();
                        pictureInfo.id = SequentialUuidHexGenerator.generate().replaceAll("-", "");
                        BASE64Decoder base64Decoder = new BASE64Decoder();
                        String imgBase64Side = jsonObjectVO.getImg_base64_side();
                        pictureInfo.data = base64Decoder.decodeBuffer(imgBase64Side);
                        log.info("push to device " + device.getInfo().ip + " picture " + pictureInfo.id);
                        SecurityMachinePicture securityMachinePicture = new SecurityMachinePicture();
                        securityMachinePicture.setCurrentTime(jsonObjectVO.getCurrentTime());
                        securityMachinePicture.setDevice_sn(jsonObjectVO.getDevice_sn());
                        securityMachinePicture.setLine_code((String) deviceList_db.get("channel"));
                        securityMachinePicture.setName(jsonObjectVO.getName());
                        securityMachinePicture.setTime(jsonObjectVO.getTime().toString());
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
                        String ajjTime = simpleDateFormat.format(jsonObjectVO.getTime());
                        LogUtils.info("ajjTime = " + ajjTime);
                        log.info("ajjTime = " + ajjTime);
                        securityMachinePicture.setAjjTime(ajjTime);
                        securityMachinePicture.setDeviceIp(device.getInfo().ip);
                        securityMachinePicture.setPictureId(pictureInfo.id);
                        securityMachinePicture.setAreaCode(areaCode);
                        securityMachinePicture.setIdle(device.getInfo().idle);
                        securityMachinePicture.setTwice((Integer) deviceList_db.get("twice"));

                        redisUtils.set(jsonObjectVO.getDevice_sn()+"-"+pictureInfo.id, securityMachinePicture);
//                    Integer integer = securityMachineDao.insertImgInfo(securityMachinePicture);
//                    log.info("integer = " + integer + jsonObjectVO.getName());
                        //大华处理图片逻辑
                        if (!device.getInfo().idle) {
                            return null;
                        }
//                    securityMachineDao.insertIdle(securityMachinePicture.getName(),device.getInfo().idle);
                        //推送给智能判图机器
                        Boolean aBoolean = device.newPushPicture(pictureInfo, device.getLoginHandle());

//                    this.FTPFunctionNew(imgBase64Side,jsonObjectVO.getDevice_sn(),jsonObjectVO.getTime(),pictureInfo.id);

                        return aBoolean;
//                    });
                }
            }
            return false;
        } catch (Exception e) {
            log.error("newPushPicture",e);
            return false;
        }
    }

    public Boolean newPushPicture1(JsonObjectVO jsonObjectVO){
        try {
            //将该服务器连接的所有的大华机器获取到
            List<DeviceClient> list = new ArrayList();
            if(MapUtils.isEmpty(deviceList_db)) {
                deviceList_db = deviceManagerDao.getAllDevice(jsonObjectVO.getDevice_sn());
            }
            for(int i=0;i<deviceClients.size();i++){
                DeviceClient device = deviceClients.get(i);
                NetSDKLib.LLong loginHandle = device.getLoginHandle();
                list.add(device);
                log.info("loginHandle = " + loginHandle);
            }
            for (DeviceClient device : list) {
                if (deviceList_db.get("ip").equals(device.getInfo().ip)){
                    DeviceClient.PictureInfo pictureInfo = new DeviceClient.PictureInfo();
                    pictureInfo.id = SequentialUuidHexGenerator.generate().replaceAll("-","");
                    BASE64Decoder base64Decoder = new BASE64Decoder();
                    String imgBase64Side = jsonObjectVO.getImg_base64_side();
                    pictureInfo.data =  base64Decoder.decodeBuffer(imgBase64Side);
                    log.info("push to device " + device.getInfo().ip + " picture " + pictureInfo.id);
                    SecurityMachinePicture securityMachinePicture = new SecurityMachinePicture();
                    securityMachinePicture.setCurrentTime(jsonObjectVO.getCurrentTime());
                    securityMachinePicture.setDevice_sn(jsonObjectVO.getDevice_sn());
                    securityMachinePicture.setLine_code((String) deviceList_db.get("channel"));
                    securityMachinePicture.setName(jsonObjectVO.getName());
                    securityMachinePicture.setTime(jsonObjectVO.getTime().toString());
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
//                    String ajjTime = simpleDateFormat.format(jsonObjectVO.getTime());
                    LogUtils.info("ajjTime = " + jsonObjectVO.getAjjTime());
                    log.info("ajjTime = " + jsonObjectVO.getAjjTime());
                    securityMachinePicture.setAjjTime(jsonObjectVO.getAjjTime());
                    securityMachinePicture.setDeviceIp(device.getInfo().ip);
                    securityMachinePicture.setPictureId(pictureInfo.id);
                    securityMachinePicture.setAreaCode(areaCode);
                    securityMachinePicture.setIdle(device.getInfo().idle);
                    securityMachinePicture.setTwice((Integer) deviceList_db.get("twice"));

                    redisUtils.set(jsonObjectVO.getDevice_sn(),securityMachinePicture);

//                    Integer integer = securityMachineDao.insertImgInfo(securityMachinePicture);

//                    log.info("integer = " + integer + jsonObjectVO.getName());

                    //大华处理图片逻辑
                    // if(!device.getInfo().idle){
                    //     return null;
                    // }
//                    securityMachineDao.insertIdle(securityMachinePicture.getName(),device.getInfo().idle);

                    //推送给大华机器
                    Boolean aBoolean = device.newPushPicture(pictureInfo, device.getLoginHandle());

//                    this.FTPFunctionNew(imgBase64Side,jsonObjectVO.getDevice_sn(),jsonObjectVO.getTime(),pictureInfo.id);

                    return aBoolean;
                }
            }
            return false;
        } catch (Exception e) {
            log.error("newPushPicture1",e);
            return false;
        }
    }
    private Long lastInvokeTime=null;
    //远程调用服务器
    public void uploadImageIdentifyData(String deviceIp, ImageIdentificationRecords records, Boolean forbid) {
        // 根据图片id查询time字段
        String DWSCode = deviceManagerDao.getDWSNcode(records.getSnCode().trim());
        //查询违禁品
        List<PackageItemInfo> list = packageItemInfoRepository.findByImageIdentificationRecordsId(records.getId());
        JSONArray array = JSONArray.parseArray(JSON.toJSONString(list));        //路径参数
        //body参数
        /**
         * {"snCode":"UT-011-20170000","dwsCode":"ZKWZ1351416","forbidId":"639a8916002b447b9872e7efbe5321be",
         * "isForbid":true,"forbidCode":"26","forbidType":"KitchenKnife","scanTime":1665108066930,
         * "companyId":"HZNP","extendedField":"{\"em_class_type\": 1,\"em_danger_grade\": 2,\"channel_id\": 0,\"n_event_id\": 5,\"n_image_count\": 2,\"n_object_num\": 2,\"illegalPackageInfo\": [{\"em_danger_grade\": 2,\"em_obj_type\": 12,\"item_name\": \"喷雾喷灌\",\"n_similarity\": 32},{\"em_danger_grade\": 2,\"em_obj_type\": 12,\"item_name\": \"喷雾喷灌\",\"n_similarity\": 32},{\"em_danger_grade\": 2,\"em_obj_type\": 12,\"item_name\": \"喷雾喷灌\",\"n_similarity\": 32}]}"}
         */
        JSONObject data = new JSONObject();
        //厂家设备SN码
        data.put("snCode", records.getSnCode().trim());
        //DWS设备SN码
        data.put("dwsCode", DWSCode);
        //安检机图片ID, 目前无法采集到
        data.put("forbidId", records.getImageId());
        //违禁品标记,只有发现违禁品才会上报，默认为true
        data.put("isForbid", forbid);
        //违禁品类型编号
        data.put("forbidCode", records.getEmDangerGrade());
        //违禁品类型名称
        data.put("forbidType", records.getEmClassType());
        //扫描时间
        data.put("scanTime", records.getScanTime());
        //公司Id
        data.put("companyId", companyId);
        //扩展字段，待定 TODO
        data.put("extendedField", array.toJSONString());
        LogUtils.info("data : " + data);
        LogUtils.info("key : " + requestSecretKey);

        String json = data.toJSONString();
        String data_digest = DigestUtil.md5Hex(json+requestSecretKey);
        Map map = new HashMap<>();
        map.put("data",json);
        map.put("data_digest",data_digest);
        map.put("msg_type",msgType);
        map.put("company_id",companyId);
        log.info("map=" + map);
        ThreadPoolUtil.getThreadPool().submit(()->{
            int pullState= records.getEmDangerGrade()>=1 ? 1 : 0;// 违禁品推送1，非违禁品不推送0
            deviceManagerDao.insertData(records.getImageId(),JSON.toJSONString(map).replaceAll("\\\\",""));
            PullData pullData=new PullData();
            pullData.setImageId(records.getImageId());
            pullData.setSnCode(records.getSnCode().trim());
            pullData.setDwsCode(DWSCode);
            pullData.setForbidId(records.getImageId());
            pullData.setIsForbid(forbid.toString());
            pullData.setForbidCode(records.getEmDangerGrade());
            pullData.setForbidType(records.getEmClassType());
            pullData.setScanTime(records.getScanTime());
            pullData.setExtendedField(array.toJSONString());
            pullData.setPullState(pullState);
            pullDataService.insert(pullData);
        });

        String statusCode=StringUtils.EMPTY;
        DateTime now=DateUtil.date();
        if(records.getEmDangerGrade()>=1) {
            statusCode = pullZto(map,500);
            String httpStatus= "SUCCESS".equalsIgnoreCase(statusCode)?"1":"0";
            ThreadPoolUtil.getThreadPool().submit(()-> {
                // 上传数据成功
                deviceManagerDao.updateHttpAndTime(records.getImageId(),DateUtil.format(now, DatePattern.NORM_DATETIME_MS_FORMAT),httpStatus);
            });
        }
        log.info("");

//        Query query = new Query();
//        Criteria criteria = new Criteria();
//        query.addCriteria(Criteria.where("pid").is(records.getImageId()));
//        Update update = new Update();
//        update.set("map",JSON.toJSONString(map));
//        String collectionName = "test";
//
//        mongoTemplate.updateFirst(query,update,collectionName);
        if(StringUtils.isBlank(ftpEnable)) {
            ftpEnable = configService.getValue(ConfigConstant.FTP_ENABLE);
        }
        if(StringUtils.equalsIgnoreCase(ftpEnable,"1")) {
            // todo FTP服务上传图片
            this.FTPFunction(records);
        }
    }

    /**
     * 推送到中通
     * @param map
     * @return
     */
    private String pullZto(Map<String,Object> map,int timeOut){
        try {
            if (StringUtils.isBlank(url)) {
                Map<String, Object> requestURL = deviceManagerDao.getRequestURL();
                url = (String) requestURL.get("requesturl");
            }
            log.info("上报数据："+JSON.toJSONString(map));
            String result2 = HttpRequest.post(url)
                    .header(Header.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=utf-8")//头信息，多个头信息多次调用此方法即可
                    .form(map)//表单内容
                    .timeout(timeOut)//超时，毫秒
                    .execute().body();
            log.info("向总部上传智能图片响应数据接口返回信息：" + result2);
            Map mapTypes = JSON.parseObject(result2);
            String statusCode = (String) mapTypes.get("statusCode");
            return statusCode;
        }catch (Exception e){
            log.error("推送中通报错",e);
            return "FAIL";
        }
    }


    private void FTPFunction(ImageIdentificationRecords records){
        pool2.submit(() -> {
            try {
                // ftp 上传文件
                // 上传文件的路径
                String  forbidId = records.getImageId();
                //查询违禁品图片名称
                Map<String,Object> pic = deviceManagerDao.getPicName(records.getImageId());

                String name = (String) pic.get("name");
                String device_sn = (String) pic.get("device_sn");
                String line_code = (String) pic.get("line_code");

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                String datePath = sdf.format(new Date());//20230215
                String filePath = picPath + device_sn + line_code + "/" + datePath + "/";
                String FullPath = filePath + name;

                log.info("FullPath = " + FullPath);

                String saveName = forbidId + "-" + records.getScanTime().getTime()+ "#SIM-" + records.getSnCode().trim() + ".jpg";

                log.info("saveName = " + saveName);

                InputStream fileInputStream = new FileInputStream(new File(FullPath));
                Integer port1 = Integer.valueOf(port);
                log.info(hostname + port1 + username + password + workingPath);
                boolean upload = FTPUtils.upload(hostname, port1, username, password, workingPath, fileInputStream, saveName);
                if (upload){
                    deviceManagerDao.updateFTP(records.getImageId());
                }
            } catch (Exception e) {
                log.error("FTP服务器存储异常",e);
            }
        });
    }


    private void balanceStart() {
        Map<String,Object> map = deviceManagerDao.getSwing();
        swing = (String)map.get("swing");
        swingTime = new Long((int)map.get("swing_time"));
        backTime = new Long((int)map.get("correct_time"));
        enable = (int)map.get("enable");

        redisUtils.set("swing",swing);
        redisUtils.set("swingTime",swingTime);
        redisUtils.set("backTime",backTime);
        redisUtils.set("enable",enable);

        Thread TcpThread = new Thread(){
            public void run(){
                SocketServerUtil.getInstance();
//                TcpServer.getInstance();
            }
        };
        TcpThread.start();
    }

    private void UDPBalance() throws InterruptedException {
        if (Integer.parseInt(redisUtils.get("enable")) == 1){
            UdpServer.getInstance();
            if (b1 == true){
                b2 = true;
            }
            b1 = true;
            //TODO 从智能判图服务器识别出异常物品后经过多少秒通知摆轮摆动，1秒= 1000L。替换对应的数字后重启项目即可加载
            //TODO 右摆
//                    Thread.sleep(swingTime);
            Thread.sleep(Long.valueOf(redisUtils.get("swingTime")));
//                    switch (swing){
            switch (redisUtils.get("swing")){
                case "left":
                    UdpServer.returnMsg(UdpServer.leftBytes);
                    break;
                case "right":
                    UdpServer.returnMsg(UdpServer.rightBytes);
                    break;
            }
            b1 = false;
            //TODO 回正
            if (b1 == false && b2 == false){
//                        Thread.sleep(backTime);
                Thread.sleep(Long.valueOf(redisUtils.get("backTime")));
                UdpServer.returnMsg(UdpServer.backBytes);
            }
            b2 = false;
            log.info("摆轮已经摆动");
        }
    }

    private void TCPBalance(){
//        if (Integer.parseInt(redisUtils.get("enable")) == 1){
//            Thread TcpBLThread = new Thread(new Runnable() {
//                @Override
//                public void run() {
                    try {
                        log.info("tcp摆轮开始");
//                        TcpServerUtil.getInstance();
                        if (b1 == true){
                            b2 = true;
                        }
                        b1 = true;
                        //TODO 从智能判图服务器识别出异常物品后经过多少秒通知摆轮摆动，1秒= 1000L。替换对应的数字后重启项目即可加载
                        //TODO 右摆
                        Thread.sleep(Long.valueOf(redisUtils.get("swingTime")));
                        switch (redisUtils.get("swing")){
                            case "left":
//                                TcpServerUtil.returnMsg(TcpServer.left);
                                SocketServerUtil.sendToALl(TcpServer.left);
                                break;
                            case "right":
//                                TcpServerUtil.returnMsg(TcpServer.right);
                                SocketServerUtil.sendToALl(TcpServer.right);
                                break;
                        }
                        b1 = false;
                        //TODO 回正
                        if (b1 == false && b2 == false){
                            Thread.sleep(Long.valueOf(redisUtils.get("backTime")));
//                            TcpServerUtil.returnMsg(TcpServer.back);
                            SocketServerUtil.sendToALl(TcpServer.back);
                        }
                        b2 = false;
                        log.info("摆轮已经摆动");
                    } catch (Exception e) {
                        log.error("TCP摆轮出错",e);
                    }
//                }
//            });
//            TcpBLThread.start();
//        }
    }

    private void uploadForUs(SecurityMachinePicture securityMachinePicture) {

        String jsonString = JSON.toJSONString(securityMachinePicture);
        log.info("jsonString = " + jsonString);

        String result2 = HttpRequest.post("http://192.168.0.164:8200/ReceiveInfo")
                .header(Header.CONTENT_TYPE, "application/json;charset=utf-8")//头信息，多个头信息多次调用此方法即可
                .body(jsonString)//表单内容
                .timeout(500)//超时，毫秒
                .execute().body();
        log.info("向服务器上传智能图片响应数据 接口返回信息 : " + result2);
    }

    private void uploadForUsDanger(PackageItemInfo packageItemInfo) {

        String jsonString = JSON.toJSONString(packageItemInfo);
        log.info("jsonString = " + jsonString);

        String result2 = HttpRequest.post("http://192.168.0.164:8200/ReceiveInfoDanger")
                .header(Header.CONTENT_TYPE, "application/json;charset=utf-8")//头信息，多个头信息多次调用此方法即可
                .body(jsonString)//表单内容
                .timeout(500)//超时，毫秒
                .execute().body();
        log.info("向服务器上传智能图片响应数据 接口返回信息 : " + result2);
    }


    private void FTPFunctionNew(String base64EncodedString,String snCode,Long time,String forbidId){
        pool2.submit(() -> {
            try {
                // ftp 上传文件
                String saveName = forbidId + "-" + time + "#SIM-" + snCode + ".jpg";

                log.info("saveName = " + saveName);

                // 解码Base64字符串为字节数组
                byte[] decodedBytes = Base64.getDecoder().decode(base64EncodedString);

                // 创建ByteArrayInputStream
                InputStream fileInputStream = new ByteArrayInputStream(decodedBytes);

                Integer port1 = Integer.valueOf(port);

                log.info(hostname + port1 + username + password + workingPath);

                boolean upload = FTPUtils.upload(hostname, port1, username, password, workingPath, fileInputStream, saveName);

            } catch (Exception e) {
                log.error("FTP服务器存储异常",e);
            }
        });
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        log.info("回调函数被回收");
    }
}
