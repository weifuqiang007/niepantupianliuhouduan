package com.niepan.modules.zhinenganjianback.demo.customize;

import static com.niepan.modules.zhinenganjianback.lib.Utils.getOsPrefix;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultDisconnectCallback;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultHaveReconnectCallBack;
import com.niepan.modules.zhinenganjianback.lib.structure.Byte64Arr;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_CHANNEL_STATE_INFO_EX;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_INTELLI_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_IN_GET_CHANNEL_STATE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_IN_SET_POLLING_CONFIG;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_OUT_GET_CHANNEL_STATE;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_OUT_SET_POLLING_CONFIG;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_SET_POLLING_CONFIG_INFO;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

public class GetChannelStateDemo {
	// SDk对象初始化
	public static final NetSDKLib netsdk = NetSDKLib.NETSDK_INSTANCE;
	public static final NetSDKLib configsdk = NetSDKLib.CONFIG_INSTANCE;

	// 判断是否初始化
	private static boolean bInit = false;
	// 判断log是否打开
	private static boolean bLogOpen = false;
	// 设备信息
	private NetSDKLib.NET_DEVICEINFO_Ex deviceInfo = new NetSDKLib.NET_DEVICEINFO_Ex();
	// 登录句柄
	private NetSDKLib.LLong m_hLoginHandle = new NetSDKLib.LLong(0);

	// 回调函数需要是静态的，防止被系统回收
	// 断线回调
	private static NetSDKLib.fDisConnect disConnectCB = DefaultDisconnectCallback.getINSTANCE();
	// 重连回调
	private static NetSDKLib.fHaveReConnect haveReConnectCB = DefaultHaveReconnectCallBack.getINSTANCE();

	// 编码格式
	public static String encode;

	static {
		String osPrefix = getOsPrefix();
		if (osPrefix.toLowerCase().startsWith("win32-amd64")) {
			encode = "GBK";
		} else if (osPrefix.toLowerCase().startsWith("linux-amd64")) {
			encode = "UTF-8";
		}
	}

	/**
	 * 获取当前时间
	 */
	public static String GetDate() {
		SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return simpleDate.format(new java.util.Date()).replaceAll("[^0-9]", "-");
	}

	/**
	 * 初始化SDK库
	 */
	public static boolean Init() {
		bInit = netsdk.CLIENT_Init(disConnectCB, null);// 进程启动时，初始化一次
		if (!bInit) {
			System.out.println("Initialize SDK failed");
			return false;
		}
		// 配置日志
		GetChannelStateDemo.enableLog();

		// 设置断线重连回调接口, 此操作为可选操作，但建议用户进行设置
		netsdk.CLIENT_SetAutoReconnect(haveReConnectCB, null);

		// 设置登录超时时间和尝试次数，可选
		// 登录请求响应超时时间设置为3S
		int waitTime = 3000;
		// 登录时尝试建立链接 1 次
		int tryTimes = 1;
		netsdk.CLIENT_SetConnectTime(waitTime, tryTimes);
		// 设置更多网络参数， NET_PARAM 的nWaittime ， nConnectTryNum 成员与 CLIENT_SetConnectTime
		// 接口设置的登录设备超时时间和尝试次数意义相同,可选
		NetSDKLib.NET_PARAM netParam = new NetSDKLib.NET_PARAM();
		// 登录时尝试建立链接的超时时间
		netParam.nConnectTime = 10000;
		// 设置子连接的超时时间
		netParam.nGetConnInfoTime = 3000;
		netsdk.CLIENT_SetNetworkParam(netParam);
		return true;
	}

	/**
	 * 打开 sdk log
	 */
	private static void enableLog() {
		NetSDKLib.LOG_SET_PRINT_INFO setLog = new NetSDKLib.LOG_SET_PRINT_INFO();
		File path = new File("sdklog/");
		if (!path.exists())
			path.mkdir();

		// 这里的log保存地址依据实际情况自己调整
		String logPath = path.getAbsoluteFile().getParent() + "\\sdklog\\" + "sdklog" + GetDate() + ".log";
		setLog.nPrintStrategy = 0;
		setLog.bSetFilePath = 1;
		System.arraycopy(logPath.getBytes(), 0, setLog.szLogFilePath, 0, logPath.getBytes().length);
		System.out.println(logPath);
		setLog.bSetPrintStrategy = 1;
		bLogOpen = netsdk.CLIENT_LogOpen(setLog);
		if (!bLogOpen)
			System.err.println("Failed to open NetSDK log");
	}

	/**
	 * 高安全登录
	 */
	public void loginWithHighLevel() {
		// 输入结构体参数
		NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY pstlnParam = new NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY() {
			{
				szIP = m_strIpAddr.getBytes();
				nPort = m_nPort;
				szUserName = m_strUser.getBytes();
				szPassword = m_strPassword.getBytes();
			}
		};
		// 输出结构体参数
		NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY pstOutParam = new NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY();

		// 写入sdk
		m_hLoginHandle = netsdk.CLIENT_LoginWithHighLevelSecurity(pstlnParam, pstOutParam);
		if (m_hLoginHandle.longValue() == 0) {
			System.err.printf("Login Device[%s] Port[%d]Failed. %s\n", m_strIpAddr, m_nPort,
					netsdk.CLIENT_GetLastError());
		} else {
			deviceInfo = pstOutParam.stuDeviceInfo; // 获取设备信息
			System.out.println("Login Success");
			System.out.println("Device Address：" + m_strIpAddr);
			System.out.println("设备包含：" + deviceInfo.byChanNum + "个通道");
		}
	}

	/**
	 * 退出
	 */
	public void logOut() {
		if (m_hLoginHandle.longValue() != 0) {
			netsdk.CLIENT_Logout(m_hLoginHandle);
			System.out.println("LogOut Success");
		}
	}

	/**
	 * 清理sdk环境并退出
	 */
	public static void cleanAndExit() {
		if (bLogOpen) {
			netsdk.CLIENT_LogClose(); // 关闭sdk日志打印
		}
		netsdk.CLIENT_Cleanup(); // 进程关闭时，调用一次
		System.exit(0);
	}

	/**
	 * 按通道获取设备智能业务的运行状态
	 */
	public void getChannelState() {
		NET_IN_GET_CHANNEL_STATE input = new NET_IN_GET_CHANNEL_STATE();
		input.nChannelNum = 0;// 视频通道数组个数 为0表示查询全部
		input.nChannel[0] = 0;// 通道号
		input.nChannel[1] = 1;
		input.nChannel[2] = 2;
		input.nChannel[3] = 3;
		input.nChannel[4] = 4;
		input.write();

		NET_OUT_GET_CHANNEL_STATE outPut = new NET_OUT_GET_CHANNEL_STATE();
		outPut.nMaxStateNum = 10;// 用户申请智能业务状态信息最大个数
		NET_CHANNEL_STATE_INFO_EX[] infos = new NET_CHANNEL_STATE_INFO_EX[outPut.nMaxStateNum];
		for (int i = 0; i < infos.length; i++) {
			infos[i] = new NET_CHANNEL_STATE_INFO_EX();
		}
		outPut.pstuStateEx = new Memory(infos[0].size() * infos.length);
		outPut.pstuStateEx.clear(infos[0].size() * infos.length);
		ToolKits.SetStructArrToPointerData(infos, outPut.pstuStateEx);
		outPut.write();

		boolean b = netsdk.CLIENT_GetChannelState(m_hLoginHandle, input.getPointer(), outPut.getPointer(), 3000);

		if (b) {
			System.out.println("CLIENT_GetChannelState success");
			outPut.read();
			// 用户申请智能业务状态信息最大个数
			System.out.println("用户申请智能业务状态信息最大个数:" + outPut.nMaxStateNum);
			System.out.println("智能业务状态信息实际个数:" + outPut.nStateNum);
			ToolKits.GetPointerDataToStructArr(outPut.pstuStateEx, infos);

			for (int i = 0; i < outPut.nStateNum; i++) {
				NET_CHANNEL_STATE_INFO_EX info = infos[i];
				System.out.println("通道号:" + info.nChannel);
				System.out.println("----------已开启的智能规则信息个数:" + info.nIntelliInfoNum);
				NET_INTELLI_INFO[] stuIntelliInfo = info.stuIntelliInfo;
				for (int j = 0; j < info.nIntelliInfoNum; j++) {
					NET_INTELLI_INFO net_intelli_info = stuIntelliInfo[j];
					System.out.println("智能规则类型个数:" + net_intelli_info.nTypeNum);
					Byte64Arr[] szType = net_intelli_info.szType;

					for (int m = 0; m < net_intelli_info.nTypeNum; m++) {
						Byte64Arr byte64Arr = szType[m];
						try {
							System.out.println("智能规则类型:" + new String(byte64Arr.arr, encode));
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
					}
					byte[] szClass = net_intelli_info.szClass;
					try {
						System.out.println("智能场景类型:" + new String(szClass, encode));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
				System.out.println("----------已关闭的智能规则信息个数:" + info.nClosedIntelliInfoNum);
				NET_INTELLI_INFO[] stuClosedIntelliInfo = info.stuClosedIntelliInfo;
				for (int j = 0; j < info.nClosedIntelliInfoNum; j++) {
					NET_INTELLI_INFO info1 = stuClosedIntelliInfo[j];
					System.out.println("智能规则类型个数:" + info1.nTypeNum);
					Byte64Arr[] szType = info1.szType;

					for (int m = 0; m < info1.nTypeNum; m++) {
						Byte64Arr byte64Arr = szType[m];
						try {
							System.out.println("智能规则类型:" + new String(byte64Arr.arr, encode));
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
					}
					byte[] szClass = info1.szClass;
					try {
						System.out.println("智能场景类型:" + new String(szClass, encode));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}								
			}
		} else {
			System.out.println("CLIENT_GetChannelState fail:" + ToolKits.getErrorCode());
		}
		// 手动释放内存
		long peer = Pointer.nativeValue(outPut.pstuStateEx);
		Native.free(peer);
		Pointer.nativeValue(outPut.pstuStateEx, 0);
	}
			
	/**
	 * 平台下发轮询配置，规则和场景字段需要根据实际场景配置
	 */
	public void setPollingConfig() {
		NET_IN_SET_POLLING_CONFIG input = new NET_IN_SET_POLLING_CONFIG();
		input.nConfigCnt = 1;// 平台下发轮询配置个数

		NET_SET_POLLING_CONFIG_INFO setInfo = new NET_SET_POLLING_CONFIG_INFO();
		setInfo.bEnable = 1;// 使能开关
		setInfo.nChannel = 0;// 通道号
		/**
		 * 规则配置个数
		 */
		setInfo.nRulelTypeCnt = 2;
		/**
		 * 规则配置
		 */
		ToolKits.StringToByteArray("SmokeDetection", setInfo.szRulelType[0].arr);
		ToolKits.StringToByteArray("FireDetection", setInfo.szRulelType[1].arr);
		/**
		 * 全局配置列表
		 */
		ToolKits.StringToByteArray("SmokeFire", setInfo.szGlobalTypeList[0].arr);
		/**
		 * 全局配置列表个数
		 */
		setInfo.nGlobalTypeListNum = 1;
		input.stuConfigInfos[0] = setInfo;
		input.write();

		NET_OUT_SET_POLLING_CONFIG outPut = new NET_OUT_SET_POLLING_CONFIG();
		outPut.write();
		boolean b = netsdk.CLIENT_SetPollingConfig(m_hLoginHandle,input.getPointer(), outPut.getPointer(), 3000);
		if (b) {
			System.out.println("CLIENT_SetPollingConfig success");
		} else {
			System.out.println("CLIENT_SetPollingConfig fail:" + ToolKits.getErrorCode());
		}
	}

	/******************************** 测试控制台 ***************************************/

	// 配置登陆地址，端口，用户名，密码
	private String m_strIpAddr = "172.12.1.198";
	private int m_nPort = 37777;
	private String m_strUser = "admin";
	private String m_strPassword = "admin123";

	public static void main(String[] args) {
		GetChannelStateDemo demo = new GetChannelStateDemo();
		demo.InitTest();
		demo.RunTest();
		demo.EndTest();

	}

	/**
	 * 初始化测试
	 */
	public void InitTest() {
		GetChannelStateDemo.Init();
		this.loginWithHighLevel();
	}

	/**
	 * 加载测试内容
	 */
	public void RunTest() {
		CaseMenu menu = new CaseMenu();
		// 设备通道信息获取
		menu.addItem(new CaseMenu.Item(this, "按通道获取设备智能业务的运行状态", "getChannelState"));
		menu.addItem(new CaseMenu.Item(this, "平台下发轮询配置", "setPollingConfig"));
		menu.run();
	}

	/**
	 * 结束测试
	 */
	public void EndTest() {
		System.out.println("End Test");
		this.logOut(); // 登出设备
		System.out.println("See You...");
		GetChannelStateDemo.cleanAndExit(); // 清理资源并退出
	}
	/******************************** 结束 ***************************************/

}
