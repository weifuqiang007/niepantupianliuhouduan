package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.DEV_EVENT_ANIMAL_DETECTION_INFO;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Pointer;
import java.io.File;


/**
 * @author 291189
 * @version 1.0
 * @description ERR221214026
 * @date 2022/12/19 9:57
 */
public class DriverDemo extends Initialization {

    int channel=-1;
    NetSDKLib.LLong    attachHandle=new NetSDKLib.LLong(0);
    /**
     * 订阅智能任务
     */

    public NetSDKLib.LLong AttachEventRealLoadPic() {
        // 先退订，设备不会对重复订阅作校验，重复订阅后会有重复的事件返回
        if(attachHandle.longValue()!=0){
            this.DetachEventRealLoadPic();
        }
        // 需要图片
        int bNeedPicture = 1;
        attachHandle = netSdk.CLIENT_RealLoadPictureEx(loginHandle, channel, NetSDKLib.EVENT_IVS_ALL, bNeedPicture,
                AnalyzerDataCB.getInstance(), null, null);
        if (attachHandle.longValue() != 0) {
            System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Success\n", channel);
        } else {
            System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Failed!LastError = %s\n", channel,
                    ToolKits.getErrorCode());
        }

        return attachHandle;
    }
    /**
     * 报警事件（智能）回调
     */
    private static class AnalyzerDataCB implements NetSDKLib.fAnalyzerDataCallBack {
        private final File picturePath;
        private static AnalyzerDataCB instance;

        private AnalyzerDataCB() {
            picturePath = new File("./AnalyzerPicture/");
            if (!picturePath.exists()) {
                picturePath.mkdirs();
            }
        }

        public static AnalyzerDataCB getInstance() {
            if (instance == null) {
                synchronized (AnalyzerDataCB.class) {
                    if (instance == null) {
                        instance = new AnalyzerDataCB();
                    }
                }
            }
            return instance;
        }

        @Override
        public int invoke(NetSDKLib.LLong lAnalyzerHandle, int dwAlarmType, Pointer pAlarmInfo, Pointer pBuffer, int dwBufSize,
                          Pointer dwUser, int nSequence, Pointer reserved) {
            if (lAnalyzerHandle == null || lAnalyzerHandle.longValue() == 0) {
                return -1;
            }

            switch (dwAlarmType) {
                case NetSDKLib.EVENT_IVS_TRAFFIC_DRIVERYAWN : { //开车打哈欠事件(对应DEV_EVENT_DRIVERYAWN_INFO)
                    System.out.println("开车打哈欠事件");
                    DEV_EVENT_ANIMAL_DETECTION_INFO msg = new DEV_EVENT_ANIMAL_DETECTION_INFO();

                    ToolKits.GetPointerData(pAlarmInfo, msg);

                    String time
                            = msg.UTC.toStringTime();
                    System.out.println("time:"+time+",channelID: "+msg.nChannelID);
                    //save picture
                   String imageName="driverYawn_"+msg.UTC.toStringTitle()+".jpg";
                    String  savePath=picturePath+"/"+imageName;

                    ToolKits.savePicture(pBuffer,dwBufSize,savePath);
                    System.out.println("save picture to "+savePath);

                    break;
                }
                case NetSDKLib.EVENT_IVS_TRAFFIC_TIREDPHYSIOLOGICAL: {
                    System.out.println("驾驶员疲劳驾驶事件");
                    //驾驶员疲劳驾驶事件
                    NetSDKLib.DEV_EVENT_TIREDPHYSIOLOGICAL_INFO msg = new NetSDKLib.DEV_EVENT_TIREDPHYSIOLOGICAL_INFO();
                    ToolKits.GetPointerData(pAlarmInfo, msg);
                    //事件时间
                    String time
                            = msg.UTC.toStringTime();
                    System.out.println("time:"+time+",channelID: "+msg.nChannelID);
                    //save picture
                    String  imageName="TIREDPHYSIOLOGICAL_"+msg.UTC.toStringTitle()+".jpg";
                    String  savePath=picturePath+"/"+imageName;

                    ToolKits.savePicture(pBuffer,dwBufSize,savePath);
                    System.out.println("save picture to "+savePath);
                    break;
                }

                case NetSDKLib.EVENT_IVS_TRAFFIC_TIREDLOWERHEAD: {
                    System.out.println("开车低头事件");
                    //开车低头事件
                    NetSDKLib.DEV_EVENT_TIREDLOWERHEAD_INFO msg = new NetSDKLib.DEV_EVENT_TIREDLOWERHEAD_INFO();
                    ToolKits.GetPointerData(pAlarmInfo, msg);
                    //事件时间
                    String time = msg.UTC.toStringTime();
                    System.out.println("time:"+time+",channelID: "+msg.nChannelID);
                    //save picture
                    String imageName = "TIREDLOWERHEAD_" + msg.UTC.toStringTitle() + ".jpg";
                    String   savePath = picturePath + "/" + imageName;

                    ToolKits.savePicture(pBuffer, dwBufSize, savePath);
                    System.out.println("save picture to " + savePath);
                        break;
                }
                case NetSDKLib.EVENT_IVS_TRAFFIC_DRIVERLOOKAROUND:{
                    System.out.println("开车左顾右盼事件");
                    //开车左顾右盼事件
                    NetSDKLib.DEV_EVENT_DRIVERLOOKAROUND_INFO msg=new NetSDKLib.DEV_EVENT_DRIVERLOOKAROUND_INFO();
                    ToolKits.GetPointerData(pAlarmInfo, msg);
                    //事件时间
                    String time = msg.UTC.toStringTime();
                    System.out.println("time:"+time+",channelID: "+msg.nChannelID);
                    //save picture
                    String imageName = "DRIVERLOOKAROUND_" + msg.UTC.toStringTitle() + ".jpg";
                    String   savePath = picturePath + "/" + imageName;

                    ToolKits.savePicture(pBuffer, dwBufSize, savePath);
                    System.out.println("save picture to " + savePath);
                    break;
                }
                case NetSDKLib.EVENT_IVS_TRAFFIC_DRIVER_CALLING: {
                    System.out.println("驾驶员打电话事件");
                    //驾驶员打电话事件
                    NetSDKLib.DEV_EVENT_TRAFFIC_DRIVER_CALLING msg = new NetSDKLib.DEV_EVENT_TRAFFIC_DRIVER_CALLING();
                    ToolKits.GetPointerData(pAlarmInfo, msg);
                    //事件时间
                    String time = msg.UTC.toStringTime();
                    System.out.println("time:" + time + ",channelID: " + msg.nChannelID);
                    //save picture
                    String imageName = "CALLING_" + msg.UTC.toStringTitle() + ".jpg";
                    String savePath = picturePath + "/" + imageName;

                    ToolKits.savePicture(pBuffer, dwBufSize, savePath);
                    System.out.println("save picture to " + savePath);
                    break;
                }
                case NetSDKLib.EVENT_IVS_TRAFFIC_DRIVER_SMOKING: {
                    System.out.println("驾驶员抽烟事件");
                    //驾驶员抽烟事件
                    NetSDKLib.DEV_EVENT_TRAFFIC_DRIVER_SMOKING msg = new NetSDKLib.DEV_EVENT_TRAFFIC_DRIVER_SMOKING();
                    ToolKits.GetPointerData(pAlarmInfo, msg);
                    //事件时间
                    String time = msg.UTC.toStringTime();
                    System.out.println("time:" + time + ",channelID: " + msg.nChannelID);
                    //save picture
                    String imageName = "SMOKING_" + msg.UTC.toStringTitle() + ".jpg";
                    String savePath = picturePath + "/" + imageName;

                    ToolKits.savePicture(pBuffer, dwBufSize, savePath);
                    System.out.println("save picture to " + savePath);
                    break;
                }
                case NetSDKLib.EVENT_ALARM_VIDEOBLIND: ///<  视频遮挡事件
                {
                    System.out.println("视频遮挡事件");
                    NetSDKLib.DEV_EVENT_ALARM_VIDEOBLIND msg = new NetSDKLib.DEV_EVENT_ALARM_VIDEOBLIND();
                    ToolKits.GetPointerData(pAlarmInfo, msg);
                    //事件时间
                    String time = msg.UTC.toStringTime();
                    System.out.println("time:" + time + ",channelID: " + msg.nChannelID);
                    //save picture
                    String imageName = "VIDEOBLIND_" + msg.UTC.toStringTitle() + ".jpg";
                    String savePath = picturePath + "/" + imageName;

                    ToolKits.savePicture(pBuffer, dwBufSize, savePath);
                    System.out.println("save picture to " + savePath);
                    break;
                }
                default:
                    System.out.println("其他事件--------------------"+ dwAlarmType);
                    break;
            }
            return 0;
        }
    }
    /**
     * 停止侦听智能事件
     */
    public void DetachEventRealLoadPic() {
        if (this.attachHandle.longValue() != 0) {
            netSdk.CLIENT_StopLoadPic(this.attachHandle);
        }
    }
    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "AttachEventRealLoadPic" , "AttachEventRealLoadPic")));
        menu.addItem((new CaseMenu.Item(this , "DetachEventRealLoadPic" , "DetachEventRealLoadPic")));

        menu.run();
    }

    public static void main(String[] args) {
        DriverDemo demo=new DriverDemo();
        InitTest("10.55.161.162",37777,"admin","admin123");
        demo.RunTest();
        LoginOut();

    }

}
