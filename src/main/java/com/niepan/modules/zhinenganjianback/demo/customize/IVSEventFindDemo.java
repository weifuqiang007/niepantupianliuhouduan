package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.structure.*;
import com.niepan.modules.zhinenganjianback.lib.utils.Initialization;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;

import static com.niepan.modules.zhinenganjianback.lib.NetSDKLib.*;

/**
 * @author 291189
 * @version 1.0
 * @description ERR221010124
 * @date 2022/11/2 14:09
 */
public class IVSEventFindDemo extends Initialization {

    LLong findlLong =new LLong(0);
    int dwCount=0;
    //智能事件开始查询
    public void  IVSEventFind(){

        NET_IN_IVSEVENT_FIND_INFO stIn=new NET_IN_IVSEVENT_FIND_INFO();

        //设置 开始时间
        stIn.stStartTime.dwYear   = 2022;
        stIn.stStartTime.dwMonth  = 11;
        stIn.stStartTime.dwDay    = 2;
        stIn.stStartTime.dwHour   = 8;
        stIn.stStartTime.dwMinute = 55;
        stIn.stStartTime.dwSecond = 0;


        //设置 结束时间
        stIn.stEndTime.dwYear   = 2022;
        stIn.stEndTime.dwMonth  = 11;
        stIn.stEndTime.dwDay    = 2;
        stIn.stEndTime.dwHour   = 20;
        stIn.stEndTime.dwMinute = 0;
        stIn.stEndTime.dwSecond = 0;
        /**
         报警诊断类型
         0:全部
         1:未处理
         2:正确
         3:错误
         4:重复正确报警
         5:重复错误报警
         */
        stIn.nVaild = 0;
        /**
         nChannelList中有效通道号个数,填0表示查询所有通道号
         */
        stIn.dwChannelCnt = 0;
        /**
         通道号列表
         */
        stIn.nChannelList[0] = 0;

        stIn.dwEventCodeCnt = 4;
        stIn.dwEventCodeList[0] = EVENT_IVS_LEAVEDETECTION;
        stIn.dwEventCodeList[1] = EVENT_IVS_CONVEYER_BELT_NONLOAD;
        stIn.dwEventCodeList[2] = EVENT_IVS_CONVEYER_BELT_BULK;
        stIn.dwEventCodeList[3] = EVENT_IVS_CONVEYER_BELT_RUNOFF;

        Pointer stTnPointer
                = new Memory(stIn.size());
          stTnPointer.clear(stIn.size());

        ToolKits.SetStructDataToPointer(stIn,stTnPointer,0);


        NET_OUT_IVSEVENT_FIND_INFO stOut=new NET_OUT_IVSEVENT_FIND_INFO();
        Pointer stOutPointer
                = new Memory(stOut.size());
        stOutPointer.clear(stOut.size());

        ToolKits.SetStructDataToPointer(stOut,stOutPointer,0);

        findlLong   = netSdk.CLIENT_IVSEventFind(loginHandle, stTnPointer, stOutPointer, 3000);

        if (findlLong.longValue()==0) {
            System.out.println("CLIENT_IVSEventFind failed. error is " + ToolKits.getErrorCode());
            return;
        }else {
            System.out.println("CLIENT_IVSEventFind Succeed");
        }

        ToolKits.GetPointerData(stOutPointer,stOut);


        dwCount  = stOut.dwCount;
        System.out.println("dwCount:"+dwCount);

    }

    /**
     * 智能事件信息查询
     */
    public void IVSEventNextFind(){

        NET_IN_IVSEVENT_NEXTFIND_INFO stNextFindIn=new NET_IN_IVSEVENT_NEXTFIND_INFO();
        /**
         单次查询返回数量
         */
        int  limit=5;
        stNextFindIn.nLimit = limit;
        /**
         查询起点偏移
         */
        stNextFindIn.nStartIndex = 0;

        Pointer stTnPointer
                = new Memory(stNextFindIn.size());
        stTnPointer.clear(stNextFindIn.size());

        ToolKits.SetStructDataToPointer(stNextFindIn,stTnPointer,0);


        NET_OUT_IVSEVENT_NEXTFIND_INFO stNextFindOut=new NET_OUT_IVSEVENT_NEXTFIND_INFO();
        /**
         pstuEventInfo的个数, 等于 NET_IN_IVSEVENT_NEXTFIND_INFO 的nLimit字段
         */

        stNextFindOut.dwEventCnt = limit;

        NET_IVSEVENT_EVENT_INFO[] infos=new NET_IVSEVENT_EVENT_INFO[limit];
        for(int i=0;i<infos.length;i++){
            infos[i]=new NET_IVSEVENT_EVENT_INFO();
        }

        /**
         事件信息数组,用户分配内存,大小为sizeof(NET_IVSEVENT_EVENT_INFO)*dwEventCnt
         */
        stNextFindOut.pstuEventInfo = new Memory(infos[0].size()*stNextFindOut.dwEventCnt);

        stNextFindOut.pstuEventInfo.clear(infos[0].size()*stNextFindOut.dwEventCnt);


        Pointer stOutPointer = new Memory(stNextFindOut.size());
        stOutPointer.clear(stNextFindOut.size());
        ToolKits.SetStructDataToPointer(stNextFindOut,stOutPointer,0);

        boolean b = netSdk.CLIENT_IVSEventNextFind(findlLong, stTnPointer, stOutPointer, 3000);

        if (!b) {
            System.out.println("CLIENT_IVSEventNextFind failed. error is " + ToolKits.getErrorCode());
            return;
        }else{
            System.out.println("CLIENT_IVSEventNextFind Succeed");
        }

        ToolKits.GetPointerData(stOutPointer,stNextFindOut);
        /**
         返回的pstuEventInfo个数
         */
        int dwRetEventCnt
                = stNextFindOut.dwRetEventCnt;

        System.out.println("dwRetEventCnt:"+dwRetEventCnt);

            ToolKits.GetPointerDataToStructArr(stNextFindOut.pstuEventInfo,infos);

        for(int i=0;i<dwRetEventCnt;i++){
            NET_IVSEVENT_EVENT_INFO info = infos[i];
            /**
             通道号 从0开始,-1表示通道号未知
             */
            System.out.println("nChannel:"+info.nChannel);
            /**
             事件编号
             */
            System.out.println("nId:"+info.nId);
            /**
             事件类型,值含义参考 EVENT_IVS_ALL 下面的智能事件类型宏定义
             */
            System.out.println("dwEventCode:"+info.dwEventCode);

             /**
             报警诊断类型
             0:未知
             1:未处理
             2:正确
             3:错误
             4:重复正确报警
             5:重复错误报警
             */
            System.out.println("nVaild:"+info.nVaild);
            /**
             * 事件发生的时间
             */
            System.out.println("stuTime:"+info.stuTime.toStringTime());
        }

    }

    /**
     * 智能事件结束查询
     */

    public void IVSEventFindClose(){

        boolean b
                = netSdk.CLIENT_IVSEventFindClose(findlLong);

        if (!b) {
            System.out.println("CLIENT_IVSEventFindClose failed. error is " + ToolKits.getErrorCode());
            return;
        }else {
            System.out.println("CLIENT_IVSEventFindClose Succeed");
        }
    }

    public void RunTest()
    {
        System.out.println("Run Test");

        CaseMenu menu = new CaseMenu();;

        menu.addItem((new CaseMenu.Item(this , "智能事件开始查询" , "IVSEventFind")));
        menu.addItem((new CaseMenu.Item(this , "智能事件信息查询" , "IVSEventNextFind")));

        menu.addItem((new CaseMenu.Item(this , "智能事件结束查询" , "IVSEventFindClose")));

        menu.run();
    }

    public static void main(String[] args) {
        IVSEventFindDemo ivsEventFindDemo=new IVSEventFindDemo();
        InitTest("172.25.239.123",37777,"admin","admin321@");
        ivsEventFindDemo.RunTest();
        LoginOut();
    }
}
