package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.ToolKits;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultDisconnectCallback;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultHaveReconnectCallBack;
import com.niepan.modules.zhinenganjianback.lib.structure.DEV_EVENT_TRAFFIC_TRAFFICCAR_INFO;
import com.niepan.modules.zhinenganjianback.lib.structure.NET_TIME_EX;
import com.niepan.modules.zhinenganjianback.lib.structure.*;
import com.sun.jna.Pointer;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;

import static com.niepan.modules.zhinenganjianback.lib.NetSDKLib.*;
import static com.niepan.modules.zhinenganjianback.lib.Utils.getOsPrefix;
/**
 * @author 291189
 * @version 1.0
 * @description GIP230209011
 * @date 2023/2/15 9:20
 */
public class EmergencyDemo {
    // SDk对象初始化
    public static final NetSDKLib netsdk = NetSDKLib.NETSDK_INSTANCE;
    public static final NetSDKLib configsdk = NetSDKLib.CONFIG_INSTANCE;

    // 判断是否初始化
    private static boolean bInit = false;
    // 判断log是否打开
    private static boolean bLogOpen = false;
    // 设备信息
    private NetSDKLib.NET_DEVICEINFO_Ex deviceInfo = new NetSDKLib.NET_DEVICEINFO_Ex();
    // 登录句柄
    private NetSDKLib.LLong m_hLoginHandle = new NetSDKLib.LLong(0);

    // 智能事件订阅句柄
    private NetSDKLib.LLong m_attachHandle = new NetSDKLib.LLong(0);

    // 回调函数需要是静态的，防止被系统回收
    // 断线回调
    private static NetSDKLib.fDisConnect disConnectCB = DefaultDisconnectCallback.getINSTANCE();
    // 重连回调
    private static NetSDKLib.fHaveReConnect haveReConnectCB = DefaultHaveReconnectCallBack.getINSTANCE();

    // 编码格式
    public static String encode;

    static {
        String osPrefix = getOsPrefix();
        if (osPrefix.toLowerCase().startsWith("win32-amd64")) {
            encode = "GBK";
        } else if (osPrefix.toLowerCase().startsWith("linux-amd64")) {
            encode = "UTF-8";
        }
    }

    /**
     * 获取当前时间
     */
    public static String GetDate() {
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDate.format(new java.util.Date()).replaceAll("[^0-9]", "-");
    }

    /**
     * 初始化SDK库
     */
    public static boolean Init() {
        bInit = netsdk.CLIENT_Init(disConnectCB, null);// 进程启动时，初始化一次
        if (!bInit) {
            System.out.println("Initialize SDK failed");
            return false;
        }
        // 配置日志
        EmergencyDemo.enableLog();

        // 设置断线重连回调接口, 此操作为可选操作，但建议用户进行设置
        netsdk.CLIENT_SetAutoReconnect(haveReConnectCB, null);

        // 设置登录超时时间和尝试次数，可选
        // 登录请求响应超时时间设置为3S
        int waitTime = 3000;
        // 登录时尝试建立链接 1 次
        int tryTimes = 1;
        netsdk.CLIENT_SetConnectTime(waitTime, tryTimes);
        // 设置更多网络参数， NET_PARAM 的nWaittime ， nConnectTryNum 成员与 CLIENT_SetConnectTime
        // 接口设置的登录设备超时时间和尝试次数意义相同,可选
        NetSDKLib.NET_PARAM netParam = new NetSDKLib.NET_PARAM();
        // 登录时尝试建立链接的超时时间
        netParam.nConnectTime = 10000;
        // 设置子连接的超时时间
        netParam.nGetConnInfoTime = 3000;
        netsdk.CLIENT_SetNetworkParam(netParam);
        return true;
    }

    /**
     * 打开 sdk log
     */
    private static void enableLog() {
        NetSDKLib.LOG_SET_PRINT_INFO setLog = new NetSDKLib.LOG_SET_PRINT_INFO();
        File path = new File("sdklog/");
        if (!path.exists())
            path.mkdir();

        // 这里的log保存地址依据实际情况自己调整
        String logPath = path.getAbsoluteFile().getParent() + "\\sdklog\\" + "sdklog" + GetDate() + ".log";
        setLog.nPrintStrategy = 0;
        setLog.bSetFilePath = 1;
        System.arraycopy(logPath.getBytes(), 0, setLog.szLogFilePath, 0, logPath.getBytes().length);
        System.out.println(logPath);
        setLog.bSetPrintStrategy = 1;
        bLogOpen = netsdk.CLIENT_LogOpen(setLog);
        if (!bLogOpen)
            System.err.println("Failed to open NetSDK log");
    }

    /**
     * 高安全登录
     */
    public void loginWithHighLevel() {
        // 输入结构体参数
        NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY pstlnParam = new NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY() {
            {
                szIP = m_strIpAddr.getBytes();
                nPort = m_nPort;
                szUserName = m_strUser.getBytes();
                szPassword = m_strPassword.getBytes();
            }
        };
        // 输出结构体参数
        NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY pstOutParam = new NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY();

        // 写入sdk
        m_hLoginHandle = netsdk.CLIENT_LoginWithHighLevelSecurity(pstlnParam, pstOutParam);
        if (m_hLoginHandle.longValue() == 0) {
            System.err.printf("Login Device[%s] Port[%d]Failed. %s\n", m_strIpAddr, m_nPort,
                    netsdk.CLIENT_GetLastError());
        } else {
            deviceInfo = pstOutParam.stuDeviceInfo; // 获取设备信息
            System.out.println("Login Success");
            System.out.println("Device Address：" + m_strIpAddr);
            System.out.println("设备包含：" + deviceInfo.byChanNum + "个通道");
        }
    }

    /**
     * 退出
     */
    public void logOut() {
        if (m_hLoginHandle.longValue() != 0) {
            netsdk.CLIENT_Logout(m_hLoginHandle);
            System.out.println("LogOut Success");
        }
    }

    /**
     * 清理sdk环境并退出
     */
    public static void cleanAndExit() {
        if (bLogOpen) {
            netsdk.CLIENT_LogClose(); // 关闭sdk日志打印
        }
        netsdk.CLIENT_Cleanup(); // 进程关闭时，调用一次
        System.exit(0);
    }


    /**
     * 选择通道
     */
    private int channelId = -1;// 逻辑通道


    /**
     * 订阅智能任务
     */
    public void AttachEventRealLoadPic() {
        // 先退订，设备不会对重复订阅作校验，重复订阅后会有重复的事件返回
        this.DetachEventRealLoadPic();
        // 需要图片
        int bNeedPicture = 1;
                                                                                 //EVENT_IVS_ALL 表示订阅所有智能事件
        m_attachHandle = netsdk.CLIENT_RealLoadPictureEx(m_hLoginHandle, channelId, EVENT_IVS_ALL, bNeedPicture, AnalyzerDataCB.getInstance(), null, null);
    /*                                                                             // EVENT_IVS_TRAFFIC_TURN_SHARP 表示订阅 急转弯事件
        m_attachHandle = netsdk.CLIENT_RealLoadPictureEx(m_hLoginHandle, channelId, EVENT_IVS_TRAFFIC_TURN_SHARP, bNeedPicture, AnalyzerDataCB.getInstance(), null, null);
                                                                                   // EVENT_IVS_TRAFFIC_ACCELERATION_RAPID 表示订阅  急加速事件
        m_attachHandle = netsdk.CLIENT_RealLoadPictureEx(m_hLoginHandle, channelId, EVENT_IVS_TRAFFIC_ACCELERATION_RAPID, bNeedPicture, AnalyzerDataCB.getInstance(), null, null);
                                                                                    //EVENT_IVS_TRAFFIC_SPEED_DROP_SHARPLY 急减速事件
        m_attachHandle = netsdk.CLIENT_RealLoadPictureEx(m_hLoginHandle, channelId, EVENT_IVS_TRAFFIC_SPEED_DROP_SHARPLY, bNeedPicture, AnalyzerDataCB.getInstance(), null, null);


*/
        if (m_attachHandle.longValue() != 0) {
            System.out.printf("Chn[%d] CLIENT_RealLoadPictureEx Success\n", channelId);
        } else {
            System.out.printf("Ch[%d] CLIENT_RealLoadPictureEx Failed!LastError = %s\n", channelId,
                    ToolKits.getErrorCode());
        }
    }

    /**
     * 报警事件（智能）回调
     */
    private static class AnalyzerDataCB implements NetSDKLib.fAnalyzerDataCallBack {
        private final File picturePath;
        private static AnalyzerDataCB instance;

        private AnalyzerDataCB() {
            picturePath = new File("./AnalyzerPicture/");
            if (!picturePath.exists()) {
                picturePath.mkdirs();
            }
        }

        public static AnalyzerDataCB getInstance() {
            if (instance == null) {
                synchronized (AnalyzerDataCB.class) {
                    if (instance == null) {
                        instance = new AnalyzerDataCB();
                    }
                }
            }
            return instance;
        }

        @Override
        public int invoke(NetSDKLib.LLong lAnalyzerHandle, int dwAlarmType, Pointer pAlarmInfo, Pointer pBuffer, int dwBufSize,
                          Pointer dwUser, int nSequence, Pointer reserved) {
            if (lAnalyzerHandle == null || lAnalyzerHandle.longValue() == 0) {
                return -1;
            }

            switch (dwAlarmType) {
                case EVENT_IVS_TRAFFIC_ACCELERATION_RAPID : {// 急加速事件(对应 NET_DEV_EVENT_TRAFFIC_ACCELERATION_RAPID_INFO)
                    System.out.println("急加速事件");
                    NET_DEV_EVENT_TRAFFIC_ACCELERATION_RAPID_INFO msg = new NET_DEV_EVENT_TRAFFIC_ACCELERATION_RAPID_INFO();
                    ToolKits.GetPointerData(pAlarmInfo, msg);

                    System.out.println("nChannelID:"+ msg.nChannelID);

                    System.out.println("nAction:"+ msg.nAction);

                    /**
                     事件名称
                     */
                    byte[] szName = msg.szName;
                    try {
                        System.out.println("szName:"+ new String(szName,encode));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    /**
                     事件发生的时间
                     */
                    NET_TIME_EX stuUTC = msg.stuUTC;
                    System.out.println("stuUTC:"+ stuUTC.toStringTime());
                    /**
                     事件编号，用来唯一标志一个事件
                     */
                    int nEventID = msg.nEventID;
                    System.out.println("nEventID:"+ nEventID);

                    /**
                     车辆信息
                     */
                    NET_MSG_OBJECT stuVehicle = msg.stuVehicle;

                    int rgbaMainColor = stuVehicle.rgbaMainColor;
                    System.out.println("rgbaMainColor:"+ rgbaMainColor);
                   //  物体上相关的带0结束符文本,比如车牌,集装箱号等等
                    byte[] szText = stuVehicle.szText;
                    // 物体子类别,根据不同的物体类型,可以取以下子类型：
                    byte[] szObjectSubType = stuVehicle.szObjectSubType;

                    try {
                        System.out.println("szText:"+ new String(szText,encode));

                        System.out.println("szObjectSubType:"+ new String(szObjectSubType,encode));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    byte bPicEnble
                            = stuVehicle.bPicEnble;
                    System.out.println("bPicEnble:"+ bPicEnble);
                    if(bPicEnble==1){
                        NET_PIC_INFO stPicInfo
                                = stuVehicle.stPicInfo;
                        if (stPicInfo!=null&&stPicInfo.dwFileLenth > 0) {
                            String picture = picturePath + "\\" + "ACCELERATION_RAPID_stuVehicle_"+System.currentTimeMillis() + ".jpg";
                            ToolKits.savePicture(pBuffer, stPicInfo.dwOffSet, stPicInfo.dwFileLenth, picture);
                        }

                    }

                    /**
                     车牌信息
                     */
                    NET_MSG_OBJECT stuObject = msg.stuObject;
                     bPicEnble = stuObject.bPicEnble;
                    System.out.println("bPicEnble:"+ bPicEnble);
                    if(bPicEnble==1){
                        NET_PIC_INFO stPicInfo
                                = stuObject.stPicInfo;
                        if (stPicInfo!=null&&stPicInfo.dwFileLenth > 0) {
                            String picture = picturePath + "\\" + "stuObject_"+System.currentTimeMillis() + ".jpg";
                            ToolKits.savePicture(pBuffer, stPicInfo.dwOffSet, stPicInfo.dwFileLenth, picture);
                        }
                    }

                    /**
                     表示交通车辆的数据库记录
                     */
                     DEV_EVENT_TRAFFIC_TRAFFICCAR_INFO stuTrafficCar=msg.stuTrafficCar;

                    /**
                     * 车牌号码
                     */

                    byte[] szPlateNumber = stuTrafficCar.szPlateNumber;

                    /**
                     * 号牌类型 "Unknown" 未知; "Normal" 蓝牌黑牌; "Yellow" 黄牌; "DoubleYellow" 双层黄尾牌
                     * "Police" 警牌;"SAR" 港澳特区号牌; "Trainning" 教练车号牌; "Personal" 个性号牌; "Agri" 农用牌
                     * "Embassy" 使馆号牌; "Moto" 摩托车号牌; "Tractor" 拖拉机号牌; "Other" 其他号牌
                     */
                    byte[] szPlateType = stuTrafficCar.szPlateType;

                    /**
                     * 车牌颜色    "Blue","Yellow", "White","Black","YellowbottomBlackText","BluebottomWhiteText","BlackBottomWhiteText","ShadowGreen","YellowGreen"
                     */
                     byte[] szPlateColor=stuTrafficCar.szPlateColor;

                    /**
                     * 车身颜色    "White", "Black", "Red", "Yellow", "Gray", "Blue","Green"
                     */
                    byte[] szVehicleColor = stuTrafficCar.szVehicleColor;

                    /**
                     * 速度    单位Km/H
                     */

                    System.out.println("nSpeed:"+stuTrafficCar.nSpeed);
                    /**
                     * 触发的相关事件    参见事件列表Event List,只包含交通相关事件。
                     */
                    byte[] szEvent
                            = stuTrafficCar.szEvent;

                    try {
                        System.out.println("szPlateNumber:"+new String(szPlateNumber,encode));

                        System.out.println("szPlateType:"+new String(szPlateType,encode));

                        System.out.println("szPlateColor:"+new String(szPlateColor,encode));

                        System.out.println("szVehicleColor:"+new String(szVehicleColor,encode));

                        System.out.println("szEvent:"+new String(szEvent,encode));

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case EVENT_IVS_TRAFFIC_TURN_SHARP  : {// 急转弯事件(对应 NET_DEV_EVENT_TRAFFIC_TURN_SHARP_INFO)
                    System.out.println("急转弯事件");
                    NET_DEV_EVENT_TRAFFIC_TURN_SHARP_INFO msg = new NET_DEV_EVENT_TRAFFIC_TURN_SHARP_INFO();
                    ToolKits.GetPointerData(pAlarmInfo, msg);
                    System.out.println("nChannelID:"+ msg.nChannelID);

                    System.out.println("nAction:"+ msg.nAction);

                    /**
                     事件名称
                     */
                    byte[] szName = msg.szName;
                    try {
                        System.out.println("szName:"+ new String(szName,encode));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    /**
                     事件发生的时间
                     */
                    NET_TIME_EX stuUTC = msg.stuUTC;
                    System.out.println("stuUTC:"+ stuUTC.toStringTime());
                    /**
                     事件编号，用来唯一标志一个事件
                     */
                    int nEventID = msg.nEventID;
                    System.out.println("nEventID:"+ nEventID);

                    /**
                     车辆信息
                     */
                    NET_MSG_OBJECT stuVehicle = msg.stuVehicle;

                    int rgbaMainColor = stuVehicle.rgbaMainColor;
                    System.out.println("rgbaMainColor:"+ rgbaMainColor);
                    //  物体上相关的带0结束符文本,比如车牌,集装箱号等等
                    byte[] szText = stuVehicle.szText;
                    // 物体子类别,根据不同的物体类型,可以取以下子类型：
                    byte[] szObjectSubType = stuVehicle.szObjectSubType;

                    try {
                        System.out.println("szText:"+ new String(szText,encode));

                        System.out.println("szObjectSubType:"+ new String(szObjectSubType,encode));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    byte bPicEnble
                            = stuVehicle.bPicEnble;
                    System.out.println("bPicEnble:"+ bPicEnble);
                    if(bPicEnble==1){
                        NET_PIC_INFO stPicInfo
                                = stuVehicle.stPicInfo;
                        if (stPicInfo!=null&&stPicInfo.dwFileLenth > 0) {
                            String picture = picturePath + "\\" + "TURN_SHARP_stuVehicle_"+System.currentTimeMillis() + ".jpg";
                            ToolKits.savePicture(pBuffer, stPicInfo.dwOffSet, stPicInfo.dwFileLenth, picture);
                        }

                    }

                    /**
                     车牌信息
                     */
                    NET_MSG_OBJECT stuObject = msg.stuObject;
                    bPicEnble = stuObject.bPicEnble;
                    System.out.println("bPicEnble:"+ bPicEnble);
                    if(bPicEnble==1){
                        NET_PIC_INFO stPicInfo
                                = stuObject.stPicInfo;
                        if (stPicInfo!=null&&stPicInfo.dwFileLenth > 0) {
                            String picture = picturePath + "\\" + "stuObject_"+System.currentTimeMillis() + ".jpg";
                            ToolKits.savePicture(pBuffer, stPicInfo.dwOffSet, stPicInfo.dwFileLenth, picture);
                        }
                    }

                    /**
                     表示交通车辆的数据库记录
                     */
                    DEV_EVENT_TRAFFIC_TRAFFICCAR_INFO stuTrafficCar=msg.stuTrafficCar;

                    /**
                     * 车牌号码
                     */

                    byte[] szPlateNumber = stuTrafficCar.szPlateNumber;

                    /**
                     * 号牌类型 "Unknown" 未知; "Normal" 蓝牌黑牌; "Yellow" 黄牌; "DoubleYellow" 双层黄尾牌
                     * "Police" 警牌;"SAR" 港澳特区号牌; "Trainning" 教练车号牌; "Personal" 个性号牌; "Agri" 农用牌
                     * "Embassy" 使馆号牌; "Moto" 摩托车号牌; "Tractor" 拖拉机号牌; "Other" 其他号牌
                     */
                    byte[] szPlateType = stuTrafficCar.szPlateType;

                    /**
                     * 车牌颜色    "Blue","Yellow", "White","Black","YellowbottomBlackText","BluebottomWhiteText","BlackBottomWhiteText","ShadowGreen","YellowGreen"
                     */
                    byte[] szPlateColor=stuTrafficCar.szPlateColor;

                    /**
                     * 车身颜色    "White", "Black", "Red", "Yellow", "Gray", "Blue","Green"
                     */
                    byte[] szVehicleColor = stuTrafficCar.szVehicleColor;

                    /**
                     * 速度    单位Km/H
                     */

                    System.out.println("nSpeed:"+stuTrafficCar.nSpeed);
                    /**
                     * 触发的相关事件    参见事件列表Event List,只包含交通相关事件。
                     */
                    byte[] szEvent
                            = stuTrafficCar.szEvent;

                    try {
                        System.out.println("szPlateNumber:"+new String(szPlateNumber,encode));

                        System.out.println("szPlateType:"+new String(szPlateType,encode));

                        System.out.println("szPlateColor:"+new String(szPlateColor,encode));

                        System.out.println("szVehicleColor:"+new String(szVehicleColor,encode));

                        System.out.println("szEvent:"+new String(szEvent,encode));

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    break;
                } case EVENT_IVS_TRAFFIC_SPEED_DROP_SHARPLY : {// 车辆速度剧减事件(对应 DEV_EVENT_TRAFFIC_SPEED_DROP_SHARPLY_INFO)
                        System.out.println("车辆速度剧减事件");
                        DEV_EVENT_TRAFFIC_SPEED_DROP_SHARPLY_INFO msg = new DEV_EVENT_TRAFFIC_SPEED_DROP_SHARPLY_INFO();
                        ToolKits.GetPointerData(pAlarmInfo, msg);

                        System.out.println("nChannelID:"+ msg.nChannelID);

                        System.out.println("nAction:"+ msg.nAction);

                        /**
                         事件名称
                         */
                        byte[] szName = msg.szName;
                        try {
                            System.out.println("szName:"+ new String(szName,encode));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        /**
                         事件发生的时间
                         */
                        NET_TIME_EX stuUTC = msg.stuUTC;
                        System.out.println("stuUTC:"+ stuUTC.toStringTime());
                        /**
                         事件编号，用来唯一标志一个事件
                         */
                        int nEventID = msg.nEventID;
                        System.out.println("nEventID:"+ nEventID);

                        /**
                         车辆信息
                         */
                        NET_MSG_OBJECT stuVehicle = msg.stuVehicle;

                        int rgbaMainColor = stuVehicle.rgbaMainColor;
                        System.out.println("rgbaMainColor:"+ rgbaMainColor);
                        //  物体上相关的带0结束符文本,比如车牌,集装箱号等等
                        byte[] szText = stuVehicle.szText;
                        // 物体子类别,根据不同的物体类型,可以取以下子类型：
                        byte[] szObjectSubType = stuVehicle.szObjectSubType;

                        try {
                            System.out.println("szText:"+ new String(szText,encode));

                            System.out.println("szObjectSubType:"+ new String(szObjectSubType,encode));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        byte bPicEnble
                                = stuVehicle.bPicEnble;
                        System.out.println("bPicEnble:"+ bPicEnble);
                        if(bPicEnble==1){
                            NET_PIC_INFO stPicInfo
                                    = stuVehicle.stPicInfo;
                            if (stPicInfo!=null&&stPicInfo.dwFileLenth > 0) {
                                String picture = picturePath + "\\" + "ACCELERATION_RAPID_stuVehicle_"+System.currentTimeMillis() + ".jpg";
                                ToolKits.savePicture(pBuffer, stPicInfo.dwOffSet, stPicInfo.dwFileLenth, picture);
                            }

                        }

                        /**
                         车牌信息
                         */
                        NET_MSG_OBJECT stuObject = msg.stuObject;
                        bPicEnble = stuObject.bPicEnble;
                        System.out.println("bPicEnble:"+ bPicEnble);
                        if(bPicEnble==1){
                            NET_PIC_INFO stPicInfo
                                    = stuObject.stPicInfo;
                            if (stPicInfo!=null&&stPicInfo.dwFileLenth > 0) {
                                String picture = picturePath + "\\" + "stuObject_"+System.currentTimeMillis() + ".jpg";
                                ToolKits.savePicture(pBuffer, stPicInfo.dwOffSet, stPicInfo.dwFileLenth, picture);
                            }
                        }

                        /**
                         /**
                         交通车辆信息
                         */
                        DEV_EVENT_TRAFFIC_TRAFFICCAR_INFO stuTrafficCar=msg.stTrafficCar;

                        /**
                         * 车牌号码
                         */

                        byte[] szPlateNumber = stuTrafficCar.szPlateNumber;

                        /**
                         * 号牌类型 "Unknown" 未知; "Normal" 蓝牌黑牌; "Yellow" 黄牌; "DoubleYellow" 双层黄尾牌
                         * "Police" 警牌;"SAR" 港澳特区号牌; "Trainning" 教练车号牌; "Personal" 个性号牌; "Agri" 农用牌
                         * "Embassy" 使馆号牌; "Moto" 摩托车号牌; "Tractor" 拖拉机号牌; "Other" 其他号牌
                         */
                        byte[] szPlateType = stuTrafficCar.szPlateType;

                        /**
                         * 车牌颜色    "Blue","Yellow", "White","Black","YellowbottomBlackText","BluebottomWhiteText","BlackBottomWhiteText","ShadowGreen","YellowGreen"
                         */
                        byte[] szPlateColor=stuTrafficCar.szPlateColor;

                        /**
                         * 车身颜色    "White", "Black", "Red", "Yellow", "Gray", "Blue","Green"
                         */
                        byte[] szVehicleColor = stuTrafficCar.szVehicleColor;

                        /**
                         * 速度    单位Km/H
                         */

                        System.out.println("nSpeed:"+stuTrafficCar.nSpeed);
                        /**
                         * 触发的相关事件    参见事件列表Event List,只包含交通相关事件。
                         */
                        byte[] szEvent
                                = stuTrafficCar.szEvent;

                        try {
                            System.out.println("szPlateNumber:"+new String(szPlateNumber,encode));

                            System.out.println("szPlateType:"+new String(szPlateType,encode));

                            System.out.println("szPlateColor:"+new String(szPlateColor,encode));

                            System.out.println("szVehicleColor:"+new String(szVehicleColor,encode));

                            System.out.println("szEvent:"+new String(szEvent,encode));

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        break;

                }
                default:
                    System.out.println("其他事件--------------------"+ dwAlarmType);
                    break;
            }
            return 0;
        }
    }

    /**
     * 停止侦听智能事件
     */
    public void DetachEventRealLoadPic() {
        if (m_attachHandle.longValue() != 0) {
            netsdk.CLIENT_StopLoadPic(m_attachHandle);
        }
    }
    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "AttachEventRealLoadPic" , "AttachEventRealLoadPic")));
        menu.addItem((new CaseMenu.Item(this , "DetachEventRealLoadPic" , "DetachEventRealLoadPic")));

        menu.run();
    }
    // 配置登陆地址，端口，用户名，密码
    private String m_strIpAddr = "10.33.121.89";
    private int m_nPort = 37778;
    private String m_strUser = "admin";
    private String m_strPassword = "admin123";
    public static void main(String[] args) {
        EmergencyDemo demo=new EmergencyDemo();
        demo.InitTest();
        demo.RunTest();
        demo.EndTest();

    }

    /**
     * 初始化测试
     */
    public void InitTest() {
        EmergencyDemo.Init();
        this.loginWithHighLevel();
    }

    /**
     * 结束测试
     */
    public void EndTest() {
        System.out.println("End Test");
        this.logOut(); // 登出设备
        System.out.println("See You...");
        EmergencyDemo.cleanAndExit(); // 清理资源并退出
    }
}
