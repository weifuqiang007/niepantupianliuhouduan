package com.niepan.modules.zhinenganjianback.demo.customize.JordanPSD;

import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;

/**
 * 下载进度信息
 *
 * @author 47040
 * @since Created at 2021/5/31 16:11
 */
public class DownloadPosInfo {

    public NetSDKLib.LLong downloadPosHandle;

    public int totalSize;

    public int downloadSize;

    public double downloadPos;

}
