package com.niepan.modules.zhinenganjianback.demo.customize;

import com.niepan.modules.zhinenganjianback.demo.util.CaseMenu;
import com.niepan.modules.zhinenganjianback.lib.NetSDKLib;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultDisconnectCallback;
import com.niepan.modules.zhinenganjianback.lib.callback.impl.DefaultHaveReconnectCallBack;
import com.niepan.modules.zhinenganjianback.VO.UpgradeVO;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import static com.niepan.modules.zhinenganjianback.lib.Utils.getOsPrefix;

/**
 * @author 291189
 * @version 1.0
 * @description ERR230307019
 * @date 2023/3/7 9:33
 */
@Component
public class StartUpgradeEx2Demo {
    // SDk对象初始化
    public static final NetSDKLib netsdk = NetSDKLib.NETSDK_INSTANCE;
    public static final NetSDKLib configsdk = NetSDKLib.CONFIG_INSTANCE;

    // 判断是否初始化
    private static boolean bInit = false;
    // 判断log是否打开
    private static boolean bLogOpen = false;
    // 设备信息
    private NetSDKLib.NET_DEVICEINFO_Ex deviceInfo = new NetSDKLib.NET_DEVICEINFO_Ex();
    // 登录句柄
    private NetSDKLib.LLong m_hLoginHandle = new NetSDKLib.LLong(0);


    // 回调函数需要是静态的，防止被系统回收
    // 断线回调
    private static NetSDKLib.fDisConnect disConnectCB = DefaultDisconnectCallback.getINSTANCE();
    // 重连回调
    private static NetSDKLib.fHaveReConnect haveReConnectCB = DefaultHaveReconnectCallBack.getINSTANCE();

    // 编码格式
    public static String encode;

    static {
        String osPrefix = getOsPrefix();
        if (osPrefix.toLowerCase().startsWith("win32-amd64")) {
            encode = "GBK";
        } else if (osPrefix.toLowerCase().startsWith("linux-amd64")) {
            encode = "UTF-8";
        }
    }

    /**
     * 获取当前时间
     */
    public static String GetDate() {
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDate.format(new java.util.Date()).replaceAll("[^0-9]", "-");
    }

    /**
     * 初始化SDK库
     */
    public static boolean Init() {
        bInit = netsdk.CLIENT_Init(disConnectCB, null);// 进程启动时，初始化一次
        if (!bInit) {
            System.out.println("Initialize SDK failed");
            return false;
        }
        // 配置日志
        StartUpgradeEx2Demo.enableLog();

        // 设置断线重连回调接口, 此操作为可选操作，但建议用户进行设置
        netsdk.CLIENT_SetAutoReconnect(haveReConnectCB, null);

        // 设置登录超时时间和尝试次数，可选
        // 登录请求响应超时时间设置为3S
        int waitTime = 3000;
        // 登录时尝试建立链接 1 次
        int tryTimes = 1;
        netsdk.CLIENT_SetConnectTime(waitTime, tryTimes);
        // 设置更多网络参数， NET_PARAM 的nWaittime ， nConnectTryNum 成员与 CLIENT_SetConnectTime
        // 接口设置的登录设备超时时间和尝试次数意义相同,可选
        NetSDKLib.NET_PARAM netParam = new NetSDKLib.NET_PARAM();
        // 登录时尝试建立链接的超时时间
        netParam.nConnectTime = 10000;
        // 设置子连接的超时时间
        netParam.nGetConnInfoTime = 3000;
        netsdk.CLIENT_SetNetworkParam(netParam);
        return true;
    }

    /**
     * 打开 sdk log
     */
    private static void enableLog() {
        NetSDKLib.LOG_SET_PRINT_INFO setLog = new NetSDKLib.LOG_SET_PRINT_INFO();
        File path = new File("sdklog/");
        if (!path.exists())
            path.mkdir();

        // 这里的log保存地址依据实际情况自己调整
        String logPath = path.getAbsoluteFile().getParent() + "\\sdklog\\" + "sdklog" + GetDate() + ".log";
        setLog.nPrintStrategy = 0;
        setLog.bSetFilePath = 1;
        System.arraycopy(logPath.getBytes(), 0, setLog.szLogFilePath, 0, logPath.getBytes().length);
        System.out.println(logPath);
        setLog.bSetPrintStrategy = 1;
        bLogOpen = netsdk.CLIENT_LogOpen(setLog);
        if (!bLogOpen)
            System.err.println("Failed to open NetSDK log");
    }

    /**
     * 高安全登录
     */
    public void loginWithHighLevel(UpgradeVO upgradeVO) {
        // 输入结构体参数
        NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY pstlnParam = new NetSDKLib.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY() {
            {
                szIP = upgradeVO.getIp().getBytes();
                nPort = m_nPort;
                szUserName = m_strUser.getBytes();
                szPassword = m_strPassword.getBytes();
            }
        };
        // 输出结构体参数
        NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY pstOutParam = new NetSDKLib.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY();

        // 写入sdk
        m_hLoginHandle = netsdk.CLIENT_LoginWithHighLevelSecurity(pstlnParam, pstOutParam);
        if (m_hLoginHandle.longValue() == 0) {
            System.err.printf("Login Device[%s] Port[%d]Failed. %s\n", upgradeVO.getIp(), m_nPort,
                    netsdk.CLIENT_GetLastError());
        } else {
            deviceInfo = pstOutParam.stuDeviceInfo; // 获取设备信息
            System.out.println("Login Success");
            System.out.println("Device Address：" + upgradeVO.getIp());
            System.out.println("设备包含：" + deviceInfo.byChanNum + "个通道");
        }
    }

    /**
     * 退出
     */
    public void logOut() {
        if (m_hLoginHandle.longValue() != 0) {
            netsdk.CLIENT_Logout(m_hLoginHandle);
            System.out.println("LogOut Success");
        }
    }

    /**
     * 清理sdk环境并退出
     */
    public static void cleanAndExit() {
        if (bLogOpen) {
            netsdk.CLIENT_LogClose(); // 关闭sdk日志打印
        }
        netsdk.CLIENT_Cleanup(); // 进程关闭时，调用一次
        System.exit(0);
    }


    /**
     * 选择通道
     */
    private int channelId = 0;// 逻辑通道

    private NetSDKLib.LLong upgradeHandle = new NetSDKLib.LLong(0);			//升级句柄

    /**
     * 开始更新
     */
    public void startUpgrade(UpgradeVO upgradeVO) {
        int emtype = NetSDKLib.EM_UPGRADE_TYPE.DH_UPGRADE_BIOS_TYPE;
        String buffer = upgradeVO.getPath();//"D:\\up\\DH_ISC-S7XXX_Chn_V1.000.12NM000.0.R.230302.BIN";

        Pointer pointerBuffer
                = GetStringToPointer(buffer, encode);
        upgradeHandle = netsdk.CLIENT_StartUpgradeEx2(m_hLoginHandle, emtype, pointerBuffer, UpgradeCallBackEx.getInstance(), null);

        if (upgradeHandle.longValue() != 0)
        {
            boolean ret =  netsdk.CLIENT_SendUpgrade(upgradeHandle);
            if (ret)
            {
                System.out.println("start to uppgrade...");
            }
            else
            {
                netsdk.CLIENT_StopUpgrade(upgradeHandle);
                upgradeHandle.setValue(0);
                System.err.printf("CLIENT_SendUpgrade Failed ! Last Error[%x]\n", netsdk.CLIENT_GetLastError());

                EndTest();
            }
        }
        else
        {
            System.err.printf("CLIENT_StartUpgradeEx Failed ! Last Error[%x]\n", netsdk.CLIENT_GetLastError());
            EndTest();
        }
    }
    //停止更新
    public void stopUpgrade() {
        if (upgradeHandle.longValue() != 0)
        {
            netsdk.CLIENT_StopUpgrade(upgradeHandle);
            upgradeHandle.setValue(0);
            System.out.printf("CLIENT_StopUpgrade upgradeHandle:%d:...", upgradeHandle);
        }
    }

    //升级回调，分两个阶段。第一阶段是文件发送，第二阶段是，写flash，既真正去升级。 这两个阶段在回调中通过nTotalSize和nSendSize的组合方式去区分。
    private static class UpgradeCallBackEx implements NetSDKLib.fUpgradeCallBackEx {
        private static class UpgradeHolder {
            private static UpgradeCallBackEx intance = new UpgradeCallBackEx();
        }
        private UpgradeCallBackEx() {}
        public static UpgradeCallBackEx getInstance() {
            return UpgradeCallBackEx.UpgradeHolder.intance;
        }

        @Override
        public void invoke(NetSDKLib.LLong lLoginID, NetSDKLib.LLong lUpgradechannel, int nTotalSize, int nSendSize, Pointer dwUserData) {

            if ((-1 == nSendSize) && (0 == nTotalSize)) // 成功
            {
                System.out.printf("Upgrade Success!! \r\n");
            }
            else if ((-2 == nSendSize) && (0 == nTotalSize)) //升级失败
            {
                System.out.printf("Upgrade Failed!! \r\n");
            }
            else
            {
                if (nTotalSize != -1) //文件传输 进度
                {
                    // 计算进度
                    NumberFormat ss = NumberFormat.getInstance();
                    ss.setMaximumFractionDigits(2);
                    String result  = ss.format((float)nSendSize/(float)nTotalSize*100);

                    System.out.println("Upgrade file transmite pos:" + result+ "%");
                }
                else	//升级进度
                {
                    // 进度
                    System.out.printf("System Upgrade pos:%d !! \r\n", nSendSize);
                }
            }
        }
    }
    public static Pointer GetStringToPointer(String src,String charset) {
        Pointer pointer = null;
        try {
            byte[] b = src.getBytes(charset);

            pointer = new Memory(b.length + 1);
            pointer.clear(b.length + 1);

            pointer.write(0, b, 0, b.length);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return pointer;
    }

    public void RunTest()
    {
        System.out.println("Run Test");
        CaseMenu menu = new CaseMenu();;
        menu.addItem((new CaseMenu.Item(this , "开始更新" , "startUpgrade")));
        menu.addItem((new CaseMenu.Item(this , "停止更新" , "stopUpgrade")));
        menu.run();
    }
    // 配置登陆地址，端口，用户名，密码
//    private String m_strIpAddr = "192.168.0.21";
    private int m_nPort = 37777;
    private String m_strUser = "admin";
    private String m_strPassword = "admin123";
    /*public static void main(String[] args) {
        StartUpgradeEx2Demo demo=new StartUpgradeEx2Demo();
        demo.InitTest();
        demo.RunTest();
        demo.EndTest();

    }*/

    /**
     * 初始化测试
     */
    public void InitTest(UpgradeVO upgradeVO) {
        StartUpgradeEx2Demo.Init();
        this.loginWithHighLevel(upgradeVO);
    }

    /**
     * 结束测试
     */
    public void EndTest() {
        System.out.println("End Test");
        this.logOut(); // 登出设备
        System.out.println("See You...");
        StartUpgradeEx2Demo.cleanAndExit(); // 清理资源并退出
    }

    public boolean RunTestUpdate(UpgradeVO upgradeVO) {
        this.startUpgrade(upgradeVO);
        return true;
    }

}
