package com.niepan.modules.zhinenganjianback.controller;

/**
 * @author: liuchenyu
 * @date: 2023/6/19
 */
import com.alibaba.fastjson.JSON;
import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.common.utils.base64MultipartFile.Base64ToMultipartFile;
import com.niepan.common.utils.json.JsonToFile;
import com.niepan.common.utils.logs.LogUtils;
import com.niepan.common.utils.minIO.MinIOUtil;
import com.niepan.common.utils.redis.RedisUtils;
import com.niepan.modules.zhinenganjianback.dao.DeviceManagerDao;
import com.niepan.modules.zhinenganjianback.demo.util.ThreadPoolUtil;
import com.niepan.modules.zhinenganjianback.dto.GetHeartInfoDto;
import com.niepan.modules.zhinenganjianback.VO.ElsVO;
import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
import com.niepan.modules.zhinenganjianback.service.ElsSecurityMachineService;
import com.niepan.modules.zhinenganjianback.service.SecurityMachineService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 二郎神安检机接口
 */
@Slf4j
@RestController
@RequestMapping
public class ElsSecurityMachineController {

    @Autowired
    private DeviceManagerDao deviceManagerDao;

    @Autowired
    private ElsSecurityMachineService elsSecurityMachineService;

    @Autowired
    HttpServletRequest request;

    @Autowired
    private SecurityMachineService securityMachineService;

    @Autowired
    private RedisUtils redisUtils;

    @Value("${file.picPath}")
    private String picPath;

    @Value("${server.port}")
    private String serverPort;

    @Autowired
    private MinIOUtil minIOUtil;

    /**
     * 二郎神上传图片接口
     * @param elsVO
     */
    @PostMapping("/sdk.post")
    public ApiResponse post(@RequestBody ElsVO elsVO){
        //sessionid , scantime , img_base64_side
//        Jedis jedis = new Jedis("localhost", 6379);
//        String mip = jedis.get(elsVO.getSessionid());
        String mip = redisUtils.get(elsVO.getSessionid());

        log.info("请求头获取的ip is : " + request.getRemoteAddr());
        log.info("根据安检机发来的sessionid去redis中获取的ip = " + mip);

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");

        String scantime = elsVO.getScantime();

        Map<String, Object> deviceMap = deviceManagerDao.getDeviceByIp(request.getRemoteAddr());

        String lineCode = (String) deviceMap.get("channel");
        String sncode = (String) deviceMap.get("sncode");

        String substring = elsVO.getScantime().substring(0, elsVO.getScantime().lastIndexOf("."));
        Timestamp timestamp = Timestamp.valueOf(substring);
        long time = timestamp.getTime();

        String picName = sncode + "@&" + lineCode + "@&" + time + ".jpg";

        /*SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String datePath = sdf.format(new Date());//20230215

        String filePath = picPath + sncode + lineCode + "/" + datePath + "/";

        JsonToFile.stringToJpg(elsVO.getImg_base64_side(),filePath+picName);*/
        ThreadPoolUtil.getThreadPool().submit(()->{
            MultipartFile multipartFile = new Base64ToMultipartFile(elsVO.getImg_base64_side().split(",")[0], "data:image/jpg;base64");
            minIOUtil.uploadFile(multipartFile,picName);
        });

        //日志记录
        LogUtils.info("photo name is " + picName + "  图片已经上传minIO");
        LogUtils.info("photo info is " + elsVO.getScantime());

        SecurityMachinePicture securityMachinePicture = new SecurityMachinePicture();
        securityMachinePicture.setName(picName);
        securityMachinePicture.setDevice_sn(sncode);
        securityMachinePicture.setLine_code(lineCode);//安天下安检机没有信道，默认为1
        securityMachinePicture.setTime(scantime);
        securityMachinePicture.setAjjTime(scantime);

        securityMachinePicture.setCurrentTime(formatter.format(date));

        Boolean res = elsSecurityMachineService.getImgInfo(securityMachinePicture,elsVO.getImg_base64_side());

        log.info("上传图片完成");
        if (res != null && res){
            LogUtils.info("图片保存成功");
            return Result.success("图片保存成功");
        }
        LogUtils.error("保存图片失败" + picName);
        return Result.error("保存图片失败");
    }

    /**
     * 向LDPM请求获取一个ldps服务地址
     * @param method
     * @param id
     * @param type
     * @param name
     * @param ip
     * @param host
     * @return
     */
    @GetMapping("/service/rest/1.0/ldps")
    public Map<String,Object> ldps(@RequestParam("method")String method, @RequestParam("id")String id,
                                   @RequestParam("type")String type, @RequestParam("name")String name,
                                   @RequestParam(value = "ip",required = false)String ip, @RequestParam("host")String host){
        Map<String,Object> result = new LinkedHashMap<>();
        LogUtils.info("向LDPM请求获取一个ldps服务地址");
        if ("getserver".equals(method)){
            result.put("resultcode",0);
            result.put("resultmsg","OK");
            result.put("address",host + ":8200");
//            result2 = "{\"resultcode\":0,\"resultmsg\":\"OK\",\"address\":\"192.168.0.49:8200\"}";
            String uuid = UUID.randomUUID().toString();
            host1 = host;
            redisUtils.set(request.getRemoteAddr(),uuid,-1);
            redisUtils.set(uuid,request.getRemoteAddr(),-1);
            log.info("ldps服务地址", JSON.toJSON(result));
        } else {
            result.put("resultcode",-1);
            result.put("resultmsg","ERROR");
            result.put("address",host + ":" + serverPort);
        }
        return result;
    }

    SimpleDateFormat num = new SimpleDateFormat("HH:mm:ss");
    String formatOld = num.format(new Date());

    private static String host1 = null;

    @RequestMapping(value = "/service/rest/1.0/smterminal",method = {RequestMethod.POST,RequestMethod.GET})
    public Map<String,Object> smterminal(@RequestParam("method")String method,
                                         @RequestParam(value = "id",required = false)String id,
                                         @RequestParam(value = "name",required = false)String name,
                                         @RequestParam(value = "ip",required = false)String ip,
                                         @RequestParam(value = "usrno",required = false)String usrno,
                                         @RequestParam(value = "sessionid",required = false)String sessionid,
                                         @RequestParam(value = "host",required = false)String host,
                                         @RequestBody(required = false) ElsVO elsVO){
        //LinkedHashMap是有序的，对方安检机只能按照有序的值来进行判断
        Map<String,Object> result = new LinkedHashMap<>();
        //使用redis将ip和sessionid进行绑定
//        Jedis jedis = new Jedis("localhost", 6379);
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
        //注册
        switch (method){
            //终端向LDPS注册
            case "register":
                log.info("二郎神 安检机发来的ip : " + ip);
                log.info("+++++++++++"+serverPort);
                LogUtils.info("终端向LDPS注册");
                String uuid = UUID.randomUUID().toString();
                redisUtils.set(request.getRemoteAddr(),uuid,-1);
                redisUtils.set(uuid,request.getRemoteAddr(),-1);
//                jedis.set(ip,uuid);
//                jedis.set(uuid,ip);
                result.put("resultcode",0);
                result.put("resultmsg","OK");
                result.put("sessionid",uuid);
                result.put("heartbeattime",10);
                log.info("二郎神 安检机发送信息："+JSON.toJSON(result));
                break;
            //终端和LDPS保持心跳。一个周期 默认30s.在注册时返回心跳周期时间。周期时间LDPS上通过配置文件配置。3个周期失败作为离线标志。
            case "keepalive":
//                String sessionIp = jedis.get(sessionid);
                String sessionIp = redisUtils.get(sessionid);
                log.info("heart beat time is :" + formatter.format(date) + "deviceIp is :" + sessionIp);
                GetHeartInfoDto getHeartInfoDto = new GetHeartInfoDto();
                getHeartInfoDto.setDevice_sn(request.getRemoteAddr());

                securityMachineService.getHeartELS(getHeartInfoDto);

                result.put("resultcode",0);
                result.put("resultmsg","OK");
                break;
            //取消注册
            case "unregister":
                LogUtils.info("取消注册");
                result.put("resultcode",0);
                result.put("resultmsg","OK");
                break;
            //获取KMS服务器信息
            case "getkmsinfo":
                LogUtils.info("获取KMS服务器信息");
                result.put("resultcode",0);
                result.put("resultmsg","OK");
                result.put("a",host + ":" + serverPort);
                break;
            //上传安检员信息 暂时不需要
            case "uploadinspector":
                result.put("resultcode",0);
                result.put("resultmsg","OK");
                break;
            //上传安检机运行状态数据
            case "uploadsmstatus":
//                String mip = jedis.get(sessionid);
                String mip = redisUtils.get(sessionid);
                if (0 == elsVO.getExceptioncode()){
                    LogUtils.info("安检机状态正常, IP 为：" + mip);
                    LogUtils.info("uploads m status time is :" + formatter.format(date) + "; machineindex is :" + elsVO.getMachineindex()
                    + "; status code :" + elsVO.getExceptioncode() + "; status msg :" + elsVO.getExceptionmemo());
                    result.put("resultcode",0);
                    result.put("resultmsg","OK");
                } else {
                    LogUtils.info("安检机状态异常, IP 为：" + mip);
                    LogUtils.info("uploads m status time is :" + formatter.format(date) + "; machineindex is :" + elsVO.getMachineindex()
                            + "; status code :" + elsVO.getExceptioncode() + "; status msg :" + elsVO.getExceptionmemo());
                    result.put("resultcode",-1);
                    result.put("resultmsg","ERROR");
                }
                break;
            //上传安检机采集的图片
            case "uploadsmimage":
                result.put("resultcode",0);
                result.put("resultmsg","OK");
                break;
            case "uploadsmcontraband":
                result.put("resultcode",0);
                result.put("resultmsg","OK");
                break;
        }

        return result;
    }
}
