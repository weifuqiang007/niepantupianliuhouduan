package com.niepan.modules.zhinenganjianback.controller;

import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.common.utils.logs.LogUtils;
import com.niepan.common.utils.redis.RedisUtils;
import com.niepan.modules.zhinenganjianback.VO.AreaVO;
import com.niepan.modules.zhinenganjianback.VO.BalanceWheelVO;
import com.niepan.modules.zhinenganjianback.model.NPDeviceRegister;
import com.niepan.modules.zhinenganjianback.VO.RequestUrlVO;
import com.niepan.modules.zhinenganjianback.service.SQLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * SQL插入模块
 * @author: liuchenyu
 * @date: 2023/6/25
 */
@RestController
@RequestMapping("/writeSQL")
public class SQLController {

    @Autowired
    private SQLService sqlService;

    @Autowired
    private RedisUtils redisUtils;

    @GetMapping("/getTableInfo")
    public ApiResponse getTableInfo(){
        Map<String,Object> res = new HashMap<>();
        List<Map<String,Object>> device = sqlService.getDevice();
        List<Map<String,Object>> area = sqlService.getArea();
        List<Map<String,Object>> url = sqlService.getRequestUrl();
        List<Map<String,Object>> balanceWheel = sqlService.getBalanceWheel();
        res.put("device",device);
        res.put("area",area);
        res.put("url",url);
        res.put("balanceWheel",balanceWheel);
        return Result.success(res);
    }

    @GetMapping("/getDevice")
    public ApiResponse getDevice(){
        LogUtils.info("开始查询设备表信息");
        List<Map<String,Object>> res = sqlService.getDevice();
        return Result.success(res);
    }

    @Transactional
    @PostMapping("/insertDevice")
    public ApiResponse insertDevice(@RequestBody List<NPDeviceRegister> npDeviceRegisterList){
        for (NPDeviceRegister npDeviceRegister : npDeviceRegisterList) {
            if (npDeviceRegister.getId()!=null && npDeviceRegister.getProvince() != null && npDeviceRegister.getCity() != null
            && npDeviceRegister.getCenter() != null && npDeviceRegister.getIp() != null && npDeviceRegister.getChannel() != null
            && npDeviceRegister.getDwscode() != null && npDeviceRegister.getSncode() != null && npDeviceRegister.getAjj_ip() != null
            && npDeviceRegister.getStatus() != null  && npDeviceRegister.getProvince() != "" && npDeviceRegister.getCity() != ""
            && npDeviceRegister.getCenter() != "" && npDeviceRegister.getIp() != "" && npDeviceRegister.getChannel() != ""
            && npDeviceRegister.getDwscode() != "" && npDeviceRegister.getSncode() != "" && npDeviceRegister.getAjj_ip() != ""
            && npDeviceRegister.getId() <= 1000 && npDeviceRegister.getProvince().length() <= 30 && npDeviceRegister.getCity().length() <= 50
            && npDeviceRegister.getCenter().length() <= 50 && npDeviceRegister.getIp().length() <= 30 && npDeviceRegister.getChannel().length() <= 30
            && npDeviceRegister.getDwscode().length() <= 30 && npDeviceRegister.getSncode().length() <= 30 && npDeviceRegister.getAjj_ip().length() <= 30
            ){
                continue;
            }
            return Result.error("数据不能为空或数据长度超出最大限制","数据不能为空或数据长度超出最大限制");
        }
        Integer resi = null;
        LogUtils.info("开始删除设备表信息");
        Integer resd = sqlService.deleteDevice();
        for (NPDeviceRegister npDeviceRegister : npDeviceRegisterList) {
            LogUtils.info("开始插入设备表信息");
            resi = sqlService.insertDevice(npDeviceRegister);
        }
        if (resi != 0 && resd != 0){
            return Result.success("插入成功");
        }
        return Result.error("插入失败");
    }

    @GetMapping("/getArea")
    public ApiResponse getArea(){
        LogUtils.info("开始查询地区表信息");
        List<Map<String,Object>> res = sqlService.getArea();
        return Result.success(res);
    }

    @Transactional
    @PostMapping("/insertArea")
    public ApiResponse insertArea(@RequestBody List<AreaVO> areaVOList){
        for (AreaVO areaVO : areaVOList) {
            if (areaVO.getUnique_code() != null && areaVO.getProvince() != null && areaVO.getCity() != null && areaVO.getCentre() != null
            && areaVO.getUnique_code() != "" && areaVO.getProvince() != "" && areaVO.getCity() != "" && areaVO.getCentre() != ""
            && areaVO.getUnique_code().length() <= 11 && areaVO.getProvince().length() <= 30 && areaVO.getCity().length() <= 30
            && areaVO.getCentre().length() <= 30
            ){
                continue;
            }
            return Result.error("数据不能为空或数据长度超出最大限制","数据不能为空或数据长度超出最大限制");
        }
        Integer resi = null;
        LogUtils.info("开始删除地区表信息");
        Integer resd = sqlService.deleteArea();
        for (AreaVO areaVO : areaVOList) {
            LogUtils.info("开始插入地区表信息");
            resi = sqlService.insertArea(areaVO);
        }
        if (resi != 0 && resd != 0){
            return Result.success("插入成功");
        }
        return Result.error("插入失败");
    }

    @GetMapping("/getRequestUrl")
    public ApiResponse getRequestUrl(){
        LogUtils.info("开始查询上传表信息");
        List<Map<String,Object>> res = sqlService.getRequestUrl();
        return Result.success(res);
    }

    @Transactional
    @PostMapping("/insertRequestUrl")
    public ApiResponse insertRequestUrl(@RequestBody List<RequestUrlVO> requestUrlVOList){
        for (RequestUrlVO requestUrlVO : requestUrlVOList) {
            if (requestUrlVO.getId() != null && requestUrlVO.getRequesturl() != null && requestUrlVO.getRequesturl() != ""
            && requestUrlVO.getId() <= 100 && requestUrlVO.getRequesturl().length() <= 100
            ){
                continue;
            }
            return Result.error("数据不能为空或数据长度超出最大限制","数据不能为空或数据长度超出最大限制");
        }
        Integer resi = null;
        LogUtils.info("开始删除上传表信息");
        Integer resd = sqlService.deleteRequestUrl();
        for (RequestUrlVO requestUrlVO : requestUrlVOList) {
            LogUtils.info("开始插入上传表信息");
            resi = sqlService.insertRequestUrl(requestUrlVO);
        }
        if (resi != 0 && resd != 0){
            return Result.success("插入成功");
        }
        return Result.error("插入失败");
    }

    @GetMapping("/getBalanceWheel")
    public ApiResponse getBalanceWheel(){
        LogUtils.info("开始查询上传表信息");
        List<Map<String,Object>> res = sqlService.getBalanceWheel();
        return Result.success(res);
    }

    @Transactional
    @PostMapping("/insertBalanceWheel")
    public ApiResponse insertBalanceWheel(@RequestBody List<BalanceWheelVO> balanceWheelVOList){
        for (BalanceWheelVO balanceWheelVO : balanceWheelVOList) {
            if (balanceWheelVO.getId() != null && balanceWheelVO.getSwing() != null && balanceWheelVO.getSwing_time() != null
            && balanceWheelVO.getCorrect_time() != null && balanceWheelVO.getSwing() != "" && balanceWheelVO.getId() <= 11
            ){
                continue;
            }
            return Result.error("数据不能为空或数据长度超出最大限制","数据不能为空或数据长度超出最大限制");
        }
        Integer resi = null;
        LogUtils.info("开始删除摆轮表信息");
        Integer resd = sqlService.deleteBalanceWheel();
        for (BalanceWheelVO balanceWheelVO : balanceWheelVOList) {
            LogUtils.info("开始插入摆轮表信息");
            resi = sqlService.insertBalanceWheel(balanceWheelVO);
            redisUtils.set("swing",balanceWheelVO.getSwing());
            redisUtils.set("swingTime",balanceWheelVO.getSwing_time());
            redisUtils.set("backTime",balanceWheelVO.getCorrect_time());
            redisUtils.set("enable",balanceWheelVO.getEnable());
        }
        if (resi != 0 && resd != 0){
            return Result.success("插入成功");
        }
        return Result.error("插入失败");
    }
}