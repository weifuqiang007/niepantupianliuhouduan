package com.niepan.modules.zhinenganjianback.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.common.utils.aesUtil.AesEncryptUtil;
import com.niepan.common.utils.logs.LogUtils;
import com.niepan.common.utils.redis.RedisUtils;
import com.niepan.common.websocket.server.WarningWebSocketServer;
import com.niepan.modules.zhinenganjianback.service.ContrabandListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author:liuchenyu
 * @data:2023/02/27
 * 智能安检模块
 *
 */
@RestController
@RequestMapping("/ContrabandList")
public class ContrabandListController {

    @Autowired
    private ContrabandListService contrabandListService;

    @Autowired
    private WarningWebSocketServer warningWebSocketServer;

    @Autowired
    RedisUtils redisUtils;

    /**
     * websocket 的测试方法
     * @throws IOException
     */
    @RequestMapping("/test")
    public void test1() throws IOException {
        warningWebSocketServer.sendMessage("1111111");
    }

    /**
     * 获取所有的地区
     *
     * todo 之后修改sql SELECT * FROM `sys_user_area` a
     *                 left join area b on a.unique_code = b.unique_code
     *                 where b.unique_code in (a.unique_code)
     *                 and a.user_id = 'cbe0dd9613414421a44ff48fd424e325'
     *
     * @return
     */
    @GetMapping("/getArea")
    public ApiResponse getArea(){
        LogUtils.info("开始查询所有地区");
        List<Map<String,Object>> res = contrabandListService.getArea();
        return Result.success(res);
    }

    /**
     * 根据条件获取信息
     * @param pageSize
     * @param pageNum
     * @param uniqueCode
     * @param machineGrade 安检机标号
     * @param machineBrand 安检机品牌
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getItem")
    public ApiResponse getItem(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                            @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                            @RequestParam(value = "machineGrade",required = false) String machineGrade,
                            @RequestParam(value = "machineBrand",required = false) String machineBrand,
                            @RequestParam(value = "startTime",required = true) String startTime,
                            @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件获取信息");
//        PageHelper.startPage(pageNum, pageSize);
        List<Map<String,Object>> res = contrabandListService.getItem(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 根据条件获取信息的统计数
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getCount")
    public ApiResponse getCount(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                @RequestParam(value = "startTime",required = true) String startTime,
                                @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件获取信息的统计数");
        List<Map<String, Object>> res = contrabandListService.getCount(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 根据唯一标识查询图片的详情 todo 未使用
     * @param pageSize
     * @param pageNum
     * @param UUID
     * @return
     */
    @Deprecated
    @GetMapping("/getDetail")
    public PageInfo getDetail(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                              @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                              @RequestParam("UUID") String UUID){
        LogUtils.info("开始根据唯一标识查询图片的详情");
        PageHelper.startPage(pageNum, pageSize);
        List<Map<String,Object>> res = contrabandListService.getDetail(UUID);
        return new PageInfo<>(res);
    }

    /**
     * 根据省市获取集散中心列表
     * @param uniqueCode
     * @return
     */
    @GetMapping("/getCentre")
    public ApiResponse getCentre(@RequestParam("uniqueCode") String uniqueCode){
        LogUtils.info("开始根据省市获取集散中心列表");
        List<Map<String,Object>> res = contrabandListService.getCentre(uniqueCode);
        return Result.success(res);
    }

    /**
     * 获取总数
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getTotal")
    public ApiResponse getTotal(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                @RequestParam(value = "startTime",required = true) String startTime,
                                @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始获取总数");
        List<Map<String, Object>> res = contrabandListService.getTotal(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 获取违禁品的总数
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getDangerTotal")
    public ApiResponse getDangerTotal(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                      @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                      @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                      @RequestParam(value = "startTime",required = true) String startTime,
                                      @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始获取违禁品的总数");
        List<Map<String, Object>> res = contrabandListService.getDangerTotal(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 整合接口，总数和违禁品总数
     * @param uniqueCode
     * @param lineCode
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getTotalAndDangerTotal")
    public ApiResponse getTotalAndDangerTotal(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                              @RequestParam(value = "lineCode",required = false) String lineCode,
                                              @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                              @RequestParam(value = "startTime",required = true) String startTime,
                                              @RequestParam(value = "endTime",required = true) String endTime){
        Map<String,Object> map = new HashMap<>();
        List<Map<String, Object>> total = contrabandListService.getTotal(uniqueCode,lineCode,machineBrand,startTime,endTime);
        for (Map<String, Object> stringObjectMap : total) {
            map.putAll(stringObjectMap);
        }
        List<Map<String, Object>> dangerTotal = contrabandListService.getDangerTotal(uniqueCode, lineCode, machineBrand, startTime, endTime);
        for (Map<String, Object> stringObjectMap : dangerTotal) {
            map.putAll(stringObjectMap);
        }
        return Result.success(map);
    }

    @GetMapping("/getTotalAndDangerTotalTwice")
    public ApiResponse getTotalAndDangerTotalTwice(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                                   @RequestParam(value = "lineCode",required = false) String lineCode,
                                                   @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                                   @RequestParam(value = "startTime",required = true) String startTime,
                                                   @RequestParam(value = "endTime",required = true) String endTime){
        Map<String,Object> map = new HashMap<>();
        List<Map<String, Object>> total = contrabandListService.getTotalTwice(uniqueCode,lineCode,machineBrand,startTime,endTime);
        for (Map<String, Object> stringObjectMap : total) {
            map.putAll(stringObjectMap);
        }
        List<Map<String, Object>> dangerTotal = contrabandListService.getDangerTotalTwice(uniqueCode, lineCode, machineBrand, startTime, endTime);
        for (Map<String, Object> stringObjectMap : dangerTotal) {
            map.putAll(stringObjectMap);
        }
        return Result.success(map);
    }

    /**
     * 获取某个违禁品类下的所有违禁品信息
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @param typeId
     * @return
     */
    @GetMapping("/getCountDetail")
    public PageInfo getCountDetail(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                                   @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                   @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                   @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                   @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                   @RequestParam(value = "startTime",required = true) String startTime,
                                   @RequestParam(value = "endTime",required = true) String endTime,
                                   @RequestParam(value = "typeId",required = true)String typeId){
        LogUtils.info("开始获取某个违禁品类下的所有违禁品信息");
        PageHelper.startPage(pageNum, pageSize,false);
        List<Map<String, Object>> res = contrabandListService.getCountDetail(machineGrade,machineBrand,startTime,endTime,typeId,uniqueCode);
        return new PageInfo<>(res);
    }

    /**
     * 根据条件获取信息的统计数（统计曲线图使用）
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getCountStatistics")
    public ApiResponse getCountStatistics(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                          @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                          @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                          @RequestParam(value = "startTime",required = true) String startTime,
                                          @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件获取信息的统计数");
        List<Map<String, Object>> res = contrabandListService.getCountStatistics(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 根据条件查询每天的过包数
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getCountByDay")
    public ApiResponse getCountByDay(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                     @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                     @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                     @RequestParam(value = "startTime",required = true) String startTime,
                                     @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件查询每天的过包数");
        List<Map<String, Object>> res = contrabandListService.getCountByDay(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }


    /**
     * 查询所有的图片信息
     * @param pageSize
     * @param pageNum
     * @return
     */
//    @GetMapping("/getPicture")
//    public PageInfo getPicture(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
//                               @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
//                               @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
//                               @RequestParam(value = "startTime",required = true) String startTime,
//                               @RequestParam(value = "endTime",required = true) String endTime,
//                               @RequestParam(value = "lineCode",required = false) String lineCode){
//        LogUtils.info("开始根据条件查询每天的过包数");
//        PageHelper.startPage(pageNum, pageSize);
//        List<Map<String, Object>> res = contrabandListService.getPicture(uniqueCode,startTime,endTime,lineCode);
//        return new PageInfo<>(res);
//    }

    /**
     * 查询所有的图片信息
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping("/getPicture")
    public ApiResponse getPicture(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                                  @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                  @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                  @RequestParam(value = "startTime",required = true) String startTime,
                                  @RequestParam(value = "endTime",required = true) String endTime,
                                  @RequestParam(value = "lineCode",required = false) String lineCode,
                                  @RequestParam(value = "isDanger",required = false) Boolean isDanger){
        LogUtils.info("开始根据条件查询每天的过包数");
        PageHelper.startPage(pageNum, pageSize);
        List res = contrabandListService.getPicture(uniqueCode,startTime,endTime,lineCode,isDanger);
        PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(res);
        String resultStr= JSON.toJSONString(mapPageInfo);
        String encrypt = AesEncryptUtil.encrypt(resultStr, AesEncryptUtil.KEY,AesEncryptUtil.IV);
        return Result.success(encrypt);
    }

    /**
     * 查询所有的二次安检图片信息
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping("/getPictureTwice")
    public ApiResponse getPictureTwice(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                                       @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                       @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                       @RequestParam(value = "startTime",required = true) String startTime,
                                       @RequestParam(value = "endTime",required = true) String endTime,
                                       @RequestParam(value = "lineCode",required = false) String lineCode,
                                       @RequestParam(value = "isDanger",required = false) Boolean isDanger){
        LogUtils.info("开始根据条件查询每天的过包数");
        PageHelper.startPage(pageNum, pageSize);
        List res = contrabandListService.getPictureTwice(uniqueCode,startTime,endTime,lineCode,isDanger);
        PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(res);
        String resultStr= JSON.toJSONString(mapPageInfo);
        String encrypt = AesEncryptUtil.encrypt(resultStr, AesEncryptUtil.KEY,AesEncryptUtil.IV);
        return Result.success(encrypt);
    }

    /**
     * 按照给定的时间查询此时间段内按时间段分割的数据
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getCountByHour")
    public ApiResponse getCountByHour(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                      @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                      @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                      @RequestParam(value = "startTime",required = true) String startTime,
                                      @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件获取信息的统计数");
        List<Map<String, Object>> res = contrabandListService.getCountByHour(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 获取卸货口
     * @param uniqueCode
     * @return
     */
    @GetMapping("/getChannel")
    public ApiResponse getChannel(@RequestParam(value = "uniqueCode",required = true) String uniqueCode){
        LogUtils.info("开始根据条件获取通道号");
        List<String> res = contrabandListService.getChannel(uniqueCode);
        return Result.success(res);
    }

    /**
     * 获取IP，sn配置表中的所有信息
     * @return
     */
    @GetMapping("/getLineCode")
    public ApiResponse getLineCode(){
        LogUtils.info("开始获取卸货口信息");
        List<Map<String, Object>> res = contrabandListService.getLineCode();
        return Result.success(res);
    }

    @GetMapping("/getGKJheart")
    public ApiResponse getGKJheart(){
        LogUtils.info("开始获取工控机状态信息");

        try {
//            Jedis jedis = new Jedis("localhost", 6379);
//            String heart = jedis.get("heart");
            String heart = redisUtils.get("heart");

            return Result.success(heart);
        } catch (Exception e) {
            LogUtils.info("工控机未开机或redis未启动");
        }
        return null;
    }

    @GetMapping("/getReportData")
    public ApiResponse getReportData(@RequestParam("imageId")String imageId){
        LogUtils.info("开始获取上报信息");
        String res = contrabandListService.getReportData(imageId);
        return Result.success(res);
    }

}
