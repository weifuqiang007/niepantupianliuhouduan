package com.niepan.modules.zhinenganjianback.controller;

import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.modules.zhinenganjianback.demo.customize.StartUpgradeEx2Demo;
import com.niepan.modules.zhinenganjianback.VO.UpgradeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 网络升级大华设备的接口
 */
@Controller
@RequestMapping("/upgarde")
public class UpgradeController {

    @Autowired
    private StartUpgradeEx2Demo startUpgradeEx2Demo;

    @RequestMapping("/update")
    public ApiResponse update(@RequestBody UpgradeVO upgradeVO){
        startUpgradeEx2Demo.InitTest(upgradeVO);
        boolean b = startUpgradeEx2Demo.RunTestUpdate(upgradeVO);
        return Result.success(b);
    }
}
