package com.niepan.modules.zhinenganjianback.controller;

import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.common.utils.base64MultipartFile.Base64ToMultipartFile;
import com.niepan.common.utils.logs.LogUtils;
import com.niepan.common.utils.minIO.MinIOUtil;
import com.niepan.modules.zhinenganjianback.VO.AtxTCPImgVO;
import com.niepan.modules.zhinenganjianback.VO.AtxTCPStatusVO;
import com.niepan.modules.zhinenganjianback.demo.util.ThreadPoolUtil;
import com.niepan.modules.zhinenganjianback.enums.AtxStatusEnum;
import com.niepan.modules.zhinenganjianback.model.AtxTCPHeartBeat;
import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
import com.niepan.modules.zhinenganjianback.model.SecurityMachineStatus;
import com.niepan.modules.zhinenganjianback.service.AtxSecurityMachineService;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author: liuchenyu
 * @date: 2023/5/23
 */
@Slf4j
@RestController
@RequestMapping("/atx/tcp/securitydevice")
public class AtxSecurityMachineTCPController {

    @Value("${file.picPath}")
    private String picPath;

    @Autowired
    private AtxSecurityMachineService atxSecurityMachineService;

    @Autowired
    private MinIOUtil minIOUtil;

    // 实现接受图片的请求
    @RequestMapping(value = "/img_send", method = RequestMethod.POST)
    public ApiResponse img_send(@RequestBody AtxTCPImgVO atxImgVO){

        log.info("atx接收图片接口，DeviceId:{};CheckTime:{}",atxImgVO.getDeviceId(),atxImgVO.getCheckTime());

        String substring = atxImgVO.getCheckTime().substring(0, atxImgVO.getCheckTime().lastIndexOf(" "));
        Timestamp timestamp = Timestamp.valueOf(substring);
        long time = timestamp.getTime();
        String picName = atxImgVO.getDeviceId() + "@#" + time + ".jpg";

        /*SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String datePath = sdf.format(new Date());//20230215
        //存放图片的路径
        String filePath = picPath + atxImgVO.getDeviceId() + "/" + datePath + "/";

        //图片存储到本地
        JsonToFile.stringToJpg(atxImgVO.getTopImageData(),filePath+picName);*/

        ThreadPoolUtil.getThreadPool().submit(()->{
            MultipartFile multipartFile = new Base64ToMultipartFile(atxImgVO.getTopImageData().split(",")[0], "data:image/jpg;base64");
            minIOUtil.uploadFile(multipartFile,picName);
        });

        //日志记录
        LogUtils.info("photo name is " + picName + "  图片已经上传minIO");
        LogUtils.info("photo info is " + atxImgVO.getCheckTime());

        //将要插入数据库的数据封装到对象中
        SecurityMachinePicture securityMachinePicture = new SecurityMachinePicture();
        securityMachinePicture.setName(picName);
        securityMachinePicture.setDevice_sn(atxImgVO.getDeviceId());
        securityMachinePicture.setLine_code("1");//安天下安检机没有信道，默认为1
        securityMachinePicture.setTime(atxImgVO.getCheckTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        String date = df.format(new Date());//2023-02-17 13:49:08
        securityMachinePicture.setCurrentTime(date);

        //数据库插入 并推送给大华设备
        Boolean res = atxSecurityMachineService.getImgInfo(securityMachinePicture,atxImgVO.getTopImageData());
        if (res){
            LogUtils.info("图片保存成功");
            return Result.success("图片保存成功");
        }
        LogUtils.error("保存图片失败" + picName);
        return Result.error("保存图片失败");
    }

    // 安检机状态和故障信息上报
    @RequestMapping(value = "/status_send", method = RequestMethod.POST)
    public ApiResponse getStatus(@RequestBody AtxTCPStatusVO atxStatusVO){

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if (atxStatusVO.getErrorType().equals("0")){
            LogUtils.info("安检机状态正常");
            LogUtils.info(formatter.format(new Date()) + "@#" + atxStatusVO.getDeviceId() +"@#"+
                    atxStatusVO.getErrorType() + "@#" + AtxStatusEnum.getValue(atxStatusVO.getErrorType()));
        }
        else{
            LogUtils.error("安检机状态异常");
            LogUtils.error(formatter.format(new Date()) + "@#" + atxStatusVO.getDeviceId() +"@#"+
                    atxStatusVO.getErrorType() + "@#" + AtxStatusEnum.getValue(atxStatusVO.getErrorType()));
        }

        SecurityMachineStatus securityMachineStatus = new SecurityMachineStatus();
        securityMachineStatus.setDevice_sn(atxStatusVO.getDeviceId());
        securityMachineStatus.setLine_code("1");//安天下安检机没有信道，默认为1
        securityMachineStatus.setStatus_code(atxStatusVO.getErrorType().toString());
        securityMachineStatus.setStatus_desc(AtxStatusEnum.getValue(atxStatusVO.getErrorType()));
        securityMachineStatus.setFault_level(atxStatusVO.getDeviceStatus().toString());
        //传来的时间是yyyy-MM-dd HH:mm:ss SSS 将其转化为时间戳
        String substring = atxStatusVO.getErrorTime().substring(0, atxStatusVO.getErrorTime().lastIndexOf(" "));
        Timestamp timestamp = Timestamp.valueOf(substring);
        long time = timestamp.getTime();
        securityMachineStatus.setTime(time);
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String date = df.format(new Date());//20230215
        securityMachineStatus.setMsgtime(date);

        String status = atxSecurityMachineService.SecurityMachineStatus(securityMachineStatus);

        return Result.success(status);
    }

    // 安检机心跳
    @RequestMapping(value = "/heart", method = RequestMethod.POST)
    public ApiResponse getHeart(@RequestBody AtxTCPHeartBeat atxHeartBeat){

        log.info(atxHeartBeat.getDataTime());
        log.info(atxHeartBeat.getProtocolVersion());
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        log.info(formatter.format(date));

        LogUtils.info("heart beat time is :"+formatter.format(date) + "deviceId is :"+atxHeartBeat.getDeviceId() +
                "ProtocolVersion is :"+ atxHeartBeat.getProtocolVersion());

        return Result.success();
    }
}
