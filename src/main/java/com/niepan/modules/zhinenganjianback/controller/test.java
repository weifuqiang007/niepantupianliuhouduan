package com.niepan.modules.zhinenganjianback.controller;

import com.niepan.common.tcpUtil.SocketServerUtil;
import com.niepan.common.tcpUtil.TcpServer;
import com.niepan.common.tcpUtil.TcpServerUtil;
import com.niepan.common.utils.logs.LogUtils;
//import com.niepan.modules.zhinenganjianback.demo.udp.UdpServer;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

/**
 * @author: liuchenyu
 * @date: 2023/5/16
 */
@RestController
public class test {

    @GetMapping("/test")
    public void test(){
//        UdpServer.getInstance();
//
//        UdpServer.returnMsg(UdpServer.leftBytes);
//
//        UdpServer.returnMsg(UdpServer.backBytes);
//
//        LogUtils.info("摆轮已经摆动");
    }

    @GetMapping("/testTCP")
    public void testTCP(String up){
        TcpServer.getInstance();
        if(StringUtils.equalsIgnoreCase(up,"left")) {
            TcpServer.returnMsg(TcpServer.left);
        }else if(StringUtils.equalsIgnoreCase(up,"right")) {
            TcpServer.returnMsg(TcpServer.right);
        }else if(StringUtils.equalsIgnoreCase(up,"back")) {
            TcpServer.returnMsg(TcpServer.back);
        }else if(StringUtils.equalsIgnoreCase(up,"start")) {
            TcpServer.returnMsg(TcpServer.start);
        }else if(StringUtils.equalsIgnoreCase(up,"end")) {
            TcpServer.returnMsg(TcpServer.end);
        }else if(StringUtils.equalsIgnoreCase(up,"reset")) {
            TcpServer.returnMsg(TcpServer.reset);
        }else{
            TcpServer.returnMsg(TcpServer.left);
            TcpServer.returnMsg(TcpServer.start);
        }

        LogUtils.info("摆轮已经摆动");
    }

    @GetMapping("/testTcpUtil")
    public void testTcpUtil(@RequestParam(value = "up",required = false) String up){
        if(StringUtils.equalsIgnoreCase(up,"left")) {
            SocketServerUtil.sendToALl(TcpServerUtil.left);
        }else if(StringUtils.equalsIgnoreCase(up,"right")) {
            SocketServerUtil.sendToALl(TcpServerUtil.right);
        }else if(StringUtils.equalsIgnoreCase(up,"back")) {
            SocketServerUtil.sendToALl(TcpServerUtil.back);
        }else if(StringUtils.equalsIgnoreCase(up,"start")) {
            SocketServerUtil.sendToALl(TcpServerUtil.start);
        }else if(StringUtils.equalsIgnoreCase(up,"end")) {
            SocketServerUtil.sendToALl(TcpServerUtil.end);
        }else if(StringUtils.equalsIgnoreCase(up,"reset")) {
            SocketServerUtil.sendToALl(TcpServerUtil.reset);
        }else{
            SocketServerUtil.sendToALl(TcpServerUtil.left);
            SocketServerUtil.sendToALl(TcpServerUtil.start);
        }
    }

    /*@PostMapping("/sdk.post")
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // 获取HTTP请求头信息
        String contentType = request.getContentType();
        int contentLength = request.getContentLength();
        String boundary = extractBoundary(contentType);
        InputStream inputStream = request.getInputStream();

        // 处理请求体数据
        OutputStream outputStream = new FileOutputStream(new File("E:\\test.jpg"));
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        boolean isFileData = false;
        String fileName = "";
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            String chunk = new String(buffer, 0, bytesRead);
            if (chunk.startsWith("--" + boundary)) {
                isFileData = false;
            } else if (chunk.contains("Content-Disposition: form-data") && chunk.contains("; filename=")) {
                isFileData = true;
                fileName = extractFileName(chunk);
            } else if (isFileData) {
                outputStream.write(buffer, 0, bytesRead);
            }
        }
        outputStream.close();
    }*/

    // 从 HTTP Content-Type 头部信息中解析出 boundary
    private String extractBoundary(String contentType) {
        int boundaryIndex = contentType.indexOf("boundary=");
        return contentType.substring(boundaryIndex + 9);
    }

    // 从 HTTP 请求体数据的 Content-Disposition 行中解析出文件名
    private String extractFileName(String chunk) {
        String[] tokens = chunk.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename=")) {
                return token.substring(token.indexOf("=") + 1).trim().replace("\"", "");
            }
        }
        return "";
    }

}
