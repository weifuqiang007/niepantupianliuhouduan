package com.niepan.modules.zhinenganjianback.controller;

import com.niepan.modules.zhinenganjianback.model.BackStageManager;
import com.niepan.modules.zhinenganjianback.service.BackstageManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: liuchenyu
 * @date: 2023/7/18
 */

@Controller
@RequestMapping("/BackStageManager")
public class BackstageManagerController {

    @Autowired
    BackstageManagerService backstageManagerService;


    @PostMapping("/addSidebar")
    public void addSidebar(BackStageManager backStageManager){

        backstageManagerService.addSidebar(backStageManager);

    }


}
