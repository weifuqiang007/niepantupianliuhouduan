package com.niepan.modules.zhinenganjianback.controller;

import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.niepan.common.utils.logs.LogUtils;
import com.niepan.modules.zhinenganjianback.VO.FTPVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: liuchenyu
 * @date: 2023/8/22
 */
@RestController
public class GetFTPController {

    //https://venus-hz.zt-express.com
    @GetMapping("/GetFTPInfo")
    public String GetFTPInfo(@RequestParam(value = "siteCode",required = true)String siteCode,
                           @RequestParam(value = "snCode",required = false)String snCode,
                           @RequestParam(value = "lineCode",required = false)String lineCode){

        FTPVO ftpvo = new FTPVO(siteCode,snCode,lineCode);

        String jsonString = JSON.toJSONString(ftpvo);
        System.out.println("jsonString = " + jsonString);

        String result2 = HttpRequest.post("https://venus-hz.zt-express.com/device/ftp")
                .header(Header.CONTENT_TYPE, "application/json;charset=utf-8")//头信息，多个头信息多次调用此方法即可
                .body(jsonString)//表单内容
                .timeout(500)//超时，毫秒
                .execute().body();
        LogUtils.info("向中通请求ftp信息 : " + result2);
        return result2;
    }
}
