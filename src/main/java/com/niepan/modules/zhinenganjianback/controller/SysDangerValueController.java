package com.niepan.modules.zhinenganjianback.controller;
import cn.hutool.core.collection.CollectionUtil;
import com.google.common.collect.Lists;
import com.niepan.common.utils.ConfigConstant;
import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.modules.sys.controller.AbstractController;
import com.niepan.modules.sys.entity.SysUserEntity;
import com.niepan.modules.sys.service.SysConfigService;
import com.niepan.modules.zhinenganjianback.VO.DangerValueVo;
import com.niepan.modules.zhinenganjianback.entity.SysDangerValue;
import com.niepan.modules.zhinenganjianback.lib.enumeration.EM_INSIDE_OBJECT_TYPE;
import com.niepan.modules.zhinenganjianback.service.SysDangerValueService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author : kyz on 2023/11/8 17:14
 */
@RestController
@RequestMapping("/dangerValue")
public class SysDangerValueController extends AbstractController {

    @Autowired
    private SysDangerValueService sysDangerValueService;
    @Autowired
    private SysConfigService configService;

    /**
     * 获取列表
     * @return
     */
    @RequestMapping("/list")
    public ApiResponse<DangerValueVo> listForAll(){
        List<SysDangerValue> list= sysDangerValueService.list();
        Map<Integer,SysDangerValue> map=new HashMap<>();
        if(CollectionUtil.isEmpty(list)){
            list=new ArrayList<>();
        }else{
            map= list.stream().collect(Collectors.toMap(SysDangerValue::getDangerCode, a->a));
        }
        for (EM_INSIDE_OBJECT_TYPE item: EM_INSIDE_OBJECT_TYPE.values()){
            if(item.equals(EM_INSIDE_OBJECT_TYPE.EM_INSIDE_OBJECT_UNKNOWN)){
                continue;
            }
            SysDangerValue dangerValue=null;
            if(map.containsKey(item.getValue())){
                dangerValue=map.get(item.getValue());
                dangerValue.setDangerName(item.getNote());
            }else{
                dangerValue=new SysDangerValue();
                dangerValue.setDangerCode(item.getValue());
                dangerValue.setDangerName(item.getNote());
                dangerValue.setDangerValue(100);
                map.put(item.getValue(),dangerValue);
            }
        }
        String dangerValue= configService.getValue(ConfigConstant.DANGER_VALUE);
        DangerValueVo vo=new DangerValueVo();
        vo.setEnable(dangerValue);
        vo.setList(Lists.newArrayList(map.values()));

        return Result.success(vo);
    }

    /**
     * 新增和修改
     * @param dangerValueList
     * @return
     */
    @RequestMapping("/save")
    @Transactional
    public ApiResponse<Boolean> insertOrUpdate(@RequestBody DangerValueVo dangerValueList){
        SysUserEntity userEntity= getUser();
        return sysDangerValueService.insertOrUpdate(userEntity,dangerValueList);
    }


}
