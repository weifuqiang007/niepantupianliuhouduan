package com.niepan.modules.zhinenganjianback.controller;

import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.modules.zhinenganjianback.dao.SecurityMachineDao;
import com.niepan.modules.zhinenganjianback.model.PackageItemInfo;
import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: liuchenyu
 * @date: 2023/8/17
 */
@RestController
public class ReceiveInfo {

    @Autowired
    private SecurityMachineDao securityMachineDao;

    @PostMapping("/ReceiveInfo")
    public ApiResponse ReceiveInfo(@RequestBody SecurityMachinePicture securityMachinePicture){
        securityMachineDao.insertInfo(securityMachinePicture);
        return Result.success();
    }

    @PostMapping("/ReceiveInfoDanger")
    public ApiResponse ReceiveInfoDanger(@RequestBody PackageItemInfo packageItemInfo){
        securityMachineDao.insertInfoDanger(packageItemInfo);
        return Result.success();
    }
}
