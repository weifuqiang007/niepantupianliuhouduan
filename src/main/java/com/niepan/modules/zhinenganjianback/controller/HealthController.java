package com.niepan.modules.zhinenganjianback.controller;

import com.niepan.modules.zhinenganjianback.model.DiskHealth;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 健康信息
 * @author: kongyz
 * @date: 2023/10/27 on 16:40
 */
@RestController
public class HealthController {

    @RequestMapping("/fileSystem")
    @ResponseBody
    public Map<String,Object> fileSystem(){
        String os = System.getProperty("os.name").toLowerCase();
        FileSystemView fsv=FileSystemView.getFileSystemView();

        List<DiskHealth> diskHealthList=new ArrayList<>();
        if(StringUtils.contains(os,"windows")){
            File[] fs= File.listRoots();
            // 显示磁盘卷标
            for (int i = 0; i < fs.length; i++) {
                if(StringUtils.isBlank(fsv.getSystemDisplayName(fs[i]))){
                    continue;
                }
                DiskHealth health=new DiskHealth();
                health.setDiskName(fs[i].getPath());
                //总大小
                Long totalSpace= fs[i].getTotalSpace();
                health.setTotalSpace(FormetFileSize(totalSpace));
                //可用大小
                Long freeSpace= fs[i].getFreeSpace();
                health.setFreeSpace(FormetFileSize(freeSpace));

                BigDecimal total=new BigDecimal(totalSpace);
                BigDecimal free=new BigDecimal(freeSpace);

                BigDecimal rate= free.divide(total,4,BigDecimal.ROUND_HALF_UP);

                health.setRate(rate);
                diskHealthList.add(health);
            }
        }
        Map<String,Object> map=new HashMap<>();
        map.put("osName",os);
        map.put("disk",diskHealthList);
        return map;
    }

    public static String FormetFileSize(long fileS) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format((double) fileS / 1024) + "K";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format((double) fileS / 1048576) + "M";
        } else {
            fileSizeString = df.format((double) fileS / 1073741824) + "G";
        }
        return fileSizeString;
    }


}
