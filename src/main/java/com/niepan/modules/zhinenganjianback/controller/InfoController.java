package com.niepan.modules.zhinenganjianback.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: kongyz
 * @date: 2023/10/11
 */
@RestController
public class InfoController {

    @RequestMapping("/gc")
    public void gc() {
        System.gc();
    }

}
