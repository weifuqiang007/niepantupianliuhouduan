package com.niepan.modules.zhinenganjianback.controller;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.http.ContentType;
import com.alibaba.fastjson.JSON;
import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.common.utils.base64MultipartFile.Base64ToMultipartFile;
import com.niepan.common.utils.logs.LogUtils;
import com.niepan.common.utils.minIO.MinIOUtil;
import com.niepan.common.utils.minIO.MinIOUtilForUs;
import com.niepan.common.websocket.server.WarningWebSocketServer;
import com.niepan.modules.zhinenganjianback.demo.util.ThreadPoolUtil;
import com.niepan.modules.zhinenganjianback.dto.GetHeartBeatDTO;
import com.niepan.modules.zhinenganjianback.dto.GetHeartInfoDto;
import com.niepan.modules.zhinenganjianback.dto.GetYwtkDto;
import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
import com.niepan.modules.zhinenganjianback.model.SecurityMachineStatus;
import com.niepan.modules.zhinenganjianback.service.SecurityMachineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author: liuchenyu
 * @date: 2023/9/12
 */


@RestController
@RequestMapping("/api/open/ywtk")
public class YouWeiTaiKeController {
    private static final Logger log= LoggerFactory.getLogger(YouWeiTaiKeController.class);

    @Value("${file.picPath}")
    private String picPath;

    // 全局变量 service的全局变量
    @Autowired
    private SecurityMachineService securityMachineService;

    @Autowired
    WarningWebSocketServer warningWebSocketServer;

    @Autowired
    private MinIOUtil minIOUtil;

    @Autowired
    private MinIOUtilForUs minIOUtilForUs;

    public static ThreadPoolExecutor pool =
            new ThreadPoolExecutor(10, 30, 10L, TimeUnit.SECONDS, new LinkedBlockingDeque<>(20));


    // 实现接受图片的请求
    @RequestMapping(value = "/sendImg", method = RequestMethod.POST,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ApiResponse getImgInfo(@RequestParam("img") MultipartFile img, HttpServletResponse response, HttpServletRequest request) throws IOException {
        String currentTime=DateUtil.format(DateUtil.date(), DatePattern.PURE_DATETIME_FORMAT);

        String snCode= request.getParameter("snCode");
        String dwsCode = request.getParameter("dwsCode");
        log.info("youWeiTaiKe接收图片接口，snCode:{};dwsCode:{};checktime:{}",snCode,dwsCode,currentTime);

        if(Objects.isNull(img)){
            return Result.error("文件内容为空");
        }

        String base64B = Base64Utils.encodeToString(img.getBytes());

        String picName = snCode+"@&"+dwsCode+"@&"+currentTime+ ".jpg";

        /*SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String datePath = sdf.format(new Date());//20230215
        //存放图片的路径
        String filePath = picPath + getYwtkDto.getDevice_sn() + getYwtkDto.getLine_code() + "/" + datePath + "/";

        //图片存储到本地
        JsonToFile.stringToJpg(getYwtkDto.getImg_base64_side(),filePath+picName);*/

//        MultipartFile multipartFile = new Base64ToMultipartFile(base64B, "data:image/jpg;base64");

        ThreadPoolUtil.getThreadPool().submit(()->{
            minIOUtil.uploadFile(img,picName);
        });

        //这里新加一个minio，，给服务器传送图片
//        pool.submit(() -> {
//            minIOUtilForUs.uploadFile(multipartFile,picName);
//        });

        //日志记录
        LogUtils.info("photo name is " + picName + "  图片已经上传minIO");
        LogUtils.info("photo info is " + currentTime);

        //将要插入数据库的数据封装到对象中
        SecurityMachinePicture securityMachinePicture = new SecurityMachinePicture();
        securityMachinePicture.setName(picName);
        securityMachinePicture.setDevice_sn(snCode);
        securityMachinePicture.setLine_code(dwsCode);
        securityMachinePicture.setTime(currentTime.toString());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        String date = df.format(new Date());//2023-02-17 13:49:08
        securityMachinePicture.setCurrentTime(date);

        Map<String,Object> map = new HashMap<>();

        map.put("lineCode",snCode);
        map.put("picName",picName);
//        map.put("picPath", getYwtkDto.getDevice_sn() + getYwtkDto.getLine_code() + "/" + datePath + "/" + picName);

        String webSocketTxt = JSON.toJSONString(map);

//        warningWebSocketServer.sendMessage(webSocketTxt);

        //数据库插入 并推送给大华设备
        Boolean res = securityMachineService.getImgInfo(securityMachinePicture,base64B);
        if (res != null && res){
            LogUtils.info("图片保存成功");
            return Result.success("图片保存成功");
        }
        LogUtils.error("保存图片失败" + picName);
        return Result.error("保存图片失败");
    }


    // 安检机状态和故障信息上报
    @RequestMapping(value = "/status_send", method = RequestMethod.POST)
    public ApiResponse getStatus(@RequestBody GetHeartBeatDTO getHeartBeatDTO){
        System.out.println(getHeartBeatDTO.getDevice_sn());
        System.out.println(getHeartBeatDTO.getLine_code());
        System.out.println(getHeartBeatDTO.getStatus_code());
        System.out.println(getHeartBeatDTO.getFault_level());
        System.out.println(getHeartBeatDTO.getTime());
        System.out.println(getHeartBeatDTO.getStatus_desc());

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if (getHeartBeatDTO.getFault_level().equals("0")){
            LogUtils.info("安检机状态正常");
            LogUtils.info(formatter.format(new Date()) +"@#"+ getHeartBeatDTO.getDevice_sn() +"@#"+
                    getHeartBeatDTO.getFault_level() +"@#" +getHeartBeatDTO.getStatus_desc());
        }
        else{
            LogUtils.error("安检机状态异常");
            LogUtils.error(formatter.format(new Date()) +"@#"+ getHeartBeatDTO.getDevice_sn() +"@#"+
                    getHeartBeatDTO.getFault_level() +"@#" +getHeartBeatDTO.getStatus_desc());
        }

        SecurityMachineStatus securityMachineStatus = new SecurityMachineStatus();
        securityMachineStatus.setDevice_sn(getHeartBeatDTO.getDevice_sn());
        securityMachineStatus.setLine_code(getHeartBeatDTO.getLine_code());
        securityMachineStatus.setStatus_code(getHeartBeatDTO.getStatus_code());
        securityMachineStatus.setStatus_desc(getHeartBeatDTO.getStatus_desc());
        securityMachineStatus.setFault_level(getHeartBeatDTO.getFault_level());
        securityMachineStatus.setTime(getHeartBeatDTO.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String date = df.format(new Date());//20230215
        securityMachineStatus.setMsgtime(date);

        String status = securityMachineService.SecurityMachineStatus(securityMachineStatus);
        System.out.println("status" + status);

        return Result.success(status);
    }

    SimpleDateFormat num = new SimpleDateFormat("HH:mm:ss");
    String formatOld = num.format(new Date());

    // 安检机心跳
    @RequestMapping(value = "/heart", method = RequestMethod.POST)
    public ApiResponse getHeart(@RequestBody GetHeartInfoDto getHeartInfoDto){

        System.out.println("Device_sn is : "+getHeartInfoDto.getDevice_sn());
        System.out.println("Line_code is : "+getHeartInfoDto.getLine_code());
        System.out.println("Status_code is : "+getHeartInfoDto.getStatus_code());
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(formatter.format(date));

        if (getHeartInfoDto.getStatus_code().equals("4")){
            LogUtils.error("heart beat time is :"+formatter.format(date) + "Device_sn is :"+getHeartInfoDto.getDevice_sn() +
                    "Line_code is :"+ getHeartInfoDto.getLine_code() + "Status_code() is :" + getHeartInfoDto.getStatus_code());

        }else {
            LogUtils.info("heart beat time is :"+formatter.format(date) + "Device_sn is :"+getHeartInfoDto.getDevice_sn() +
                    "Line_code is :"+ getHeartInfoDto.getLine_code() + "Status_code() is :" + getHeartInfoDto.getStatus_code());
        }
        try {

            String format = num.format(new Date());

            Date dateOld=num.parse(formatOld);
            Date date2=num.parse(format);

            if (date2.getTime() - dateOld.getTime() > 10000){
                securityMachineService.getHeart(getHeartInfoDto);
                formatOld = format;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Result.success();
    }

}