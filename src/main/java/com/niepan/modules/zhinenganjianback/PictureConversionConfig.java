package com.niepan.modules.zhinenganjianback;

import cn.hutool.core.date.DateUtil;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author: liuchenyu
 * @date: 2023/5/31
 */
@SpringBootConfiguration
public class PictureConversionConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // /images/**是静态映射， file:/root/images/是文件在服务器的路径
        registry.addResourceHandler("/image/**")
                .addResourceLocations("file:///D:/niepan/filePath/fileToDaHua");
    }
}
