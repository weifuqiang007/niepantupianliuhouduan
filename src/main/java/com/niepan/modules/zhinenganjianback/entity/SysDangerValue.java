package com.niepan.modules.zhinenganjianback.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName sys_danger_value
 */
@TableName(value ="sys_danger_value")
public class SysDangerValue implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 违禁品编号
     */
    private Integer dangerCode;

    /**
     * 违禁品名称
     */
    private String dangerName;

    /**
     * 阈值
     */
    private Integer dangerValue;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createName;

    /**
     * 最后一次编辑时间
     */
    private Date updateTime;

    /**
     * 最后一次编辑人
     */
    private String updateName;

    /**
     * 是否删除,1是;0否
     */
    private String delFlag;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 违禁品编号
     */
    public Integer getDangerCode() {
        return dangerCode;
    }

    /**
     * 违禁品编号
     */
    public void setDangerCode(Integer dangerCode) {
        this.dangerCode = dangerCode;
    }

    /**
     * 违禁品名称
     */
    public String getDangerName() {
        return dangerName;
    }

    /**
     * 违禁品名称
     */
    public void setDangerName(String dangerName) {
        this.dangerName = dangerName;
    }

    /**
     * 阈值
     */
    public Integer getDangerValue() {
        return dangerValue;
    }

    /**
     * 阈值
     */
    public void setDangerValue(Integer dangerValue) {
        this.dangerValue = dangerValue;
    }

    /**
     * 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 创建人
     */
    public String getCreateName() {
        return createName;
    }

    /**
     * 创建人
     */
    public void setCreateName(String createName) {
        this.createName = createName;
    }

    /**
     * 最后一次编辑时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 最后一次编辑时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 最后一次编辑人
     */
    public String getUpdateName() {
        return updateName;
    }

    /**
     * 最后一次编辑人
     */
    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

    /**
     * 是否删除,1是;0否
     */
    public String getDelFlag() {
        return delFlag;
    }

    /**
     * 是否删除,1是;0否
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SysDangerValue other = (SysDangerValue) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getDangerCode() == null ? other.getDangerCode() == null : this.getDangerCode().equals(other.getDangerCode()))
            && (this.getDangerName() == null ? other.getDangerName() == null : this.getDangerName().equals(other.getDangerName()))
            && (this.getDangerValue() == null ? other.getDangerValue() == null : this.getDangerValue().equals(other.getDangerValue()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getCreateName() == null ? other.getCreateName() == null : this.getCreateName().equals(other.getCreateName()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getUpdateName() == null ? other.getUpdateName() == null : this.getUpdateName().equals(other.getUpdateName()))
            && (this.getDelFlag() == null ? other.getDelFlag() == null : this.getDelFlag().equals(other.getDelFlag()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getDangerCode() == null) ? 0 : getDangerCode().hashCode());
        result = prime * result + ((getDangerName() == null) ? 0 : getDangerName().hashCode());
        result = prime * result + ((getDangerValue() == null) ? 0 : getDangerValue().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getCreateName() == null) ? 0 : getCreateName().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getUpdateName() == null) ? 0 : getUpdateName().hashCode());
        result = prime * result + ((getDelFlag() == null) ? 0 : getDelFlag().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", dangerCode=").append(dangerCode);
        sb.append(", dangerName=").append(dangerName);
        sb.append(", dangerValue=").append(dangerValue);
        sb.append(", createTime=").append(createTime);
        sb.append(", createName=").append(createName);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", updateName=").append(updateName);
        sb.append(", delFlag=").append(delFlag);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}