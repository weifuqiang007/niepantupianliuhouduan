package com.niepan.modules.zhinenganjianback.dao;

import com.niepan.modules.zhinenganjianback.model.SecurityMachineStatus;

public interface AtxSecurityMachineDao {
    Integer securityMachineStatus(SecurityMachineStatus securityMachineStatus);
}
