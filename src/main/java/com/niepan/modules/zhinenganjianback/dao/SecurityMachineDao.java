package com.niepan.modules.zhinenganjianback.dao;

import com.niepan.modules.zhinenganjianback.model.PackageItemInfo;
import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
import com.niepan.modules.zhinenganjianback.model.SecurityMachineStatus;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface SecurityMachineDao {

    void insertImage(@Param("emDangerGrade") int emDangerGrade,
                     @Param("emObjType") int emObjType,
                     @Param("emObjName") String emObjName,
                     @Param("nSimilarity") int nSimilarity,
                     @Param("stuBoundingBoxs") String stuBoundingBoxs);

    void updateThreshold(@Param("value") int value,
                         @Param("note") String note,
                         @Param("threshold") Integer threshold);

    @MapKey("id")
    List<Map<String,Object>> getObjectByType(@Param("value") int value);

    void insertThresholdForOld(@Param("emObjType") int emObjType,
                               @Param("itemName") String itemName,
                               @Param("threshole") int threshole,
                               @Param("personName") String personName,
                               @Param("createTime") Date createTime);



    Integer savePicture(@Param("name") String name,
                        @Param("device_sn") String device_sn,
                        @Param("line_code") String line_code,
                        @Param("time") Long time,
                        @Param("currentTime")String currentTime);


    /**
     * 安检机状态插入
     * @param securityMachineStatus
     * @return
     */
    Integer securityMachineStatus(SecurityMachineStatus securityMachineStatus);


    Integer insertImgInfo(SecurityMachinePicture securityMachinePicture);


    void replenishPicture(@Param("id") String id,
                          @Param("size") int size,
                          @Param("date") String date);


    void updateStatus(@Param("name")  String name);


    void insertIdle(@Param("name") String name, @Param("idle") Boolean idle);

    void insertInfo(SecurityMachinePicture securityMachinePicture);

    void insertInfoTwice(SecurityMachinePicture securityMachinePicture);

    void insertInfoDanger(PackageItemInfo packageItemInfo);
}
