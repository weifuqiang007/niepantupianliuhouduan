package com.niepan.modules.zhinenganjianback.dao;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface DeviceManagerDao {

    void insertImage(@Param("emDangerGrade") int emDangerGrade,
                     @Param("emObjType") int emObjType,
                     @Param("emObjName") String emObjName,
                     @Param("nSimilarity") int nSimilarity,
                     @Param("stuBoundingBoxs") String stuBoundingBoxs,
                     @Param("id") String id,
                     @Param("areaCode") String areaCode);

    void updateThreshold(@Param("value") int value,
                         @Param("note") String note,
                         @Param("threshold") Integer threshold,
                         @Param("personName") String personName,
                         @Param("deviceIP") String deviceIP,
                         @Param("enable") Integer enable);

    @MapKey("id")
    List<Map<String,Object>> getObjectByType(@Param("value") int value);

    void insertThresholdForOld(@Param("emObjType") int emObjType,
                               @Param("itemName") String itemName,
                               @Param("threshole") int threshole,
                               @Param("personName") String personName,
                               @Param("deviceIP") String deviceIP,
                               @Param("enable") Integer enable);


    Integer savePicture(@Param("uuid") String uuid,
                        @Param("name") String name,
                        @Param("device_sn") String device_sn,
                        @Param("line_code") String line_code,
                        @Param("time") Long time,
                        @Param("currentTime")String currentTime);

    Map<String, Object> getAllDevice(@Param("device_sn") String device_sn);

    List<Map<String, Object>> getDevice(@Param("ip") String ip);

    Map<String,Object> getDeviceIp(@Param("valueOf") String valueOf);

    String getDWSNcode(@Param("snCode") String snCode);

    String getCurrentTime(@Param("id") String id);

    Map<String,Object> getPicName(@Param("picture_id") String picture_id);

    void updateHttp(@Param("id") String id);

    /**
     * 修改推送中通状态和推送时间
     * @param id
     * @param ajjTime   修改为推送时间
     */
    void updateHttpAndTime(@Param("id") String id, @Param("ajjTime")String ajjTime,@Param("httpStatus")String httpStatus);

    void updateFTP(@Param("imageId") String imageId);

    Map<String,Object> getRequestURL();

    Map<String, Object> getSwing();

    String getAllDevice1(@Param("deviceid") String deviceid);

    void updateHttpNoDanger(@Param("id") String id);

    Map<String, Object> getDeviceByIp(@Param("mip") String mip);

    void insertData(@Param("imageId") String imageId, @Param("toJSONString") String toJSONString);

//    String getDWSNcode(@Param("deviceIp") String deviceIp);
}
