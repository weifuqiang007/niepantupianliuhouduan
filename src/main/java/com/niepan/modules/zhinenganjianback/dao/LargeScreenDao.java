package com.niepan.modules.zhinenganjianback.dao;

import com.niepan.modules.zhinenganjianback.VO.AreaProhibitedItemCount;
import com.niepan.modules.zhinenganjianback.VO.DataVo;
import com.niepan.modules.zhinenganjianback.VO.HeartDataVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author: liuchenyu
 * @date: 2023/3/27
 */
public interface LargeScreenDao {
    //保存正常件信息
    int saveNormalInfo(@Param("tableName") String tableName, @Param("data") DataVo data, @Param("parseTime") String  parseTime);


    //保存违禁品信息
    int saveDangerInfo(@Param("tableName") String tableName, @Param("data") DataVo data, @Param("parseTime") String  parseTime);

    /**
     * 创建正常件表
     * @param normalName
     */
    void createNormalTable(String normalName);

    /**
     * 创建违禁品表
     * @param contrabandName
     */
    void createContrabandTable(String contrabandName);

    //选着数据库
    @Select("use ${databaseName}")
    void useDatabase(@Param("databaseName") String databaseName);

    /**
     * 检查表是否存在
     * @param databaseName
     * @param tableName
     * @return
     */
    @Select("SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = #{databaseName} AND table_name = #{tableName}")
    int checkTableExists(@Param("databaseName") String databaseName, @Param("tableName") String tableName);
    /**
     * 获取所有的地区
     * @return
     */
    List<Map<String, Object>> getArea();

    /**
     * 获取所有的省及其违禁品数量
     * @param province
     * @return
     */
    Map<String, Object> getCountByArea(@Param("province") String province);

    /**
     * 获取指定省下的所有市
     * @param areaName
     * @return
     */
    List<Map<String, Object>> getAreaByName(@Param("areaName") String areaName);

    /**
     * 获取指定市的违禁品数量
     * @param city
     * @return
     */
    Map<String, Object> getCountByCity(@Param("city") String city);

    /**
     * 获取指定市下的所有区
     * @param areaName
     * @return
     */
    List<Map<String, Object>> getAreaByCity(@Param("areaName") String areaName);

    /**
     * 获取指定区下的违禁品数量
     * @param centre
     * @return
     */
    Map<String, Object> getCountByCentre(@Param("centre") String centre);

    /**
     * 按照给定的日，周，月返回给定时间内的违禁品数量
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> getDangerCountByTime(@Param("machineGrade") String machineGrade,
                                                   @Param("machineBrand") String machineBrand,
                                                   @Param("startTime") String startTime,
                                                   @Param("endTime") String endTime);

    List<Map<String, Object>> getDangerRank(@Param("machineGrade") String machineGrade,
                                            @Param("machineBrand") String machineBrand,
                                            @Param("startTime") String startTime,
                                            @Param("endTime") String endTime,
                                            @Param("areaName") String areaName);

    List<Map<String, Object>> getTotal();

    Integer getCount(@Param("startTime") String startTime,
                     @Param("endTime") String endTime,
                     @Param("areaName") String areaName);

    Map<String, Object> getCountByType(@Param("areaName") String areaName,
                                             @Param("machineGrade") String machineGrade,
                                             @Param("machineBrand") String machineBrand,
                                             @Param("day") String day,
                                             @Param("value") int value);

    Integer getTodayCount(@Param("areaName") String areaName,
                          @Param("format") String format);

    Integer getDangerCount(@Param("areaName") String areaName,
                           @Param("format") String format);

    List<Map<String, Object>> getDangerByType(@Param("areaName") String areaName,
                                              @Param("format") String format);

    Integer getTodayDangerCount(@Param("areaName") String areaName,
                                @Param("format") String format);

    List<Map<String, Object>> getTotalByCentre(@Param("areaName") String areaName,
                                               @Param("startTime") String startTime,
                                               @Param("endTime") String endTime);

    List<Map<String, Object>> getFirstTotalByCentre(@Param("areaName") String areaName,
                                                    @Param("startTime") String startTime,
                                                    @Param("endTime") String endTime);

    Integer getAllCount(@Param("areaName") String areaName,
                        @Param("startTime") String startTime,
                        @Param("endTime") String endTime);

    Integer DangerCount(@Param("areaName") String areaName,
                        @Param("startTime") String startTime,
                        @Param("endTime") String endTime);

    List<Map<String, Object>> getDangerArea(@Param("startTime") String startTime,
                                            @Param("endTime") String endTime,
                                            @Param("areaName") String areaName);

    Integer getAllCountNew(@Param("areaName") String areaName,
                           @Param("startTime") String startTime,
                           @Param("endTime") String endTime);

    Integer DangerCountNew(@Param("areaName") String areaName,
                           @Param("startTime") String startTime,
                           @Param("endTime") String endTime);

    List<Map<String, Object>> getDangerByTypeNew(@Param("endTime") String today,
                                                 @Param("startTime") String beforeMonth,
                                                 @Param("tableName") String tableName);

    List<String> getTableName(@Param("areaName") String areaName);

    List<String> getDangerTableName(@Param("areaName") String areaName);


    Integer getTodayDangerCountNew(@Param("areaName") String areaName,
                                   @Param("endTime") String today,
                                   @Param("startTime") String beforeMonth);

    List<String> getAreaNew();


    Map<String, Object> getCountByAreaNew(@Param("areaName") String province,
                                          @Param("startTime") String beforeMonth,
                                          @Param("endTime") String today);

    List<String> getAreaByCityNew(@Param("areaName") String areaName);

    Map<String, Object> getCountByCentreNew(@Param("county") String county,
                                            @Param("startTime") String beforeMonth,
                                            @Param("endTime") String today);

    List<String> getAreaByNameNew(@Param("areaName") String areaName);

    Map<String, Object> getCountByCityNew(@Param("city") String city,
                                          @Param("startTime") String beforeMonth,
                                          @Param("endTime") String today);

    List<Map<String, Object>> getDangerRankNew(@Param("startTime") String beforeMonth,
                                               @Param("endTime") String today,
                                               @Param("areaName") String areaName,
                                               @Param("tableName") String tableName);

    Integer getCountNew(@Param("startTime") String beforeMonth,
                        @Param("endTime") String today,
                        @Param("areaName") String areaName);


    List<Map<String, Object>> getStatistics(@Param("startTime") String beforeMonth,
                                            @Param("endTime") String today,
                                            @Param("areaName") String areaName);

    List<Map<String, Object>> getAreaAll(@Param("startTime") String beforeMonth,
                                         @Param("endTime") String today,
                                         @Param("areaName") String areaName);

    List<Map<String, Object>> getStatisticsByProvince(@Param("startTime") String beforeMonth,
                                                      @Param("endTime") String today,
                                                      @Param("areaName") String areaName);

    List<Map<String, Object>> getStatisticsByCity(@Param("startTime") String beforeMonth,
                                                  @Param("endTime") String today,
                                                  @Param("areaName") String areaName);

    List<Map<String, Object>> getStatisticsByCentre(@Param("startTime") String beforeMonth,
                                                    @Param("endTime") String today,
                                                    @Param("areaName") String areaName);

    /**
     * 获取周违禁品数量
     * @param startTime
     * @param endTime
     * @param areaName
     * @return
     */
    List<AreaProhibitedItemCount> getListWeek(@Param("startTime")String startTime, @Param("endTime")String endTime, @Param("areaName")String areaName);

    /**
     * 获取今日违禁品数量
     * @param Time
     * @param areaName
     * @return
     */
    List<AreaProhibitedItemCount> getListToday(@Param("Time")String Time, @Param("areaName")String areaName);

    List<Map<String, Object>> getDangerByChannel(@Param("tableName") String tableName);

    String getTableNameByCounty(@Param("areaName") String areaName);

    HeartDataVO getHeartBeatByDay(@Param("parseTime") String parseTime);

    Integer insertHeartBeatData(HeartDataVO data);

    Integer updateHeartBeatData(HeartDataVO data);
}
