package com.niepan.modules.zhinenganjianback.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;


/**
 * @author:liuchenyu
 * @data:2023/02/27
 */
@Repository
public interface ContrabandListMapper {
    /**
     * 获取所有的地区
     * @return
     */
    List<Map<String, Object>> getArea();

    /**
     * 根据条件查询违禁品
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> getItem(@Param("uniqueCode") String uniqueCode,
                                      @Param("machineGrade") String machineGrade,
                                      @Param("machineBrand") String machineBrand,
                                      @Param("startTime") String startTime,
                                      @Param("endTime") String endTime);

    /**
     * 根据条件查询违禁品的数量统计
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    Map<String, Object> getCount(@Param("uniqueCode") String uniqueCode,
                                 @Param("machineGrade") String machineGrade,
                                 @Param("machineBrand") String machineBrand,
                                 @Param("startTime") String startTime,
                                 @Param("endTime") String endTime);

    /**
     * 根据唯一标识查询图片的详情
     * @param uuid
     * @return
     */
    List<Map<String, Object>> getDetail(@Param("uuid") String uuid);

    /**
     * 根据省市获取集散中心列表
     * @param uniqueCode
     * @return
     */
    List<Map<String, Object>> getCentre(@Param("uniqueCode") String uniqueCode);

    /**
     * 获取总数以及违禁品的总数
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>>  getTotal(@Param("uniqueCode") String uniqueCode,
                                 @Param("machineGrade") String machineGrade,
                                 @Param("machineBrand") String machineBrand,
                                 @Param("startTime") String startTime,
                                 @Param("endTime") String endTime);

    /**
     * 获取某个违禁品类下的所有违禁品信息
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @param typeId
     * @return
     */
    List<Map<String, Object>> getCountDetail(@Param("machineGrade") String machineGrade,
                                             @Param("machineBrand") String machineBrand,
                                             @Param("startTime") String startTime,
                                             @Param("endTime") String endTime,
                                             @Param("typeId") String typeId,
                                             @Param("uniqueCode") String uniqueCode);

    long getCountDetail_COUNT(@Param("machineGrade") String machineGrade,
                                             @Param("machineBrand") String machineBrand,
                                             @Param("startTime") String startTime,
                                             @Param("endTime") String endTime,
                                             @Param("typeId") String typeId,
                                             @Param("uniqueCode") String uniqueCode);

    /**
     * 根据条件获取信息的统计数（统计曲线图使用）
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param day
     * @return
     */
    List<Map<String, Object>> getCountStatistics(@Param("uniqueCode") String uniqueCode,
                                                 @Param("machineGrade") String machineGrade,
                                                 @Param("machineBrand") String machineBrand,
                                                 @Param("day") String day);

    /**
     * 获取违禁品的总数
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> getDangerTotal(@Param("uniqueCode") String uniqueCode,
                                             @Param("machineGrade") String machineGrade,
                                             @Param("machineBrand") String machineBrand,
                                             @Param("startTime") String startTime,
                                             @Param("endTime") String endTime);

    /**
     * 根据条件查询每天的过包数
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param day
     * @return
     */
    /*Map<String, Object> getCountByDay(@Param("uniqueCode") String uniqueCode,
                                      @Param("machineGrade") String machineGrade,
                                      @Param("machineBrand") String machineBrand,
                                      @Param("day") String day);*/

    /**
     * 根据条件获取信息的统计数（统计曲线图使用）
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param day
     * @param value
     * @return
     */


    /**
     * 查询所有的图片信息
     * @return
     */
    List<Map<String, Object>> getPicture(@Param("uniqueCode") String uniqueCode,
                                         @Param("startTime") String startTime,
                                         @Param("endTime") String endTime,
                                         @Param("machineGrade") String lineCode,
                                         @Param("isDanger") Boolean isDanger);

    long getPicture_COUNT(@Param("uniqueCode") String uniqueCode,
                                       @Param("startTime") String startTime,
                                       @Param("endTime") String endTime,
                                       @Param("machineGrade") String lineCode);


    List<String> getObjType(@Param("picture_id") String picture_id);

    /*String getCountByType(@Param("uniqueCode") String uniqueCode,
                          @Param("machineGrade") String machineGrade,
                          @Param("machineBrand") String machineBrand,
                          @Param("startTime") String localTimeStart,
                          @Param("endTime") String localTimeEnd,
                          @Param("value") int value);*/

    List<Map<String, Object>> getCountByType(@Param("uniqueCode") String uniqueCode,
                          @Param("machineGrade") String machineGrade,
                          @Param("machineBrand") String machineBrand,
                          @Param("startTime") String localTimeStart,
                          @Param("endTime") String localTimeEnd);

    Map<String, Object> getCountByDay(@Param("uniqueCode") String uniqueCode,
                                      @Param("machineGrade") String machineGrade,
                                      @Param("machineBrand") String machineBrand,
                                      @Param("startTime") String localTimeStart,
                                      @Param("endTime") String localTimeEnd);

    List<String>  getChannel(@Param("uniqueCode") String uniqueCode);

    List<Map<String, Object>> getCount1(@Param("uniqueCode") String uniqueCode,
                                  @Param("machineGrade") String machineGrade,
                                  @Param("machineBrand") String machineBrand,
                                  @Param("startTime") String startTime,
                                  @Param("endTime") String endTime);

    List<Map<String, Object>> getLineCode();

    String getReportData(@Param("imageId") String imageId);

    List<Map<String, Object>> getPictureTwice(@Param("uniqueCode") String uniqueCode,
                                              @Param("startTime") String startTime,
                                              @Param("endTime") String endTime,
                                              @Param("lineCode") String lineCode,
                                              @Param("isDanger") Boolean isDanger);

    List<Map<String, Object>> getTotalTwice(@Param("uniqueCode") String uniqueCode,
                                            @Param("lineCode") String lineCode,
                                            @Param("machineBrand") String machineBrand,
                                            @Param("startTime") String startTime,
                                            @Param("endTime") String endTime);

    List<Map<String, Object>> getDangerTotalTwice(@Param("uniqueCode") String uniqueCode,
                                                  @Param("lineCode") String lineCode,
                                                  @Param("machineBrand") String machineBrand,
                                                  @Param("startTime") String startTime,
                                                  @Param("endTime") String endTime);
}
