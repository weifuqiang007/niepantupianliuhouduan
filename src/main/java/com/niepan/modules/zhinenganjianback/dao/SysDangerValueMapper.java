package com.niepan.modules.zhinenganjianback.dao;

import com.niepan.modules.zhinenganjianback.entity.SysDangerValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【sys_danger_value】的数据库操作Mapper
* @createDate 2023-11-08 17:09:01
* @Entity com.niepan.modules.zhinenganjianback.entity.SysDangerValue
*/
public interface SysDangerValueMapper extends BaseMapper<SysDangerValue> {

}




