package com.niepan.modules.zhinenganjianback.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.niepan.modules.zhinenganjianback.model.PackageItemInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PackageItemInfoRepository extends BaseMapper<PackageItemInfo> {
    List<PackageItemInfo> findByImageIdentificationRecordsId(@Param("recordsId") String recordsId);

    String  findByImageName(@Param("recordsId") String recordsId);
}

