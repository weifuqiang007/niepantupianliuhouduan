package com.niepan.modules.zhinenganjianback.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.niepan.modules.zhinenganjianback.dto.AreaCount;

/**
* @author zq4568
* @description 针对表【area_count】的数据库操作Mapper
* @createDate 2023-08-16 17:12:33
* @Entity com.niepan.entity.AreaCount
*/
public interface AreaCountMapper extends BaseMapper<AreaCount> {


    String dayAndAreaCode(String areaCode);

    int updateDangerCount(Integer integer);

    int updateNoDangerCount(Integer integer);
}
