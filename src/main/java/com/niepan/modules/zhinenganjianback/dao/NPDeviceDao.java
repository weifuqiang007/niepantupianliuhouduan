package com.niepan.modules.zhinenganjianback.dao;

import com.niepan.modules.zhinenganjianback.model.NPDeviceRegister;

import java.util.ArrayList;

/**
 * @author weifuqiang
 * @date 2023/3/8
 */
public interface NPDeviceDao {
    ArrayList<String> ListAllDevice(Integer status);
}
