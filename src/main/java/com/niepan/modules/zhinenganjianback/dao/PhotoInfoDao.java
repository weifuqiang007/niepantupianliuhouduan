package com.niepan.modules.zhinenganjianback.dao;


import com.niepan.modules.zhinenganjianback.model.PhotoInfo;
import org.springframework.stereotype.Repository;

/**
 * @author weifuqiang
 * @create_time 2022/12/18
 */
@Repository
public interface PhotoInfoDao {

    Integer Add(PhotoInfo photoInfo);
}
