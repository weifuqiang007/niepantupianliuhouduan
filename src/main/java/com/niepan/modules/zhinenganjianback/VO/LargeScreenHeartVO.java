package com.niepan.modules.zhinenganjianback.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * @author: liuchenyu
 * @date: 2023/8/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LargeScreenHeartVO {

    /**
     * 加密过后的数据
     */
    @NotBlank(message = "data_digest不能为空")
    private String data_digest;

    /**
     * 数据
     */
    @Valid
    private HeartDataVO data;

    /**
     * 消息类型，必定为ZTO_SECURITY_HEARTBEAT
     */
    @NotBlank(message = "msg_type不能为空")
    private String msg_type;

}
