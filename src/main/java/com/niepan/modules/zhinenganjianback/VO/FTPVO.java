package com.niepan.modules.zhinenganjianback.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author: liuchenyu
 * @date: 2023/8/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FTPVO {

    /**
     * 网点编号
     */
    @NotBlank(message = "网点编号不能为空")
    private String siteCode;

    /**
     * 设备SN码
     */
    private String snCode;

    /**
     * 分拣线编号
     */
    private String lineCode;
}
