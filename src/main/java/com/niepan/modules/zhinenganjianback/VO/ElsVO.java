package com.niepan.modules.zhinenganjianback.VO;

import io.swagger.models.auth.In;
import lombok.Data;

/**
 * @author: liuchenyu
 * @date: 2023/6/19
 */
@Data
public class ElsVO {

    /**
     * 上传安检机运行状态数据
     */
    //设备型号
    private String machinetype;

    //设备编号
    private String machineindex;

    //安检员编号
    private String usrindex;

    //射线源类型
    private String raytype;

    //射线源电压
    private Integer voltage;

    //射线源电流
    private Integer current;

    //安检机探测器类型
    private String scannertype;

    //控制板连接状态
    private Boolean panelconnected;

    //XRay连接状态
    private Boolean xrayconnected;

    //Scanner连接状态
    private Boolean scannerconnected;

    //温度
    private String temperature;

    //湿度
    private String humidity;

    //异常编码
    private Integer exceptioncode;

    //异常描述
    private String exceptionmemo;

    //上次上电时间：1970-09-15 00:00:00
    private String lastpoweron;

    //上次关机时间：1970-09-15 00:00:00
    private String lastpoweroff;

    //软件注销时间：1970-09-15 00:00:00
    private String lastsoftlogoff;

    //软件退出注销时间：1970-09-15 00:00:00
    private String exitlogoff;

    //用户注销-退出注销持续时间：1970-09-15 00:00:00
    private String usrlogoffduration;

    //该用户在持续时间过包计数
    private Integer usr_dura_pack;

    //扫描总数
    private Integer packagecount;

    //TIP注入数
    private Integer tip_inject;

    //TIP识别数量
    private Integer tip_recognit;

    //TIP识别率
    private String tip_discriminat;


    /**
     * 上传安检机采集的图片
     */
    //图片url
    private String imageurl;

    //缩略图url
    private String thumbnailurl;

    //扫描时间
    private String scantime;

    //采集编号,同危险品图片的scanindex关联
    private String scanindex;

    /**
     * 接收图片
     */
    //sessionid
    private String sessionid;

    //base64
    private String img_base64_side;
}
