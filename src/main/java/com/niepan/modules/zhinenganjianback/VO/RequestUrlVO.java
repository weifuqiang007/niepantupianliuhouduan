package com.niepan.modules.zhinenganjianback.VO;

import lombok.Data;

/**
 * @author: liuchenyu
 * @date: 2023/6/25
 */
@Data
public class RequestUrlVO {
    private Integer id;

    /**
     * 上传url
     */
    private String requesturl;

    /**
     * minio的ak
     */
    private String access_key;

    /**
     * minio的sk
     */
    private String secret_key;

    /**
     * 地区编码
     */
    private String area_code;

    /**
     * 工控机的ip
     */
    private String gkj_ip;

    /**
     * 服务器的ak
     */
    private String fwq_ak;

    /**
     * 服务器的sk
     */
    private String fwq_sk;

    /**
     * 服务器的minioip
     */
    private String fwq_ip;
}
