package com.niepan.modules.zhinenganjianback.VO;

import lombok.Data;

import java.util.List;

@Data
public class ProhibitedItemCount {

    private List<AreaProhibitedItemCount> week;

    private List<AreaProhibitedItemCount> day;
}
