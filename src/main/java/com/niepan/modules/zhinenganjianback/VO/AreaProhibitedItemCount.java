package com.niepan.modules.zhinenganjianback.VO;

import lombok.Data;

@Data
public class AreaProhibitedItemCount {

  private String province;

  private String city;

  private String centre;

  private Integer value;

  private String unique_code;

  private Integer totalDangerCount;

  private String name;

  private Integer sort;

  private Integer weekValue;

  private Integer dayValue;

}
