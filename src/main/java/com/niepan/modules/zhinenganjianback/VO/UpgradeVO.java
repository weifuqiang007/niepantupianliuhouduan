package com.niepan.modules.zhinenganjianback.VO;

import lombok.Data;

@Data
public class UpgradeVO {
    String ip;
    String path;
}
