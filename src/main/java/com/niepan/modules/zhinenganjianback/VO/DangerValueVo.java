package com.niepan.modules.zhinenganjianback.VO;
import com.niepan.modules.zhinenganjianback.entity.SysDangerValue;
import java.util.List;

/**
 * @Author : kyz on 2023/11/9 13:07
 */
public class DangerValueVo {

    /**
     * 开关
     */
    private String enable;

    /**
     * 列表
     */
    private List<SysDangerValue> list;


    public String getEnable() {
        return enable;
    }
    public void setEnable(String enable) {
        this.enable = enable;
    }
    public List<SysDangerValue> getList() {
        return list;
    }
    public void setList(List<SysDangerValue> list) {
        this.list = list;
    }
}
