package com.niepan.modules.zhinenganjianback.mongoDBVO;

import lombok.Data;
//import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;


/**
 * @author: liuchenyu
 * @date: 2023/7/3
 */
@Data
//@Document(collection = "picinfo") //指定要对应的文档名（表名）
//@Accessors(chain = true)
public class PicInfo {
    @Id
    private String id;

    private String pid;

    private byte[] picture;

    private String map;
}
