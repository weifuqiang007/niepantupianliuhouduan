package com.niepan.service;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.*;

/**
 * @author: liuchenyu
 * @date: 2023/6/12
 */
public class systemtest {

    private static String EXTRACT_PATH = System.getProperty("E:\\info");

    @Test
    public void test(){
        System.out.println(EXTRACT_PATH);
    }


    @Test
    public void test2(){
        String str="{\"data_digest\":\"274e831de4a41baf5a6bb4cbf0611f6f\",\"data\":\"{\"isForbid\":true,\"companyId\":\"HZNP\",\"snCode\":\"9E07FB1GAJ00002\",\"forbidType\":\"32, 1, 38, 7, 39, 45, 14, 46\",\"forbidId\":\"89fdc0070189fdc0985f402880b10005\",\"extendedField\":\"[]\",\"scanTime\":1692179404000,\"forbidCode\":1,\"dwsCode\":\"123\"}\",\"company_id\":\"HZNP\",\"msg_type\":\"ZTO_SECURITY_INFO\"}";
        Map<String,Object> map=jsonToMap(str);
        System.out.println(map);
    }



    /**
     * 应用场景：Json 排序
     *
     * @param jsonStr JSON字符串
     */
    public static Map<String, Object> jsonToMap(String jsonStr) {
        Map<String, Object> treeMap = new TreeMap();
        //
        JSONObject json = JSONObject.parseObject(jsonStr, Feature.OrderedField);
        Iterator<String> keys = json.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            Object value = json.get(key);
            //判断传入kay-value中是否含有&#34;&#34; 或null
            if (json.get(key) == null || value == null) {
                //当JSON字符串存在null时,不将该kay-value放入Map中,即显示的结果不包括该kay-value
                continue;
            }
            // 判断是否为JSONArray(json数组)
            if (value instanceof JSONArray) {
                JSONArray jsonArray = (JSONArray) value;
                List<Object> arrayList = new ArrayList<>();
                for (Object object : jsonArray) {
                    // 判断是否为JSONObject&#xff0c;如果是 转化成TreeMap
                    if (object instanceof JSONObject) {
                        object = jsonToMap(object.toString());
                    }
                    arrayList.add(object);
                }
                treeMap.put(key, arrayList);
            } else {
                //判断该JSON中是否嵌套JSON
                boolean flag = isJSONValid(value.toString());
                if (flag) {
                    //若嵌套json了,通过递归再对嵌套的json(即子json)进行排序
                    value = jsonToMap(value.toString());
                }
                // 其他基础类型直接放入treeMap
                // JSONObject可进行再次解析转换
                treeMap.put(key, value);
            }
        }
        return treeMap;
    }

    /**
     * 校验是否是json字符串
     */
    public final static boolean isJSONValid(String json) {
        try {
            if (null == JSONObject.parseObject(json)) {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    @Test
    public void testNullString(){
        String protocol=StringUtils.EMPTY;
        System.out.println(StringUtils.equalsIgnoreCase(protocol,"utp"));
    }
}
