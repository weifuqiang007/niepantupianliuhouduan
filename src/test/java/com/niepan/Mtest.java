package com.niepan;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alibaba.druid.sql.dialect.oracle.ast.OracleDataTypeIntervalYear;
import io.swagger.models.auth.In;
import java.text.SimpleDateFormat;
import org.apache.commons.lang.StringUtils;
import org.apache.el.stream.Stream;
import org.checkerframework.checker.units.qual.A;
import org.checkerframework.checker.units.qual.K;
import org.junit.Test;
import org.springframework.core.annotation.MergedAnnotationPredicates;
import sun.rmi.log.LogInputStream;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: kongyz
 * @date: 2023/10/20 on 15:42
 */
public class Mtest {

    /**
     * 找出列表中出现次数最多的元素,并按照顺序排序 取前三个
     * words = ['look', 'into', 'my', 'AAA', 'the', 'AAA', 'the', 'eyes', 'not', 'BBB', 'the', 'AAA', 'dont', 'BBB', 'around',
     * 'the', 'AAA', 'look', 'into', 'BBB', 'AAA', "BBB", 'under']
     * <p>
     * {'AAA': 5, 'the': 4, 'BBB': 4, 'look': 2, 'into': 2, 'my': 1, 'eyes': 1, 'not': 1, 'dont': 1, 'around': 1, 'under': 1}
     * [('AAA', 5), ('the', 4), ('BBB', 4)]
     */


    @Test
    public void test() {

        // System.out.println(DateUtil.date().getTime());
        System.out.println("1698931971332:");

        SimpleDateFormat ff = new SimpleDateFormat(DatePattern.NORM_DATETIME_MS_PATTERN);
        ff.setTimeZone(TimeZone.getTimeZone("UTC"));
        // DateTime usTime = DateUtil.parse(dateStr, ff);

        // System.out.println(DateUtil.format(DateUtil.date(1698931971332L),ff));

        System.out.println(DateUtil.format(DateUtil.date(1698903311965L),DatePattern.NORM_DATETIME_MS_PATTERN));

        String[] words = {"look", "into", "my", "AAA", "the", "AAA", "the", "eyes", "not", "BBB", "the",
                "AAA", "dont", "BBB", "around", "the", "AAA", "look", "into", "BBB", "AAA", "BBB", "under"};



    }


}
